package com.gridbuddy.props;

import ru.qatools.properties.Property;
import ru.qatools.properties.PropertyLoader;
import ru.qatools.properties.Resource;

@Resource.Classpath("properties")
public interface Properties {

    @Property("service.url")
    String serviceUrl();

    @Property("environment")
    String environment();

    @Property("browserName")
    String browserName();

    @Property("remoteWebDriverUrl")
    String remoteWebDriverUrl();

    @Property("user")
    String user();

    @Property("password")
    String password();

    Properties props = PropertyLoader.newInstance().populate(Properties.class);

}