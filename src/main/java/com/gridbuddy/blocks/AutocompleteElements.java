package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface AutocompleteElements extends ExtendedWebElement<AutocompleteElements> {

    @FindBy("./li[contains(@class,'lookupData')]/span[@name='label']")
    ExtendedList<HtmlElement> labels();

    @FindBy("./li[contains(@class,'ui-autocomplete-message')][2]")
    HtmlElement emptyAutocomplete();

}
