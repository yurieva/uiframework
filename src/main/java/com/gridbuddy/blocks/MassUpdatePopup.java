package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface MassUpdatePopup extends ExtendedWebElement<MassUpdatePopup> {

    @FindBy(".//tr[@class='gradientHeader']/td")
    ExtendedList<HtmlElement> parentColumns();

    @FindBy(".//input[@value='Apply to Selected Records' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement applyToSelectedRecords();

    @FindBy(".//input[@value='Apply to All Records' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement applyToAllRecords();

    @FindBy(".//input[@value='Clear All' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement clearAll();

    @FindBy(".//input[@value='Collapse All' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement collapseAll();

    @FindBy(".//input[@value='Expand All' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement expandAll();

    @FindBy(".//table[@id='massUpdatesTable']/tbody/tr[2]/td[count(//tr[@class='gradientHeader']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//input")
    HtmlElement parentInputField(@Param("name") String name);
    
    @FindBy(".//table[@id='massUpdatesTable']/tbody/tr[2]/td[count(//tr[@class='gradientHeader']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//select")
    Select parentSelectField(@Param("name") String name);

    @FindBy(".//tr[@id]/following-sibling::tr[not(contains(@class,'dataCard'))]")
    ExtendedList<MassUpdateChildRecord> childRecords();

    @FindBy(".//tr[@id]/following-sibling::tr[.//span[text()='{{ object }}']]")
    MassUpdateChildRecord childRecord(@Param("object") String object);

    @FindBy(".//div[@id='massUpdateMsg']")
    HtmlElement messgage();
}
