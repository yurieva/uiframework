package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface CustomRecord extends ExtendedWebElement<CustomRecord> {

    @FindBy(".//span[contains(@class,'gbAdd')]")
    HtmlElement add();

    @FindBy(".//span[contains(@class,'gbRemove')]")
    HtmlElement remove();

    @FindBy(".//span[2]")
    HtmlElement title();
}
