package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface AdvancedFilterConditions extends ExtendedWebElement<AdvancedFilterConditions> {

    @FindBy(".//input[@class='advFilter']")
    HtmlElement inputField();

    @FindBy(".//span[@class='clearAdvFilter']")
    HtmlElement clearFilter();

    @FindBy(".//span[@class='spanLink']")
    HtmlElement tips();

}
