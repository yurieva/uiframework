package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface EditAction extends ExtendedWebElement<EditAction> {

    @FindBy(".//span[text()='*']/parent::td[text()='Action Name']/following-sibling::td/input[@type='text']")
    HtmlElement actionName();

    @FindBy(".//td[text()='Description']/following-sibling::td/input[@type='text']")
    HtmlElement actionDescription();

    @FindBy(".//span[text()='*']/parent::td[text()='Object']/following-sibling::td/select")
    Select object();

    @FindBy(".//span[text()='*']/parent::td[text()='Type']/following-sibling::td/select")
    Select type();

    @FindBy(".//span[text()='*']/parent::td[text()='Location']/following-sibling::td/select")
    Select location();

    @FindBy(".//span[text()='*']/parent::td[text()='Display Behavior']/following-sibling::td/select")
    Select behavior();

    @FindBy(".//td[text()='Overlay Height']/following-sibling::td/input[@type='text']")
    HtmlElement overlayHeight();

    @FindBy(".//td[text()='Overlay Width']/following-sibling::td/input[@type='text']")
    HtmlElement overlayWidht();

    @FindBy(".//td[text()='Top Position']/following-sibling::td/input[@type='text']")
    HtmlElement topPosition();

    @FindBy(".//td[text()='Left Position']/following-sibling::td/input[@type='text']")
    HtmlElement leftPosition();

    @FindBy(".//span[text()='*']/parent::td[text()='Content Source']/following-sibling::td/select")
    Select contentSource();

    @FindBy(".//span[text()='*']/parent::td[text()='Content']/following-sibling::td/input[@type='text']")
    HtmlElement content();

    @FindBy(".//span[text()='*']/parent::td[text()='ID Parameter']/following-sibling::td/input[@type='text']")
    HtmlElement idParameter();

    @FindBy(".//td[text()='Confirm Action?']/following-sibling::td/input[@type='checkbox']")
    Checkbox confirmAction();
}
