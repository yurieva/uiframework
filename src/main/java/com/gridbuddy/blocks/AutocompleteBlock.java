package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface AutocompleteBlock extends ExtendedWebElement<AutocompleteBlock>{
	
    @FindBy("./li[contains(@class,'lookupData')]/span[@name='label'] | ./li[@class='ui-menu-item']")
    ExtendedList<HtmlElement> autocompleteItems();
    
    @FindBy("./li[contains(@class,'ui-autocomplete-message')][2]")
    HtmlElement emptyAutocomplete();

}
