package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ConditionalFormatting extends ExtendedWebElement<ConditionalFormatting> {

    @FindBy(".//input[@value='Save' and @type='submit']")
    HtmlElement save();

    @FindBy(".//input[@id='addRuleBtn']")
    HtmlElement addRule();

    @FindBy(".//input[@id='deleteRuleBtn']")
    HtmlElement deleteRule();

    @FindBy(".//tr[@id='rules']/following-sibling::tr/td")
    ExtendedList<HtmlElement> rules();

    @FindBy(".//td[@class='ruleName']/input")
    HtmlElement ruleName();

    @FindBy(".//select[@id='colorCodeGoverningField']")
    Select conditionField();

    @FindBy(".//div[@class='inputColorCoding']/select[not(contains(@style,'none'))]")
    Select conditionOperator();

    @FindBy(".//div[@class='inputColorCoding' and not(contains(@style,'none'))]//input[@id='colorCodeValueInput'] | .//div[@class='picklistColorCoding' and not(contains(@style,'none'))]//select")
    HtmlElement conditionValue();

    @FindBy(".//select[@id='colorCodeColoredField']")
    Select formattingField();

    @FindBy(".//input[@id='colorWholeRowChk']")
    Checkbox entireRow();

    @FindBy(".//label[contains(text(),'Background')]/following-sibling::div//div[@data-colorid]")
    ExtendedList<HtmlElement> backgroundColors();

    @FindBy(".//label[contains(text(),'Text')]/following-sibling::div//div[@data-colorid]")
    ExtendedList<HtmlElement> textColors();

    @FindBy(".//span[@id='colorCodingBold']")
    HtmlElement bold();

    @FindBy(".//span[@id='colorCodingItalic']")
    HtmlElement italic();

    @FindBy(".//span[@id='colorCodingUnderline']")
    HtmlElement underline();
}
