package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.Checkbox;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface GBRelatedObject extends ExtendedWebElement<GBRelatedObject> {

    @FindBy("./td[1]/following-sibling::td/*")
    ExtendedList<HtmlElement> fields();

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody//tr[@name=\"r0\"]//table[@class='childTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]/*")
    HtmlElement field(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody//tr[@name=\"r0\"]//table[@class='childTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]//input")
    HtmlElement inputField(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody//tr[@name=\"r0\"]//table[@class='childTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]/select")
    Select select(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody//tr[@name=\"r0\"]//table[@class='childTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]//input")
    HtmlElement additionalInputField(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody//tr[@name=\"r0\"]//table[@class='childTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]//input[contains(@class,'gbd')]")
    HtmlElement dateField(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody//tr[@name=\"r0\"]//table[@class='childTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]//input[contains(@class,'gbt')]")
    HtmlElement timeField(@Param("name") String name);

    @FindBy("./following-sibling::tr[contains(@class,'dataCard')][1]")
    GBDataCardRecord dataCardRow();

    @FindBy("./td[1]/span[contains(@class,'arrow')]")
    HtmlElement expandIcon();

    @FindBy("./td[1]/input[@type='checkbox']")
    Checkbox checkbox();

    @FindBy("./td[1]/div[@title='Actions']")
    HtmlElement actions();

    @FindBy(".//span[contains(@class,'icon-wrap')]")
    HtmlElement dataCardIcon();

    @FindBy("./following-sibling::tr[@id and contains(@class,'pldisplayed')][{{ rowNumber }}]")
    GBRelatedObject clonedRecord(@Param("rowNumber") String rowNumber);

}
