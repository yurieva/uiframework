package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ActionRecordGW2 extends ExtendedWebElement<ActionRecordGW2> {

    @FindBy(".//input")
    Checkbox checkbox();

    @FindBy(".//td[2]")
    HtmlElement actionObject();

    @FindBy(".//td[3]")
    HtmlElement actionName();

    @FindBy(".//td[4]")
    HtmlElement contentSource();

    @FindBy(".//td[5]")
    HtmlElement displayBehavior();

    @FindBy(".//td[6]")
    HtmlElement type();

    @FindBy(".//td[7]")
    HtmlElement description();

}
