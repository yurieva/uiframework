package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface CustomTheme extends ExtendedWebElement<CustomTheme> {

    @FindBy(".//select[@class='elementDropdown']")
    Select elementDropdown();

    @FindBy(".//a[text()='Clear']")
    HtmlElement clearLink();

    @FindBy(".//div[text()='Background color:']/following-sibling::div//div[@data-colorid]")
    ExtendedList<HtmlElement> backgroundColors();

    @FindBy(".//div[text()='Text color:']/following-sibling::div//div[@data-colorid]")
    ExtendedList<HtmlElement> textColors();

    @FindBy(".//div[text()='Text color:']/following-sibling::input[@type='checkbox']")
    Checkbox useHexCodeForTextColor();

    @FindBy(".//div[text()='Background color:']/following-sibling::input[@type='checkbox']")
    Checkbox useHexCodeForBackground();

    @FindBy(".//div[text()='Text color:']/following-sibling::div/input")
    HtmlElement hexCodeInputForTextColor();

    @FindBy(".//div[text()='Background color:']/following-sibling::div/input")
    HtmlElement hexCodeInputForBackgroundColor();
}
