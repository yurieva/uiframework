package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ForecastViewCell extends ExtendedWebElement<ForecastViewCell> {

    @FindBy("./div[count(//table[@id='gbMainTable']//tr[contains(@class,'pdr')][1]/td[@class='fieldCol']/div[text()='{{ name }}']/preceding-sibling::div) + 1]//input")
    HtmlElement inputField(@Param("name") String name);
    
    @FindBy("./div[count(//table[@id='gbMainTable']//tr[contains(@class,'pdr')][1]/td[@class='fieldCol']/div[text()='{{ name }}']/preceding-sibling::div) + 1]//span[@class='chkd']")
    Checkbox checkedCheckbox(@Param("name") String name);
    
    @FindBy("./div[count(//table[@id='gbMainTable']//tr[contains(@class,'pdr')][1]/td[@class='fieldCol']/div[text()='{{ name }}']/preceding-sibling::div) + 1]")
    HtmlElement div(@Param("name") String name);
    
    @FindBy("./div[count(//table[@id='gbMainTable']//tr[contains(@class,'pdr')][1]/td[@class='fieldCol']/div[text()='{{ name }}']/preceding-sibling::div) + 1]//div | ./div[count(//table[@id='gbMainTable']//tr[contains(@class,'pdr')][1]/td[@class='fieldCol']/div[text()='{{ name }}']/preceding-sibling::div) + 1]//span")
    HtmlElement textField(@Param("name") String name);
    
    @FindBy("./div[count(//table[@id='gbMainTable']//tr[contains(@class,'pdr')][1]/td[@class='fieldCol']/div[text()='{{ name }}']/preceding-sibling::div) + 1]/select")
    Select selectField(@Param("name") String name);
    
    @FindBy(".//span[contains(@class,'icon-ellipses')]")
    HtmlElement cogIcon();
    
    @FindBy(".//span[contains(@class, 'rfIcon')]")
    HtmlElement showAllLookup();

}
