package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.Checkbox;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface FilterByFieldRow extends ExtendedWebElement<FilterByFieldRow> {

    @FindBy(".//select[@class='filterByField']")
    Select field();

    @FindBy(".//select[@class='filterOperator']")
    Select operator();

    @FindBy(".//input[contains(@class,'filterValue')]")
    Select value();

    @FindBy(".//span[text()='Clear']")
    HtmlElement clearFilter();

    @FindBy(".//input[@class='filterLockCheckbox']")
    Checkbox lock();

}
