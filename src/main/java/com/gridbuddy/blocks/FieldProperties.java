package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface FieldProperties extends ExtendedWebElement<FieldProperties> {

    @FindBy("//label[text()='Label']/following-sibling::input")
    HtmlElement label();

    @FindBy("//label[text()='Read-only']/following-sibling::input[@id='readOnlyChk']")
    Checkbox readOnlyCheckbox();

    @FindBy("//label[text()='Read-only']/following-sibling::input[@id='readOnlyRd']")
    HtmlElement readOnlyRadiobutton();

    @FindBy("//label[text()='Required']/following-sibling::input[@id='rqrdChk']")
    Checkbox requiredCheckbox();

    @FindBy("//label[text()='Required']/following-sibling::input[@id='rqrdRd']")
    Checkbox requiredRadiobutton();

    @FindBy("//label[text()='Quick filter']/following-sibling::input[@id='qFilterChk']")
    Checkbox quickFilter();

    @FindBy("//label[text()='Column width']/following-sibling::input")
    HtmlElement columnWidth();

    @FindBy("//label[text()='Summary Type']/following-sibling::select")
    Select summaryType();

    @FindBy("//div[@class='fpButtons']/input[@value='OK']")
    HtmlElement ok();

    @FindBy("//div[@class='fpButtons']/input[@value='Cancel']")
    HtmlElement cancel();
}
