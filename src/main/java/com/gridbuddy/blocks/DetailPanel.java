package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface DetailPanel extends ExtendedWebElement<DetailPanel> {

	@FindBy(".//div[text()='{{ name }}']/following-sibling::div/span | .//div[text()='{{ name }}']/following-sibling::div")
	HtmlElement textField(@Param("name") String name);

	@FindBy(".//div[text()='{{ name }}']/following-sibling::div//input")
	HtmlElement inputField(@Param("name") String name);

	@FindBy(".//div[text()='{{ name }}']/following-sibling::div//textarea")
	HtmlElement textareaField(@Param("name") String name);

	@FindBy(".//div[text()='{{ name }}']/following-sibling::div//select")
	Select selectField(@Param("name") String name);

	@FindBy(".//div[text()='{{ name }}']/following-sibling::div//input[contains(@class,'gbd')]")
	HtmlElement dateField(@Param("name") String name);
	
	@FindBy("./div[@id='dataPanelContent']")
	HtmlElement detailPanelBody();

}
