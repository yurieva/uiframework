package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface GBTable extends ExtendedWebElement<GBTable> {

	@FindBy("./tbody/tr[@id and not(contains(@class,'dataCard'))]")
	ExtendedList<GBRecord> records();

	@FindBy(".//tr[contains(@class,'summaryRow')]//td[@name][count(//table[@id='gbMainTable']/tbody/tr[1]//span[text()=\"{{ columnName }}\"]/../preceding-sibling::td[@name]) + 1]")
	HtmlElement summaryField(@Param("columnName") String columnName);

	@FindBy("./tbody/tr[@class='pdr']")
	ExtendedList<ForecastViewRecord> forecastViewRecords();

	@FindBy(".//tr[@class='gradientHeader']/td[@data-col]")
	ExtendedList<HtmlElement> forecactViewHeaders();

	@FindBy("//table[@id='gbMainTableFrozenHeader']//tr[@class='gradientHeader']/td[@data-col]")
	ExtendedList<HtmlElement> forecastViewFrozenHeaders();

	@FindBy("//tr[contains(@class,'grp')]/td/span[@title]")
	ExtendedList<HtmlElement> groupingRows();

	@FindBy("./tbody/tr[@id]/preceding-sibling::tr[contains(@class,'grp{{ groupLevel }}')][last()]/following-sibling::tr[contains(@class,'grp{{ groupLevel }}')][1]/preceding-sibling::tr[@id]")
	ExtendedList<GBGroupRecord> groupedResults(@Param("groupLevel") int groupLevel);

	@FindBy(".//tr[contains(@class,'gradientHeader')][1]/td[@name]")
	ExtendedList<GBRecordHeader> topHeaders();

	@FindBy(".//input[@class='selectAllChk']")
	Checkbox selectAll();

	@FindBy("//ul[contains(@class,'ui-autocomplete') and not(contains(@style,'none'))]")
	AutocompleteBlock autocompleteBlock();

	@FindBy("//div[@id='gbMainTableFrozenHeaderContainer']//span[@title='Field Actions']")
	HtmlElement forecastViewHeaderRowCogIcon();
	
}
