package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface GBRecordHeader extends ExtendedWebElement<GBRecordHeader> {

    @FindBy(".//span[@class='fieldLabel']")
    HtmlElement name();

    @FindBy(".//span[contains(@class,'icon-cog') and not(contains(@class,'gbff-inactive'))]")
    HtmlElement actionIcon();

}
