package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface GBRelatedRecord extends ExtendedWebElement<GBRelatedRecord> {

    @FindBy(".//h3[contains(@class,'collapsed')][1]/span/span[contains(@class,'arrow')]")
    HtmlElement expandIcon();

    @FindBy(".//h3[contains(@class,'expanded')][1]/span/span[contains(@class,'arrow')]")
    HtmlElement collapseIcon();
    
    @FindBy(".//h3[1]/span[2]")
    HtmlElement title();

    @FindBy(".//h3[1]/span/span[@class='recordCnt']")
    HtmlElement recordsCount();

    @FindBy(".//h3[1]/span/span[@class='createNew']")
    HtmlElement newLink();

    @FindBy(".//tr[@class='childHeaderRow']/td[@name]")
    ExtendedList<GBRecordHeader> headers();

    @FindBy(".//tr[@id]")
    ExtendedList<GBRelatedObject> records();

}
