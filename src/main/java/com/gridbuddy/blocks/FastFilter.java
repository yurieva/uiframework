package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface FastFilter extends ExtendedWebElement<FastFilter> {

	@FindBy("//span[text()='Field Actions']")
	HtmlElement title();

	@FindBy(".//input[@value='Apply']")
	HtmlElement apply();

	@FindBy(".//input[@class='filterVal']")
	HtmlElement filterValue();

	@FindBy(".//div[@id='ff-operator']/select")
	Select operator();

	@FindBy(".//input[@value='Clear']")
	HtmlElement clearButton();

	@FindBy(".//input[contains(@class,'gbd')]")
	HtmlElement dateField();

	@FindBy(".//input[contains(@class,'gbt')]")
	HtmlElement timeField();

	@FindBy(".//input[contains(@class,'muApplyAll')]")
	HtmlElement all();

	@FindBy(".//input[contains(@class,'muApplySel')]")
	HtmlElement selected();

	@FindBy(".//span[text()='A → Z']")
	HtmlElement sortAscending();

	@FindBy(".//span[text()='Z → A']")
	HtmlElement sortDescending();

	@FindBy(".//span[text()='Reset']")
	HtmlElement resetSorting();

	@FindBy(".//span[@class='closeX']")
	HtmlElement closePopup();

}
