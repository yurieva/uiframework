package com.gridbuddy.blocks;

import io.qameta.htmlelements.element.ExtendedWebElement;

public interface OverlayBlock extends ExtendedWebElement<OverlayBlock> {

}
