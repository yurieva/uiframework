package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ConfigureChart extends ExtendedWebElement<ConfigureChart> {

    @FindBy(".//input[@value='Save' and @type='submit']")
    HtmlElement save();

    @FindBy(".//input[@value='Embed']")
    HtmlElement embed();

    @FindBy(".//input[@id='resetChartButton']")
    HtmlElement reset();

    @FindBy(".//input[@id='chartTitleInput']")
    HtmlElement chartName();

    @FindBy(".//select[@id='chartSizeSelect']")
    Select chartSize();

    @FindBy(".//span[@id='line']")
    HtmlElement lineChart();

    @FindBy(".//span[@id='bar']")
    HtmlElement barChart();

    @FindBy(".//span[@id='donut']")
    HtmlElement donutChart();

    @FindBy(".//span[@id='tile']")
    HtmlElement tileChart();

    @FindBy(".//select[@id='chartXWedgeSelect']")
    Select xAxis();

    @FindBy(".//select[@id='chartYValueSelect']")
    Select yAxis();

    @FindBy(".//select[@id='chartGroupBySelect']")
    Select groupBy();

    @FindBy(".//input[@id='chartShowLabelsInput']")
    Checkbox showLabels();

    @FindBy(".//select[@id='chartTileFieldSelect']")
    Select field();

    @FindBy(".//select[@id='chartSummaryTypeSelect']")
    Select summaryType();

    @FindBy(".//input[@id='chartDescriptionInput']")
    HtmlElement description();

    @FindBy("//div[@id='chartColorSelect']//div[@data-colorid]")
    ExtendedList<HtmlElement> colors();

}
