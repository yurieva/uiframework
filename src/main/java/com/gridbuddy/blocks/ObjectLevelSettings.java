package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ObjectLevelSettings extends ExtendedWebElement<ObjectLevelSettings> {

    @FindBy(".//div[contains(@id,'CRUDBody') and not(contains(@class,'none'))]//div[contains(text(),'Plural Label')]/following-sibling::input")
    HtmlElement pluralLabel();

    @FindBy(".//div[contains(@id,'CRUDBody') and not(contains(@class,'none'))]//div[contains(text(),'Label')]/following-sibling::input[@class='objLabel']")
    HtmlElement label();

    @FindBy(".//div[contains(@id,'CRUDBody') and not(contains(@class,'none'))]//div[contains(text(),'Edit')]/following-sibling::span[contains(text(),'Make object editable')]/preceding-sibling::input")
    Checkbox edit();

    @FindBy(".//div[contains(@id,'CRUDBody') and not(contains(@class,'none'))]//div[contains(text(),'Create')]/following-sibling::input")
    Checkbox create();

    @FindBy(".//div[contains(@id,'CRUDBody') and not(contains(@class,'none'))]//div[contains(text(),'Delete')]/following-sibling::input")
    Checkbox delete();

    @FindBy(".//div[contains(@id,'CRUDBody') and not(contains(@class,'none'))]//div[contains(text(),'Editable Related Column')]/following-sibling::input")
    Checkbox editableRelatedColumn();

    @FindBy(".//div[contains(@id,'CRUDBody') and not(contains(@class,'none'))]//div[contains(text(),'Column Label')]/following-sibling::input")
    HtmlElement columnLabel();

    @FindBy(".//div[contains(@id,'CRUDBody') and not(contains(@class,'none'))]//div[contains(text(),'Flat View')]/following-sibling::input")
    Checkbox flatView();

    @FindBy(".//div[@id='objectLevelCRUDBtns']/input[@value='OK']")
    HtmlElement ok();

    @FindBy(".//div[@id='objectLevelCRUDBtns']/input[@value='Cancel']")
    HtmlElement cancel();

}
