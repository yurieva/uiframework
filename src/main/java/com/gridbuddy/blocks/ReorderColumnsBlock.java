package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ReorderColumnsBlock extends ExtendedWebElement<ReorderColumnsBlock> {

    @FindBy(".//span[contains(@class,'objectLabel') and text()='{{ name }}']")
    HtmlElement object(@Param("name") String name);
    
    @FindBy(".//span[text()='{{ name }}']/preceding-sibling::span/input")
    Checkbox checkbox(@Param("name") String name);
    
    @FindBy(".//input[@value='Save' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement save();
}
