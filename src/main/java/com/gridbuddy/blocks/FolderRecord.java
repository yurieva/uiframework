package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface FolderRecord extends ExtendedWebElement<FolderRecord> {

    @FindBy("./input")
    Checkbox checkbox();

    @FindBy(".//a")
    HtmlElement folderName();

}
