package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface PreviewTabs extends ExtendedWebElement<PreviewTabs> {

    @FindBy(".//li[@role='tab']")
    ExtendedList<HtmlElement> tabs();

    @FindBy("//a[text()='{{ name }}']/parent::li[@role='tab']")
    HtmlElement tab(@Param("name") String name);

    @FindBy(".//div[contains(@id,'tabs') and not(contains(@style,'none'))]")
    HtmlElement preview();

}
