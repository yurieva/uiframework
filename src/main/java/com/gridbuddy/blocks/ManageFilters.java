package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.HtmlElement;

public interface ManageFilters extends OverlayBlock {

    @FindBy(".//div[@id='filterName' or @class='filterNameInput']//input")
    HtmlElement filterName();

    @FindBy(".//select[@class='filterOptions']")
    Select myFilter();

    @FindBy(".//div[@id='parentChildFields']/div")
    ExtendedList<ObjectFilters> objectFilters();

    @FindBy(".//div[@id='parentChildFields']/div[.//h3[contains(text(),'{{ objectName }}')]]")
    ObjectFilters objectFilter(@Param("objectName") String objectName);

    @FindBy(".//div[@class='pbHeader']//input[@id='saveUDFAction']")
    HtmlElement save();

    @FindBy(".//div[@class='pbHeader']//input[@id='cloneUDFAction']")
    HtmlElement clone();

    @FindBy(".//div[@class='pbHeader']//input[@id='revertActionBtn']")
    HtmlElement delete();

    @FindBy(".//select[@class='filterByOption']")
    Select filterByRelatedObject();

}
