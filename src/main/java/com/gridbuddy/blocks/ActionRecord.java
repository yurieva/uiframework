package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ActionRecord extends ExtendedWebElement<ActionRecord> {

    @FindBy(".//input[@class='deleteChk']")
    Checkbox checkbox();

    @FindBy(".//td[@class='actionInfoUnderline']")
    HtmlElement actionName();

    @FindBy(".//td[contains(@class,'objectInfo')]")
    HtmlElement actionObject();
}
