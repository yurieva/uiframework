package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ObjectsContainer extends ExtendedWebElement<ObjectsContainer> {

    @FindBy(".//input[@class='searchBox']")
    HtmlElement search();

    @FindBy(".//div[@class='relatedChildObjects']/div/span[@class='objectLabel']")
    ExtendedList<HtmlElement> relatedObjects();

    @FindBy(".//div[@class='allObjects ']/div/span[@class='objectLabel']")
    ExtendedList<HtmlElement> additionalObjects();

}
