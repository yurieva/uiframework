package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.Checkbox;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface GBRecord extends ExtendedWebElement<GBRecord> {

    @FindBy("./preceding-sibling::tr[contains(@class,'gradientHeader')][1]/td[@name]")
    ExtendedList<GBRecordHeader> topHeaders();

    @FindBy("./following-sibling::tr[contains(@class,'gradientHeader')][1]/td[@name]")
    ExtendedList<GBRecordHeader> bottomHeaders();

    @FindBy("./td[1]/following-sibling::td/*")
    ExtendedList<HtmlElement> fields();

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]")
    HtmlElement cell(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]/*")
    HtmlElement field(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]//input")
    HtmlElement inputField(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]//textarea")
    HtmlElement textareaField(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]//input[contains(@class,'gbd')]")
    HtmlElement dateField(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]//input[@class='gbt']")
    HtmlElement timeField(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]/select")
    Select select(@Param("name") String name);

    @FindBy("./td[1]/span[contains(@class,'arrow') and contains(@class,'right')]")
    HtmlElement expandIcon();

    @FindBy("./td[1]/span[contains(@class,'arrow') and not(contains(@class,'right'))]")
    HtmlElement collapseIcon();

    @FindBy("./td[1]/input[@type='checkbox']")
    Checkbox checkbox();

    @FindBy("./td[1]/div[@title='Actions']")
    HtmlElement actions();

    @FindBy("./td[1]/span[contains(@class,'icon-wrap')]")
    HtmlElement dataCardIcon();

    @FindBy("./following-sibling::tr[@class='cr'][1]")
    GBRelatedRecord relatedRecord();

    @FindBy("./following-sibling::tr[@class='cr']")
    ExtendedList<GBRelatedRecord> relatedRecords();

    @FindBy("./following-sibling::tr[contains(@class,'dataCard')][1]")
    GBDataCardRecord dataCardRow();

    @FindBy("./following-sibling::tr[@id and contains(@class,'pldisplayed')][{{ rowNumber }}]")
    GBRecord clonedRecord(@Param("rowNumber") String rowNumber);

    @FindBy("./following-sibling::tr[@class='cr']//td[text()='No records found.']")
    HtmlElement noChildRecordsFound();

    @FindBy(".//div[text()='more']")
    HtmlElement more();

    @FindBy(".//span[@class='grpValue']")
    HtmlElement groupFieldName();
}
