package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;

public interface AddedObjects extends ExtendedWebElement<AddedObjects> {

    @FindBy(".//div[@class='topSection']")
    SelectedObjectsHeader headerRow();

    @FindBy("./div[contains(@class,'primaryFieldsSection')]/div[@id]")
    ExtendedList<Field> fields();

    @FindBy(".//div[contains(@class,'dataCardSection')]")
    DataCard dataCard();

    @FindBy(".//div[contains(@class,'parent')]//select")
    Select parentSelect();

    @FindBy(".//div[contains(@class,'child')]//select")
    Select childSelect();

}
