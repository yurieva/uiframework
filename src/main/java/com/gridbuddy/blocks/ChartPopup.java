package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ChartPopup extends ExtendedWebElement<ChartPopup> {

    @FindBy(".//h3[@class='xAxisLabel']")
    HtmlElement xAxisLabel();

    @FindBy(".//h3[@class='yAxisLabel']")
    HtmlElement yAxisLabel();

    @FindBy(".//div[@class='chart-legend-container']/h3")
    HtmlElement chartLegend();

    @FindBy(".//canvas[@class='chartCanvas']")
    HtmlElement chart();

    @FindBy(".//div[@class='chartHeader']")
    HtmlElement chartName();

    @FindBy(".//div[@class='nodataMessage']")
    HtmlElement nodataMessage();
}
