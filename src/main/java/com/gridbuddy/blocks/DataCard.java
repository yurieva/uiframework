package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;

public interface DataCard extends ExtendedWebElement<DataCard>{

    @FindBy(".//div[@id]")
    ExtendedList<Field> fields();

}
