package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface MassUpdateChildRecord extends ExtendedWebElement<MassUpdateChildRecord> {

	@FindBy(".//table[@class='childTable']/tbody/tr[2]/td[count(//tr[@class='childHeaderRow']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//input")
	HtmlElement inputField(@Param("name") String name);

	@FindBy(".//table[@class='childTable']/tbody/tr[2]/td[count(//tr[@class='childHeaderRow']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//input[contains(@class,'gbd')]")
	HtmlElement dateField(@Param("name") String name);

	@FindBy(".//table[@class='childTable']/tbody/tr[2]/td[count(//tr[@class='childHeaderRow']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//input[contains(@class,'gbt')]")
	HtmlElement timeField(@Param("name") String name);

	@FindBy(".//table[@class='childTable']/tbody/tr[2]/td[count(//tr[@class='childHeaderRow']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//textarea")
	HtmlElement textareaField(@Param("name") String name);

	@FindBy(".//table[@class='childTable']/tbody/tr[2]/td[count(//tr[@class='childHeaderRow']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//select")
	Select selectField(@Param("name") String name);

	@FindBy(".//h3[contains(@class,'expanded')]//span[contains(@class,'arrow')]")
	HtmlElement collapseIcon();

	@FindBy(".//h3[contains(@class,'collapsed')]//span[contains(@class,'arrow')]")
	HtmlElement expandIcon();

	@FindBy(".//span[@class='leafTitle']")
	HtmlElement objectTitle();

}
