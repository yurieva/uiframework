package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface SortByFieldRow extends ExtendedWebElement<SortByFieldRow> {

    @FindBy(".//select[contains(@class,'orderByFieldValue')]")
    Select field();

    @FindBy(".//select[contains(@class,'sortOrderValue')]")
    Select direction();
    
    @FindBy(".//span[text()='Clear']")
    HtmlElement clearSorting();

}
