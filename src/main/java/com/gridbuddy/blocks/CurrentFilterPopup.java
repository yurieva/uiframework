package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface CurrentFilterPopup extends ExtendedWebElement<CurrentFilterPopup> {

    @FindBy(".//h3[@class='metadataPanelObjectName' and text()='{{ name }}']")
    HtmlElement object(@Param("name") String name);

}
