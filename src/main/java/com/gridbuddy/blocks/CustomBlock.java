package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;

public interface CustomBlock extends ExtendedWebElement<CustomBlock> {

    @FindBy(".//div[@id and contains(@class,'custom')]")
    ExtendedList<CustomRecord> records();

    @FindBy(".//span[text()='{{ recordName }}']/..")
    CustomRecord record(@Param("recordName") String recordName);
}
