package com.gridbuddy.blocks;

import io.qameta.htmlelements.element.ExtendedWebElement;

public interface Checkbox extends ExtendedWebElement<Checkbox> {

    default void setChecked(boolean state) {
        if (isSelected() != state) {
            click();
        }
    }
}
