package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface GBGroupRecord extends ExtendedWebElement<GBGroupRecord> {


    @FindBy("./preceding-sibling::tr[contains(@class,'grp{{ groupLevelNumber }}')]//span[@class='grpContent']")
    ExtendedList<GBRecord> groupField(@Param("groupLevelNumber") int groupLevelNumber);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]//input")
    HtmlElement inputField(@Param("name") String name);

    @FindBy("./td[1]/following-sibling::td[count(//table[@id='gbMainTable']/tbody/tr[1]//span[text()=\"{{ name }}\"]/../preceding-sibling::td[@name]) + 1]//div")
    Select block(@Param("name") String name);
}
