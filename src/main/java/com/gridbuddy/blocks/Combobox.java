package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface Combobox extends ExtendedWebElement<Combobox> {

    @FindBy(".//strong[text()='{{ name }}']/preceding-sibling::input[@type='checkbox']")
    HtmlElement selectAll(@Param("name") String name);

    @FindBy(".//span[text()='{{ name }}']/preceding-sibling::input")
    Checkbox item(@Param("name") String name);

    @FindBy(".//span")
    ExtendedList<HtmlElement> items();
}
