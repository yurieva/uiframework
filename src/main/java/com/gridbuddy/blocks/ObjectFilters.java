package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ObjectFilters extends ExtendedWebElement<ObjectFilters> {

    @FindBy(".//h3")
    HtmlElement objectName();

    @FindBy(".//div[contains(@class,'filter') and not(contains(@class,'none'))]")
    ExtendedList<FilterByFieldRow> filterByFieldRows();

    @FindBy(".//span[@title='Add filter condition']")
    HtmlElement addFilterCondition();

    @FindBy(".//div[contains(@class,'sortByRow') and not(contains(@class,'none'))]")
    ExtendedList<SortByFieldRow> sortByFieldRows();

    @FindBy(".//span[@title='Add sort condition']")
    HtmlElement addSortCondition();

    @FindBy("//div[contains(@class,'groupRow') and not(contains(@class,'none'))]")
    ExtendedList<GroupByFieldRow> groupByFieldRows();
    
    @FindBy("//div[contains(@class,'columnGroupingRow') and not(contains(@class,'none'))]")
    ExtendedList<ForecastViewRow> forecastVeiwRows();

    @FindBy(".//span[@title='Add Group by condition']")
    HtmlElement addGroupingCondition();

    @FindBy(".//input[@class='recordLimitValue']")
    HtmlElement maxRecordsLimit();

    @FindBy(".//tr[@class='advFilterRow']")
    AdvancedFilterConditions advancedFilterConditions();

    @FindBy(".//label[contains(text(),'All Records')]/preceding-sibling::input[@type='radio']")
    HtmlElement allRecords();

    @FindBy(".//label[contains(text(),\"User's Records\")]/preceding-sibling::input[@type='radio']")
    HtmlElement userRecords();
}
