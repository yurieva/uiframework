package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface MassCreatePopup extends ExtendedWebElement<MassCreatePopup> {

    @FindBy(".//input[@id='massCreateParents' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement massCreateParents();
    
    @FindBy(".//input[@id='massCreateRelated' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement massCreateChild();	

    @FindBy(".//input[@id='massCreateNumber']")
    HtmlElement numberOfRecords();

    @FindBy(".//span[@class='leafTitle']")
    ExtendedList<HtmlElement> childObjects();

    @FindBy(".//tr[@class='gradientHeader']/td")
    ExtendedList<HtmlElement> parentColumns();

    @FindBy(".//table[@id='massUpdatesTable']/tbody/tr[2]/td[count(//tr[@class='gradientHeader']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//input")
    HtmlElement parentInputField(@Param("name") String name);
    
    @FindBy(".//table[@id='massUpdatesTable']/tbody/tr[2]/td[count(//tr[@class='gradientHeader']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//textarea")
    HtmlElement parentTextareaField(@Param("name") String name);

    @FindBy(".//table[@id='massUpdatesTable']/tbody/tr[2]/td[count(//tr[@class='gradientHeader']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//input[contains(@class,'gbd')]")
    HtmlElement parentDateField(@Param("name") String name);

    @FindBy(".//table[@id='massUpdatesTable']/tbody/tr[2]/td[count(//tr[@class='gradientHeader']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//input[contains(@class,'gbt')]")
    HtmlElement parentTimeField(@Param("name") String name);
    
    @FindBy(".//table[@id='massUpdatesTable']/tbody/tr[2]/td[count(//tr[@class='gradientHeader']/td[text()=\"{{ name }}\"]/preceding-sibling::td) + 1]//select")
    Select parentSelectField(@Param("name") String name);
    
    @FindBy(".//tr[@id]/following-sibling::tr[.//span[text()='{{ object }}']]")
    MassUpdateChildRecord childRecord(@Param("object") String object);

}
