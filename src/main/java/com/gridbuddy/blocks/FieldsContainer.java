package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface FieldsContainer extends ExtendedWebElement<FieldsContainer> {

    @FindBy(".//input[@class='searchBox']")
    HtmlElement search();

    @FindBy(".//div[@class='objectFields']/div[@id and span[@role='button' and not(@class='gbAddFieldDisabled')]]")
    ExtendedList<Field> fields();

    @FindBy(".//div[@class='loadingText']")
    HtmlElement loading();

}
