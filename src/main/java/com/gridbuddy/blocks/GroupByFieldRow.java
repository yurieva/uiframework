package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;

public interface GroupByFieldRow extends ExtendedWebElement<GroupByFieldRow> {

    @FindBy(".//select[contains(@class,'groupByFieldValue')]")
    Select groupedField();

    @FindBy(".//select[@id='groupByOrderSelect']")
    Select sortBy();

    @FindBy(".//select[@class='groupBySortOrderValue']")
    Select sortDirection();

}
