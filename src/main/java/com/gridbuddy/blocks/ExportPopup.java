package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;

public interface ExportPopup extends OverlayBlock {

    @FindBy("//span[text()='{{ name }}']/preceding-sibling::input")
    Checkbox object(@Param("name") String name);
}
