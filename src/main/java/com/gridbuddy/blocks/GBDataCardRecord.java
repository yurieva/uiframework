package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface GBDataCardRecord extends ExtendedWebElement<GBDataCardRecord> {
    
    @FindBy(".//label[text()='{{ name }}']/following-sibling::div[@class='cardCell']//input")
    HtmlElement inputField(@Param("name") String name);
    
    @FindBy(".//label[text()='{{ name }}']/following-sibling::div[@class='cardCell']//select")
    Select selectField(@Param("name") String name);

}
