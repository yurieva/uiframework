package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface TabBlock extends ExtendedWebElement<TabBlock> {

    @FindBy(".//label[contains(text(),'Name:')]/following-sibling::input[1]")
    HtmlElement name();

    @FindBy(".//label[contains(text(),'Content:')]/following-sibling::input[1]")
    HtmlElement content();

    @FindBy(".//span[@title='Click to see all values, or type in the value field and matching values will appear.']")
    HtmlElement arrow();

    @FindBy("//li[@class='ui-menu-item']")
    ExtendedList<HtmlElement> contentList();

    @FindBy(".//span[@class='removeTab']")
    HtmlElement remove();
}
