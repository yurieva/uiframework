package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface Field extends ExtendedWebElement<Field> {

    @FindBy(".//span[@class='gbAddField']")
    HtmlElement addIcon();

    @FindBy(".//span[@class='fieldLabel']")
    HtmlElement fieldName();

    @FindBy(".//span[contains(@class,'fieldApiName')]")
    HtmlElement fieldApiName();

    @FindBy(".//span[@class='gbRemoveField']")
    HtmlElement removeIcon();

    @FindBy(".//span[contains(@class,'icon-ellipses')]")
    HtmlElement fieldProperties();

}
