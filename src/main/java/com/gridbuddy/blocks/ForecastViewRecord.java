package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ForecastViewRecord extends ExtendedWebElement<ForecastViewRecord> {

	@FindBy(".//td[@class='fieldCol']/div[@name]")
	ExtendedList<HtmlElement> fields();

	@FindBy(".//td[@class='fieldCol']/div[contains(text(),\"{{ name }}\")]/span[contains(@class,'icon-cog')]")
	HtmlElement cogIcon(@Param("name") String name);

	@FindBy(".//td[contains(@class,'dc')][@id]//div[@class='allFields']")
	ExtendedList<ForecastViewCell> dataCells();
	
	@FindBy(".//td[contains(@class,'dc')]")
	ExtendedList<ForecastViewCell> cells();

	@FindBy(".//td[@class='fieldCol']/div[contains(text(),\"{{ name }}\")]")
	HtmlElement field(@Param("name") String name);
}
