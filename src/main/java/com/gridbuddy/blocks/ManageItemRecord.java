package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ManageItemRecord extends ExtendedWebElement<ManageItemRecord> {

    @FindBy(".//input[@type='checkbox']")
    Checkbox checkbox();

    @FindBy("./td[count(//td[contains(text(),'{{ name }}')]/preceding-sibling::td) + 1]")
    HtmlElement cell(@Param("name") String name);

}
