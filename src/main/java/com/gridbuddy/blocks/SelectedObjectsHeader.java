package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface SelectedObjectsHeader extends ExtendedWebElement<SelectedObjectsHeader> {

    @FindBy(".//span[@class='objectLabel']")
    HtmlElement objectLabel();

    @FindBy(".//span[@class='relName']")
    HtmlElement relName();

    @FindBy(".//span[contains(@class,'icon-wrap')]")
    HtmlElement dataCardButton();

    @FindBy(".//span[contains(@class,'icon-ellipses')]")
    HtmlElement objectLevelSettings();

}
