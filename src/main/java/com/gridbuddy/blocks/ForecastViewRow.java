package com.gridbuddy.blocks;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface ForecastViewRow extends ExtendedWebElement<ForecastViewRow> {

    @FindBy(".//select[contains(@class,'columnGroupingFieldValue')]")
    Select columnGrouping();

    @FindBy(".//select[contains(@class,'columnGroupingOrderSelect')]")
    Select format();

    @FindBy(".//select[contains(@class,'columnGroupingSortOrderValue')]")
    Select sortDirection();
    
    @FindBy(".//span[text()='Clear']")
    HtmlElement clearFilter();
    
    

}
