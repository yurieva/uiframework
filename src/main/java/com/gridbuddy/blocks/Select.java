package com.gridbuddy.blocks;

import static com.gridbuddy.matchers.IsCollectionHasSize.hasSize;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.testng.Assert.fail;

import java.util.List;
import java.util.stream.Collectors;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface Select extends ExtendedWebElement<Select> {

    @FindBy(".//option")
    ExtendedList<HtmlElement> options();

    public default void select(String text) {
        options().should(hasSize(greaterThan(0)));
        for (HtmlElement option : options()) {
            if (option.getText().equals(text)) {
                org.openqa.selenium.support.ui.Select select = new org.openqa.selenium.support.ui.Select(this);
                select.selectByVisibleText(text);
                return;
            }
        }
        fail(text + " item is missing");
    }

    public default void selectByValue(String text) {
        options().should(hasSize(greaterThan(0)));
        for (HtmlElement option : options()) {
            if (option.getAttribute("value").equals(text)) {
                org.openqa.selenium.support.ui.Select select = new org.openqa.selenium.support.ui.Select(this);
                select.selectByValue(text);
                return;
            }
        }
        fail(text + " item is missing");
    }

    public default boolean hasOption(String text) {
        return options().stream().filter(i -> i.getText().equals(text)).count() > 0;
    }

    public default List<String> getOptions() {
        return options().stream().map(i -> i.getText()).collect(Collectors.toList());
    }

    public default void shouldNotHaveOption(String text) {
        options().filter(i -> i.getText().equals(text)).should("Option " + text + " is displayed", hasSize(0));
    }

    public default void shouldHaveOption(String text) {
        options().filter(i -> i.getText().equals(text)).should("Option " + text + " is not displayed", hasSize(1));
    }

    public default String getSelectedOption() {
        org.openqa.selenium.support.ui.Select select = new org.openqa.selenium.support.ui.Select(this);
        return select.getFirstSelectedOption().getText();
    }

    public default String getSelectedValue() {
        org.openqa.selenium.support.ui.Select select = new org.openqa.selenium.support.ui.Select(this);
        return select.getFirstSelectedOption().getAttribute("value");
    }

    public default void shouldHaveValueSelected(String expected) {
        org.openqa.selenium.support.ui.Select select = new org.openqa.selenium.support.ui.Select(this);
        assertThat(select.getFirstSelectedOption().getText(), equalTo(expected));
    }

}
