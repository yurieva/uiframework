package com.gridbuddy.objects;

public class ParentObjects {

    public static final String ACCOUNT = "Account";

    public static final String CONTACT = "Contact";

    public static final String OPPORTUNITY = "Opportunity";

}
