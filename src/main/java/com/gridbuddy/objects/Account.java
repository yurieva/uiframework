package com.gridbuddy.objects;

import java.util.Arrays;
import java.util.List;

public class Account {

    public static class AccountFields {

        public static final String ACCOUNT_NAME = "Account Name";
        public static final String ACCOUNT_FAX = "Account Fax";
        public static final String ACCOUNT_SITE = "Account Site";
        public static final String ACCOUNT_ID = "Account ID";
        public static final String ACCOUNT_CONTACT_ROLES = "Account Contact Roles";
        public static final String CONTACTS = "Contacts";
        public static final String ACTIVE = "Active";
        public static final String ACCOUNT_DESCRIPTION = "Account Description";
        public static final String ACCOUNT_TYPE = "Account Type";
        public static final String BILLING_CITY = "Billing City";
        public static final String BILLING_COUNTRY = "Billing Country";
        public static final String BILLING_STATE = "Billing State/Province";
        public static final String CREATED_DATE = "Created Date";
        public static final String ACCOUNT_NUMBER = "Account Number";
        public static final String ACCOUNT_PHONE = "Account Phone";
        public static final String ACCOUNT_RATING = "Account Rating";
        public static final String ACCOUNT_SOURCE = "Account Source";
        public static final String ACCOUNT_CURRENCY = "Account Currency";

        public final static List<String> ACTIVE_STATUSES = Arrays.asList("--None--", "Yes", "No");

        public final static List<String> ACCOUNT_RATINGS = Arrays.asList("--None--", "Hot", "Cold", "Warm");

        public final static List<String> ACCOUNT_SOURCES = Arrays.asList("--None--", "Web", "Phone Inquiry",
                "Partner Referral", "Purchased List", "Other");

        public final static List<String> ACCOUNT_TYPES = Arrays.asList("--None--", "Prospect", "Customer - Direct",
                "Customer - Channel", "Channel Partner / Reseller", "Installation Partner", "Technology Partner",
                "Other");

    }

    public class AccountRelatedObjects {

        public static final String CONTACT = "Contact";

        public static final String EVENT = "Event";

        public static final String CONTRACT = "Contract";

        public static final String ACCOUNT_HISTORY = "Account History";

        public static final String OPPORTUNITY = "Opportunity";

    }

}
