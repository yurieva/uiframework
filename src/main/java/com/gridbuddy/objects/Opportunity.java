package com.gridbuddy.objects;

public class Opportunity {

    public class OpportunityFields {

        public static final String AMOUNT = "Amount";

        public static final String NAME = "Name";

        public static final String CLOSE_DATE = "Close Date";

        public static final String STAGE = "Stage";

        public static final String OWNER = "Owner";
        
        public static final String ACCOUNT_NAME = "Account Name";

    }

}
