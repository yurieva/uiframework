package com.gridbuddy.objects;

import java.util.List;
import java.util.Random;

public class ObjectUtils {

    public static String getRandomValue(List<String> list) {
        return list.get(new Random().nextInt(list.size()));
    }
    
    public static String getNonEmptyRandomValue(List<String> list) {
        return list.get(new Random().nextInt(list.size() - 1) + 1);
    }

}
