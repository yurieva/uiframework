package com.gridbuddy.objects;

import java.util.Arrays;
import java.util.List;

public class Contact {

    public static class ContactFields {

        public static final String ACCOUNT_NAME = "Account Name";
        public static final String ASSISTANTS_NAME = "Assistant's Name";
        public static final String ASST_PHONE = "Asst. Phone";
        public static final String BIRTHDATE = "Birthdate";
        public static final String BUSINESS_FAX = "Business Fax";
        public static final String BUSINESS_PHONE = "Business Phone";
        public static final String CLEAN_STATUS = "Clean Status";
        public static final String CONTACT_DESCRIPTION = "Contact Description";
        public static final String CONTACT_ID = "Contact ID";
        public static final String CREATED_BY = "Created By";
        public static final String LAST_NAME = "Last Name";
        public static final String FULL_NAME = "Full Name";

        public static final List<String> CLEAN_STATUSES = Arrays.asList("In Sync", "Different", "Reviewed", "Not Found",
                "Inactive", "Not Compared", "Select Match", "Skipped");

    }

}
