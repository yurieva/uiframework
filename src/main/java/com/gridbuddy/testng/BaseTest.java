package com.gridbuddy.testng;

import java.lang.reflect.Method;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.gridbuddy.steps.LoginSteps;
import com.gridbuddy.webdriver.DefaultWebDriverManager;
import com.gridbuddy.webdriver.WebDriverManager;

public class BaseTest {

    private WebDriverManager driverManager;

    private LoginSteps steps;

    private String testMethod;

    public LoginSteps getUser() {
        return steps;
    }

    @BeforeMethod(alwaysRun = true)
    public void before(Method method) {
        driverManager = new DefaultWebDriverManager();
        testMethod = method.getName() + " - " + method.getAnnotation(Test.class).description();
        driverManager.startWebDriver(testMethod);
        steps = new LoginSteps(driverManager.getWebDriver(), driverManager.getWebDriver().getWindowHandle(), "");
    }

    @AfterMethod(alwaysRun = true)
    public void after(ITestResult result) {
        if (ITestResult.FAILURE == result.getStatus()) {
            driverManager.makeScreenshot();
        }
        driverManager.stopWebDriver();
    }

}
