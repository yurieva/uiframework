package com.gridbuddy.pages;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.HtmlElement;

public interface GWPage extends SFBasePage {

    @FindBy("//input[@value='Next' and not(contains(@class,'none'))]")
    HtmlElement next();

    @FindBy("//input[@value='Refresh' and not(contains(@class,'none'))]")
    HtmlElement refresh();
    
    @FindBy("//input[@value='Clone' and not(contains(@class,'none'))]")
    HtmlElement clone();

    @FindBy("//input[@value='Back' and not(contains(@class,'none'))]")
    HtmlElement back();

    @FindBy("//input[@value='Save' and not(contains(@class,'none'))]")
    HtmlElement save();

    @FindBy("//input[@value='Delete' and not(contains(@class,'none'))]")
    HtmlElement delete();

    @FindBy("//a[text()='Launch Grid']")
    HtmlElement launchGrid();

    @FindBy("//div[@class='rMessages']/div|//div[@id='vldWarning']/div[@class='msgBody']")
    ExtendedList<HtmlElement> errors();

}
