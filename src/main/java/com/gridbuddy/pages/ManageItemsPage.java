package com.gridbuddy.pages;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.HtmlElement;

public interface ManageItemsPage extends SFBasePage {

    @FindBy("//div[@class='pbHeader']//input[@value='{{ name }}' and not(@disabled)]")
    HtmlElement topRowButton(@Param("name") String name);

    @FindBy("//h2[@class='mainTitle']")
    HtmlElement pageTitle();

}
