package com.gridbuddy.pages;

import com.gridbuddy.blocks.Checkbox;
import com.gridbuddy.blocks.CustomBlock;
import com.gridbuddy.blocks.PreviewTabs;
import com.gridbuddy.blocks.Select;
import com.gridbuddy.blocks.TabBlock;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.HtmlElement;

public interface ManageTabbedPagesPage extends ManageItemsPage {

    @FindBy("//input[@value='Save' and not(contains(@style,'none'))]")
    HtmlElement save();

    @FindBy("//input[@value='New' and not(contains(@style,'none'))]")
    HtmlElement newButton();

    @FindBy("//h1[@class='pageTitle']")
    HtmlElement pageTitle();

    @FindBy("//h1[contains(text(),'Select a page:')]/following-sibling::select")
    Select selectPage();

    @FindBy("//label[text()='Title']/following-sibling::input")
    HtmlElement title();

    @FindBy("//label[text()='Select an Object']/following-sibling::select")
    Select selectObject();

    @FindBy("//label[text()='Tab Height']/following-sibling::input")
    HtmlElement tabHeight();

    @FindBy("//label[text()='Description']/following-sibling::input")
    HtmlElement description();

    @FindBy("//label[text()='Global Buttons']/following-sibling::input[@type='checkbox']")
    Checkbox globalButtons();

    @FindBy("//label[text()='Show Salesforce Header']/following-sibling::input[@type='checkbox']")
    Checkbox showSalesforceHeader();

    @FindBy("//label[text()='Show Salesforce Sidebar']/following-sibling::input[@type='checkbox']")
    Checkbox showSalesforceSidebar();

    @FindBy("//div[@class='tabDefinition']")
    ExtendedList<TabBlock> tabs();

    @FindBy("//input[@value='Add Tab' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement addTab();

    @FindBy("//div[@class='previewSection']")
    PreviewTabs previewTabs();

    @FindBy("//div[text()='Available CSS']/following-sibling::div[@id='availableCSSCode']")
    CustomBlock availableCss();

    @FindBy("//div[text()='Selected CSS']/following-sibling::div[@id='selectedCSSCode']")
    CustomBlock selectedCss();

    @FindBy("//div[text()='Available JavaScript']/following-sibling::div[@id='availableJSCode']")
    CustomBlock availableJavascript();

    @FindBy("//div[text()='Selected JavaScript']/following-sibling::div[@id='selectedJSCode']")
    CustomBlock selectedJavascript();

    @FindBy("//a[text()='Add/Edit' and contains(@href,'ManageCustomCode')]")
    HtmlElement addEditCustomCode();

}
