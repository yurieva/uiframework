package com.gridbuddy.pages;

import com.gridbuddy.blocks.CustomTheme;
import com.gridbuddy.blocks.ManageItemRecord;
import com.gridbuddy.blocks.Select;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.HtmlElement;

public interface ManageThemePage extends ManageItemsPage {

    @FindBy("//input[@class='themeSummary']")
    HtmlElement themeName();

    @FindBy("//select[@class='themeType']")
    Select scope();

    @FindBy("//div[@class='themeConfigItem']")
    ExtendedList<CustomTheme> themeConfigItem();

    @FindBy("//a[text()='Add']")
    HtmlElement add();

    @FindBy("//tr[@id and @class='themeRow']")
    ExtendedList<ManageItemRecord> themes();

}
