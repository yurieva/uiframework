package com.gridbuddy.pages;

import com.gridbuddy.blocks.Checkbox;
import com.gridbuddy.blocks.CustomBlock;
import com.gridbuddy.blocks.Select;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.HtmlElement;

public interface GW1Page extends GWPage {

    @FindBy("//select[@class='gridList']")
    Select grid();

    @FindBy("//a[text()='Refresh Grid Meta Data']")
    HtmlElement refreshMetaData();

    @FindBy("//td[contains(text(),'URL')]/following-sibling::td//input")
    HtmlElement url();

    @FindBy("//input[@class='nameInputField' and not(@disabled)]")
    HtmlElement name();

    @FindBy("//select[@class='parentObjectSelect']")
    Select parentObject();

    @FindBy("//input[contains(@class,'{{ name }}')]")
    Checkbox checkbox(@Param("name") String name);

    @FindBy("//input[@type='checkbox']")
    ExtendedList<Checkbox> checkboxes();

    @FindBy("//input[not(@disabled)]/parent::div[@class='folder']/span[text()='{{ name }}']/preceding-sibling::input")
    Checkbox folder(@Param("name") String name);

    @FindBy("//td[text()='Created By']/following-sibling::td")
    HtmlElement createdBy();

    @FindBy("//td[text()='Last Modified By']/following-sibling::td")
    HtmlElement lastModifiedBy();

    @FindBy("//td[contains(text(),'Edit')]/following-sibling::td/input[@type='checkbox']")
    Checkbox edit();

    @FindBy("//div[contains(text(),'Create')]/preceding-sibling::div/input[@type='checkbox']")
    Checkbox create();

    @FindBy("//div[contains(text(),'Mass create')]/preceding-sibling::div/input[@type='checkbox']")
    Checkbox massCreate();

    @FindBy("//div[contains(text(),'Roll back on save')]/preceding-sibling::div/input[@type='checkbox']")
    Checkbox rollBackOnSave();

    @FindBy("//div[contains(text(),'Mass update')]/preceding-sibling::div/input[@type='checkbox']")
    Checkbox massUpdate();

    @FindBy("//td[contains(text(),'Delete')]/following-sibling::td/input[@type='checkbox']")
    Checkbox deleteCheckbox();

    @FindBy("//div[contains(text(),'Delete all')]/preceding-sibling::div/input[@type='checkbox']")
    Checkbox deleteAll();

    @FindBy("//div[contains(text(),'Roll back on delete')]/preceding-sibling::div/input[@type='checkbox']")
    Checkbox rollBackOnDelete();

    @FindBy("//td[contains(text(),'User-defined filtering')]/following-sibling::td/input[@type='checkbox']")
    Checkbox userDefinedFiltering();

    @FindBy("//td[contains(text(),'User-defined columns')]/following-sibling::td/input[@type='checkbox']")
    Checkbox userDefinedColumns();

    @FindBy("//td[contains(text(),'Export')]/following-sibling::td/input[@type='checkbox']")
    Checkbox export();

    @FindBy("//td[contains(text(),'Show header')]/following-sibling::td/input[@type='checkbox']")
    Checkbox showHeader();

    @FindBy("//td[contains(text(),'Show sidebar')]/following-sibling::td/input[@type='checkbox']")
    Checkbox showSidebar();

    @FindBy("//td[contains(text(),'Display as read-only')]/following-sibling::td/input[@type='checkbox']")
    Checkbox displayAsReadOnly();

    @FindBy("//td[contains(text(),'Repeat parent object header')]/following-sibling::td/input[@type='checkbox']")
    Checkbox repeatHeader();

    @FindBy("//td[contains(text(),'Compact view')]/following-sibling::td/input[@type='checkbox']")
    Checkbox compactView();

    @FindBy("//td[contains(text(),'Records per page')]/following-sibling::td/select")
    Select recordsPerPage();

    @FindBy("//label[contains(text(),'Show related objects expanded')]/preceding-sibling::input[@type='radio']")
    Checkbox showRelatedObjectsExpanded();

    @FindBy("//label[contains(text(),'Show related objects collapsed')]/preceding-sibling::input[@type='radio']")
    Checkbox showRelatedObjectsCollapsed();

    @FindBy("//label[contains(text(),'Hide related objects')]/preceding-sibling::input[@type='radio']")
    Checkbox hideRelatedObjects();

    @FindBy("//span[contains(text(),'Show groupings expanded')]/preceding-sibling::input[@type='checkbox']")
    Checkbox showGroupingsExpanded();

    @FindBy("//span[contains(text(),'Show record details')]/preceding-sibling::input[@type='checkbox']")
    Checkbox showRecordDetails();

    @FindBy("//div[text()='Available Themes']/following-sibling::div[@id='availableThemes']")
    CustomBlock availableThemes();

    @FindBy("//div[text()='Selected Themes']/following-sibling::div[@id='selectedThemes']")
    CustomBlock selectedThemes();

    @FindBy("//a[text()='Add/Edit' and contains(@href,'ManageCustomTheme')]")
    HtmlElement addEditCustomTheme();

    @FindBy("//a[text()='Add/Edit' and contains(@href,'ManageFolders')]")
    HtmlElement addEditCustomFolder();

    @FindBy("//div[text()='Available CSS']/following-sibling::div[@id='availableCSSCode']")
    CustomBlock availableCss();

    @FindBy("//div[text()='Selected CSS']/following-sibling::div[@id='selectedCSSCode']")
    CustomBlock selectedCss();

    @FindBy("//div[text()='Available JavaScript']/following-sibling::div[@id='availableJSCode']")
    CustomBlock availableJavascript();

    @FindBy("//div[text()='Selected JavaScript']/following-sibling::div[@id='selectedJSCode']")
    CustomBlock selectedJavascript();

    @FindBy("//a[text()='Add/Edit' and contains(@href,'ManageCustomCode')]")
    HtmlElement addEditCustomCode();

}
