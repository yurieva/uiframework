package com.gridbuddy.pages;

import io.qameta.htmlelements.WebPage;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.HtmlElement;

public interface LoginPage extends WebPage {

    @FindBy(".//input[@id='username']")
    HtmlElement username();

    @FindBy(".//input[@id='password']")
    HtmlElement password();

    @FindBy(".//input[@id='Login']")
    HtmlElement login();
}
