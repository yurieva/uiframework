package com.gridbuddy.pages;

import com.gridbuddy.blocks.Select;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.HtmlElement;

public interface GWLandingPage extends SFBasePage {

    @FindBy("//select[@class='gridList']")
    Select manageGrids();

    @FindBy("//a[text()='Manage Actions']")
    HtmlElement manageActions();

    @FindBy("//a[text()='Manage Folders']")
    HtmlElement manageFolders();

    @FindBy("//a[text()='Manage Tabbed Pages']")
    HtmlElement manageTabbedPages();

    @FindBy("//a[text()='Manage Custom Code']")
    HtmlElement manageCustomCode();

    @FindBy("//a[text()='Manage Custom Themes']")
    HtmlElement manageCustomThemes();

    @FindBy("//a[text()='Refresh All Grid Meta Data']")
    HtmlElement refreshMetaData();

    @FindBy("//a[text()='Disable Meta Data Service']")
    HtmlElement disableMetaData();

    @FindBy("//label[contains(text(),'Disable GridBuddy usage tracking')]/preceding-sibling::input[@type='checkbox']")
    HtmlElement disableTracking();

    @FindBy("//label[contains(text(),'Disable record locking for all grids')]/preceding-sibling::input[@type='checkbox']")
    HtmlElement disableLocking();

}
