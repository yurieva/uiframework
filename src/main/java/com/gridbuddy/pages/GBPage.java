package com.gridbuddy.pages;

import com.gridbuddy.blocks.AutocompleteElements;
import com.gridbuddy.blocks.ChartPopup;
import com.gridbuddy.blocks.Checkbox;
import com.gridbuddy.blocks.CurrentFilterPopup;
import com.gridbuddy.blocks.DetailPanel;
import com.gridbuddy.blocks.ExportPopup;
import com.gridbuddy.blocks.FastFilter;
import com.gridbuddy.blocks.GBTable;
import com.gridbuddy.blocks.ManageFilters;
import com.gridbuddy.blocks.MassCreatePopup;
import com.gridbuddy.blocks.MassUpdatePopup;
import com.gridbuddy.blocks.OverlayBlock;
import com.gridbuddy.blocks.ReorderColumnsBlock;
import com.gridbuddy.blocks.Select;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.HtmlElement;

public interface GBPage extends SFBasePage {

    @FindBy("//table[@id='gbMainTable']")
    GBTable table();

    @FindBy("//div[contains(@class,'top')]")
    HtmlElement topRowWithButtons();

    @FindBy("//span[@class='refreshBtn']/input[@value='Refresh']")
    HtmlElement refresh();

    @FindBy("//input[@title='Save and continue editing']")
    HtmlElement save();

    @FindBy("//span[@class='showBtnContainer']/button[contains(@class,'toggleRecords')][1]")
    HtmlElement show();

    @FindBy("//button[text()='More' and not(contains(@class,'gbBtnDisabled'))]/span")
    HtmlElement more();

    @FindBy("//button[text()=' Mass Update' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement massUpdate();
    
    @FindBy("//input[@value = 'Read Only' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement readOnly();

    @FindBy("//button[contains(text(),'Groupings...') and not(contains(@class,'gbBtnDisabled '))]")
    HtmlElement groupings();

    @FindBy("//button[text()=' Filter' and not(contains(@class,'gbBtnDisabled'))]")
    HtmlElement filter();

    @FindBy("//button[contains(@class,'createNew')]")
    HtmlElement newButton();

    @FindBy("//li[contains(@class,'relatedRecordsItem')]/input")
    Checkbox relatedRecords();

    @FindBy("//li[contains(@class,'relatedObjectsItem')]/input")
    Checkbox relatedObjects();

    @FindBy("//div[@class='msgDetail'] | //div[contains(@style,'block')]/div[@class='msgBody']")
    HtmlElement message();

    @FindBy("//div[@id='fastFilter']")
    FastFilter fastFilter();

    @FindBy("//div[@id='gridStatus']")
    HtmlElement loader();

    @FindBy("//ul[@id='gbActionsMenu']/li[contains(text(),'{{ name }}')]")
    HtmlElement action(@Param("name") String name);

    @FindBy("//div[@id='actionsFrameBox']")
    OverlayBlock gridOverlay();

    @FindBy("//div[@class='pageActionsMenu']//li[contains(text(),'{{ name }}')]")
    HtmlElement gridMenuItem(@Param("name") String name);

    @FindBy("//div[@class='pageActionsMenu']//li")
    ExtendedList<HtmlElement> gridMenuItems();

    @FindBy("//div[contains(@class,'gbUserFilter')]")
    ManageFilters manageFiltersBlock();

    @FindBy("//span[contains(text(),'My Filter:')]/select")
    Select myFilter();

    @FindBy("//div[@class='groupingsMenu']//span[text()=' Expand groupings']/preceding-sibling::input")
    Checkbox expandGroupings();

    @FindBy("//div[@class='groupingsMenu']//span[text()=' Show record details']/preceding-sibling::input")
    Checkbox showRecordDetails();

    @FindBy(".//span[contains(text(),'Page')]/input")
    HtmlElement page();

    @FindBy("//span[@id='gbRfl']")
    HtmlElement openRecord();

    @FindBy("//h2[@class='gridTitle']")
    HtmlElement gridTitle();

    @FindBy("//h2[@class='parentName']")
    HtmlElement parentObject();

    @FindBy("//span[contains(@class,'pageSizeOpts')]/label")
    HtmlElement perPageLabel();

    @FindBy("//span[contains(@class,'recordCntRange')]")
    HtmlElement totalLabel();

    @FindBy("//div[contains(@class,'gbExportPage')]")
    ExportPopup exportPopup();

    @FindBy("//div[@id='massUpdatesMsgBox']")
    MassCreatePopup massCreatePopup();

    @FindBy("//div[@id='massUpdatesMsgBox']")
    MassUpdatePopup massUpdatePopup();

    @FindBy("//span[contains(@class,'metadataPanelIcon')]")
    HtmlElement infoIcon();

    @FindBy("//span[contains(@class,'chartIcon')]")
    HtmlElement chartIcon();

    @FindBy("//div[@class='filterCriteriaBody']")
    CurrentFilterPopup currentFilterPopup();

    @FindBy("//div[@id='reorderCols']")
    ReorderColumnsBlock reorderColumnsBlock();

    @FindBy("//div[contains(@style,'block')]//span[@class='closeX']")
    HtmlElement closeButton();

    @FindBy("//div[contains(@style,'block')]//span[@class='closeRefresh']")
    HtmlElement closeAndRefreshButton();

    @FindBy("//div[contains(@style,'block')]//span[@class='title']")
    HtmlElement popupTitle();

    @FindBy("//div[@id='gridChart']")
    ChartPopup chart();

    @FindBy("//a[text()='{{ name }}']")
    HtmlElement tab(@Param("name") String name);
    
    @FindBy("//ul[contains(@class, 'autocomplete')]")
    ExtendedList<AutocompleteElements> autocompleteWidget();
    
    @FindBy("//div[@id='dataPanel']")
    DetailPanel detailPanel();

}
