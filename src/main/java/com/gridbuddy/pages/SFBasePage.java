package com.gridbuddy.pages;

import io.qameta.htmlelements.WebPage;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.HtmlElement;

public interface SFBasePage extends WebPage {

    @FindBy("//div[@class='bBottom']//span[contains(text(),'{{ name }}')]")
    HtmlElement menuItem(@Param("name") String name);

    @FindBy("//div[@class='msgDetail']")
    HtmlElement message();

    @FindBy("//div[@class='bBottom']")
    HtmlElement sidebar();

    @FindBy("//iframe[contains(@id,'vfFrameId')]")
    HtmlElement iframe();

    @FindBy("//iframe[contains(@id,'vfFrameId')]")
    ExtendedList<HtmlElement> iframes();

    @FindBy("//button[contains(@class,'userProfile')]")
    HtmlElement profileButton();

    @FindBy("//a[text()='Log Out']")
    HtmlElement logOut();
}
