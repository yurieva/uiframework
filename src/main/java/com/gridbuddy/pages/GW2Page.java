package com.gridbuddy.pages;

import com.gridbuddy.blocks.ActionRecordGW2;
import com.gridbuddy.blocks.AddedObjects;
import com.gridbuddy.blocks.ConditionalFormatting;
import com.gridbuddy.blocks.ConfigureChart;
import com.gridbuddy.blocks.Field;
import com.gridbuddy.blocks.FieldProperties;
import com.gridbuddy.blocks.FieldsContainer;
import com.gridbuddy.blocks.ObjectLevelSettings;
import com.gridbuddy.blocks.ObjectsContainer;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.HtmlElement;

public interface GW2Page extends GWPage {

    @FindBy("//div[@class='fieldsContainer']")
    FieldsContainer fields();

    @FindBy("//div[@class='objectsContainer']")
    ObjectsContainer objects();

    @FindBy("//div[@class='selectedFieldsContainer']//div[contains(@class,'primaryFieldsSection')]/div")
    ExtendedList<Field> selectedFields();

    @FindBy("//div[@class='selectedFieldsContent']/div")
    ExtendedList<AddedObjects> selectedObjects();

    @FindBy("//table[@id='gbActions']//tr[@id]")
    ExtendedList<ActionRecordGW2> actions();

    @FindBy("//a[text()='Manage Actions']")
    HtmlElement manageActions();

    @FindBy("//div[contains(@class,'colorCodingContainer')]")
    ConditionalFormatting conditionalFormatting();

    @FindBy("//div[contains(@class,'chartConfigOptions')]")
    ConfigureChart configureChart();

    @FindBy("//div[@id='objectLevelCRUD']")
    ObjectLevelSettings objectLevelSettings();

    @FindBy("//div[@id='gbFieldPropPopup']")
    FieldProperties fieldProperties();
}
