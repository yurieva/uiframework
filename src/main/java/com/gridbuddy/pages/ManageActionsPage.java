package com.gridbuddy.pages;

import com.gridbuddy.blocks.ActionRecord;
import com.gridbuddy.blocks.EditAction;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;

public interface ManageActionsPage extends ManageItemsPage {

    @FindBy("//table[@id='actionDetails']/tbody/tr[not(contains(@style,'none')) and contains(@class,'actionRow')]")
    EditAction editActionBlock();

    @FindBy("//table[@id='actionsList']//tr[@id]")
    ExtendedList<ActionRecord> actions();

}
