package com.gridbuddy.pages;

import com.gridbuddy.blocks.Combobox;
import com.gridbuddy.blocks.FolderRecord;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.HtmlElement;

public interface ManageFoldersPage extends ManageItemsPage {

    @FindBy("//input[@class='addFolder']")
    HtmlElement createFolderName();

    @FindBy("//input[@id='updateFolder']")
    HtmlElement editFolderName();

    @FindBy("//td[@class='content'][1]")
    Combobox profiles();

    @FindBy("//td[@class='content'][2]")
    Combobox grids();

    @FindBy("//input[not(@disabled)]/parent::div[@class='folder']")
    ExtendedList<FolderRecord> folders();

}
