package com.gridbuddy.pages;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.HtmlElement;

public interface GridsPage extends SFBasePage {

    @FindBy("//td[@class='searchGrids']//span[@class='arrow']")
    HtmlElement gridsDropdownArrow();

    @FindBy("//li[@itemid=//span[text()='{{ folderName }}']/../@itemid and @class='item-grid']/span[text()='{{ gridName }}']")
    HtmlElement grid(@Param("folderName") String foldername, @Param("gridName") String gridName);
}
