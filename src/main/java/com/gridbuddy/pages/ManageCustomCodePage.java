package com.gridbuddy.pages;

import com.gridbuddy.blocks.ManageItemRecord;
import com.gridbuddy.blocks.Select;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.HtmlElement;

public interface ManageCustomCodePage extends ManageItemsPage {

    @FindBy("//input[@class='codeSummary']")
    HtmlElement componentName();

    @FindBy("//select[@class='codeType']")
    Select type();

    @FindBy("//select[@class='codeScope']")
    Select scope();

    @FindBy("//textarea[@class='ace_text-input']")
    HtmlElement codeBody();

    @FindBy("//tr[@id and @class='codeRow']")
    ExtendedList<ManageItemRecord> codes();

}
