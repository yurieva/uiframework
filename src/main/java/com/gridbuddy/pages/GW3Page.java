package com.gridbuddy.pages;

import com.gridbuddy.blocks.ObjectFilters;
import com.gridbuddy.blocks.Select;

import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedList;

public interface GW3Page extends GWPage {

    @FindBy("//select[@class='filterByOption']")
    Select filterByRelatedObject();

    @FindBy("//div[@id='parentChildFields']/div")
    ExtendedList<ObjectFilters> objectFilters();

    @FindBy("//div[@id='parentChildFields']/div[.//h3[contains(text(),'{{ objectName }}')]]")
    ObjectFilters objectFilter(@Param("objectName") String objectName);

    @FindBy("//span[contains(text(),'Saved Filter:')]/following-sibling::select[not(@disabled)]")
    Select savedFilter();

    @FindBy("//input[@class='filterNameInput' and not(@disabled)]")
    Select filterName();

}
