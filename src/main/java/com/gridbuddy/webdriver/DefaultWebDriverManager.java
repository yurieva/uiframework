package com.gridbuddy.webdriver;

import static com.gridbuddy.props.Properties.props;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.qameta.allure.Attachment;

public class DefaultWebDriverManager implements WebDriverManager {

    private WebDriver driver;

    @Override
    public void startWebDriver() {
        String environmentName = props.environment();
        switch (environmentName) {
        case "local": {
            if (props.browserName().equals("chrome")) {
                System.setProperty("webdriver.chrome.driver", "tools\\chromedriver.exe");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                break;
            }
        }
        case "remote": {
            try {
                ChromeOptions options = new ChromeOptions();
                options.addArguments("auto-select-desktop-capture-source=\"Entire screen\"",
                        "use-fake-device-for-media-stream", "use-fake-ui-for-media-stream",
                        "--ignore-certificate-errors", "--user-agent=Jmeter", "--restrict-iframe-permissions",
                        "--disable-popup-blocking");
                DesiredCapabilities capability = new DesiredCapabilities();
                capability.setBrowserName(props.browserName());
                capability.setCapability(ChromeOptions.CAPABILITY, options);
                driver = new RemoteWebDriver(new URL(props.remoteWebDriverUrl()), capability);
                driver.manage().window().maximize();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            break;
        }
        }
    }

    @Override
    public void stopWebDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    public WebDriver getWebDriver() {
        return driver;
    }

    @Override
    public void startWebDriver(String testName) {
        String environmentName = props.environment();
        switch (environmentName) {
        case "local": {
            if (props.browserName().equals("chrome")) {
                System.setProperty("webdriver.chrome.driver", "tools\\chromedriver.exe");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                break;
            }
        }
        case "remote": {
            try {
                ChromeOptions options = new ChromeOptions();
                options.addArguments("auto-select-desktop-capture-source=\"Entire screen\"",
                        "use-fake-device-for-media-stream", "use-fake-ui-for-media-stream",
                        "--ignore-certificate-errors", "webdriver.http.factory=apache", "--restrict-iframe-permissions",
                        "--disable-popup-blocking");
                DesiredCapabilities capability = new DesiredCapabilities();
                capability.setBrowserName(props.browserName());
                capability.setCapability(ChromeOptions.CAPABILITY, options);
                capability.setCapability("name", testName);
                capability.setCapability("idleTimeout", "150");
                driver = new RemoteWebDriver(new URL(props.remoteWebDriverUrl()), capability);
                ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
                driver.manage().window().setSize(new Dimension(1920, 1080));
            } catch (IOException e) {
                e.printStackTrace();
            }
            break;
        }
        }
    }

    @Override
    @Attachment
    public byte[] makeScreenshot() {
        try {
            return Files.readAllBytes(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE).toPath());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
