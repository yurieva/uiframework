package com.gridbuddy.webdriver;

import org.openqa.selenium.WebDriver;

public interface WebDriverManager {

    WebDriver getWebDriver();

    void stopWebDriver();

    void startWebDriver();
    
    void startWebDriver(String testName);

    byte[] makeScreenshot();

}
