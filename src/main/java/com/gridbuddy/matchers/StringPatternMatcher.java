/*  Copyright (c) 2000-2006 hamcrest.org
 */
package com.gridbuddy.matchers;

import java.util.regex.Pattern;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class StringPatternMatcher extends TypeSafeMatcher<String> {

	private String pattern;

	public StringPatternMatcher(String pattern) {
		this.pattern = pattern;
	}

	@Factory
	public static Matcher<String> matchesPattern(String pattern) {
		return new StringPatternMatcher(pattern);
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("string matches pattern ").appendValue(pattern);

	}

	@Override
	protected boolean matchesSafely(String item) {
		Pattern p = Pattern.compile(pattern);
		return p.matcher(item).find();
	}

}