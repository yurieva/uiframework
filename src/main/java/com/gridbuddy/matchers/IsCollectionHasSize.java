package com.gridbuddy.matchers;

import static org.hamcrest.Matchers.equalTo;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class IsCollectionHasSize<E> extends TypeSafeMatcher<Collection<? extends E>> {

    private int timeout = 15;

    private Matcher<Integer> intMatcher;

    public IsCollectionHasSize(Matcher<Integer> intMatcher) {
        this.intMatcher = intMatcher;
    }

    public IsCollectionHasSize(Matcher<Integer> intMatcher, int timeout) {
        this.intMatcher = intMatcher;
        this.timeout = timeout;
    }

    @Override
    protected boolean matchesSafely(Collection<? extends E> element) {
        long waitUntil = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeout);
        while (System.currentTimeMillis() <= waitUntil && !intMatcher.matches(element.size())) {
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return intMatcher.matches(element.size());
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("collection size is ").appendDescriptionOf(intMatcher);
    }

    @Override
    public void describeMismatchSafely(Collection<? extends E> elements, Description mismatchDescription) {
        mismatchDescription.appendText("collection size is ").appendValue(elements.size()).appendText(" while waiting ")
                .appendValue(timeout).appendText(" seconds");
    }

    @Factory
    public static <E> Matcher<Collection<? extends E>> hasSize(int size) {
        return new IsCollectionHasSize<E>(equalTo(size));
    }

    @Factory
    public static <E> Matcher<Collection<? extends E>> hasSize(int size, int timeout) {
        return new IsCollectionHasSize<E>(equalTo(size), timeout);
    }

    @Factory
    public static <E> Matcher<Collection<? extends E>> hasSize(Matcher<Integer> intMatcher) {
        return new IsCollectionHasSize<E>(intMatcher);
    }
}