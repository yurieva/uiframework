package com.gridbuddy.matchers;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class IsEqual extends TypeSafeMatcher<String> {

    private final String expectedValue;

    public IsEqual(String equalArg) {
        expectedValue = equalArg;
    }

    @Override
    public boolean matchesSafely(String actual) {
        return actual.toLowerCase().equals(expectedValue.toLowerCase());
    }

    @Override
    public void describeTo(Description description) {
        description.appendValue(expectedValue);
    }

    @Factory
    public static Matcher<String> equalToIgnoringCase(String expected) {
        return new IsEqual(expected);
    }

}