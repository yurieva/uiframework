package com.gridbuddy.matchers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class IsIpsSorted extends TypeSafeMatcher<List<String>> {

    private boolean order;

    public IsIpsSorted(boolean order) {
        this.order = order;
    }

    @Override
    protected boolean matchesSafely(List<String> elements) {
        Comparator<String> ipComparator = new Comparator<String>() {
            @Override
            public int compare(String ip1, String ip2) {
                return toNumeric(ip1).compareTo(toNumeric(ip2));
            }
        };
        List<String> ips = new ArrayList<String>(elements);
        ips.sort(ipComparator);
        if (order) {
            return ips.equals(elements);
        } else {
            Collections.reverse(ips);
            return ips.equals(elements);
        }
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("collection is sorted");
    }

    @Override
    public void describeMismatchSafely(List<String> elements, Description mismatchDescription) {
        if (order) {
            mismatchDescription.appendText("collection is not sorted ascending: ").appendText(
                    String.join(",", elements.stream().map(i -> i.toString()).collect(Collectors.toList())));
        } else {
            mismatchDescription.appendText("collection is not sorted descending: ").appendText(
                    String.join(",", elements.stream().map(i -> i.toString()).collect(Collectors.toList())));
        }
    }

    @Factory
    public static Matcher<List<String>> isIpsSortedAscending() {
        return new IsIpsSorted(true);
    }

    @Factory
    public static Matcher<List<String>> isIpsSortedDescending() {
        return new IsIpsSorted(false);
    }

    private Long toNumeric(String ip) {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(ip).useDelimiter("\\.");
        Long l = (sc.nextLong() << 24) + (sc.nextLong() << 16) + (sc.nextLong() << 8) + (sc.nextLong());
        sc.close();
        return l;
    }
}