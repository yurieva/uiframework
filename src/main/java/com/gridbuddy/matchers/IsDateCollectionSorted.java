package com.gridbuddy.matchers;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.google.common.collect.Ordering;

public class IsDateCollectionSorted extends TypeSafeMatcher<Collection<LocalDate>> {

	private boolean order;

	public IsDateCollectionSorted(boolean order) {
		this.order = order;
	}

	@Override
	protected boolean matchesSafely(Collection<LocalDate> elements) {
		if (order) {
			return Ordering.natural().isOrdered(elements);
		} else {
			return Ordering.natural().reverse().isOrdered(elements);
		}
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("collection is sorted");
	}

	@Override
	public void describeMismatchSafely(Collection<LocalDate> elements, Description mismatchDescription) {
		if (order) {
			mismatchDescription.appendText("collection is not sorted ascending: ").appendText(
					String.join(",", elements.stream().map(i -> i.toString()).collect(Collectors.toList())));
		} else {
			mismatchDescription.appendText("collection is not sorted descending: ").appendText(
					String.join(",", elements.stream().map(i -> i.toString()).collect(Collectors.toList())));
		}
	}

	@Factory
	public static Matcher<Collection<LocalDate>> isDateCollectionSortedAscending() {
		return new IsDateCollectionSorted(true);
	}

	@Factory
	public static Matcher<Collection<LocalDate>> isDateCollectionSortedDescending() {
		return new IsDateCollectionSorted(false);
	}
}