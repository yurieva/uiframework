package com.gridbuddy.matchers;

import java.util.concurrent.TimeUnit;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class DoesElementExistMatcher extends TypeSafeMatcher<WebElement> {
    @Override
    protected boolean matchesSafely(WebElement element) {
        long waitUntil = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(20);
        boolean isDisplayed = false;
        WebElement el = null;
        while (System.currentTimeMillis() <= waitUntil && !isDisplayed) {
            try {
                Thread.sleep(250);
                el = element.findElement(By.xpath("self::*"));
                if (el != null) {
                    isDisplayed = true;
                } else {
                    isDisplayed = false;
                }
            } catch (Exception e) {
            }
        }
        return isDisplayed;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("element existing on page");
    }

    @Override
    public void describeMismatchSafely(WebElement element, Description mismatchDescription) {
        mismatchDescription.appendText("element ").appendValue(element).appendText(" not existing on page");
    }

    @Factory
    public static Matcher<WebElement> exists() {
        return new DoesElementExistMatcher();
    }
}
