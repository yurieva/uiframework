package com.gridbuddy.matchers;

import java.util.concurrent.TimeUnit;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.openqa.selenium.WebElement;

public class IsElementNotDisplayedMatcher extends TypeSafeMatcher<WebElement> {

    private int timeout = 15;

    public IsElementNotDisplayedMatcher() {
    }

    public IsElementNotDisplayedMatcher(int timeout) {
        this.timeout = timeout;
    }

    @Override
    protected boolean matchesSafely(WebElement element) {
        long waitUntil = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeout);
        boolean isDisplayed = true;
        while (System.currentTimeMillis() <= waitUntil && isDisplayed) {
            try {
                isDisplayed = element.isDisplayed();
            } catch (Exception e) {
                return true;
            }
        }
        return !isDisplayed;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("element is not displayed on page");
    }

    @Override
    public void describeMismatchSafely(WebElement element, Description mismatchDescription) {
        mismatchDescription.appendText("element ").appendValue(element).appendText(" is displayed on page");
    }

    @Factory
    public static Matcher<WebElement> isNotDisplayed() {
        return new IsElementNotDisplayedMatcher();
    }

    @Factory
    public static Matcher<WebElement> isNotDisplayed(int timeout) {
        return new IsElementNotDisplayedMatcher(timeout);
    }
}