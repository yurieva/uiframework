package com.gridbuddy.matchers;

import java.util.concurrent.TimeUnit;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class HasTextMatcher extends TypeSafeMatcher<WebElement> {

    private final String textMatcher;

    private int timeout = 10;
    
    HasTextMatcher(String textMatcher) {
        this.textMatcher = textMatcher;
    }

    HasTextMatcher(String textMatcher, int timeout) {
        this.textMatcher = textMatcher;
        this.timeout = timeout;
    }

    @Override
    public boolean matchesSafely(WebElement item) {
        long waitUntil = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeout);
        boolean isDisplayed = false;
        while (System.currentTimeMillis() <= waitUntil && !isDisplayed) {
            try {
                Thread.sleep(250);
                isDisplayed = textMatcher.equals(item.getText());
            } catch (NoSuchElementException | InterruptedException e) {
            }
        }
        return isDisplayed;
    }

    public void describeTo(Description description) {
        description.appendText("element text ").appendValue(textMatcher);
    }

    @Override
    protected void describeMismatchSafely(WebElement item, Description mismatchDescription) {
        try {
            String text = item.getText();
            mismatchDescription.appendText("text of element ").appendValue(item).appendText(" was ").appendValue(text)
                    .appendText(" while waiting ").appendValue(timeout).appendText(" seconds");
        } catch (NoSuchElementException e) {
            mismatchDescription.appendText("element ").appendValue(item).appendText(" is not displayed while waiting ")
                    .appendValue(timeout).appendText(" seconds");
        }
    }

    @Factory
    public static Matcher<WebElement> hasText(final String textMatcher) {
        return new HasTextMatcher(textMatcher);
    }

    @Factory
    public static Matcher<WebElement> hasText(final String textMatcher, int timeout) {
        return new HasTextMatcher(textMatcher, timeout);
    }
}