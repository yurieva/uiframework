package com.gridbuddy.matchers;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.gridbuddy.blocks.Checkbox;


public class IsCheckboxCheckedMatcher extends TypeSafeMatcher<Checkbox> {
    @Override
    protected boolean matchesSafely(Checkbox element) {
        return element.isSelected();
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("checkbox is enabled");
    }

    @Override
    public void describeMismatchSafely(Checkbox element, Description mismatchDescription) {
        mismatchDescription.appendText("checkbox ").appendValue(element).appendText(" is not checked");
    }

    @Factory
    public static Matcher<Checkbox> isChecked() {
        return new IsCheckboxCheckedMatcher();
    }
}