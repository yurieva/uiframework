package com.gridbuddy.matchers;

import java.util.concurrent.TimeUnit;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.openqa.selenium.WebElement;

public class IsNewFrameDisplayedMatcher extends TypeSafeMatcher<WebElement> {

    private int timeout = 3;

    private String currentFrame;

    public IsNewFrameDisplayedMatcher(String currentFrame) {
        this.currentFrame = currentFrame;
    }

    public IsNewFrameDisplayedMatcher(int timeout, String currentFrame) {
        this.timeout = timeout;
        this.currentFrame = currentFrame;
    }

    @Override
    protected boolean matchesSafely(WebElement element) {
        long waitUntil = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeout);
        boolean isNewFrameDisplayed = false;
        while (System.currentTimeMillis() <= waitUntil && !isNewFrameDisplayed) {
            try {
                Thread.sleep(250);
                String fid = element.getAttribute("id");
                isNewFrameDisplayed = !fid.equals(currentFrame);
                
            } catch (Exception e) {
            }
        }
        return true;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("element is displayed on page");
    }

    @Override
    public void describeMismatchSafely(WebElement element, Description mismatchDescription) {
        mismatchDescription.appendText("element ").appendValue(element).appendText(" is not displayed on page");
    }

    @Factory
    public static Matcher<WebElement> isNewFrameDisplayed(String oldFrameId) {
        return new IsNewFrameDisplayedMatcher(oldFrameId);
    }

    @Factory
    public static Matcher<WebElement> isNewFrameDisplayed(int timeout, String oldFrameId) {
        return new IsNewFrameDisplayedMatcher(timeout, oldFrameId);
    }
}