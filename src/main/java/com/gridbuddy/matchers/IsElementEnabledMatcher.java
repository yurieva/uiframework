package com.gridbuddy.matchers;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.openqa.selenium.WebElement;

public class IsElementEnabledMatcher extends TypeSafeMatcher<WebElement> {
    @Override
    protected boolean matchesSafely(WebElement element) {
        return element.isEnabled();
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("element is enabled");
    }

    @Override
    public void describeMismatchSafely(WebElement element, Description mismatchDescription) {
        mismatchDescription.appendText("element ").appendValue(element).appendText(" is not enabled");
    }

    @Factory
    public static Matcher<WebElement> isEnabled() {
        return new IsElementEnabledMatcher();
    }
}