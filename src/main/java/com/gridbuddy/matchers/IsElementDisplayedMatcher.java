package com.gridbuddy.matchers;

import java.util.concurrent.TimeUnit;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.openqa.selenium.WebElement;

public class IsElementDisplayedMatcher extends TypeSafeMatcher<WebElement> {

    private int timeout = 15;

    public IsElementDisplayedMatcher() {
    }

    public IsElementDisplayedMatcher(int timeout) {
        this.timeout = timeout;
    }

    @Override
    protected boolean matchesSafely(WebElement element) {
        long waitUntil = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeout);
        boolean isDisplayed = false;
        while (System.currentTimeMillis() <= waitUntil && !isDisplayed) {
            try {
                Thread.sleep(250);
                isDisplayed = element.isDisplayed();
            } catch (Exception e) {
            }
        }
        return isDisplayed;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("element is displayed on page");
    }

    @Override
    public void describeMismatchSafely(WebElement element, Description mismatchDescription) {
        mismatchDescription.appendText("element ").appendValue(element).appendText(" is not displayed on page");
    }

    @Factory
    public static Matcher<WebElement> isDisplayed() {
        return new IsElementDisplayedMatcher();
    }

    @Factory
    public static Matcher<WebElement> isDisplayed(int timeout) {
        return new IsElementDisplayedMatcher(timeout);
    }
}