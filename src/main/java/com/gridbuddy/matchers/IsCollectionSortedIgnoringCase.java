package com.gridbuddy.matchers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class IsCollectionSortedIgnoringCase extends TypeSafeMatcher<Collection<String>> {

    private boolean order;

    public IsCollectionSortedIgnoringCase(boolean order) {
        this.order = order;
    }

    @Override
    protected boolean matchesSafely(Collection<String> elements) {
        List<String> list = new ArrayList<>(elements);
        if (order) {
            list.sort(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
            });
        } else {
            list.sort(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o2.toLowerCase().compareTo(o1.toLowerCase());
                }
            });
        }
        return list.equals(elements);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("collection is sorted");
    }

    @Override
    public void describeMismatchSafely(Collection<String> elements, Description mismatchDescription) {
        if (order) {
            mismatchDescription.appendText("collection is not sorted ascending: ").appendText(
                    String.join(",", elements.stream().map(i -> i.toString()).collect(Collectors.toList())));
        } else {
            mismatchDescription.appendText("collection is not sorted descending: ").appendText(
                    String.join(",", elements.stream().map(i -> i.toString()).collect(Collectors.toList())));
        }
    }

    @Factory
    public static Matcher<Collection<String>> isSortedAscendingIgnoringCase() {
        return new IsCollectionSortedIgnoringCase(true);
    }

    @Factory
    public static Matcher<Collection<String>> isSortedDescendingIgnoringCase() {
        return new IsCollectionSortedIgnoringCase(false);
    }
}