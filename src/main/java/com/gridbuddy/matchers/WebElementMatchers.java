package com.gridbuddy.matchers;

import org.hamcrest.Matcher;
import org.openqa.selenium.WebElement;

import io.qameta.htmlelements.matcher.HasTextMatcher;

public final class WebElementMatchers {

    private WebElementMatchers() {
    }

    public static Matcher<WebElement> exists() {
        return DoesElementExistMatcher.exists();
    }

    public static Matcher<WebElement> isDisplayed() {
        return IsElementDisplayedMatcher.isDisplayed();
    }

    public static Matcher<WebElement> isEnabled() {
        return IsElementEnabledMatcher.isEnabled();
    }

    public static Matcher<WebElement> hasText(final Matcher<String> matcher) {
        return HasTextMatcher.hasText(matcher);
    }

    public static Matcher<WebElement> hasText(final String text) {
        return HasTextMatcher.hasText(text);
    }

    public static Matcher<WebElement> hasAttribute(final String attribute, final Matcher<String> valueMatcher) {
        return HasAttributeMatcher.hasAttribute(attribute, valueMatcher);
    }

    public static Matcher<WebElement> hasAttribute(final String attribute, final String value) {
        return HasAttributeMatcher.hasAttribute(attribute, value);
    }

    public static Matcher<WebElement> hasClass(final Matcher<String> matcher) {
        return HasAttributeMatcher.hasAttribute("class", matcher);
    }

    public static Matcher<WebElement> hasClass(final String value) {
        return HasAttributeMatcher.hasAttribute("class", value);
    }

    public static Matcher<WebElement> hasName(final Matcher<String> matcher) {
        return HasAttributeMatcher.hasAttribute("name", matcher);
    }

    public static Matcher<WebElement> hasName(final String value) {
        return HasAttributeMatcher.hasAttribute("name", value);
    }

    public static Matcher<WebElement> hasId(final Matcher<String> matcher) {
        return HasAttributeMatcher.hasAttribute("id", matcher);
    }

    public static Matcher<WebElement> hasId(final String value) {
        return HasAttributeMatcher.hasAttribute("id", value);
    }

    public static Matcher<WebElement> hasValue(final Matcher<String> matcher) {
        return HasAttributeMatcher.hasAttribute("value", matcher);
    }

    public static Matcher<WebElement> hasValue(final String value) {
        return HasAttributeMatcher.hasAttribute("value", value);
    }
}
