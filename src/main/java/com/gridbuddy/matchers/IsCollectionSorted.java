package com.gridbuddy.matchers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.google.common.collect.Ordering;

public class IsCollectionSorted<E> extends TypeSafeMatcher<Collection<? extends Comparable<E>>> {

    private boolean order;

    public IsCollectionSorted(boolean order) {
        this.order = order;
    }

    @Override
    protected boolean matchesSafely(Collection<? extends Comparable<E>> elements) {
        if (order) {
            return Ordering.natural().isOrdered(elements);
        } else {
            return Ordering.natural().reverse().isOrdered(elements);
        }
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("collection is sorted");
    }

    @Override
    public void describeMismatchSafely(Collection<? extends Comparable<E>> elements, Description mismatchDescription) {
        if (order) {
            mismatchDescription.appendText("collection is not sorted ascending: ").appendText(
                    String.join(",", elements.stream().map(i -> i.toString()).collect(Collectors.toList())));
        } else {
            mismatchDescription.appendText("collection is not sorted descending: ").appendText(
                    String.join(",", elements.stream().map(i -> i.toString()).collect(Collectors.toList())));
        }
    }

    @Factory
    public static <E> Matcher<Collection<? extends Comparable<E>>> isSortedAscending() {
        return new IsCollectionSorted<E>(true);
    }

    @Factory
    public static <E> Matcher<Collection<? extends Comparable<E>>> isSortedDescending() {
        return new IsCollectionSorted<E>(false);
    }
}