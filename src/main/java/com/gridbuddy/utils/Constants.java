package com.gridbuddy.utils;

public class Constants {

    public static final int FIRST_ROW = 0;

    public static final int SECOND_ROW = 1;

    public static final int THIRD_ROW = 2;

}
