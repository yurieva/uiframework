package com.gridbuddy.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import io.qameta.allure.Step;
import io.qameta.htmlelements.WebPage;
import io.qameta.htmlelements.WebPageFactory;

public class WebDriverSteps {

    private Set<String> windowHandles = new HashSet<>();

    private WebDriver driver;

    protected JavascriptExecutor executor;

    protected Actions actions;

    protected String mainWindowHandle;

    protected String currentFrameId;

    public WebDriverSteps(WebDriver driver, String windowHandle, String currentFrameId) {
        this.driver = driver;
        this.mainWindowHandle = windowHandle;
        executor = (JavascriptExecutor) driver;
        actions = new Actions(driver);
        this.currentFrameId = currentFrameId;
    }

    protected <T extends WebPage> T on(Class<T> pageClass) {
        return new WebPageFactory().get(driver, pageClass);
    }

    protected WebDriver getDriver() {
        return driver;
    }

    protected <T> T openPage(String url, T stepsObject) {
        driver.get(url);
        return stepsObject;
    }

    protected void confirmAlert() {
        try {
            driver.switchTo().alert().accept();
        } catch (Exception e) {
        }
    }
    
    @Step
    public void getPage(String url) {
        driver.get(url);
    }

    protected void switchToNewWindow(Set<String> openedWindowHandles) {
        Set<String> handles = driver.getWindowHandles();
        handles.removeAll(openedWindowHandles);
        assertThat("New window is not opened", handles, hasSize(1));
        driver.switchTo().window(handles.stream().collect(Collectors.toList()).get(0));
    }

    protected void switchToDefaultWindow() {
        getDriver().switchTo().defaultContent();
        driver.switchTo().window(mainWindowHandle);
    }

    protected void checkUrlContainsString(String url) {
        assertThat(driver.getCurrentUrl(), containsString(url));
    }

    protected void wait(int millisec) {
        try {
            Thread.sleep(millisec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Set<String> getWindowHandles() {
        return windowHandles;
    }

    public void setWindowHandles(Set<String> windowHandles) {
        this.windowHandles.clear();
        this.windowHandles.addAll(windowHandles);
    }

}
