package com.gridbuddy.steps;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import io.qameta.allure.Step;

public class SFAccountSteps extends SFPageSteps {

    @Step
    public GBSteps switchToFrame(int frame) {
        getDriver().switchTo().frame(1);
        getDriver().switchTo().frame(0);
        System.out.println(getDriver().findElement(By.xpath("//body")).getAttribute("innerHTML"));
        return new GBSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    public SFAccountSteps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

}
