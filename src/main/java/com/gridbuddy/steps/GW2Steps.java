package com.gridbuddy.steps;

import static com.gridbuddy.matchers.HasTextMatcher.hasText;
import static com.gridbuddy.matchers.IsCheckboxCheckedMatcher.isChecked;
import static com.gridbuddy.matchers.IsCollectionHasSize.hasSize;
import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gridbuddy.blocks.ActionRecordGW2;
import com.gridbuddy.blocks.AddedObjects;
import com.gridbuddy.pages.GW2Page;

import io.qameta.allure.Step;

public class GW2Steps extends GWSteps {

    public GW2Steps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

    @Step
    public GW2Steps selectRelatedObject(String object) {
        onGW2Page().objects().relatedObjects().should(hasSize(greaterThan(0))).stream()
                .filter(j -> j.getText().equals(object)).findFirst().get().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GW2Steps selectAdditionalObject(String object) {
        onGW2Page().objects().additionalObjects().should(hasSize(greaterThan(0))).stream()
                .filter(j -> j.getText().equals(object)).findFirst().get().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GW2Steps selectFields(List<String> fields) {
        wait(3000);
        fields.forEach(i -> {
            onGW2Page().fields().should(isDisplayed()).fields().should(hasSize(greaterThanOrEqualTo(1))).stream()
                    .filter(j -> j.getText().equals(i)).findFirst().get().should(isDisplayed()).addIcon().click();
        });
        return this;
    }

    @Step
    public GW2Steps selectFields(int amount) {
        wait(3000);
        onGW2Page().fields().should(isDisplayed()).fields().should(hasSize(greaterThan(0)));
        for (int i = 0; i < amount; i++) {
            onGW2Page().fields().fields().get(0).addIcon().click();
        }
        return this;
    }

    @Step
    public GW3Steps clickNext() {
        onGW2Page().next().should(isDisplayed()).click();
        switchToNewFrame();
        return new GW3Steps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW1Steps clickBack() {
        onGW2Page().back().should(isDisplayed()).click();
        switchToNewFrame();
        return new GW1Steps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW2Steps clickResetChart() {
        onGW2Page().configureChart().reset().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GW2Steps shouldNotSeeChildObject(String object) {
        assertThat(onGW2Page().objects().additionalObjects().stream().map(WebElement::getText)
                .collect(Collectors.toList()), not(hasItem(object)));
        return this;
    }

    @Step
    public GW2Steps shouldSeeAction(String action) {
        onGW2Page().actions().convert(i -> i.actionName().getText()).should(hasItem(action));
        return this;
    }

    @Step
    public GW2Steps shouldNotSeeAction(String action) {
        onGW2Page().actions().convert(i -> i.actionName().getText()).should(not(hasItem(action)));
        return this;
    }

    @Step
    public GW2Steps selectAction(String action) {
        onGW2Page().actions().filter(i -> i.actionName().getText().equals(action)).should(hasSize(1)).get(0).checkbox()
                .setChecked(true);
        return this;
    }

    public GW2Steps unselectAction(String action) {
        onGW2Page().actions().filter(i -> i.actionName().getText().equals(action)).should(hasSize(1)).get(0).checkbox()
                .setChecked(false);
        return this;
    }

    @Step
    public ManageActionsSteps clickManageActionsLink() {
        onGW2Page().manageActions().should(isDisplayed()).click();
        switchToNewFrame();
        return new ManageActionsSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW2Steps checkActionFields(String action, String object, String contentSource, String displayBehavior,
            String type, String description) {
        ActionRecordGW2 a = onGW2Page().actions().filter(i -> i.actionName().getText().equals(action)).get(0);
        a.actionObject().should(isDisplayed()).should(hasText(object));
        a.contentSource().should(isDisplayed()).should(hasText(contentSource));
        a.displayBehavior().should(isDisplayed()).should(hasText(displayBehavior));
        a.type().should(isDisplayed()).should(hasText(type));
        a.description().should(isDisplayed()).should(hasText(description));
        return this;
    }

    @Step
    public GW2Steps addLineChart(String chartName, String size, String xAxis, String yAxis, String groupBy,
            boolean showLabels) {
        return addLineBarChart("line", chartName, size, xAxis, yAxis, groupBy, showLabels);
    }

    @Step
    public GW2Steps addBarChart(String chartName, String size, String xAxis, String yAxis, String groupBy,
            boolean showLabels) {
        return addLineBarChart("bar", chartName, size, xAxis, yAxis, groupBy, showLabels);
    }

    @Step
    public GW2Steps addDonutChart(String chartName, String size, String xAxis, String yAxis) {
        onGW2Page().configureChart().chartName().should(isDisplayed()).clear();
        onGW2Page().configureChart().chartName().should(isDisplayed()).sendKeys(chartName);
        onGW2Page().configureChart().donutChart().should(isDisplayed()).click();
        onGW2Page().configureChart().chartSize().should(isDisplayed()).select(size);
        onGW2Page().configureChart().xAxis().should(isDisplayed()).select(xAxis);
        onGW2Page().configureChart().yAxis().should(isDisplayed()).select(yAxis);
        return this;
    }

    @Step
    public GW2Steps addTileChart(String chartName, String size, String field, String summaryType, String description,
            int color) {
        onGW2Page().configureChart().chartName().should(isDisplayed()).clear();
        onGW2Page().configureChart().chartName().should(isDisplayed()).sendKeys(chartName);
        onGW2Page().configureChart().donutChart().should(isDisplayed()).click();
        onGW2Page().configureChart().chartSize().should(isDisplayed()).select(size);
        onGW2Page().configureChart().field().should(isDisplayed()).select(field);
        onGW2Page().configureChart().summaryType().should(isDisplayed()).select(summaryType);
        onGW2Page().configureChart().description().should(isDisplayed()).sendKeys(description);
        onGW2Page().configureChart().description().should(isDisplayed()).sendKeys(description);
        return this;
    }

    @Step
    public GW2Steps addConditionFormattingRule(String ruleName, String field, String operator, String value,
            String applyToField, boolean isEntireRow, int backgroundColor, int textColor, String... textFormatting) {
        executor.executeScript("window.scrollTo(0, 500)");
        wait(1000);
        if (!onGW2Page().conditionalFormatting().rules().convert(WebElement::getText).contains("New Rule")) {
            onGW2Page().conditionalFormatting().addRule().click();
        }
        onGW2Page().conditionalFormatting().ruleName().should(isDisplayed()).clear();
        onGW2Page().conditionalFormatting().ruleName().should(isDisplayed()).sendKeys(ruleName);
        onGW2Page().conditionalFormatting().conditionField().selectByValue(field);
        onGW2Page().conditionalFormatting().conditionOperator().select(operator);
        onGW2Page().conditionalFormatting().conditionValue().sendKeys(value);
        // if (onGW2Page().conditionalFormatting().is().size() == 0) {
        //
        // } else {
        // onGW2Page().conditionalFormatting().conditionOperator().select(value);
        // }
        onGW2Page().conditionalFormatting().formattingField().selectByValue(applyToField);
        onGW2Page().conditionalFormatting().entireRow().setChecked(isEntireRow);
        if (backgroundColor >= 0) {
            onGW2Page().conditionalFormatting().backgroundColors().get(backgroundColor).click();
        }
        if (textColor >= 0) {
            onGW2Page().conditionalFormatting().textColors().get(textColor).click();
        }
        Arrays.asList(textFormatting).forEach(i -> {
            if ("bold".equals(i)) {
                onGW2Page().conditionalFormatting().bold().click();
            }
            if ("underline".equals(i)) {
                onGW2Page().conditionalFormatting().underline().click();
            }
            if ("italic".equals(i)) {
                onGW2Page().conditionalFormatting().italic().click();
            }
        });
        return this;
    }

    @Step
    public GW2Steps addConditionFormattingRule(String ruleName, String field, String value, String applyToField,
            boolean isEntireRow, int backgroundColor, int textColor, String... textFormatting) {
        executor.executeScript("window.scrollTo(0, 500)");
        wait(1000);
        if (!onGW2Page().conditionalFormatting().rules().convert(WebElement::getText).contains("New Rule")) {
            onGW2Page().conditionalFormatting().addRule().click();
        }
        onGW2Page().conditionalFormatting().ruleName().should(isDisplayed()).clear();
        onGW2Page().conditionalFormatting().ruleName().should(isDisplayed()).sendKeys(ruleName);
        onGW2Page().conditionalFormatting().conditionField().selectByValue(field);
        onGW2Page().conditionalFormatting().conditionValue().sendKeys(value);
        // if (onGW2Page().conditionalFormatting().is().size() == 0) {
        //
        // } else {
        // onGW2Page().conditionalFormatting().conditionOperator().select(value);
        // }
        onGW2Page().conditionalFormatting().formattingField().selectByValue(applyToField);
        onGW2Page().conditionalFormatting().entireRow().setChecked(isEntireRow);
        if (backgroundColor >= 0) {
            onGW2Page().conditionalFormatting().backgroundColors().get(backgroundColor).click();
        }
        if (textColor >= 0) {
            onGW2Page().conditionalFormatting().textColors().get(textColor).click();
        }
        Arrays.asList(textFormatting).forEach(i -> {
            if ("bold".equals(i)) {
                onGW2Page().conditionalFormatting().bold().click();
            }
            if ("underline".equals(i)) {
                onGW2Page().conditionalFormatting().underline().click();
            }
            if ("italic".equals(i)) {
                onGW2Page().conditionalFormatting().italic().click();
            }
        });
        return this;
    }

    @Step
    public GW2Steps shouldSeeAddedFields(List<String> expectedFields) {
        assertThat(onGW2Page().selectedFields().should(hasSize(greaterThan(0))).stream()
                .map(i -> i.fieldName().getText()).collect(Collectors.toList()), equalTo(expectedFields));
        return this;
    }

    @Step
    public GW2Steps addedFieldsListShouldHaveSize(int expectedSize) {
        onGW2Page().selectedFields().should(hasSize(42));
        return this;
    }

    @Step
    public GW2Steps openObjectSettings(String objectTitle) {
        onGW2Page().selectedObjects().filter(i -> i.headerRow().objectLabel().getText().contains(objectTitle))
                .should(hasSize(1)).get(0).headerRow().objectLevelSettings().click();
        return this;
    }

    @Step
    public GW2Steps checkFlatViewCheckbox() {
        onGW2Page().objectLevelSettings().flatView().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW2Steps enterObjectLabel(String label) {
        onGW2Page().objectLevelSettings().label().should(isDisplayed()).clear();
        onGW2Page().objectLevelSettings().label().sendKeys(label);
        return this;
    }

    @Step
    public GW2Steps enterObjectPluralLabel(String label) {
        onGW2Page().objectLevelSettings().pluralLabel().should(isDisplayed()).clear();
        onGW2Page().objectLevelSettings().pluralLabel().sendKeys(label);
        return this;
    }

    @Step
    public GW2Steps enterObjectColumnLabel(String label) {
        onGW2Page().objectLevelSettings().columnLabel().should(isDisplayed()).clear();
        onGW2Page().objectLevelSettings().columnLabel().sendKeys(label);
        return this;
    }

    @Step
    public GW2Steps checkEditCheckbox() {
        onGW2Page().objectLevelSettings().edit().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW2Steps uncheckEditCheckbox() {
        onGW2Page().objectLevelSettings().edit().should(isDisplayed()).setChecked(false);
        return this;
    }

    @Step
    public GW2Steps uncheckCreateCheckbox() {
        onGW2Page().objectLevelSettings().create().should(isDisplayed()).setChecked(false);
        return this;
    }

    @Step
    public GW2Steps checkCreateCheckbox() {
        onGW2Page().objectLevelSettings().create().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW2Steps uncheckDeleteCheckbox() {
        onGW2Page().objectLevelSettings().delete().should(isDisplayed()).setChecked(false);
        return this;
    }

    @Step
    public GW2Steps checkDeleteCheckbox() {
        onGW2Page().objectLevelSettings().delete().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW2Steps shouldSeeEditCheckboxChecked() {
        onGW2Page().objectLevelSettings().edit().should(isDisplayed()).should(isChecked());
        return this;
    }

    @Step
    public GW2Steps shouldSeeEditableRelatedColumnCheckboxChecked() {
        onGW2Page().objectLevelSettings().editableRelatedColumn().should(isDisplayed()).should(isChecked());
        return this;
    }

    @Step
    public GW2Steps shouldSeeFlatViewCheckboxUnchecked() {
        onGW2Page().objectLevelSettings().flatView().should(isDisplayed()).should(not(isChecked()));
        return this;
    }

    @Step
    public GW2Steps shouldSeeEditableRelatedColumnCheckboxUnchecked() {
        onGW2Page().objectLevelSettings().editableRelatedColumn().should(isDisplayed()).should(not(isChecked()));
        return this;
    }

    @Step
    public GW2Steps checkEditableRelatedColumnCheckbox() {
        onGW2Page().objectLevelSettings().editableRelatedColumn().should(isDisplayed()).setChecked(true);
        return this;
    }
    
    @Step
    public GW2Steps uncheckEditableRelatedColumnCheckbox() {
        onGW2Page().objectLevelSettings().editableRelatedColumn().should(isDisplayed()).setChecked(false);
        return this;
    }

    @Step
    public GW2Steps shouldSeeCreateCheckboxUnchecked() {
        onGW2Page().objectLevelSettings().create().should(isDisplayed()).should(not(isChecked()));
        return this;
    }

    @Step
    public GW2Steps saveObjectLevelSettings() {
        onGW2Page().objectLevelSettings().ok().should(isDisplayed()).click();
        return this;
    }

    private GW2Steps addLineBarChart(String chart, String chartName, String size, String xAxis, String yAxis,
            String groupBy, boolean showLabels) {
        onGW2Page().configureChart().chartName().should(isDisplayed()).clear();
        onGW2Page().configureChart().chartName().should(isDisplayed()).sendKeys(chartName);
        if (chart.equals("line")) {
            onGW2Page().configureChart().lineChart().should(isDisplayed()).click();
        } else {
            onGW2Page().configureChart().barChart().should(isDisplayed()).click();
        }
        onGW2Page().configureChart().chartSize().should(isDisplayed()).select(size);
        onGW2Page().configureChart().xAxis().should(isDisplayed()).select(xAxis);
        onGW2Page().configureChart().yAxis().should(isDisplayed()).select(yAxis);
        onGW2Page().configureChart().groupBy().should(isDisplayed()).select(groupBy);
        onGW2Page().configureChart().showLabels().should(isDisplayed()).setChecked(showLabels);
        onGW2Page().configureChart().save().should(isDisplayed()).click();
        switchToNewFrame();
        return this;
    }

    @Step
    public GW2Steps saveGw2() {
        onGW2Page().save().should(isDisplayed()).click();
        switchToNewFrame();
        return this;
    }

    @Step
    public GW2Steps openFieldProperties(String object, String field) {
        onGW2Page().selectedObjects().filter(i -> i.headerRow().objectLabel().getText().contains(object))
                .should(hasSize(1)).get(0).fields().filter(i -> i.fieldName().getText().equals(field))
                .should(hasSize(1)).get(0).fieldProperties().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GW2Steps selectSummaryType(String summaryType) {
        onGW2Page().fieldProperties().summaryType().should(isDisplayed()).select(summaryType);
        return this;
    }

    @Step
    public GW2Steps shouldSeeSummaryType(String summaryType) {
        onGW2Page().fieldProperties().summaryType().shouldHaveValueSelected(summaryType);
        return this;
    }

    @Step
    public GW2Steps setObjectMapping(String childObject, String parentObjectField, String childObjectField) {
        AddedObjects object = onGW2Page().selectedObjects()
                .filter(i -> i.headerRow().objectLabel().getText().equals(childObject)
                        && i.headerRow().relName().getText().equals("Unrelated"))
                .should(hasSize(1)).get(0);
        object.parentSelect().select(parentObjectField);
        object.childSelect().select(childObjectField);
        return this;
    }

    @Step
    public GW2Steps selectReadOnlyRadioButton() {
        onGW2Page().fieldProperties().readOnlyRadiobutton().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GW2Steps checkReadOnlyCheckbox() {
        onGW2Page().fieldProperties().readOnlyCheckbox().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW2Steps uncheckReadOnlyCheckbox() {
        onGW2Page().fieldProperties().readOnlyCheckbox().should(isDisplayed()).setChecked(false);
        return this;
    }

    @Step
    public GW2Steps saveFieldProperties() {
        onGW2Page().fieldProperties().ok().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GW2Steps closeFieldPropertiesPopup() {
        onGW2Page().fieldProperties().cancel().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GW2Steps shouldSeeAlert(String text) {
        Alert alert = getDriver().switchTo().alert();
        assertThat(alert.getText(), equalTo(text));
        alert.accept();
        return this;
    }

    @Step
    public GW2Steps removeConditionalFormattingRule(String ruleName) {
        onGW2Page().conditionalFormatting().rules().filter(i -> i.getText().equals(ruleName)).should(hasSize(1)).get(0)
                .click();
        onGW2Page().conditionalFormatting().deleteRule().should(isDisplayed()).click();
        getDriver().switchTo().alert().accept();
        return this;
    }

    @Step
    public GW2Steps removeChildFields(String childObject, List<String> fields) {
        AddedObjects object = onGW2Page().selectedObjects()
                .filter(i -> i.headerRow().objectLabel().getText().equals(childObject)
                        && !i.headerRow().relName().getText().equals("Parent"))
                .should(hasSize(1)).get(0);
        for (String field : fields) {
            object.fields().filter(i -> i.fieldName().getText().equals(field)).get(0).removeIcon().should(isDisplayed())
                    .click();
        }
        return this;
    }

    public GW2Steps openChildDataCard(String childObject) {
        onGW2Page().selectedObjects()
                .filter(i -> i.headerRow().objectLabel().getText().equals(childObject)
                        && !i.headerRow().relName().getText().equals("Parent"))
                .should(hasSize(1)).get(0).headerRow().dataCardButton().should(isDisplayed()).click();
        return this;
    }

    public GW2Steps openParentDataCard() {
        onGW2Page().selectedObjects().filter(i -> i.headerRow().relName().getText().equals("Parent")).should(hasSize(1))
                .get(0).headerRow().dataCardButton().should(isDisplayed()).click();
        return this;
    }

    public GW2Steps moveParentObjectFieldToDataCard(String field) {
        WebElement f = onGW2Page().selectedObjects().filter(i -> i.headerRow().relName().getText().equals("Parent"))
                .should(hasSize(1)).get(0).fields().filter(i -> i.fieldName().getText().equals(field)).get(0);
        WebElement datacard = onGW2Page().selectedObjects()
                .filter(i -> i.headerRow().relName().getText().equals("Parent")).should(hasSize(1)).get(0).dataCard();
        actions.dragAndDrop(f, datacard).perform();
        return this;
    }
    
    public GW2Steps moveParentObjectFieldToDataCard(String... fields) {
    	for (String field : fields) {
    		WebElement f = onGW2Page().selectedObjects().filter(i -> i.headerRow().relName().getText().equals("Parent"))
                    .should(hasSize(1)).get(0).fields().filter(i -> i.fieldName().getText().equals(field)).get(0);
            WebElement datacard = onGW2Page().selectedObjects()
                    .filter(i -> i.headerRow().relName().getText().equals("Parent")).should(hasSize(1)).get(0).dataCard();
            actions.dragAndDrop(f, datacard).perform();
		}
        return this;
    }

    public GW2Steps moveChildObjectFieldToDataCard(String childObject, String field) {
        WebElement f = onGW2Page().selectedObjects()
                .filter(i -> i.headerRow().objectLabel().getText().equals(childObject)
                        && !i.headerRow().relName().getText().equals("Parent"))
                .should(hasSize(1)).get(0).fields().filter(i -> i.fieldName().should(isDisplayed()).getText().equals(field)).get(0);
        WebElement datacard = onGW2Page().selectedObjects()
                .filter(i -> i.headerRow().objectLabel().getText().equals(childObject)
                        && !i.headerRow().relName().getText().equals("Parent"))
                .should(hasSize(1)).get(0).dataCard();
        actions.dragAndDrop(f, datacard).perform();
        return this;
    }

    @Step
    public GW2Steps removeFields(List<String> fields) {
        AddedObjects object = onGW2Page().selectedObjects()
                .filter(i -> i.headerRow().relName().getText().equals("Parent")).should(hasSize(1)).get(0);
        for (String field : fields) {
            object.fields().filter(i -> i.fieldName().getText().equals(field)).get(0).removeIcon().should(isDisplayed())
                    .click();
        }
        return this;
    }

    private GW2Page onGW2Page() {
        return on(GW2Page.class);
    }

}
