package com.gridbuddy.steps;

import static com.gridbuddy.matchers.HasTextMatcher.hasText;
import static com.gridbuddy.matchers.IsCollectionHasSize.hasSize;
import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

import org.openqa.selenium.WebDriver;

import com.gridbuddy.pages.ManageCustomCodePage;

import io.qameta.allure.Step;

public class ManageCustomCodeSteps extends SFPageSteps {

    public ManageCustomCodeSteps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

    @Step
    public ManageCustomCodeSteps shouldBeOnManageCustomCodePage() {
        onManageCustomCodePage().pageTitle().should(isDisplayed()).should(hasText("Manage Custom Code Components"));
        return this;
    }

    @Step
    public GWLandingSteps goBack() {
        getDriver().navigate().back();
        switchToNewFrame();
        return new GWLandingSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public ManageCustomCodeSteps createCustomCode(String name, String type, String scope, String code) {
        onManageCustomCodePage().topRowButton("New").click();
        onManageCustomCodePage().componentName().should(isDisplayed()).clear();
        onManageCustomCodePage().componentName().sendKeys(name);
        onManageCustomCodePage().type().should(isDisplayed()).select(type);
        onManageCustomCodePage().scope().should(isDisplayed()).select(scope);
        onManageCustomCodePage().codeBody().clear();
        onManageCustomCodePage().codeBody().sendKeys(code);
        onManageCustomCodePage().topRowButton("Save").should(isDisplayed()).click();
        switchToNewFrame();
        onManageCustomCodePage().codes().convert(i -> i.cell("Component Name").getText()).should(hasItem(name));
        return this;
    }

    @Step
    public ManageCustomCodeSteps deleteCustomCode(String name) {
        onManageCustomCodePage().codes().should(hasSize(greaterThan(0))).filter(i -> i.cell("Component Name").getText().equals(name))
        .forEach(i -> i.checkbox().setChecked(true));
        onManageCustomCodePage().topRowButton("Delete").should(isDisplayed()).click();
        try {
            getDriver().switchTo().alert().accept();
        } catch (Exception e) {
        }
        switchToNewFrame();
        onManageCustomCodePage().codes().convert(i -> i.cell("Component Name").getText()).should(not(hasItem(name)));
        return this;
    }

    private ManageCustomCodePage onManageCustomCodePage() {
        return on(ManageCustomCodePage.class);
    }

}
