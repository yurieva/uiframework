package com.gridbuddy.steps;

import static com.gridbuddy.props.Properties.props;

import org.openqa.selenium.WebDriver;

import com.gridbuddy.pages.LoginPage;

import io.qameta.allure.Step;

public class LoginSteps extends WebDriverSteps {

	public LoginSteps(WebDriver driver, String windowHandle, String currentFrameId) {
		super(driver, windowHandle, currentFrameId);
	}

	@Step
	public SFPageSteps login() {
		openPage(props.serviceUrl(), this);
		onLoginPage().username().sendKeys(props.user());
		onLoginPage().password().sendKeys(props.password());
		onLoginPage().login().submit();
		return new SFPageSteps(getDriver(), mainWindowHandle, currentFrameId);
	}

	@Step
	public SFPageSteps loginToLightningOrg() {
		openPage(props.serviceUrl(), this);
		onLoginPage().username().sendKeys("qa.lightningfieldservice@appbuddy.com");
		onLoginPage().password().sendKeys("BigSales2018");
		onLoginPage().login().submit();
		return new SFPageSteps(getDriver(), mainWindowHandle, currentFrameId);
	}

	private LoginPage onLoginPage() {
		return on(LoginPage.class);
	}
}
