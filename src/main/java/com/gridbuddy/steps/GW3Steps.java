package com.gridbuddy.steps;

import static com.gridbuddy.matchers.HasAttributeMatcher.hasAttribute;
import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;
import static com.gridbuddy.matchers.IsElementEnabledMatcher.isEnabled;
import static io.qameta.htmlelements.matcher.HasTextMatcher.hasText;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.gridbuddy.blocks.Select;
import com.gridbuddy.matchers.HasAttributeMatcher;
import com.gridbuddy.pages.GW3Page;

import io.qameta.allure.Step;

public class GW3Steps extends GWSteps {

    public GW3Steps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

    @Step
    public GW3Steps shouldSeeAlert(String text) {
        Alert alert = getDriver().switchTo().alert();
        assertThat(alert.getText(), equalTo(text));
        alert.accept();
        return this;
    }

    @Step
    public GBSteps switchToDefaultContent() {
        getDriver().switchTo().defaultContent();
        currentFrameId = "";
        return new GBSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW3Steps setRelatedFilter(String filterValue) {
        onGW3Page().filterByRelatedObject().select(filterValue);
        return this;
    }

    @Step
    public GW3Steps shouldSeeRelatedFilterValue(String filterValue) {
        onGW3Page().filterByRelatedObject().shouldHaveValueSelected(filterValue);
        return this;
    }

    @Step
    public GW3Steps shouldNotSeeFilterByRelatedObject() {
        onGW3Page().filterByRelatedObject().should(not(isDisplayed()));
        return this;
    }

    @Step
    public GW3Steps selectSortByField(String object, int row, String field, String order) {
        onGW3Page().objectFilter(object).should(isDisplayed()).sortByFieldRows().get(row).field().select(field);
        onGW3Page().objectFilter(object).should(isDisplayed()).sortByFieldRows().get(row).direction().select(order);
        return this;
    }

    @Step
    public GW3Steps selectGroupByCondition(String object, int row, String field, String sortBy, String order) {
        onGW3Page().objectFilter(object).should(isDisplayed()).groupByFieldRows().get(row).groupedField().select(field);
        onGW3Page().objectFilter(object).should(isDisplayed()).groupByFieldRows().get(row).sortBy().select(sortBy);
        onGW3Page().objectFilter(object).should(isDisplayed()).groupByFieldRows().get(row).sortDirection()
                .select(order);
        return this;
    }

    @Step
    public GW3Steps selectForecastViewCondition(String object, int row, String field, String order) {
        onGW3Page().objectFilter(object).should(isDisplayed()).forecastVeiwRows().get(row).columnGrouping()
                .should(isDisplayed()).select(field);
        Select direction = onGW3Page().objectFilter(object).should(isDisplayed()).forecastVeiwRows().get(row)
                .sortDirection().should(isDisplayed());
        direction.shouldHaveValueSelected("Ascending");
        direction.select(order);
        return this;
    }

    @Step
    public GW3Steps selectForecastViewCondition(String object, int row, String field, String format, String order) {
        onGW3Page().objectFilter(object).should(isDisplayed()).forecastVeiwRows().get(row).columnGrouping()
                .should(isDisplayed()).select(field);
        onGW3Page().objectFilter(object).should(isDisplayed()).forecastVeiwRows().get(row).format()
                .should(isDisplayed()).select(format);
        Select direction = onGW3Page().objectFilter(object).should(isDisplayed()).forecastVeiwRows().get(row)
                .sortDirection().should(isDisplayed());
        direction.shouldHaveValueSelected("Ascending");
        direction.select(order);
        return this;
    }

    @Step
    public GW3Steps clearForecastViewCondition(String object, int row) {
        onGW3Page().objectFilter(object).should(isDisplayed()).forecastVeiwRows().get(row).clearFilter()
                .should(isDisplayed()).click();
        return this;
    }

    @Step
    public GW3Steps shouldSeeForecastFiewFormatFieldDisabled(String object, int row) {
        onGW3Page().objectFilter(object).forecastVeiwRows().get(row).format().should(isDisplayed())
                .should(not(isEnabled()));
        return this;
    }

    @Step
    public GW3Steps setRecordsLimit(String object, int count) {
        onGW3Page().objectFilter(object).maxRecordsLimit().sendKeys(String.valueOf(count));
        return this;
    }

    @Step
    public GW3Steps setFilterCondition(String object, int row, String field, String operator, String value) {
        int rowSize = onGW3Page().objectFilter(object).should(isDisplayed()).filterByFieldRows().size();
        while (rowSize <= row) {
            onGW3Page().objectFilter(object).addFilterCondition().should(isDisplayed()).click();
            rowSize = onGW3Page().objectFilter(object).filterByFieldRows().size();
        }
        onGW3Page().objectFilter(object).filterByFieldRows().get(row).field().should(isDisplayed()).select(field);
        onGW3Page().objectFilter(object).filterByFieldRows().get(row).operator().should(isDisplayed()).select(operator);
        try {
            onGW3Page().objectFilter(object).filterByFieldRows().get(row).value().should(isDisplayed()).clear();
        } catch (Exception e) {
        }
        onGW3Page().objectFilter(object).filterByFieldRows().get(row).value().should(isDisplayed()).sendKeys(value,
                Keys.TAB);
        return this;
    }

    @Step
    public GW3Steps addFilterRow(String object) {
        onGW3Page().objectFilter(object).addFilterCondition().click();
        return this;
    }

    @Step
    public GW3Steps createUserFilter() {
        onGW3Page().savedFilter().should(isDisplayed()).select("Create New...");
        switchToNewFrame();
        return this;
    }

    @Step
    public GW3Steps enterFilterName(String filterName) {
        onGW3Page().filterName().should(isDisplayed()).clear();
        onGW3Page().filterName().should(isDisplayed()).sendKeys(filterName);
        return this;
    }

    @Step
    public GW3Steps setAdvancedFilterCondition(String object, String condition) {
        onGW3Page().objectFilter(object).advancedFilterConditions().inputField().should(isDisplayed())
                .sendKeys(condition);
        return this;
    }

    @Step
    public GW2Steps clickBack() {
        onGW3Page().back().should(isDisplayed()).click();
        switchToNewFrame();
        return new GW2Steps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW3Steps shouldSeeFilterSelected(String filter) {
        onGW3Page().savedFilter().shouldHaveValueSelected(filter);
        return this;
    }

    @Step
    public GW3Steps saveFilter() {
        onGW3Page().save().should(isDisplayed()).click();
        switchToNewFrame();
        return this;
    }
    
    
    @Step
    public GW3Steps shouldSeeErrorMessage(String message) {
    	onGW3Page().errors().get(0).should(isDisplayed()).should(hasText(containsString(message)));
        return this;
    }

    @Step
    public GW3Steps selectFilter(String filter) {
        onGW3Page().savedFilter().should(isDisplayed()).select(filter);
        wait(3000);
        return this;
    }

    @Step
    public GW3Steps clearFilter(String object, int row) {
        onGW3Page().objectFilter(object).filterByFieldRows().get(row).clearFilter().should(isDisplayed()).click();
        onGW3Page().objectFilter(object).filterByFieldRows().get(row).field().should(isDisplayed())
                .shouldHaveValueSelected("--No Field Selected--");
        onGW3Page().objectFilter(object).filterByFieldRows().get(row).operator().should(isDisplayed())
                .shouldHaveValueSelected("--No Operator Selected--");
        onGW3Page().objectFilter(object).filterByFieldRows().get(row).value()
                .should(HasAttributeMatcher.hasAttribute("value", ""));
        return this;
    }

    @Step
    public GW3Steps clearSorting(String object, int row) {
        onGW3Page().objectFilter(object).sortByFieldRows().get(row).clearSorting().should(isDisplayed()).click();
        onGW3Page().objectFilter(object).sortByFieldRows().get(row).field().should(isDisplayed())
                .shouldHaveValueSelected("--No Field Selected--");
        onGW3Page().objectFilter(object).sortByFieldRows().get(row).direction().should(isDisplayed())
                .shouldHaveValueSelected("--No Sort Direction Selected--");
        return this;
    }

    @Step
    public GW3Steps shouldNotSeeErrorMessage() {
        onGW3Page().errors().should(hasSize(1)).get(0).should(hasText(""));
        return this;
    }

    @Step
    public GW3Steps setSortingCondition(String object, int row, String field, String direction) {
        int rowSize = onGW3Page().objectFilter(object).should(isDisplayed()).sortByFieldRows().size();
        while (rowSize <= row) {
            onGW3Page().objectFilter(object).addSortCondition().should(isDisplayed()).click();
            rowSize = onGW3Page().objectFilter(object).sortByFieldRows().size();
        }
        onGW3Page().objectFilter(object).sortByFieldRows().get(row).field().should(isDisplayed()).select(field);
        onGW3Page().objectFilter(object).sortByFieldRows().get(row).direction().should(isDisplayed()).select(direction);
        return this;
    }

    @Step
    public GW3Steps shouldSeeSortingCondition(String object, int row, String field, String direction) {
        onGW3Page().objectFilter(object).sortByFieldRows().get(row).field().should(isDisplayed())
                .shouldHaveValueSelected(field);
        onGW3Page().objectFilter(object).sortByFieldRows().get(row).direction().should(isDisplayed())
                .shouldHaveValueSelected(direction);
        return this;
    }

    @Step
    public GW3Steps shouldSeeFilterCondition(String object, int row, String field, String operator, String value) {
        onGW3Page().objectFilter(object).filterByFieldRows().get(row).field().should(isDisplayed())
                .shouldHaveValueSelected(field);
        onGW3Page().objectFilter(object).filterByFieldRows().get(row).operator().should(isDisplayed())
                .shouldHaveValueSelected(operator);
        onGW3Page().objectFilter(object).filterByFieldRows().get(row).value().should(hasAttribute("value", value));
        return this;
    }

    @Step
    public GW3Steps clickRefresh() {
        onGW3Page().refresh().should(isDisplayed()).click();
        switchToNewFrame();
        return this;
    }

    @Step
    public GW3Steps setGroupByCondition(String object, int row, String field, String sortBy, String order) {
        int rowSize = onGW3Page().objectFilter(object).should(isDisplayed()).groupByFieldRows().size();
        while (rowSize <= row) {
            onGW3Page().objectFilter(object).addGroupingCondition().should(isDisplayed()).click();
            rowSize = onGW3Page().objectFilter(object).groupByFieldRows().size();
        }

        onGW3Page().objectFilter(object).should(isDisplayed()).groupByFieldRows().get(row).groupedField().select(field);
        onGW3Page().objectFilter(object).should(isDisplayed()).groupByFieldRows().get(row).sortBy().select(sortBy);
        onGW3Page().objectFilter(object).should(isDisplayed()).groupByFieldRows().get(row).sortDirection()
                .select(order);
        return this;
    }

    @Step
    public GW3Steps setGroupByCondition(String object, int row, String field) {
        int rowSize = onGW3Page().objectFilter(object).should(isDisplayed()).groupByFieldRows().size();
        while (rowSize <= row) {
            onGW3Page().objectFilter(object).addGroupingCondition().should(isDisplayed()).click();
            rowSize = onGW3Page().objectFilter(object).groupByFieldRows().size();
        }

        onGW3Page().objectFilter(object).should(isDisplayed()).groupByFieldRows().get(row).groupedField().select(field);
        return this;
    }

    private GW3Page onGW3Page() {
        return on(GW3Page.class);
    }
}
