package com.gridbuddy.steps;

import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;
import static io.qameta.htmlelements.matcher.HasTextMatcher.hasText;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gridbuddy.pages.GWPage;

import io.qameta.allure.Step;

public class GWSteps extends SFPageSteps {

    public GWSteps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

    @Step
    public GWSteps saveGrid() {
        onGWPage().save().should(isDisplayed()).click();
        switchToNewFrame();
        return this;
    }

    @Step
    public GBSteps launchGrid() {
    	switchToNewFrame();
        onGWPage().launchGrid().should(isDisplayed()).click();
        switchToNewFrame();
        return new GBSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GWSteps shouldSeeErrorMessage(String message) {
        onGWPage().errors().get(0).should(isDisplayed()).should(hasText(containsString(message)));
        return this;
    }

    @Step
    public GWSteps shouldSeeErrorMessages(String... messages) {
        assertThat(onGWPage().errors().convert(WebElement::getText), containsInAnyOrder(messages));
        return this;
    }

    @Step
    public GWSteps shouldNotSeeErrorMessage() {
        onGWPage().errors().should(hasSize(1)).get(0).should(hasText(""));
        return this;
    }

    public GBSteps switchToDefaultContent() {
        getDriver().switchTo().defaultContent();
        return new GBSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW1Steps switchToEditSettingsPopup() {
        switchToNewFrame();
        getDriver().switchTo().frame("actionsFrame");
        return new GW1Steps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW3Steps switchToEditAdminFiltersPopup() {
        switchToNewFrame();
        getDriver().switchTo().frame("actionsFrame");
        return new GW3Steps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW2Steps switchToEditFieldsPopup() {
        switchToNewFrame();
        getDriver().switchTo().frame("actionsFrame");
        return new GW2Steps(getDriver(), mainWindowHandle, currentFrameId);
    }

    private GWPage onGWPage() {
        return on(GWPage.class);
    }

}
