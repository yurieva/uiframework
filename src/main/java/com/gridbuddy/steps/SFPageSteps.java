package com.gridbuddy.steps;

import static com.gridbuddy.matchers.DoesElementExistMatcher.exists;
import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;
import static com.gridbuddy.matchers.IsNewFrameDisplayedMatcher.isNewFrameDisplayed;

import org.openqa.selenium.WebDriver;

import com.gridbuddy.pages.LoginPage;
import com.gridbuddy.pages.SFBasePage;

import io.qameta.allure.Step;
import io.qameta.htmlelements.element.HtmlElement;

public class SFPageSteps extends WebDriverSteps {

    public SFPageSteps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

    @Step
    public GWLandingSteps openGridWizard() {
        switchToNewFrame();
        getDriver().switchTo().defaultContent();
        onSFPage().menuItem("Grid Wizard").should(isDisplayed()).click();
        switchToNewFrame();
        return new GWLandingSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GridsSteps openGrids() {
        switchToNewFrame();
        getDriver().switchTo().defaultContent();
        onSFPage().menuItem("Grids").should(isDisplayed()).click();
        switchToNewFrame();
        return new GridsSteps(getDriver(), mainWindowHandle, currentFrameId);
    }
    

    @Step
    public GBSteps openTabbedPage(String tabbedPage) {
        switchToNewFrame();
        getDriver().switchTo().defaultContent();
        onSFPage().menuItem(tabbedPage).should(isDisplayed()).click();
        return new GBSteps(getDriver(), mainWindowHandle, currentFrameId);
    }
    
    @Step
    public LoginSteps logout() {
        switchToNewFrame();
        getDriver().switchTo().defaultContent();
        onSFPage().profileButton().should(isDisplayed()).click();
        onSFPage().logOut().should(isDisplayed()).click();
        on(LoginPage.class).username().should(isDisplayed());
        wait(3000);
        return new LoginSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    protected void switchToNewFrame() {
        getDriver().switchTo().defaultContent();
        wait(3000);
        onSFPage().iframe().should(exists()).should(isNewFrameDisplayed(currentFrameId));
        boolean isSwiched = false;
        for (HtmlElement iframe : onSFPage().iframes()) {
            if (!iframe.getAttribute("id").equals(currentFrameId)) {
                currentFrameId = iframe.getAttribute("id");
                getDriver().switchTo().frame(currentFrameId);
                isSwiched = true;
                System.out.println("to new");
                break;
            }
        }
        if (!isSwiched) {
            getDriver().switchTo().frame(0);
            System.out.println("to old");
        }
        try {
            getDriver().switchTo().frame("gridFrame");
            System.out.println("to grid");
        } catch (Exception e) {
        }
    }
    
    protected void switchToNewFrame(int gridFrame) {
        getDriver().switchTo().defaultContent();
        onSFPage().iframe().should(exists()).should(isNewFrameDisplayed(currentFrameId));
        boolean isSwiched = false;
        for (HtmlElement iframe : onSFPage().iframes()) {
            if (!iframe.getAttribute("id").equals(currentFrameId)) {
                currentFrameId = iframe.getAttribute("id");
                getDriver().switchTo().frame(currentFrameId);
                isSwiched = true;
                System.out.println("to new");
                break;
            }
        }
        if (!isSwiched) {
            getDriver().switchTo().frame(0);
            System.out.println("to old");
        }
        try {
            getDriver().switchTo().frame(gridFrame);
            System.out.println("to grid");
        } catch (Exception e) {
        }
    }

    private SFBasePage onSFPage() {
        return on(SFBasePage.class);
    }

}
