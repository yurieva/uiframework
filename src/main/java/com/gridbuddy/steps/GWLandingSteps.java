package com.gridbuddy.steps;

import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;

import com.gridbuddy.pages.GW1Page;
import com.gridbuddy.pages.GWLandingPage;

import io.qameta.allure.Step;

public class GWLandingSteps extends SFPageSteps {

    public GWLandingSteps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

    @Step
    public GW1Steps createNew() {
        onGWLandingPage().manageGrids().should(isDisplayed());
        wait(1000);
        onGWLandingPage().manageGrids().select("Create New...");
        switchToNewFrame();
        return new GW1Steps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GWLandingSteps deleteGrid(String gridName) {
        onGWLandingPage().manageGrids().should(isDisplayed());
        if (onGWLandingPage().manageGrids().hasOption(gridName)) {
            onGWLandingPage().manageGrids().select(gridName);
            switchToNewFrame();
            onGW1Page().delete().should(isDisplayed()).click();
            confirmAlert();
            switchToNewFrame();
            onGWLandingPage().manageActions().should(isDisplayed());
//            onGWLandingPage().manageGrids().shouldNotHaveOption(gridName);
        }
        return this;
    }

    @Step
    public GBSteps openGrid(String gridName) {
        onGWLandingPage().manageGrids().should(isDisplayed()).select(gridName);
        setWindowHandles(getDriver().getWindowHandles());
        onGW1Page().launchGrid().click();
        switchToNewWindow(getWindowHandles());
        return new GBSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW1Steps createGrid(String gridName, String parentObject) {
        createNew();
        onGW1Page().addEditCustomCode().should(isDisplayed());
        onGW1Page().name().should(isDisplayed()).sendKeys(gridName);
        onGW1Page().parentObject().selectByValue(parentObject);
        onGW1Page().save().should(isDisplayed()).click();
        switchToNewFrame();
        return new GW1Steps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW1Steps selectGrid(String gridName) {
        onGWLandingPage().manageGrids().select(gridName);
        switchToNewFrame();
        return new GW1Steps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public ManageActionsSteps openManageActions() {
        onGWLandingPage().manageActions().should(isDisplayed()).click();
        switchToNewFrame();
        return new ManageActionsSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public ManageFoldersSteps openManageFolders() {
        onGWLandingPage().manageFolders().should(isDisplayed()).click();
        switchToNewFrame();
        return new ManageFoldersSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public ManageTabbedPagesSteps openManageTabbedPages() {
        onGWLandingPage().manageTabbedPages().should(isDisplayed()).click();
        switchToNewFrame();
        return new ManageTabbedPagesSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public ManageCustomCodeSteps openManageCustomCode() {
        onGWLandingPage().manageCustomCode().should(isDisplayed()).click();
        switchToNewFrame();
        return new ManageCustomCodeSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GWLandingSteps shouldBeOnGwLandingPage() {
        onGWLandingPage().manageActions().should(isDisplayed());
        return this;
    }

    @Step
    public GWLandingSteps shouldSeeAlert(String text) {
        Alert alert = getDriver().switchTo().alert();
        assertThat(alert.getText(), equalTo(text));
        alert.accept();
        return this;
    }

    protected GWLandingPage onGWLandingPage() {
        return on(GWLandingPage.class);
    }

    protected GW1Page onGW1Page() {
        return on(GW1Page.class);
    }

}
