package com.gridbuddy.steps;

import static com.gridbuddy.matchers.HasAttributeMatcher.hasAttribute;
import static com.gridbuddy.matchers.HasCssValueMatcher.hasCssValue;
import static com.gridbuddy.matchers.IsCheckboxCheckedMatcher.isChecked;
import static com.gridbuddy.matchers.IsCollectionHasSize.hasSize;
import static com.gridbuddy.matchers.IsCollectionSorted.isSortedAscending;
import static com.gridbuddy.matchers.IsCollectionSorted.isSortedDescending;
import static com.gridbuddy.matchers.IsCollectionSortedIgnoringCase.isSortedAscendingIgnoringCase;
import static com.gridbuddy.matchers.IsCollectionSortedIgnoringCase.isSortedDescendingIgnoringCase;
import static com.gridbuddy.matchers.IsDateCollectionSorted.isDateCollectionSortedAscending;
import static com.gridbuddy.matchers.IsDateCollectionSorted.isDateCollectionSortedDescending;
import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;
import static com.gridbuddy.matchers.IsElementNotDisplayedMatcher.isNotDisplayed;
import static com.gridbuddy.matchers.StringContains.containsStringIgnoringCase;
import static com.gridbuddy.matchers.StringPatternMatcher.matchesPattern;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static io.qameta.htmlelements.matcher.HasTextMatcher.hasText;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.not;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matcher;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gridbuddy.blocks.ForecastViewCell;
import com.gridbuddy.blocks.ForecastViewRecord;
import com.gridbuddy.blocks.GBRecord;
import com.gridbuddy.matchers.HasTextMatcher;
import com.gridbuddy.pages.GBPage;

import io.qameta.allure.Step;
import io.qameta.htmlelements.element.HtmlElement;

public class GBSteps extends SFPageSteps {

	public GBSteps(WebDriver driver, String windowHandle, String currentFrameId) {
		super(driver, windowHandle, currentFrameId);
	}

	@Step
	public GBSteps getGbPage(String page) {
		getDriver().get(page);
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps shouldNotSeeChartIcon() {
		onGBPage().chartIcon().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeRelatedObject(String object) {
		assertThat(onGBPage().table().records().get(0).relatedRecord().title().getAttribute("innerHTML"),
				containsString(object));
		return this;
	}

	@Step
	public GBSteps shouldSeeRelatedObjectOnRelatedObjectPopupTitle(String object) {
		assertThat(onGBPage().popupTitle().getAttribute("innerHTML"), containsString(object));
		return this;
	}

	@Step
	public GBSteps shouldSeeParentObjectLabel(String expectedText) {
		onGBPage().parentObject().should(isDisplayed()).should(hasText(expectedText));
		return this;
	}

	@Step
	public GBSteps shouldSeeObjectPluralLabelInPaginationSection(String expectedText) {
		onGBPage().perPageLabel().should(isDisplayed()).should(hasText(containsString(expectedText)));
		onGBPage().totalLabel().should(isDisplayed()).should(hasText(containsString(expectedText)));
		return this;
	}

	@Step
	public GBSteps shouldSeeColumns(List<String> expectedColumns) {
		assertThat(onGBPage().table().topHeaders().stream().map(i -> i.name().getText()).collect(Collectors.toList()),
				equalTo(expectedColumns));
		return this;
	}

	@Step
	public GBSteps shouldSeeChildObjectColumns(int child, List<String> expectedColumns) {
		assertThat(
				onGBPage().table().records().should(hasSize(greaterThan(0))).get(FIRST_ROW).relatedRecords().get(0)
						.headers().stream().map(i -> i.name().getText()).collect(Collectors.toList()),
				equalTo(expectedColumns));
		return this;
	}

	@Step
	public GBSteps shouldSeeColumn(String expectedColumn) {
		onGBPage().table().records().get(FIRST_ROW).topHeaders().convert(i -> i.name().getText())
				.should(hasItem(expectedColumn));
		return this;
	}

	@Step
	public GBSteps columnsNumberShouldBe(int number) {
		onGBPage().table().records().get(FIRST_ROW).topHeaders().should(hasSize(number));
		return this;
	}

	@Step
	public GBSteps shouldSeeGBTable() {
		onGBPage().table().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeNumberOfRecords(int recordsNumber) {
		assertThat(onGBPage().table().records(), hasSize(lessThanOrEqualTo(recordsNumber)));
		return this;
	}

	@Step
	public GBSteps shouldSeeDataRows() {
		onGBPage().table().records().should(everyItem(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps shouldNotSeeDataRows() {
		onGBPage().table().records().should(everyItem(isNotDisplayed()));
		return this;
	}

	@Step
	public GBSteps parentRecordTextFieldShouldMetCriteria(String field, Matcher<String> matcher) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).convert(i -> i.field(field))
				.should(everyItem(hasText(matcher)));
		return this;
	}

	@Step
	public GBSteps parentRecordInputFieldShouldMetCriteria(String field, Matcher<String> matcher) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).convert(i -> i.inputField(field))
				.should(everyItem(hasAttribute("value", matcher)));
		return this;
	}

	@Step
	public GBSteps parentRecordInputFieldShouldMetAnyCriteria(String fieldOne, Matcher<String> matcher, String fieldTwo,
			Matcher<String> matcherTwo) {
		List<GBRecord> list = onGBPage().table().records().should(hasSize(greaterThan(0)))
				.filter(i -> !matcher.matches(i.field(fieldOne).getAttribute("value")))
				.filter(i -> !matcherTwo.matches(i.field(fieldTwo).getAttribute("value"))).stream()
				.collect(Collectors.toList());
		assertThat(
				"Parent records do not match advanced filter conditions: " + String.join(",",
						list.stream().map(i -> i.field(fieldOne).getAttribute("value")).collect(Collectors.toList())),
				list, hasSize(0));
		return this;
	}

	@Step
	public GBSteps parentRecordInputFieldShouldMetCriteria(String fieldOne, Matcher<String> matcher, String fieldTwo,
			Matcher<String> matcherTwo, String fieldThree, Matcher<String> matcherThree) {
		List<GBRecord> list = onGBPage().table().records().should(hasSize(greaterThan(0)))
				.filter(i -> !matcher.matches(i.field(fieldOne).getAttribute("value")))
				.filter(i -> !matcherTwo.matches(i.field(fieldTwo).getAttribute("value"))).stream()
				.collect(Collectors.toList());
		assertThat(
				"Parent records do not match advanced filter conditions: " + String.join(",",
						list.stream().map(i -> i.field(fieldOne).getAttribute("value")).collect(Collectors.toList())),
				list.stream().map(i -> i.field(fieldThree).getAttribute("value")).collect(Collectors.toList()),
				everyItem(not(matcherThree)));
		return this;
	}

	@Step
	public GBSteps parentRecordInputFieldShouldMetCriteria(int row, String field, Matcher<String> matcher) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).get(row).inputField(field)
				.should(hasAttribute("value", matcher));
		return this;
	}

	@Step
	public GBSteps parentRecordDataCardInputFieldShouldMetCriteria(int row, String field, Matcher<String> matcher) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).get(row).dataCardRow().inputField(field)
				.should(hasAttribute("value", matcher));
		return this;
	}

	@Step
	public GBSteps parentRecordDataCardSelectFieldShouldHaveItemSelected(int row, String field, String item) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).get(row).dataCardRow().selectField(field)
				.shouldHaveValueSelected(item);
		return this;
	}

	@Step
	public GBSteps childRecordDataCardInputFieldShouldMetCriteria(int row, int childRow, String field,
			Matcher<String> matcher) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).get(row).relatedRecord().records().get(childRow)
				.dataCardRow().inputField(field).should(hasAttribute("value", matcher));
		return this;
	}

	@Step
	public GBSteps parentRecordTextFieldShouldMetCriteria(int row, String field, Matcher<String> matcher) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).get(row).field(field).should(hasText(matcher));
		return this;
	}

	@Step
	public GBSteps editableRelatedColumnFieldShouldMetCriteria(int row, String field, Matcher<String> matcher) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).get(row).cell(field).should(hasText(matcher));
		return this;
	}

	@Step
	public GBSteps relatedRecordInputFieldShouldMetCriteria(String field, Matcher<String> matcher) {
		assertThat(onGBPage().table().records().stream().flatMap(i -> i.relatedRecord().records().stream()).map(i -> {
			if (i.field(field).getAttribute("value") == null) {
				return i.inputField(field);
			} else {
				return i.field(field);
			}
		}).collect(Collectors.toList()), everyItem(hasAttribute("value", matcher)));
		return this;
	}

	@Step
	public GBSteps relatedRecordInputFieldShouldMetCriteria(int parent, int related, String field,
			Matcher<String> matcher) {
		onGBPage().table().records().get(parent).relatedRecord().records().get(related).inputField(field)
				.should(hasAttribute("value", matcher));
		return this;
	}

	@Step
	public GBSteps relatedRecordTextFieldShouldMetCriteria(int parentRow, int relatedRow, String field,
			Matcher<String> matcher) {
		onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).field(field)
				.should(hasText(matcher));
		return this;
	}

	@Step
	public GBSteps editParentRecordInputField(int parentRow, String field, String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).inputField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editParentRecordDataCardInputField(int parentRow, String field, String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).dataCardRow().inputField(field)
				.should(isDisplayed());
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editParentRecordDataCardSelectField(int parentRow, String field, String value) {
		onGBPage().table().records().get(parentRow).dataCardRow().selectField(field).should(isDisplayed())
				.select(value);
		return this;
	}

	@Step
	public GBSteps editChildRecordDataCardInputField(int parentRow, int childRow, String field, String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).relatedRecord().records().get(childRow)
				.dataCardRow().inputField(field).should(isDisplayed());
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editParentRecordTextareaField(int parentRow, String field, String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).textareaField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editParentRecordDateField(int parentRow, String field, String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).dateField(field);
		el.clear();
		el.sendKeys(value);
		onGBPage().gridTitle().click();
		return this;
	}

	@Step
	public GBSteps editParentRecordTimeField(int parentRow, String field, String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).timeField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editClonedParentRecordInputField(int parentRow, int clonedRow, String field, String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).clonedRecord(String.valueOf(clonedRow + 1))
				.field(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editParentRecordSelectField(int parentRow, String field, String value) {
		onGBPage().table().records().get(parentRow).field(field).findElement(By.xpath("..")).click();
		onGBPage().table().records().get(parentRow).select(field).select(value);
		return this;
	}

	@Step
	public GBSteps editRelatedRecordInputField(int parentRow, int relatedRow, String field, String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow)
				.field(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editRelatedRecordInputField(int parentRow, String object, int relatedRow, String field,
			String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).relatedRecords()
				.filter(i -> i.title().getText().contains(object)).get(0).records().get(relatedRow).inputField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editRelatedRecordAdditionalInputField(int parentRow, int relatedRow, String field, String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow)
				.additionalInputField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editClonedRelatedRecordInputField(int parentRow, int relatedRow, int clonedRow, String field,
			String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow)
				.clonedRecord(String.valueOf(clonedRow + 1)).field(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editRelatedRecordDateField(int parentRow, int relatedRow, String field, String value) {
		HtmlElement el = onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow)
				.inputField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editRelatedRecordSelectField(int parentRow, int relatedRow, String field, String value) {
		onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).field(field)
				.findElement(By.xpath("..")).click();
		onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).select(field)
				.select(value);
		return this;
	}

	@Step
	public GBSteps relatedRecordSelectFieldShouldHaveItems(int parentRow, int relatedRow, String field,
			String... items) {
		onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).field(field)
				.findElement(By.xpath("..")).click();
		for (String item : items) {
			onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).select(field)
					.shouldHaveOption(item);
		}
		return this;
	}

	@Step
	public GBSteps relatedRecordSelectFieldShouldNotHaveItems(int parentRow, int relatedRow, String field,
			String... items) {
		onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).field(field)
				.findElement(By.xpath("..")).click();
		for (String item : items) {
			onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).select(field)
					.shouldNotHaveOption(item);
		}
		return this;
	}

	@Step
	public GBSteps saveChanges() {
		executor.executeScript("window.scrollTo(0, 0);");
		onGBPage().save().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps parentObjectFieldShouldHaveValue(int parentRow, String field, String value) {
		HtmlElement element = onGBPage().table().records().get(parentRow).field(field);
		if (element.getAttribute("value") == null) {
			onGBPage().table().records().get(parentRow).inputField(field).should(hasAttribute("value", value));
			return this;
		}
		assertThat(element, hasAttribute("value", value));
		return this;
	}

	@Step
	public GBSteps parentObjectsShouldHaveFieldValues(String field, String... values) {
		Iterable<Matcher<? super String>> matchers = Arrays.asList(values).stream().map(i -> equalTo(i))
				.collect(Collectors.toList());
		HtmlElement element = onGBPage().table().records().get(0).field(field);
		if (element.getAttribute("value") == null) {
			onGBPage().table().records().convert(i -> i.inputField(field).getAttribute("value"))
					.should(everyItem(anyOf(matchers)));
			return this;
		}
		onGBPage().table().records().convert(i -> i.field(field).getAttribute("value"))
				.should(everyItem(anyOf(matchers)));
		return this;
	}

	@Step
	public GBSteps parentObjectsShouldHaveFieldContainingValues(String field, String... values) {
		Iterable<Matcher<? super String>> matchers = Arrays.asList(values).stream()
				.map(i -> containsStringIgnoringCase(i)).collect(Collectors.toList());
		HtmlElement element = onGBPage().table().records().get(0).field(field);
		if (element.getAttribute("value") == null) {
			onGBPage().table().records().convert(i -> i.inputField(field).should(isDisplayed()).getAttribute("value"))
					.should(everyItem(anyOf(matchers)));
			return this;
		}
		onGBPage().table().records().convert(i -> i.field(field).should(isDisplayed()).getAttribute("value"))
				.should(everyItem(anyOf(matchers)));
		return this;
	}

	@Step
	public GBSteps parentObjectFieldShouldHaveText(int parentRow, String field, String value) {
		if ("--None--".equals(value)) {
			value = StringUtils.EMPTY;
		}
		onGBPage().table().records().get(parentRow).field(field).should(hasText(value));
		return this;
	}

	@Step
	public GBSteps parentObjectSelectFieldShouldHaveOptionSelected(int parentRow, String field, String value) {
		onGBPage().table().records().get(parentRow).select(field).shouldHaveValueSelected(value);
		return this;
	}

	@Step
	public GBSteps relatedObjectFieldShouldHaveValue(int parentRow, int relatedRow, String field, String value) {
		HtmlElement element = onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow)
				.field(field);

		if (element.getAttribute("value") == null) {
			onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).inputField(field)
					.should(hasAttribute("value", value));
			return this;
		}
		assertThat(element, hasAttribute("value", value));
		return this;
	}

	@Step
	public GBSteps shouldSeeChildObjectWithFieldValue(int parentRow, String field, String value) {
		onGBPage().table().records().get(parentRow).relatedRecord().records()
				.filter(i -> i.inputField(field).getAttribute("value").equals(value)).should(hasSize(1));
		return this;
	}

	@Step
	public GBSteps shouldNotSeeChildObjectWithFieldValue(int parentRow, String field, String value) {
		onGBPage().table().records().get(parentRow).relatedRecord().records()
				.filter(i -> i.inputField(field).getAttribute("value").equals(value)).should(hasSize(0));
		return this;
	}

	@Step
	public GBSteps selectChildRecord(int parentRow, String field, String value) {
		onGBPage().table().records().get(parentRow).relatedRecord().records()
				.filter(i -> i.inputField(field).getAttribute("value").equals(value)).should(hasSize(1)).get(0)
				.checkbox().setChecked(true);
		return this;
	}

	@Step
	public GBSteps parentObjectShouldHaveRelateObjectWithTheFieldValue(int parentRow, String field, String value) {
		HtmlElement element = onGBPage().table().records().get(parentRow).relatedRecord().records().get(0).field(field);

		if (element.getAttribute("value") == null) {
			onGBPage().table().records().get(parentRow).relatedRecord().records()
					.convert(i -> i.inputField(field).getAttribute("value")).should(hasItem(value));
			return this;
		}
		onGBPage().table().records().get(parentRow).relatedRecord().records()
				.convert(i -> i.field(field).getAttribute("value")).should(hasItem(value));
		return this;
	}

	@Step
	public GBSteps relatedObjectFieldShouldHaveText(int parentRow, int relatedRow, String field, String value) {
		if ("--None--".equals(value)) {
			value = StringUtils.EMPTY;
		}
		onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).field(field)
				.should(hasText(value));
		return this;
	}

	@Step
	public GBSteps checkRelatedRecordsCheckbox() {
		onGBPage().show().should(isDisplayed()).click();
		onGBPage().relatedRecords().should(isDisplayed()).setChecked(true);
		return this;
	}

	@Step
	public GBSteps shouldSeeSuccessMessage() {
		onGBPage().message().should(HasTextMatcher.hasText("Save successful."));
		return this;
	}

	@Step
	public GBSteps shouldSeeMessage(String message) {
		onGBPage().message().should(hasText(message));
		return this;
	}

	@Step
	public GBSteps clickAddNewParentObject() {
		onGBPage().newButton().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps clickAddNewRelatedObject(int parentRow) {
		onGBPage().table().records().get(parentRow).relatedRecord().newLink().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps clickAddNewRelatedObject(int parentRow, String object) {
		onGBPage().table().records().get(parentRow).relatedRecords().filter(i -> i.title().getText().contains(object))
				.get(0).newLink().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps selectAction(int row, String actionName) {
		setWindowHandles(getDriver().getWindowHandles());
		onGBPage().table().records().get(row).actions().should(isDisplayed()).click();
		onGBPage().action(actionName).should(isDisplayed()).click();
		confirmAlert();
		wait(1000);
		return this;
	}

	@Step
	public GBSteps openActions(int row) {
		onGBPage().table().records().get(row).actions().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps shouldSeeAction(String actionName) {
		onGBPage().action(actionName).should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldNotSeeAction(String actionName) {
		onGBPage().action(actionName).should(not(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps shouldNotSeeColumnOnMassCreatePopup(String column) {
		onGBPage().massCreatePopup().parentColumns().convert(WebElement::getText)
				.should(not(hasItem(column.toUpperCase())));
		return this;
	}

	@Step
	public GBSteps shouldNotSeeColumnOnMassUpdatePopup(String column) {
		onGBPage().massUpdatePopup().parentColumns().convert(WebElement::getText)
				.should(not(hasItem(column.toUpperCase())));
		return this;
	}

	@Step
	public GBSteps shouldSeeColumnOnMassCreatePopup(String column) {
		onGBPage().massCreatePopup().parentColumns().convert(WebElement::getText).should(hasItem(column.toUpperCase()));
		return this;
	}

	@Step
	public GBSteps shouldSeeColumnOnMassUpdatePopup(String column) {
		onGBPage().massUpdatePopup().parentColumns().convert(WebElement::getText).should(hasItem(column.toUpperCase()));
		return this;
	}

	@Step
	public GBSteps switchToNewWindow() {
		mainWindowHandle = getDriver().getWindowHandle();
		for (String handle : getDriver().getWindowHandles()) {
			if (!handle.equals(mainWindowHandle)) {
				getDriver().switchTo().window(handle);
			}
		}
		return this;
	}

	@Step
	public GBSteps switchToWindow() {
		mainWindowHandle = getDriver().getWindowHandle();
		switchToNewWindow(getWindowHandles());
		return this;
	}

	@Step
	public GBSteps switchToMainWindow() {
		switchToDefaultWindow();
		return this;
	}

	@Step
	public GBSteps shouldSeePageUrl(String url) {
		checkUrlContainsString(url);
		return this;
	}

	@Step
	public GBSteps applyColumnFilter(String columnName, String operator, String value) {
		onGBPage().table().topHeaders().stream().filter(i -> i.name().getText().contains(columnName.toUpperCase()))
				.findFirst().get().actionIcon().click();
		onGBPage().fastFilter().operator().should(isDisplayed()).select(operator);
		onGBPage().fastFilter().should(isDisplayed()).filterValue().clear();
		onGBPage().fastFilter().should(isDisplayed()).filterValue().sendKeys(value);
		onGBPage().fastFilter().apply().click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps openColumnActions(String columnName) {
		onGBPage().table().records().get(FIRST_ROW).topHeaders().stream()
				.filter(i -> i.name().getText().contains(columnName.toUpperCase())).findFirst().get().actionIcon()
				.click();
		return this;
	}

	@Step
	public GBSteps enterColumnDateValue(String value) {
		onGBPage().fastFilter().should(isDisplayed()).dateField().should(isDisplayed()).clear();
		onGBPage().fastFilter().should(isDisplayed()).dateField().should(isDisplayed()).sendKeys(value);
		onGBPage().fastFilter().should(isDisplayed()).title().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps enterColumnTimeValue(String value) {
		onGBPage().fastFilter().should(isDisplayed()).timeField().should(isDisplayed()).clear();
		onGBPage().fastFilter().should(isDisplayed()).timeField().should(isDisplayed()).sendKeys(value);
		return this;
	}

	@Step
	public GBSteps clickAllButton() {
		onGBPage().fastFilter().should(isDisplayed()).all().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps clickSelectedButton() {
		onGBPage().fastFilter().should(isDisplayed()).selected().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps clearColumnFilter(String columnName) {
		onGBPage().table().should(isDisplayed()).topHeaders().stream()
				.filter(i -> i.getText().toUpperCase().contains(columnName.toUpperCase())).findFirst().get()
				.actionIcon().click();
		onGBPage().fastFilter().clearButton().should(isDisplayed()).click();
		confirmAlert();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps selectFilter(String filter) {
		onGBPage().myFilter().should(isDisplayed()).select(filter);
		switchToNewFrame();
		onGBPage().loader().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps applyRelatedColumnFilter(int row, String columnName, String value) {
		onGBPage().table().records().get(row).relatedRecord().headers().stream()
				.filter(i -> i.name().getText().contains(columnName.toUpperCase())).findFirst().get().actionIcon()
				.click();
		onGBPage().fastFilter().should(isDisplayed()).filterValue().sendKeys(value);
		onGBPage().fastFilter().apply().click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps shouldSeePopup() {
		onGBPage().gridOverlay().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps closePopup() {
		switchToNewFrame();
		onGBPage().closeButton().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps closePopupAndRefresh() {
		switchToNewFrame();
		onGBPage().closeAndRefreshButton().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps openMoreMenu() {
		onGBPage().more().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps openFilterPopup() {
		onGBPage().filter().should(isDisplayed()).click();
		getDriver().switchTo().frame("userFilterFrame");
		onGBPage().loader().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps clickEditSettingsMenuItem() {
		onGBPage().gridMenuItems().filter(i -> i.getText().contains("Edit Settings")).should(hasSize(1)).get(0).click();
		return this;
	}

	@Step
	public GBSteps clickEditAdminFiltersMenuItem() {
		onGBPage().gridMenuItems().filter(i -> i.getText().contains("Edit Admin Filters")).should(hasSize(1)).get(0)
				.click();
		return this;
	}

	@Step
	public GBSteps clickEditFieldsMenuItem() {
		onGBPage().gridMenuItems().filter(i -> i.getText().contains("Edit Fields")).should(hasSize(1)).get(0).click();
		return this;
	}

	@Step
	public GW1Steps switchToEditSettingsPopup() {
		switchToNewFrame();
		getDriver().switchTo().frame("actionsFrame");
		onGBPage().loader().should(isNotDisplayed());
		return new GW1Steps(getDriver(), mainWindowHandle, currentFrameId);
	}

	@Step
	public GW3Steps switchToEditAdminFiltersPopup() {
		switchToNewFrame();
		getDriver().switchTo().frame("actionsFrame");
		onGBPage().loader().should(isNotDisplayed());
		return new GW3Steps(getDriver(), mainWindowHandle, currentFrameId);
	}

	@Step
	public GW2Steps switchToEditFieldsPopup() {
		switchToNewFrame();
		getDriver().switchTo().frame("actionsFrame");
		onGBPage().loader().should(isNotDisplayed());
		return new GW2Steps(getDriver(), mainWindowHandle, currentFrameId);
	}

	@Step
	public GBSteps openExportPopup() {
		onGBPage().more().should(isDisplayed()).click();
		onGBPage().gridMenuItems().filter(i -> i.getText().contains("Export")).should(hasSize(1)).get(0).click();
		getDriver().switchTo().frame("exportFrame");
		onGBPage().loader().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps openMassCreatePopup() {
		onGBPage().more().should(isDisplayed()).click();
		onGBPage().gridMenuItems().filter(i -> i.getText().contains("Mass Create")).should(hasSize(1)).get(0).click();
		onGBPage().loader().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps openEditSettingsPopup() {
		onGBPage().more().should(isDisplayed()).click();
		onGBPage().gridMenuItems().filter(i -> i.getText().contains("Edit Settings")).should(hasSize(1)).get(0).click();
		onGBPage().loader().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps openEditFieldsPopup() {
		onGBPage().more().should(isDisplayed()).click();
		onGBPage().gridMenuItems().filter(i -> i.getText().contains("Edit Fields")).should(hasSize(1)).get(0).click();
		onGBPage().loader().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps openEditAdminFilters() {
		onGBPage().more().should(isDisplayed()).click();
		onGBPage().gridMenuItems().filter(i -> i.getText().contains("Edit Admin Filters")).should(hasSize(1)).get(0)
				.click();
		onGBPage().loader().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps openMassUpdatePopup() {
		onGBPage().massUpdate().should(isDisplayed()).click();
		onGBPage().loader().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps openReorderColumnsPopup() {
		onGBPage().more().should(isDisplayed()).click();
		onGBPage().gridMenuItems().filter(i -> i.getText().contains("Reorder/Hide Columns")).should(hasSize(1)).get(0)
				.click();
		onGBPage().loader().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps openInfoPopup() {
		onGBPage().infoIcon().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps shouldSeeObjectOnInfoPopup(String object) {
		onGBPage().currentFilterPopup().object(object).should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeObjectReorderColumnsPopup(String object) {
		onGBPage().reorderColumnsBlock().object(object).should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps uncheckCheckboxesOnReorderPopup(String... checkboxes) {
		Arrays.asList(checkboxes)
				.forEach(i -> onGBPage().reorderColumnsBlock().checkbox(i).should(isDisplayed()).setChecked(false));
		return this;
	}

	@Step
	public GBSteps dragAndDropColumn(String columnName) {
		WebElement label = onGBPage().reorderColumnsBlock().checkbox(columnName)
				.findElement(By.xpath("../following-sibling::span"));
		actions.dragAndDropBy(label, 0, -100).perform();
		return this;
	}

	@Step
	public GBSteps saveReorderSettings() {
		onGBPage().reorderColumnsBlock().save().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps shouldSeeGridMenuItem(String item) {
		assertThat(onGBPage().gridMenuItems().stream().map(i -> i.getText()).collect(Collectors.toList()),
				hasItem(item));
		return this;
	}

	@Step
	public GBSteps shouldNotSeeGridMenuItem(String item) {
		assertThat(onGBPage().gridMenuItems().stream().map(i -> i.getText()).collect(Collectors.toList()),
				not(hasItem(item)));
		return this;
	}

	@Step
	public GBSteps createNewFiltrer() {
		onGBPage().manageFiltersBlock().myFilter().select("Create New...");
		onGBPage().loader().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps setFilterCondition(String object, int row, String field, String operator, String value) {
		int rowSize = onGBPage().manageFiltersBlock().objectFilter(object).filterByFieldRows().size();
		while (rowSize <= row) {
			onGBPage().manageFiltersBlock().objectFilter(object).addFilterCondition().should(isDisplayed()).click();
			rowSize = onGBPage().manageFiltersBlock().objectFilter(object).filterByFieldRows().size();
		}
		onGBPage().manageFiltersBlock().objectFilter(object).filterByFieldRows().get(row).field().should(isDisplayed())
				.select(field);
		onGBPage().manageFiltersBlock().objectFilter(object).filterByFieldRows().get(row).operator()
				.should(isDisplayed()).select(operator);
		onGBPage().manageFiltersBlock().objectFilter(object).filterByFieldRows().get(row).value().should(isDisplayed())
				.clear();
		onGBPage().manageFiltersBlock().objectFilter(object).filterByFieldRows().get(row).value().should(isDisplayed())
				.sendKeys(value, Keys.TAB);
		return this;
	}

	@Step
	public GBSteps selectSortByField(String object, int row, String field, String order) {
		int rowSize = onGBPage().manageFiltersBlock().objectFilter(object).sortByFieldRows().size();
		while (rowSize <= row) {
			onGBPage().manageFiltersBlock().objectFilter(object).addSortCondition().should(isDisplayed()).click();
			rowSize = onGBPage().manageFiltersBlock().objectFilter(object).sortByFieldRows().size();
		}
		onGBPage().manageFiltersBlock().objectFilter(object).sortByFieldRows().get(row).field().select(field);
		onGBPage().manageFiltersBlock().objectFilter(object).sortByFieldRows().get(row).direction().select(order);
		return this;
	}

	@Step
	public GBSteps setAdvancedFilterCondition(String object, String condition) {
		onGBPage().manageFiltersBlock().objectFilter(object).advancedFilterConditions().inputField()
				.should(isDisplayed()).sendKeys(condition);
		return this;
	}

	@Step
	public GBSteps enterFilterName(String filterName) {
		onGBPage().manageFiltersBlock().filterName().should(isDisplayed()).clear();
		onGBPage().manageFiltersBlock().filterName().should(isDisplayed()).sendKeys(filterName);
		return this;
	}

	@Step
	public GBSteps saveFilter() {
		onGBPage().manageFiltersBlock().save().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps myFilterShouldHaveValueSelected(String value) {
		assertThat("Wrong option is selected", onGBPage().myFilter().getSelectedOption(), equalTo(value));
		return this;
	}

	@Step
	public GBSteps closeFilterPopup() {
		onGBPage().manageFiltersBlock().sendKeys(Keys.ESCAPE);
		getDriver().switchTo().defaultContent();
		return this;
	}

	@Step
	public GBSteps openGroupingsMenu() {
		onGBPage().groupings().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps shouldSeeExpandGroupingsCheckboxChecked() {
		onGBPage().expandGroupings().should(isDisplayed()).should(isChecked());
		return this;
	}

	@Step
	public GBSteps expandParentRecord(int row) {
		onGBPage().table().records().get(row).expandIcon().click();
		return this;
	}

	@Step
	public GBSteps expandRelatedRecord(int row) {
		onGBPage().table().records().get(row).relatedRecord().expandIcon().click();
		return this;
	}

	@Step
	public GBSteps shouldSeeShowRecordDetailsCheckboxChecked() {
		onGBPage().showRecordDetails().should(isDisplayed()).should(isChecked());
		return this;
	}

	@Step
	public GBSteps shouldSeeExpandGroupingsCheckboxUnchecked() {
		onGBPage().expandGroupings().should(isDisplayed()).should(not(isChecked()));
		return this;
	}

	@Step
	public GBSteps shouldSeeShowRecordDetailsCheckboxUnchecked() {
		onGBPage().showRecordDetails().should(isDisplayed()).should(not(isChecked()));
		return this;
	}

	@Step
	public GBSteps checkShowRecordDetailsCheckbox() {
		onGBPage().showRecordDetails().should(isDisplayed()).setChecked(true);
		return this;
	}

	@Step
	public GBSteps cloneParentRecord(int row) {
		onGBPage().table().records().get(row).actions().should(isDisplayed()).click();
		onGBPage().action("Clone record").should(isDisplayed()).click();
		onGBPage().table().records().get(row).clonedRecord("1").should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps cloneRelatedRecord(int parentRow, int relatedRow) {
		executor.executeScript("window.scrollTo(0, 0);");
		onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).actions()
				.should(isDisplayed()).click();
		onGBPage().action("Clone record").should(isDisplayed()).click();
		onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).clonedRecord("1")
				.should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps checkParentObjectClonedSelectFields(int row, List<String> fieldsForChecking) {
		fieldsForChecking.forEach(i -> {
			String value = onGBPage().table().records().get(row).field(i).getText();
			assertThat(onGBPage().table().records().get(row).clonedRecord("1").select(i).getSelectedOption()
					.replaceAll("--None--", ""), equalTo(value));
		});
		return this;
	}

	@Step
	public GBSteps checkParentObjectClonedInputFields(int row, List<String> fieldsForChecking) {
		fieldsForChecking.forEach(i -> {
			String value = onGBPage().table().records().get(row).field(i).getAttribute("value");
			if (value != null) {
				assertThat(onGBPage().table().records().get(row).clonedRecord("1").field(i).getAttribute("value"),
						equalTo(value));
			} else {
				value = onGBPage().table().records().get(row).inputField(i).getAttribute("value");
				assertThat(onGBPage().table().records().get(row).clonedRecord("1").inputField(i).getAttribute("value"),
						equalTo(value));
			}

		});
		return this;
	}

	@Step
	public GBSteps parentRecordClonedDateTimeFieldShouldHaveValue(int row, String field, LocalDateTime dateTime) {
		assertThat(onGBPage().table().records().get(row).clonedRecord("1").dateField(field).getAttribute("value"),
				equalTo(dateTime.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))));
		assertThat(onGBPage().table().records().get(row).clonedRecord("1").timeField(field).getAttribute("value"),
				equalTo(dateTime.format(DateTimeFormatter.ofPattern("h:mm a"))));
		return this;
	}

	@Step
	public GBSteps parentRecordDateTimeFieldShouldHaveValue(int row, String field, LocalDateTime dateTime) {
		assertThat(onGBPage().table().records().get(row).dateField(field).getAttribute("value"),
				equalTo(dateTime.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))));
		assertThat(onGBPage().table().records().get(row).timeField(field).getAttribute("value"),
				equalTo(dateTime.format(DateTimeFormatter.ofPattern("h:mm a"))));
		return this;
	}

	@Step
	public GBSteps parentRecordDateTimeFieldShouldHaveValue(int row, String field, String date, String time) {
		assertThat(onGBPage().table().records().get(row).dateField(field).getAttribute("value"), equalTo(date));
		assertThat(onGBPage().table().records().get(row).timeField(field).getAttribute("value"), equalTo(time));
		return this;
	}

	@Step
	public GBSteps relatedRecordDateTimeFieldShouldHaveValue(int row, int relatedRow, String field, String date,
			String time) {
		assertThat(onGBPage().table().records().get(row).relatedRecord().records().get(relatedRow).dateField(field)
				.getAttribute("value"), equalTo(date));
		assertThat(onGBPage().table().records().get(row).relatedRecord().records().get(relatedRow).timeField(field)
				.getAttribute("value"), equalTo(time));
		return this;
	}

	@Step
	public GBSteps relatedRecordDateTimeFieldShouldHaveValue(int row, int relatedRow, String field,
			LocalDateTime dateTime) {
		assertThat(onGBPage().table().records().get(row).relatedRecord().records().get(relatedRow).dateField(field)
				.getAttribute("value"), equalTo(dateTime.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))));
		assertThat(onGBPage().table().records().get(row).relatedRecord().records().get(relatedRow).timeField(field)
				.getAttribute("value"), equalTo(dateTime.format(DateTimeFormatter.ofPattern("h:mm a"))));
		return this;
	}

	@Step
	public GBSteps checkRelatedObjectClonedSelectFields(int parentRow, int relatedRow, List<String> fieldsForChecking) {
		fieldsForChecking.forEach(i -> {
			String value = onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow)
					.field(i).getText();
			assertThat(
					onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow)
							.clonedRecord("1").select(i).getSelectedOption().replaceAll("--None--", ""),
					equalTo(value));
		});
		return this;
	}

	@Step
	public GBSteps checkRelatedObjectClonedInputFields(int parentRow, int relatedRow, List<String> fieldsForChecking) {
		fieldsForChecking.forEach(i -> {
			String value = onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow)
					.field(i).getAttribute("value");
			if (value != null) {
				assertThat(onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow)
						.clonedRecord("1").field(i).getAttribute("value"), equalTo(value));
			} else {
				value = onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow)
						.inputField(i).getAttribute("value");
				assertThat(onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow)
						.clonedRecord("1").inputField(i).getAttribute("value"), equalTo(value));
			}

		});
		return this;
	}

	@Step
	public GBSteps setRelatedFilter(String filterValue) {
		onGBPage().manageFiltersBlock().filterByRelatedObject().select(filterValue);
		return this;
	}

	@Step
	public GBSteps goToPage(int page) {
		onGBPage().page().clear();
		onGBPage().page().sendKeys(String.valueOf(page), Keys.ENTER);
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps cellContainingTextShouldHaveColorBackground(String columnName, String value, String color) {
		assertThat(onGBPage().table().records()
				.filter(i -> i.field(columnName).getAttribute("value").toLowerCase().contains(value.toLowerCase()))
				.stream().map(i -> i.cell(columnName)).collect(Collectors.toList()),
				everyItem(hasCssValue("background-color", color)));
		return this;
	}

	@Step
	public GBSteps dateCellMatchingCriteriaShouldHaveColorBackground(String columnName,
			Matcher<ChronoLocalDate> matcher, String color) {
		assertThat(onGBPage().table().records()
				.filter(i -> matcher.matches(LocalDate.parse(i.dateField(columnName).getAttribute("value"),
						DateTimeFormatter.ofPattern("MM/dd/yyyy"))))
				.stream().map(i -> i.cell(columnName)).collect(Collectors.toList()),
				everyItem(hasCssValue("background-color", color)));
		return this;
	}

	@Step
	public GBSteps shouldSeeMassUpdateButton() {
		onGBPage().massUpdate().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeSalesforceHader() {
		switchToNewFrame();
		getDriver().switchTo().defaultContent();
		onGBPage().menuItem("Grid Wizard").should(isDisplayed());
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps shouldNotSeeSalesforceHader() {
		switchToNewFrame();
		getDriver().switchTo().defaultContent();
		onGBPage().menuItem("Grid Wizard").should(isNotDisplayed());
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps shouldNotSeeSaveButton() {
		onGBPage().save().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeSaveButton() {
		onGBPage().save().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldNotSeeNewButton() {
		onGBPage().newButton().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldNotSeeMassUpdateButton() {
		onGBPage().massUpdate().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldNotSeeMassCreateButton() {
		onGBPage().more().should(isDisplayed()).click();
		onGBPage().gridMenuItems().filter(i -> i.getText().contains("Mass Create")).should(hasSize(0));
		return this;
	}

	@Step
	public GBSteps shouldNotSeeNewButtonForRelatedObject() {
		onGBPage().table().records().get(0).relatedRecord().newLink().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeNewButtonForRelatedObject() {
		onGBPage().table().records().get(0).relatedRecord().newLink().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeNewButton() {
		onGBPage().newButton().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldNotSeeSidebar() {
		switchToNewFrame();
		getDriver().switchTo().defaultContent();
		onGBPage().sidebar().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeSidebar() {
		switchToNewFrame();
		getDriver().switchTo().defaultContent();
		onGBPage().sidebar().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeObjectOnExportPopup(String object) {
		onGBPage().exportPopup().object(object).should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeObjectOnMassCreatePopup(String object) {
		onGBPage().massCreatePopup().massCreateParents().should(isDisplayed())
				.should(hasAttribute("value", "Create " + object));
		return this;
	}

	@Step
	public GBSteps shouldSeeRelatedObjectOnMassCreatePopup(String object) {
		onGBPage().massCreatePopup().childObjects().filter(i -> i.getText().contains(object)).should(hasSize(1));
		return this;
	}

	@Step
	public SFAccountSteps openAccountPage() {
		Set<String> currentWindowHandles = getDriver().getWindowHandles();
		onGBPage().openRecord().should(isDisplayed()).click();
		mainWindowHandle = getDriver().getWindowHandle();
		switchToNewWindow(currentWindowHandles);
		wait(15000);
		return new SFAccountSteps(getDriver(), mainWindowHandle, currentFrameId);
	}

	@Step
	public GBSteps openRelatedObjectsPopup(int row) {
		onGBPage().table().records().get(row).more().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps fieldShouldBeEditable(int row, String field) {
		onGBPage().table().records().get(row).inputField(field).should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps fieldShouldBeEditable(int parentRow, int childRow, String field) {
		onGBPage().table().records().get(parentRow).relatedRecord().records().get(childRow).inputField(field)
				.should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps fieldShouldBeNotEditable(int row, String field) {
		onGBPage().table().records().get(row).inputField(field).should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps fieldShouldBeNotEditable(int parentRow, int childRow, String field) {
		onGBPage().table().records().get(parentRow).relatedRecord().records().get(childRow).inputField(field)
				.should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps selectParentRecord(int row) {
		onGBPage().table().records().get(row).checkbox().setChecked(true);
		return this;
	}

	@Step
	public GBSteps selectRelatedRecord(int parentRow, int relatedRow) {
		onGBPage().table().records().get(parentRow).relatedRecord().records().get(relatedRow).checkbox()
				.setChecked(true);
		return this;
	}

	@Step
	public GBSteps deleteSelectedItems() {
		onGBPage().more().should(isDisplayed()).click();
		onGBPage().gridMenuItems().filter(i -> i.getText().contains("Delete")).should(hasSize(1)).get(0).click();
		getDriver().switchTo().alert().accept();
		try {
			switchToNewFrame();
		} catch (UnhandledAlertException e) {
		}
		return this;
	}

	@Step
	public GBSteps shouldSeeAlert(String text) {
		Alert alert = getDriver().switchTo().alert();
		assertThat(alert.getText(), equalTo(text));
		alert.accept();
		return this;
	}

	@Step
	public GBSteps shouldSeeParentObjectsCollapsed() {
		assertThat(onGBPage().table().records().stream().map(i -> i.expandIcon()).collect(Collectors.toList()),
				everyItem(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps shouldSeeParentObjectsExpanded() {
		assertThat(onGBPage().table().records().stream().map(i -> i.collapseIcon()).collect(Collectors.toList()),
				everyItem(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps shouldSeeRelatedObjectsExpanded() {
		assertThat(onGBPage().table().records().get(0).relatedRecords().stream().map(i -> i.collapseIcon())
				.collect(Collectors.toList()), everyItem(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps shouldSeeRelatedObjectsCollapsed() {
		assertThat(onGBPage().table().records().get(0).relatedRecords().stream().map(i -> i.expandIcon())
				.collect(Collectors.toList()), everyItem(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps shouldSeeRelatedObjectsExpanded(String... object) {
		List<String> objects = Arrays.asList(object);
		assertThat(
				onGBPage().table().records().get(0).relatedRecords().filter(i -> objects.contains(i.title().getText()))
						.stream().map(i -> i.collapseIcon()).collect(Collectors.toList()),
				everyItem(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps shouldSeeRelatedObjectsCollapsed(String... object) {
		List<String> objects = Arrays.asList(object);
		assertThat(
				onGBPage().table().records().get(0).relatedRecords().filter(i -> objects.contains(i.title().getText()))
						.stream().map(i -> i.expandIcon()).collect(Collectors.toList()),
				everyItem(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps editMassUpdateInputField(String field, String value) {
		HtmlElement el = onGBPage().massUpdatePopup().parentInputField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editMassUpdateSelectField(String field, String value) {
		onGBPage().massUpdatePopup().parentSelectField(field).select(value);
		return this;
	}

	@Step
	public GBSteps editMassUpdateInputField(String object, String field, String value) {
		HtmlElement el = onGBPage().massUpdatePopup().childRecord(object).inputField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editMassUpdateDateField(String object, String field, String value) {
		HtmlElement el = onGBPage().massUpdatePopup().childRecord(object).dateField(field).should(isDisplayed());
		el.clear();
		el.sendKeys(value);
		onGBPage().massUpdatePopup().click();
		return this;
	}

	@Step
	public GBSteps editMassUpdateTimeField(String object, String field, String value) {
		HtmlElement el = onGBPage().massUpdatePopup().childRecord(object).timeField(field).should(isDisplayed());
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps shouldSeeMassUpdateFieldEmpty(String object, String field) {
		onGBPage().massUpdatePopup().childRecord(object).inputField(field).should(isDisplayed())
				.should(hasAttribute("value", ""));
		return this;
	}

	@Step
	public GBSteps shouldSeeMassUpdateFieldEmpty(String field) {
		onGBPage().massUpdatePopup().parentInputField(field).should(hasAttribute("value", ""));
		return this;
	}

	@Step
	public GBSteps clickClearAllOnMassUpdatePopup() {
		onGBPage().massUpdatePopup().clearAll().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps clickCollapseAllOnMassUpdatePopup() {
		onGBPage().massUpdatePopup().collapseAll().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps clickExpandAllOnMassUpdatePopup() {
		onGBPage().massUpdatePopup().expandAll().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps shouldSeeChildObjectsOnMassUpdatePopupExpanded() {
		assertThat(onGBPage().massUpdatePopup().childRecords().stream().map(i -> i.collapseIcon())
				.collect(Collectors.toList()), everyItem(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps shouldSeeChildObjectsOnMassUpdatePopupCollapsed() {
		assertThat(onGBPage().massUpdatePopup().childRecords().stream().map(i -> i.expandIcon())
				.collect(Collectors.toList()), everyItem(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps shouldSeeMassUpdateMessage(String text) {
		onGBPage().massUpdatePopup().messgage().should(isDisplayed()).should(hasText(text));
		return this;
	}

	@Step
	public GBSteps clickApplyToAllRecordsButton() {
		onGBPage().massUpdatePopup().applyToAllRecords().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps clickApplyToSelectedRecordsButton() {
		onGBPage().massUpdatePopup().applyToSelectedRecords().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps cellsShouldHaveColorBackground(String columnName, String color) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).convert(i -> i.cell(columnName))
				.should(everyItem(hasCssValue("background-color", color)));
		return this;
	}

	@Step
	public GBSteps cellsShouldNotHaveColorBackground(String columnName, String color) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).convert(i -> i.cell(columnName))
				.should(everyItem(not(hasCssValue("background-color", color))));
		return this;
	}

	@Step
	public GBSteps cellShouldHaveColorBackground(int row, String columnName, String color) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).get(row).cell(columnName)
				.should(hasCssValue("background-color", color));
		return this;
	}

	@Step
	public GBSteps cellsShouldHaveTextColor(String columnName, String color) {
		onGBPage().table().records().should(hasSize(greaterThan(0)))
				.convert(i -> i.field(columnName).findElement(By.xpath("..")))
				.should(everyItem(hasCssValue("color", color)));
		return this;
	}

	@Step
	public GBSteps cellsShouldHaveItalicFontStyle(String columnName) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).convert(i -> i.cell(columnName))
				.should(everyItem(hasCssValue("font-style", "italic")));
		return this;
	}

	@Step
	public GBSteps cellsMatchingCriteriaShouldHaveItalicFontStyle(String columnName, Matcher<String> matcher) {
		assertThat(onGBPage().table().records().should(hasSize(greaterThan(0))).stream()
				.filter(i -> matcher.matches(i.field(columnName).getText())).map(i -> i.field(columnName))
				.collect(Collectors.toList()), everyItem(hasCssValue("font-style", "italic")));
		return this;
	}

	@Step
	public GBSteps cellsShouldHaveBoldFontStyle(String columnName) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).convert(i -> i.cell(columnName))
				.should(everyItem(hasCssValue("font-weight", "700")));
		return this;
	}

	@Step
	public GBSteps cellsShouldNotHaveItalicFontStyle(String columnName) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).convert(i -> i.cell(columnName))
				.should(everyItem(not(hasCssValue("font-style", "italic"))));
		return this;
	}

	@Step
	public GBSteps cellsShouldNotHaveBoldFontStyle(String columnName) {
		onGBPage().table().records().should(hasSize(greaterThan(0))).convert(i -> i.cell(columnName))
				.should(everyItem(not(hasCssValue("font-weight", "700"))));
		return this;
	}

	@Step
	public GBSteps childCellShouldHaveColorBackground(int parent, int child, String columnName, String color) {
		assertThat(
				onGBPage().table().records().should(hasSize(greaterThan(0))).get(parent).relatedRecord().records()
						.get(child).field(columnName).findElement(By.xpath("..")),
				hasCssValue("background-color", color));
		return this;
	}

	@Step
	public GBSteps childRowWithCellMatchingCriteriaShouldHaveColorBackground(String columnName, Matcher<String> matcher,
			String color) {
		List<WebElement> fields = onGBPage().table().records().stream()
				.flatMap(i -> Stream.of(i.relatedRecord().records())).flatMap(List::stream)
				.filter(i -> matcher.matches(i.field(columnName).getAttribute("value")))
				.flatMap(i -> Stream.of(i.fields())).flatMap(List::stream).map(i -> i.findElement(By.xpath("..")))
				.collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("background-color", color)));
		return this;
	}

	@Step
	public GBSteps childRowWithCellMatchingCriteriaShouldNotHaveColorBackground(String columnName,
			Matcher<String> matcher, String color) {
		List<WebElement> fields = onGBPage().table().records().stream()
				.flatMap(i -> Stream.of(i.relatedRecord().records())).flatMap(List::stream)
				.filter(i -> matcher.matches(i.field(columnName).getAttribute("value")))
				.flatMap(i -> Stream.of(i.fields())).flatMap(List::stream).map(i -> i.findElement(By.xpath("..")))
				.collect(Collectors.toList());
		assertThat(fields, everyItem(not(hasCssValue("background-color", color))));
		return this;
	}

	@Step
	public GBSteps refreshGrid() {
		onGBPage().refresh().should(isDisplayed()).click();
		// getDriver().switchTo().defaultContent();
		try {
			getDriver().switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps topRowWithButtonsShouldHaveColorBackground(String color) {
		onGBPage().topRowWithButtons().should(hasCssValue("background-color", color));
		return this;
	}

	@Step
	public GBSteps openChart() {
		onGBPage().chartIcon().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps shouldSeeLineBarChartData(String chartName, String xAxis, String yAxis, String legend) {
		onGBPage().chart().chart().should(isDisplayed());
		onGBPage().chart().chartName().should(isDisplayed()).should(hasText(chartName));
		onGBPage().chart().chartLegend().should(isDisplayed()).should(hasText(legend));
		onGBPage().chart().xAxisLabel().should(isDisplayed()).should(hasText(xAxis));
		onGBPage().chart().yAxisLabel().should(isDisplayed()).should(hasText(yAxis));
		onGBPage().chart().nodataMessage().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeePieChartData(String chartName, String xAxis, String legend) {
		onGBPage().chart().chart().should(isDisplayed());
		onGBPage().chart().chartName().should(isDisplayed()).should(hasText(chartName));
		onGBPage().chart().chartLegend().should(isDisplayed()).should(hasText(legend));
		onGBPage().chart().xAxisLabel().should(isDisplayed()).should(hasText(xAxis));
		onGBPage().chart().nodataMessage().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeChartSize(String size) {
		switch (size) {
		case "Small":
			onGBPage().chart().chart().should(hasAttribute("width", "320"));
			onGBPage().chart().chart().should(hasAttribute("height", "256"));
			break;
		case "Medium":
			onGBPage().chart().chart().should(hasAttribute("width", "480"));
			onGBPage().chart().chart().should(hasAttribute("height", "384"));
			break;
		case "Large":
			onGBPage().chart().chart().should(hasAttribute("width", "640"));
			onGBPage().chart().chart().should(hasAttribute("height", "512"));
			break;
		}
		return this;
	}

	@Step
	public GBSteps shouldSeeSummaryValue(String columnName, Double summary) {
		assertThat(Double.parseDouble(
				onGBPage().table().summaryField(columnName).should(isDisplayed()).getText().replaceAll("[^\\d.]", "")),
				equalTo(summary));
		return this;
	}

	@Step
	public GBSteps rowWithCellContainingValueShouldHaveColorBackground(String columnName, String value, String color) {
		List<WebElement> fields = onGBPage().table().records().stream()
				.filter(i -> i.inputField(columnName).getAttribute("value").toLowerCase().contains(value.toLowerCase()))
				.flatMap(i -> Stream.of(i.fields())).flatMap(List::stream).map(i -> i.findElement(By.xpath("..")))
				.collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("background-color", color)));
		return this;
	}

	@Step
	public GBSteps rowWithCellContainingValueShouldHaveTextColor(String columnName, String value, String color) {
		List<WebElement> fields = onGBPage().table().records().stream()
				.filter(i -> i.inputField(columnName).getAttribute("value").toLowerCase().contains(value.toLowerCase()))
				.flatMap(i -> Stream.of(i.fields())).flatMap(List::stream).map(i -> i.findElement(By.xpath("..")))
				.collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("color", color)));
		return this;
	}

	@Step
	public GBSteps shouldSeeRecords() {
		onGBPage().loader().should(isNotDisplayed());
		assertThat(onGBPage().table().should(isDisplayed()).records(), hasSize(greaterThan(0)));
		return this;
	}

	@Step
	public GBSteps expandDataCard(int parentRow) {
		onGBPage().table().records().get(0).dataCardIcon().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps expandDataCard(int parentRow, int childRow) {
		onGBPage().table().records().get(0).relatedRecord().records().get(childRow).dataCardIcon().should(isDisplayed())
				.click();
		return this;
	}

	@Step
	public GBSteps shouldNotSeeRecords() {
		assertThat(onGBPage().table().should(isDisplayed()).records(), hasSize(0));
		return this;
	}

	@Step
	public GBSteps shouldNotSeeChildRecords(int parent) {
		onGBPage().table().should(isDisplayed()).records().get(0).noChildRecordsFound().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps myFilterShouldHaveValue(String value) {
		onGBPage().myFilter().shouldHaveOption(value);
		return this;
	}

	@Step
	public GBSteps myFilterShouldNotHaveValue(String value) {
		onGBPage().myFilter().shouldNotHaveOption(value);
		return this;
	}

	@Step
	public GBSteps myFilterShouldNotDisplayed() {
		onGBPage().myFilter().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldNotSeeMyFilter() {
		onGBPage().myFilter().should(isNotDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeForecastViewRecords() {
		onGBPage().table().should(isDisplayed()).forecastViewRecords().should(hasSize(greaterThan(0)));
		return this;
	}

	@Step
	public GBSteps clickReadOnlyButton() {
		onGBPage().readOnly().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps shouldSeeCogIconsNextToFields() {
		List<WebElement> cogIcons = onGBPage().table().should(isDisplayed()).forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.fields())).flatMap(List::stream)
				.map(i -> i.findElement(By.xpath("./span[contains(@class,'icon-cog')]"))).collect(Collectors.toList());
		assertThat(cogIcons, everyItem(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps forecastViewCogIconsShouldHaveGreyColor() {
		List<WebElement> cogIcons = onGBPage().table().should(isDisplayed()).forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.fields())).flatMap(List::stream)
				.map(i -> i.findElement(By.xpath("./span[contains(@class,'icon-cog')]"))).collect(Collectors.toList());
		assertThat(cogIcons,
				everyItem(allOf(hasCssValue("color", "rgba(34, 34, 34, 1)"), hasCssValue("opacity", "0.2"))));
		return this;
	}

	@Step
	public GBSteps forecastViewCogIconsShouldHaveGreenColor(String field) {
		onGBPage().table().forecastViewRecords().convert(i -> i.cogIcon(field))
				.should(everyItem(hasCssValue("color", "rgba(4, 132, 75, 1)")));
		return this;
	}

	@Step
	public GBSteps forecastViewCogIconsShouldHaveGreyColor(String field) {
		onGBPage().table().forecastViewRecords().convert(i -> i.cogIcon(field))
				.should(everyItem(allOf(hasCssValue("color", "rgba(34, 34, 34, 1)"), hasCssValue("opacity", "0.2"))));
		return this;
	}

	@Step
	public GBSteps openForecastViewFieldActionsMenu(String field) {
		onGBPage().table().should(isDisplayed()).forecastViewRecords().get(0).cogIcon(field).should(isDisplayed())
				.click();
		return this;
	}

	@Step
	public GBSteps shouldSeeForecastViewHeaders(List<String> expectedHeaders) {
		assertThat(onGBPage().table().forecactViewHeaders().stream().map(i -> i.getAttribute("data-ufval"))
				.collect(Collectors.toList()), equalTo(expectedHeaders));
		return this;
	}

	@Step
	public GBSteps shouldSeeForecastViewByQuaterHeaders() {
		onGBPage().table().forecactViewHeaders().convert(i -> i.getAttribute("innerHTML")).should(
				everyItem(matchesPattern("Q(1|2|3|4) \\d{4}<span class=\"fcvRecordCnt\"> \\(\\d+\\)<\\/span>")));
		return this;
	}

	@Step
	public GBSteps shouldSeeQuaterHeader(String header) {
		String quater = com.gridbuddy.utils.StringUtils.getByRegex("Q(1|2|3|4)", header);
		String count = com.gridbuddy.utils.StringUtils.getByRegex("\\((\\d+)\\)", header);
		String year = com.gridbuddy.utils.StringUtils.getByRegex("(\\d{4})", header);
		onGBPage().table().forecactViewHeaders().filter(i -> {
			Pattern p = Pattern
					.compile("Q" + quater + " " + year + "<span class=\"fcvRecordCnt\"> \\(" + count + "\\)<\\/span>");
			return p.matcher(i.getAttribute("innerHTML")).find();
		}).should(hasSize(1));
		return this;
	}

	@Step
	public GBSteps shouldSeeForecastViewByMonthHeaders() {
		onGBPage().table().forecactViewHeaders().stream().map(i -> i.getAttribute("data-ufval"))
				.forEach(i -> LocalDate.parse(i + "-01", DateTimeFormatter.ofPattern("yyyy-MM-dd")));
		return this;
	}

	@Step
	public GBSteps editForecastViewInputField(int row, int cell, String field, String value) {
		HtmlElement f = onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).inputField(field)
				.should(isDisplayed());
		f.clear();
		f.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editForecastViewSelectField(int row, int cell, String field, String value) {
		onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).div(field).should(isDisplayed())
				.click();
		onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).selectField(field).select(value);
		onGBPage().table().forecastViewRecords().get(row).fields().get(0).click();
		return this;
	}

	@Step
	public GBSteps transposedGridTextFieldShouldHaveWidth(int row, int cell, String field, String width) {
		onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).div(field).should(isDisplayed())
				.should(hasCssValue("width", width));
		return this;
	}

	@Step
	public GBSteps shouldSeeActionsWidget(int row, int cell) {
		onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).cogIcon().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps switchToEmbedGrid() {
		wait(15000);
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps shouldSeeGridName(String title) {
		onGBPage().gridTitle().should(isDisplayed(30)).should(hasText(title));
		return this;
	}

	@Step
	public GBSteps recordsShouldBeSortedByInputFieldIgnoringCase(String field, String direction) {
		switch (direction) {
		case "Ascending":
			assertThat(onGBPage().table().records().stream().map(i -> i.inputField(field).getAttribute("value"))
					.collect(Collectors.toList()), isSortedAscendingIgnoringCase());
			break;
		case "Descending":
			assertThat(onGBPage().table().records().stream().map(i -> i.inputField(field).getAttribute("value"))
					.collect(Collectors.toList()), isSortedDescendingIgnoringCase());
			break;
		}
		return this;
	}

	@Step
	public GBSteps recordsShouldBeSortedByTextFieldIgnoringCase(String field, String direction) {
		switch (direction) {
		case "Ascending":
			assertThat(onGBPage().table().records().stream().map(i -> i.field(field).getText())
					.collect(Collectors.toList()), isSortedAscendingIgnoringCase());
			break;
		case "Descending":
			assertThat(onGBPage().table().records().stream().map(i -> i.field(field).getText())
					.collect(Collectors.toList()), isSortedDescendingIgnoringCase());
			break;
		}
		return this;
	}

	@Step
	public GBSteps recordsShouldBeSortedByTextField(String field, String direction) {
		switch (direction) {
		case "Ascending":
			assertThat(onGBPage().table().records().stream().map(i -> i.field(field).getText())
					.collect(Collectors.toList()), isSortedAscending());
			break;
		case "Descending":
			assertThat(onGBPage().table().records().stream().map(i -> i.field(field).getText())
					.collect(Collectors.toList()), isSortedDescending());
			break;
		}
		return this;
	}

	@Step
	public GBSteps cloneFilter() {
		onGBPage().manageFiltersBlock().clone().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps editFilter(String filterName) {
		onGBPage().manageFiltersBlock().myFilter().should(isDisplayed()).select(filterName);
		return this;
	}

	@Step
	public GBSteps deleteFilter() {
		onGBPage().manageFiltersBlock().delete().should(isDisplayed()).click();
		getDriver().switchTo().alert().accept();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps deleteFilter(String filterName) {
		if (onGBPage().myFilter().getOptions().contains(filterName)) {
			onGBPage().filter().should(isDisplayed()).click();
			onGBPage().manageFiltersBlock().myFilter().should(isDisplayed()).select(filterName);
			onGBPage().manageFiltersBlock().delete().should(isDisplayed()).click();
			getDriver().switchTo().alert().accept();
		}
		switchToNewFrame();
		return this;
	}

	@Step
	public String getCellValue(int row, String column) {
		return onGBPage().table().records().get(row).inputField(column).getAttribute("value");
	}

	@Step
	public String getCellText(int row, String column) {
		return onGBPage().table().records().get(row).field(column).getText();
	}

	@Step
	public GBSteps shouldSeeGroupingResults(int groupLevelNumber, String groupField) {
		onGBPage().table().groupedResults(groupLevelNumber).get(0).groupField(groupLevelNumber).get(0)
				.should(hasText(containsString(groupField)));
		return this;
	}

	@Step
	public GBSteps shouldSeeGroupingFieldDiv(int groupLevelNumber, String groupField) {
		String fieldName = onGBPage().table().groupedResults(groupLevelNumber).get(0).groupField(groupLevelNumber)
				.get(0).groupFieldName().getText().replaceAll(" \\(\\d*\\)", "");
		List<WebElement> results = onGBPage().table().groupedResults(groupLevelNumber).stream()
				.map(i -> i.block(groupField)).collect(Collectors.toList());
		assertThat(results, everyItem(hasText(fieldName)));
		return this;
	}

	@Step
	public GBSteps shouldSeeGroupingFieldInput(int groupLevelNumber, String groupField) {
		String fieldName = onGBPage().table().groupedResults(groupLevelNumber).get(0).groupField(groupLevelNumber)
				.get(0).groupFieldName().getText().replaceAll(" \\(\\d*\\)", "");
		List<WebElement> results = onGBPage().table().groupedResults(groupLevelNumber).stream()
				.map(i -> i.inputField(groupField)).collect(Collectors.toList());
		assertThat(results, everyItem(hasAttribute("value", fieldName)));
		return this;
	}

	@Step
	public GBSteps setGroupByCondition(String object, int row, String field, String sortBy, String order) {
		int rowSize = onGBPage().manageFiltersBlock().objectFilter(object).should(isDisplayed()).groupByFieldRows()
				.size();
		while (rowSize <= row) {
			onGBPage().manageFiltersBlock().objectFilter(object).addGroupingCondition().should(isDisplayed()).click();
			rowSize = onGBPage().manageFiltersBlock().objectFilter(object).groupByFieldRows().size();
		}

		onGBPage().manageFiltersBlock().objectFilter(object).should(isDisplayed()).groupByFieldRows().get(row)
				.groupedField().select(field);
		onGBPage().manageFiltersBlock().objectFilter(object).should(isDisplayed()).groupByFieldRows().get(row).sortBy()
				.select(sortBy);
		onGBPage().manageFiltersBlock().objectFilter(object).should(isDisplayed()).groupByFieldRows().get(row)
				.sortDirection().select(order);
		return this;
	}

	@Step
	public GBSteps setGroupByCondition(String object, int row, String field) {
		int rowSize = onGBPage().manageFiltersBlock().objectFilter(object).groupByFieldRows().size();
		while (rowSize <= row) {
			onGBPage().manageFiltersBlock().objectFilter(object).addGroupingCondition().should(isDisplayed()).click();
			rowSize = onGBPage().manageFiltersBlock().objectFilter(object).groupByFieldRows().size();
		}
		onGBPage().manageFiltersBlock().objectFilter(object).should(isDisplayed()).groupByFieldRows().get(row)
				.groupedField().select(field);
		return this;
	}

	@Step
	public GBSteps everyParentObjectShouldHaveRelatedObjects() {
		onGBPage().table().records().forEach(i -> {
			i.relatedRecords().should(hasSize(greaterThan(0)));
		});
		return this;
	}

	@Step
	public GBSteps everyParentObjectShouldNotHaveRelatedObjects() {
		onGBPage().table().records().forEach(i -> {
			i.noChildRecordsFound().should(isDisplayed());
		});
		return this;
	}

	@Step
	public GBSteps confirmAlertAndClosePopup(String alert) {
		switchToNewFrame();
		onGBPage().closeButton().should(isDisplayed()).click();
		shouldSeeAlert(alert).switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps clickTab(String tab) {
		getDriver().switchTo().defaultContent();
		currentFrameId = onGBPage().iframe().getAttribute("id");
		getDriver().switchTo().frame(currentFrameId);
		onGBPage().tab(tab).should(isDisplayed()).click();
		currentFrameId = "";
		wait(5000);
		switchToNewFrame();
		return new GBSteps(getDriver(), mainWindowHandle, currentFrameId);
	}

	@Step
	public GBSteps clickTab(String tab, int tabIndex) {
		getDriver().switchTo().defaultContent();
		currentFrameId = onGBPage().iframe().getAttribute("id");
		getDriver().switchTo().frame(currentFrameId);
		onGBPage().tab(tab).should(isDisplayed()).click();
		currentFrameId = "";
		wait(5000);
		switchToNewFrame(tabIndex);
		return new GBSteps(getDriver(), mainWindowHandle, currentFrameId);
	}

	public Double getParentRecordsFieldsSum(String field) {
		return onGBPage().table().records().stream().mapToDouble(i -> {
			try {
				return Double.parseDouble(i.inputField(field).getAttribute("value").replaceAll(",", ""));
			} catch (Exception e) {
				return 0;
			}
		}).sum();
	}

	@Step
	public GBSteps parentRecordSelectFieldShouldHaveItems(int parentRow, String field, String... items) {
		onGBPage().table().records().get(parentRow).field(field).findElement(By.xpath("..")).click();
		for (String item : items) {
			onGBPage().table().records().get(parentRow).select(field).shouldHaveOption(item);
		}
		return this;
	}

	@Step
	public GBSteps parentRecordSelectFieldShouldHaveItemSelected(int parentRow, String field, String item) {
		onGBPage().table().records().get(parentRow).select(field).shouldHaveValueSelected(item);
		return this;
	}

	@Step
	public GBSteps parentRecordSelectFieldShouldNotHaveItems(int parentRow, String field, String... items) {
		onGBPage().table().records().get(parentRow).field(field).findElement(By.xpath("..")).click();
		for (String item : items) {
			onGBPage().table().records().get(parentRow).select(field).shouldNotHaveOption(item);
		}
		return this;
	}

	@Step
	public GBSteps editMassCreateInputField(String field, String value) {
		HtmlElement el = onGBPage().massCreatePopup().parentInputField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editMassCreateInputField(String object, String field, String value) {
		HtmlElement el = onGBPage().massCreatePopup().childRecord(object).inputField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editMassCreateTextareaField(String field, String value) {
		HtmlElement el = onGBPage().massCreatePopup().parentTextareaField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editMassCreateTextareaField(String object, String field, String value) {
		HtmlElement el = onGBPage().massCreatePopup().childRecord(object).textareaField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editMassCreateSelectField(String field, String value) {
		onGBPage().massCreatePopup().parentSelectField(field).select(value);
		return this;
	}

	@Step
	public GBSteps editMassCreateSelectField(String object, String field, String value) {
		onGBPage().massCreatePopup().childRecord(object).selectField(field).select(value);
		return this;
	}

	@Step
	public GBSteps editMassCreateDateField(String field, String value) {
		HtmlElement el = onGBPage().massCreatePopup().parentDateField(field);
		el.clear();
		el.sendKeys(value);
		onGBPage().massCreatePopup().numberOfRecords().click();
		return this;
	}

	@Step
	public GBSteps editMassCreateTimeField(String field, String value) {
		HtmlElement el = onGBPage().massCreatePopup().parentTimeField(field);
		el.clear();
		el.sendKeys(value);
		return this;
	}

	@Step
	public GBSteps enterNumberOfRecords(int number) {
		HtmlElement el = onGBPage().massCreatePopup().numberOfRecords();
		el.clear();
		el.sendKeys(String.valueOf(number));
		return this;
	}

	@Step
	public GBSteps clickCreateParentRecordsButton() {
		onGBPage().massCreatePopup().massCreateParents().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps clickCreateRelated() {
		onGBPage().massCreatePopup().massCreateChild().should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps forecastViewCurrencyFieldMatchingCriteriaShouldHaveColorBackground(String field,
			Matcher<Double> matcher, String color) {
		List<WebElement> fields = onGBPage().table().forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream).filter(i -> {
					String attr = i.inputField(field).getAttribute("value");
					if ("".equals(attr)) {
						return false;
					} else {
						return matcher.matches(Double.parseDouble(attr.replaceAll(",", "")));
					}
				}).map(i -> i.inputField(field).findElement(By.xpath("./ancestor::div[contains(@class,'pgc')]")))
				.collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("background-color", color)));
		return this;
	}

	@Step
	public GBSteps forecastViewCurrencyFieldMatchingCriteriaShouldHaveTextColor(String field, Matcher<Double> matcher,
			String color) {
		List<WebElement> fields = onGBPage().table().forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream).filter(i -> {
					String attr = i.inputField(field).getAttribute("value");
					if ("".equals(attr)) {
						return false;
					} else {
						return matcher.matches(Double.parseDouble(attr.replaceAll(",", "")));
					}
				}).map(i -> i.inputField(field)).collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("color", color)));
		return this;
	}

	@Step
	public GBSteps forecastViewCurrencyFieldMatchingCriteriaShouldHaveFontStyle(String field, Matcher<Double> matcher,
			String color) {
		List<WebElement> fields = onGBPage().table().forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream).filter(i -> {
					String attr = i.inputField(field).getAttribute("value");
					if ("".equals(attr)) {
						return false;
					} else {
						return matcher.matches(Double.parseDouble(attr.replaceAll(",", "")));
					}
				}).map(i -> i.inputField(field)).collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("font-style", color)));
		return this;
	}

	@Step
	public GBSteps forecastViewTextFieldMatchingCriteriaShouldHaveColorBackground(String field, Matcher<String> matcher,
			String color) {
		List<WebElement> fields = onGBPage().table().forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream)
				.filter(i -> matcher.matches(i.textField(field).getText()))
				.map(i -> i.textField(field).findElement(By.xpath(".."))).collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("background-color", color)));
		return this;
	}

	@Step
	public GBSteps forecastViewCellWithTextFieldMatchingCriteriaShouldHaveBorderStyle(String field,
			Matcher<String> matcher, String color) {
		List<WebElement> fields = onGBPage().table().forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream)
				.filter(i -> matcher.matches(i.textField(field).getText())).collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("border", color)));
		return this;
	}

	@Step
	public GBSteps forecastViewCellsShouldHaveTitleEqualToText(String field) {
		List<WebElement> fields = onGBPage().table().forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream).filter(i -> !i.textField(field).getText()
						.trim().equals(i.textField(field).getAttribute("title").trim()))
				.collect(Collectors.toList());
		assertThat(fields, hasSize(0));
		return this;
	}

	@Step
	public GBSteps forecastViewTextFieldMatchingCriteriaShouldHaveTextColor(String field, Matcher<String> matcher,
			String color) {
		List<WebElement> fields = onGBPage().table().forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream)
				.filter(i -> matcher.matches(i.textField(field).getText())).map(i -> i.textField(field))
				.collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("color", color)));
		return this;
	}

	@Step
	public GBSteps forecastViewCellWithTextFieldMatchingCriteriaShouldHaveTextColor(String field,
			Matcher<String> matcher, String color) {
		List<WebElement> fields = onGBPage().table().forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream)
				.filter(i -> matcher.matches(i.textField(field).getText())).collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("color", color)));
		return this;
	}

	@Step
	public GBSteps forecastViewTextFieldMatchingCriteriaShouldHaveFontStyle(String field, Matcher<String> matcher,
			String color) {
		List<WebElement> fields = onGBPage().table().forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream)
				.filter(i -> matcher.matches(i.textField(field).getText())).map(i -> i.textField(field))
				.collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("font-style", color)));
		return this;
	}

	@Step
	public GBSteps forecastViewCellWithTextFieldMatchingCriteriaShouldHaveFontStyle(String field,
			Matcher<String> matcher, String color) {
		List<WebElement> fields = onGBPage().table().forecastViewRecords().stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream)
				.filter(i -> matcher.matches(i.textField(field).getText())).collect(Collectors.toList());
		assertThat(fields, everyItem(hasCssValue("font-style", color)));
		return this;
	}

	@Step
	public GBSteps forecastViewFieldShouldHaveColorBackground(int row, int cell, String field, String color) {
		onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).div(field)
				.should(hasCssValue("background-color", color));
		return this;
	}

	@Step
	public GBSteps forecastViewSelectShouldHaveFontStyle(int row, int cell, String field, String color) {
		onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).selectField(field)
				.should(hasCssValue("font-style", color));
		return this;
	}

	@Step
	public GBSteps forecastViewSelectShouldHaveTextColor(int row, int cell, String field, String color) {
		onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).selectField(field)
				.should(hasCssValue("color", color));
		return this;
	}

	@Step
	public GBSteps shouldSeeDataCardIcon() {
		onGBPage().table().records().get(0).dataCardIcon().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeChildObjectDataCardIcon() {
		onGBPage().table().records().get(0).relatedRecord().records().get(0).dataCardIcon().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeForecastField(String field) {
		onGBPage().table().forecastViewRecords().get(0).field(field).should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeAutocompleteWidget(int row, int cell) {
		onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).showAllLookup().click();
		onGBPage().table().autocompleteBlock().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeAutocompleteWidget() {
		onGBPage().table().autocompleteBlock().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeValidAutocompleteOptions(int row, String optionSubstring) {
		String[] keyvords = optionSubstring.split(" ");
		List<String> list = onGBPage().autocompleteWidget().get(row).labels().should(hasSize(greaterThan(0))).stream()
				.map(i -> i.getAttribute("innerHTML")).collect(Collectors.toList());
		Iterable<Matcher<? super String>> matchers = Arrays.asList(keyvords).stream().map(i -> containsString(i))
				.collect(Collectors.toList());
		assertThat(list, everyItem(anyOf(matchers)));
		return this;
	}

	@Step
	public GBSteps shouldSeeSavedAutocompleteOption(int row, int cell, String field, String value) {
		assertThat(onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).inputField(field)
				.getAttribute("value"), containsString(value));
		return this;
	}

	@Step
	public GBSteps setAutocompleteOption(int option) {
		onGBPage().table().autocompleteBlock().should(isDisplayed()).autocompleteItems().should(hasSize(greaterThan(0)))
				.get(option).click();
		return this;
	}

	@Step
	public GBSteps shouldSeeEmptyAutocompleteWidget(int row) {
		onGBPage().autocompleteWidget().get(FIRST_ROW).should(isDisplayed())
				.should(hasText(containsString("(no matches found)")));
		return this;
	}

	@Step
	public GBSteps clickColumnHeader(String column) {
		onGBPage().table().should(isDisplayed()).records().get(0).topHeaders()
				.filter(i -> i.name().getText().toLowerCase().contains(column.toLowerCase())).get(0).click();
		return this;
	}

	@Step
	public GBSteps selectAllRecords() {
		onGBPage().table().selectAll().should(isDisplayed()).setChecked(true);
		return this;
	}

	@Step
	public GBSteps shouldSeeFieldActionsPopup() {
		onGBPage().fastFilter().operator().should(isDisplayed());
		onGBPage().fastFilter().filterValue().should(isDisplayed());
		onGBPage().fastFilter().apply().should(isDisplayed());
		onGBPage().fastFilter().sortAscending().should(isDisplayed());
		onGBPage().fastFilter().sortDescending().should(isDisplayed());
		onGBPage().fastFilter().resetSorting().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps applyForecastViewFieldFilter(String field, String operator, String value) {
		onGBPage().table().forecastViewHeaderRowCogIcon().should(isDisplayed());
		onGBPage().table().should(isDisplayed()).forecastViewRecords().get(0).cogIcon(field).should(isDisplayed())
				.click();
		onGBPage().fastFilter().operator().should(isDisplayed()).select(operator);
		try {
			onGBPage().fastFilter().filterValue().should(isDisplayed()).clear();
		} catch (Exception e) {
		}
		onGBPage().fastFilter().filterValue().should(isDisplayed()).sendKeys(value);
		try {
			wait(1000);
			onGBPage().table().autocompleteBlock().autocompleteItems().get(0).click();
		} catch (Exception e) {
		}
		onGBPage().fastFilter().apply().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps selectForecastViewFieldFilterAndCloseActionsPopup(String field, String operator, String value) {
		onGBPage().table().forecastViewHeaderRowCogIcon().should(isDisplayed());
		onGBPage().table().should(isDisplayed()).forecastViewRecords().get(0).cogIcon(field).should(isDisplayed())
				.click();
		onGBPage().fastFilter().operator().should(isDisplayed()).select(operator);
		try {
			onGBPage().fastFilter().filterValue().should(isDisplayed()).clear();
		} catch (Exception e) {
		}
		onGBPage().fastFilter().filterValue().should(isDisplayed()).sendKeys(value);
		try {
			wait(1000);
			onGBPage().table().autocompleteBlock().autocompleteItems().get(0).click();
		} catch (Exception e) {
		}
		onGBPage().fastFilter().closePopup().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps forecastRecordNumberFieldShouldMetCriteria(String field, Matcher<Double> matcher) {
		List<Double> values = onGBPage().table().forecastViewRecords().should(hasSize(greaterThan(0))).stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream)
				.map(i -> Double.parseDouble(i.inputField(field).getAttribute("value").replaceAll(",", "")))
				.collect(Collectors.toList());
		assertThat(values, everyItem(matcher));
		return this;
	}

	@Step
	public GBSteps forecastRecordInputFieldShouldMetCriteria(String field, Matcher<String> matcher) {
		List<String> values = onGBPage().table().forecastViewRecords().should(hasSize(greaterThan(0))).stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream)
				.map(i -> i.inputField(field).getAttribute("value")).collect(Collectors.toList());
		assertThat(values, everyItem(matcher));
		return this;
	}

	@Step
	public GBSteps forecastRecordCheckboxShouldBeChecked(String field) {
		List<WebElement> values = onGBPage().table().forecastViewRecords().should(hasSize(greaterThan(0))).stream()
				.flatMap(i -> Stream.of(i.dataCells())).flatMap(List::stream).map(i -> i.checkedCheckbox(field))
				.collect(Collectors.toList());
		assertThat(values, everyItem(isDisplayed()));
		return this;
	}

	@Step
	public GBSteps sortForecastViewAscending(String field) {
		onGBPage().table().forecastViewHeaderRowCogIcon().should(isDisplayed());
		onGBPage().table().should(isDisplayed()).forecastViewRecords().get(0).cogIcon(field).should(isDisplayed())
				.click();
		onGBPage().fastFilter().sortAscending().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps sortForecastViewDescending(String field) {
		onGBPage().table().forecastViewHeaderRowCogIcon().should(isDisplayed());
		onGBPage().table().should(isDisplayed()).forecastViewRecords().get(0).cogIcon(field).should(isDisplayed())
				.click();
		onGBPage().fastFilter().sortDescending().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps resetForecastViewSorting(String field) {
		onGBPage().table().forecastViewHeaderRowCogIcon().should(isDisplayed());
		onGBPage().table().should(isDisplayed()).forecastViewRecords().get(0).cogIcon(field).should(isDisplayed())
				.click();
		onGBPage().fastFilter().resetSorting().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps clearForecastViewFilter(String field) {
		onGBPage().table().forecastViewHeaderRowCogIcon().should(isDisplayed());
		onGBPage().table().should(isDisplayed()).forecastViewRecords().get(0).cogIcon(field).should(isDisplayed())
				.click();
		onGBPage().fastFilter().clearButton().should(isDisplayed()).click();
		switchToNewFrame();
		return this;
	}

	@Step
	public GBSteps forecastViewRecordsShouldBeSortedByDateField(String field, String direction) {
		for (int i = 0; i < onGBPage().table().forecactViewHeaders().size(); i++) {
			List<ForecastViewRecord> records = onGBPage().table().forecastViewRecords().should(hasSize(greaterThan(0)));
			List<LocalDate> values = new ArrayList<>();
			for (ForecastViewRecord forecastViewRecord : records) {
				try {
					values.add(LocalDate.parse(
							forecastViewRecord.dataCells().get(i).inputField(field).getAttribute("value"),
							DateTimeFormatter.ofPattern("MM/dd/yyyy")));
				} catch (IndexOutOfBoundsException e) {
				}
			}
			switch (direction) {
			case "Ascending":
				assertThat(values, isDateCollectionSortedAscending());
				break;
			case "Descending":
				assertThat(values, isDateCollectionSortedDescending());
				break;
			}
		}
		return this;
	}

	@Step
	public GBSteps forecastViewRecordsShouldNotBeSortedByDateField(String field, String direction) {
		for (int i = 0; i < onGBPage().table().forecactViewHeaders().size(); i++) {
			List<ForecastViewRecord> records = onGBPage().table().forecastViewRecords().should(hasSize(greaterThan(0)));
			List<LocalDate> values = new ArrayList<>();
			for (ForecastViewRecord forecastViewRecord : records) {
				try {
					values.add(LocalDate.parse(
							forecastViewRecord.dataCells().get(i).inputField(field).getAttribute("value"),
							DateTimeFormatter.ofPattern("MM/dd/yyyy")));
				} catch (IndexOutOfBoundsException e) {
				}
			}
			switch (direction) {
			case "Ascending":
				if (values.size() > 1) {
					assertThat(values, not(isDateCollectionSortedAscending()));
				}
				break;
			case "Descending":
				if (values.size() > 1) {
					assertThat(values, not(isDateCollectionSortedDescending()));
				}
				break;
			}
		}
		return this;
	}

	@Step
	public GBSteps shouldSeeTheNumberOfRecordsInColumnHeaders() {
		for (int i = 0; i < onGBPage().table().forecactViewHeaders().size(); i++) {
			List<ForecastViewRecord> records = onGBPage().table().forecastViewRecords().should(hasSize(greaterThan(0)));
			List<ForecastViewCell> values = new ArrayList<>();
			for (ForecastViewRecord forecastViewRecord : records) {
				try {
					ForecastViewCell c = forecastViewRecord.cells().get(i);
					if (!"".equals(c.getAttribute("id"))) {
						values.add(forecastViewRecord.cells().get(i));
					}
				} catch (IndexOutOfBoundsException e) {
				}
			}
			assertThat(values, hasSize(Integer.parseInt(com.gridbuddy.utils.StringUtils.getByRegex("\\((\\d+)\\)",
					onGBPage().table().forecactViewHeaders().get(i).getAttribute("innerHTML")))));
		}
		return this;
	}

	@Step
	public GBSteps clearForecastViewCondition(String object, int row) {
		onGBPage().manageFiltersBlock().objectFilter(object).forecastVeiwRows().get(row).clearFilter()
				.should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps shouldSeeEmptyForecastViewColumns() {
		onGBPage().table().forecactViewHeaders().filter(i -> {
			Pattern p = Pattern.compile("<span class=\"fcvRecordCnt\"> \\(0\\)<\\/span>");
			return p.matcher(i.getAttribute("innerHTML")).find();
		}).should(hasSize(greaterThan(0)));
		return this;
	}

	@Step
	public GBSteps shouldNotSeeEmptyForecastViewColumns() {
		onGBPage().table().forecactViewHeaders().filter(i -> {
			Pattern p = Pattern.compile("<span class=\"fcvRecordCnt\"> \\(0\\)<\\/span>");
			return p.matcher(i.getAttribute("innerHTML")).find();
		}).should(hasSize(0));
		return this;
	}

	@Step
	public GBSteps shouldSeeForecatViewColumnRecordsCount(String columnName, int expected) {
		String column = onGBPage().table().forecactViewHeaders().convert(i -> i.getAttribute("innerHTML"))
				.filter(i -> i.contains(columnName)).get(0);
		assertThat(Integer.parseInt(com.gridbuddy.utils.StringUtils.getByRegex("\\((\\d+)\\)", column)),
				equalTo(expected));
		return this;
	}

	@Step
	public GBSteps editForecastViewDetailPanelInputField(String field, String value) {
		onGBPage().detailPanel().should(isDisplayed()).inputField(field).should(isDisplayed()).clear();
		onGBPage().detailPanel().should(isDisplayed()).inputField(field).should(isDisplayed()).sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editForecastViewDetailPanelTextareaField(String field, String value) {
		onGBPage().detailPanel().should(isDisplayed()).textareaField(field).should(isDisplayed()).clear();
		onGBPage().detailPanel().should(isDisplayed()).textareaField(field).should(isDisplayed()).sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editForecastViewDetailPanelDateField(String field, String value) {
		onGBPage().detailPanel().should(isDisplayed()).dateField(field).should(isDisplayed()).clear();
		onGBPage().detailPanel().should(isDisplayed()).dateField(field).should(isDisplayed()).sendKeys(value);
		return this;
	}

	@Step
	public GBSteps editForecastViewDetailPanelSelectField(String field, String value) {
		onGBPage().detailPanel().should(isDisplayed()).selectField(field).should(isDisplayed()).select(value);
		return this;
	}

	@Step
	public GBSteps forecastViewDetailPanelInputFieldShoudHaveBackgroundColor(String field, String color) {
		onGBPage().detailPanel().should(isDisplayed()).inputField(field).should(hasCssValue("background-color", color));
		return this;
	}

	@Step
	public GBSteps forecastViewDetailPanelSelectFieldShoudHaveBackgroundColor(String field, String color) {
		onGBPage().detailPanel().should(isDisplayed()).selectField(field)
				.should(hasCssValue("background-color", color));
		return this;
	}

	@Step
	public GBSteps forecastViewDetailPanelTextareaFieldShoudHaveBackgroundColor(String field, String color) {
		onGBPage().detailPanel().should(isDisplayed()).textareaField(field)
				.should(hasCssValue("background-color", color));
		return this;
	}

	@Step
	public GBSteps forecastViewDetailPanelShoudHaveBackgroundColor(String color) {
		onGBPage().detailPanel().detailPanelBody().should(isDisplayed()).should(hasCssValue("background-color", color));
		return this;
	}

	@Step
	public GBSteps forecastViewDataCellShoudHaveBackgroundColor(int row, int cell, String color) {
		assertThat(onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).findElement(By.xpath("..")),
				hasCssValue("background-color", color));
		return this;
	}

	@Step
	public GBSteps shouldSeeForecastViewDetailPanel() {
		onGBPage().detailPanel().should(isDisplayed());
		return this;
	}

	@Step
	public GBSteps shouldSeeForecastViewDetailPanelInputFieldValue(String field, Matcher<String> value) {
		onGBPage().detailPanel().should(isDisplayed()).inputField(field).should(isDisplayed())
				.should(hasAttribute("value", value));
		return this;
	}

	@Step
	public GBSteps shouldSeeForecastViewDetailPanelTextFieldValue(String field, Matcher<String> value) {
		onGBPage().detailPanel().should(isDisplayed()).textField(field).should(isDisplayed()).should(hasText(value));
		return this;
	}

	@Step
	public GBSteps shouldSeeForecastViewDetailPanelTextFieldValue(String field, String value) {
		return shouldSeeForecastViewDetailPanelTextFieldValue(field, equalTo(value));
	}

	@Step
	public GBSteps shouldSeeForecastViewDetailPanelTextareaFieldValue(String field, Matcher<String> value) {
		onGBPage().detailPanel().should(isDisplayed()).textareaField(field).should(isDisplayed())
				.should(hasAttribute("value", value));
		return this;
	}

	@Step
	public GBSteps shouldSeeForecastViewDetailPanelDateFieldValue(String field, Matcher<String> value) {
		onGBPage().detailPanel().should(isDisplayed()).dateField(field).should(isDisplayed())
				.should(hasAttribute("value", value));
		return this;
	}

	@Step
	public GBSteps shouldSeeForecastViewDetailPanelSelectFieldValueSelected(String field, String value) {
		onGBPage().detailPanel().should(isDisplayed()).selectField(field).should(isDisplayed())
				.shouldHaveValueSelected(value);
		return this;
	}

	@Step
	public GBSteps selectForecastViewCellAction(int row, int cell, String action) {
		onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).cogIcon().should(isDisplayed()).click();
		onGBPage().action(action).should(isDisplayed()).click();
		return this;
	}

	@Step
	public GBSteps openForecastViewCellActionsMenu(int row, int cell) {
		onGBPage().table().forecastViewRecords().get(row).dataCells().get(cell).cogIcon().should(isDisplayed()).click();
		return this;
	}

	public String getForecastViewDetailPanelInputFieldValue(String field) {
		return onGBPage().detailPanel().should(isDisplayed()).inputField(field).should(isDisplayed())
				.getAttribute("value");
	}

	public String getForecastViewDetailPanelTextareaFieldValue(String field) {
		return onGBPage().detailPanel().should(isDisplayed()).textareaField(field).should(isDisplayed())
				.getAttribute("value");
	}

	public LocalDate getForecatViewQuater(int column) {
		String emptyQuarter = onGBPage().table().forecactViewHeaders().get(column).getAttribute("innerHTML");
		String quarter = com.gridbuddy.utils.StringUtils.getByRegex("Q(1|2|3|4)", emptyQuarter);
		String year = com.gridbuddy.utils.StringUtils.getByRegex("(\\d{4})", emptyQuarter);
		String string = "";
		switch (quarter) {
		case "1":
			string = "01/01/" + year;
			break;
		case "2":
			string = "04/01/" + year;
			break;
		case "3":
			string = "07/01/" + year;
			break;
		case "4":
			string = "10/01/" + year;
			break;
		}
		return LocalDate.parse(string, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
	}

	public int getForecatViewColumnRecordsCount(int column) {
		String quarter = onGBPage().table().forecactViewHeaders().get(column).getAttribute("innerHTML");
		return Integer.parseInt(com.gridbuddy.utils.StringUtils.getByRegex("\\((\\d+)\\)", quarter));
	}

	public int getForecatViewColumnRecordsCount(String columnName) {
		String column = onGBPage().table().forecactViewHeaders().convert(i -> i.getAttribute("innerHTML"))
				.filter(i -> i.contains(columnName)).get(0);
		return Integer.parseInt(com.gridbuddy.utils.StringUtils.getByRegex("\\((\\d+)\\)", column));
	}

	public LocalDate getEmptyForecatViewQuater() {
		String emptyQuarter = onGBPage().table().forecactViewHeaders().filter(i -> {
			Pattern p = Pattern.compile("Q(1|2|3|4) \\d{4}<span class=\"fcvRecordCnt\"> \\(0\\)<\\/span>");
			return p.matcher(i.getAttribute("innerHTML")).find();
		}).get(0).getAttribute("innerHTML");
		String quarter = com.gridbuddy.utils.StringUtils.getByRegex("Q(1|2|3|4)", emptyQuarter);
		String year = com.gridbuddy.utils.StringUtils.getByRegex("(\\d{4})", emptyQuarter);
		String string = "";
		switch (quarter) {
		case "1":
			string = "01/01/" + year;
			break;
		case "2":
			string = "04/01/" + year;
			break;
		case "3":
			string = "07/01/" + year;
			break;
		case "4":
			string = "10/01/" + year;
			break;
		}
		return LocalDate.parse(string, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
	}

	public GBSteps switchToGrid() {
		switchToNewFrame();
		return this;
	}

	public String getAutocompleteOption(int row, int option) {
		return onGBPage().autocompleteWidget().get(row).labels().get(option).getAttribute("innerHTML");
	}

	public String getParentObjectInputFieldValue(int row, String field) {
		return onGBPage().table().records().get(row).field(field).getAttribute("value");
	}

	public LocalDateTime getParentObjectDateTimeValue(int row, String field) {
		return LocalDateTime.parse(
				onGBPage().table().records().get(row).dateField(field).getAttribute("value") + " "
						+ onGBPage().table().records().get(row).timeField(field).getAttribute("value"),
				DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a"));
	}

	public LocalDateTime getRelatedObjectDateTimeValue(int parentRow, int childRow, String field) {
		return LocalDateTime.parse(
				onGBPage().table().records().get(parentRow).relatedRecord().records().get(childRow).dateField(field)
						.getAttribute("value")
						+ " "
						+ onGBPage().table().records().get(parentRow).relatedRecord().records().get(childRow)
								.timeField(field).getAttribute("value"),
				DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a"));
	}

	public List<String> getChildObjectsInputFieldValues(int row, String field) {
		return onGBPage().table().records().get(row).relatedRecord().records()
				.convert(i -> i.inputField(field).getAttribute("value"));
	}

	public int getRecordsCount() {
		return onGBPage().table().records().size();
	}

	public String getPageUrl() {
		return getDriver().getCurrentUrl();
	}

	private GBPage onGBPage() {
		return on(GBPage.class);
	}

	public Double getSummaryValue(String columnName) {
		return Double.parseDouble(
				onGBPage().table().summaryField(columnName).should(isDisplayed()).getText().replaceAll("[^\\d.]", ""));
	}

}
