package com.gridbuddy.steps;

import static com.gridbuddy.matchers.HasTextMatcher.hasText;
import static com.gridbuddy.matchers.IsCollectionHasSize.hasSize;
import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;

import com.gridbuddy.blocks.ActionRecord;
import com.gridbuddy.pages.ManageActionsPage;

import io.qameta.allure.Step;

public class ManageActionsSteps extends SFPageSteps {

    public ManageActionsSteps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

    @Step
    public ManageActionsSteps clickNewButton() {
        onActionsPage().topRowButton("New").should(isDisplayed()).click();
        return this;
    }

    @Step
    public ManageActionsSteps shouldNotSeeCongaOptionsInContentSourceDropdown() {
        onActionsPage().editActionBlock().contentSource().shouldNotHaveOption("Conga Composer");
        onActionsPage().editActionBlock().contentSource().shouldNotHaveOption("Conga Conductor");
        return this;
    }

    @Step
    public ManageActionsSteps shouldSeeContentSourceOption(String option) {
        onActionsPage().editActionBlock().contentSource().shouldHaveOption(option);
        return this;
    }

    @Step
    public ManageActionsSteps selectType(String type) {
        onActionsPage().editActionBlock().type().should(isDisplayed()).select(type);
        return this;
    }

    @Step
    public ManageActionsSteps shouldSeeContentSourceOptions() {
        assertThat(onActionsPage().editActionBlock().contentSource().getOptions(), equalTo(Arrays
                .asList("--Select Content Source--", "Button", "Formula Field", "URL", "Grid", "Visualforce Page")));
        return this;
    }

    @Step
    public ManageActionsSteps checkCreateActionFields() {
        onActionsPage().editActionBlock().actionName().should(isDisplayed());
        onActionsPage().editActionBlock().actionDescription().should(isDisplayed());
        onActionsPage().editActionBlock().object().should(isDisplayed());
        onActionsPage().editActionBlock().type().should(isDisplayed());
        onActionsPage().editActionBlock().location().should(isDisplayed());
        onActionsPage().editActionBlock().behavior().should(isDisplayed());
        onActionsPage().editActionBlock().overlayHeight().should(isDisplayed());
        onActionsPage().editActionBlock().overlayWidht().should(isDisplayed());
        onActionsPage().editActionBlock().topPosition().should(isDisplayed());
        onActionsPage().editActionBlock().leftPosition().should(isDisplayed());
        onActionsPage().editActionBlock().content().should(isDisplayed());
        onActionsPage().editActionBlock().contentSource().should(isDisplayed());
        onActionsPage().editActionBlock().idParameter().should(isDisplayed());
        onActionsPage().editActionBlock().confirmAction().should(isDisplayed());
        return this;
    }

    @Step
    public ManageActionsSteps createAction(String actionName, String description, String object, String actionType,
            String actionLocation, String behavior, String contentSource, String content) {
        onActionsPage().editActionBlock().actionName().should(isDisplayed()).sendKeys(actionName);
        onActionsPage().editActionBlock().actionDescription().should(isDisplayed()).sendKeys(description);
        onActionsPage().editActionBlock().object().should(isDisplayed()).selectByValue(object);
        onActionsPage().editActionBlock().type().should(isDisplayed()).select(actionType);
        if (!"Single Record".equals(actionType)) {
            onActionsPage().editActionBlock().location().should(isDisplayed()).select(actionLocation);
        }
        onActionsPage().editActionBlock().behavior().should(isDisplayed()).select(behavior);
        onActionsPage().editActionBlock().contentSource().should(isDisplayed()).select(contentSource);
        onActionsPage().editActionBlock().content().should(isDisplayed()).sendKeys(content);
        if ("URL".equals(contentSource) || "Visualforce Page".equals(contentSource) || "Grid".equals(contentSource)) {
            onActionsPage().editActionBlock().idParameter().should(isDisplayed()).sendKeys("action_param");
        }
        onActionsPage().editActionBlock().confirmAction().should(isDisplayed()).setChecked(true);
        onActionsPage().topRowButton("Save").should(isDisplayed()).click();
        switchToNewFrame();
        onActionsPage().message().should(isDisplayed()).should(hasText("Save successful."));
        onActionsPage().actions().convert(i -> i.actionName().getText()).should(hasItem(actionName));
        return this;
    }

    @Step
    public ManageActionsSteps deleteAction(String actionName) {
        List<ActionRecord> r = onActionsPage().actions().should(hasSize(greaterThan(0)))
                .filter(i -> i.actionName().getText().equals(actionName));
        if (r.size() > 0) {
            for (ActionRecord actionRecord : r) {
                actionRecord.checkbox().setChecked(true);
            }
            onActionsPage().topRowButton("Delete").click();
            confirmAlert();
            switchToNewFrame();
            onActionsPage().message().should(isDisplayed()).should(hasText("Delete successful."));
            onActionsPage().actions().convert(i -> i.actionName().getText()).should(not(hasItem(actionName)));
        }
        return this;

    }

    @Step
    public ManageActionsSteps selectAction(String actionName) {
        onActionsPage().actions().filter(i -> i.actionName().getText().equals(actionName)).should(hasSize(1))
                .get(FIRST_ROW).actionName().click();
        return this;
    }

    @Step
    public ManageActionsSteps selectAction(int position) {
        onActionsPage().actions().get(position).actionName().click();
        return this;
    }

    @Step
    public ManageActionsSteps setActionCheckbox(String actionName, boolean state) {
        onActionsPage().actions().filter(i -> i.actionName().getText().equals(actionName)).should(hasSize(1))
                .get(FIRST_ROW).checkbox().setChecked(state);
        return this;
    }

    @Step
    public ManageActionsSteps selectBehavior(String behavior) {
        onActionsPage().editActionBlock().behavior().should(isDisplayed()).select(behavior);
        return this;
    }

    @Step
    public ManageActionsSteps saveChanges() {
        onActionsPage().topRowButton("Save").should(isDisplayed()).click();
        onActionsPage().message().should(isDisplayed()).should(hasText("Save successful."));
        return this;
    }

    @Step
    public GWLandingSteps goBack() {
        getDriver().navigate().back();
        switchToNewFrame();
        return new GWLandingSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public ManageActionsSteps shouldBeOnManageActionsPage() {
        onActionsPage().pageTitle().should(isDisplayed()).should(hasText("Manage Actions"));
        return this;
    }

    private ManageActionsPage onActionsPage() {
        return on(ManageActionsPage.class);
    }

}
