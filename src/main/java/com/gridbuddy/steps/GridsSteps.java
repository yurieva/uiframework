package com.gridbuddy.steps;

import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;

import org.openqa.selenium.WebDriver;

import com.gridbuddy.pages.GridsPage;
import com.gridbuddy.pages.SFBasePage;

import io.qameta.allure.Step;

public class GridsSteps extends SFPageSteps {

    public GridsSteps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

    @Step
    public GridsSteps openGridsDropdown() {
        getDriver().switchTo().defaultContent();
        currentFrameId = onSFPage().iframe().getAttribute("id");
        getDriver().switchTo().frame(currentFrameId);
        onGridsPage().gridsDropdownArrow().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GridsSteps shouldSeeGridUnderFolder(String folderName, String gridName) {
        onGridsPage().grid(folderName, gridName).should(isDisplayed());
        return this;
    }

    @Step
    public GBSteps openGrid(String folderName, String gridName) {
        getDriver().switchTo().defaultContent();
        currentFrameId = onSFPage().iframe().getAttribute("id");
        getDriver().switchTo().frame(currentFrameId);
        wait(5000);
        onGridsPage().gridsDropdownArrow().should(isDisplayed()).click();
        onGridsPage().grid(folderName, gridName).should(isDisplayed()).click();
        currentFrameId = "";
        wait(5000);
        switchToNewFrame();
        return new GBSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    protected GridsPage onGridsPage() {
        return on(GridsPage.class);
    }
    
    protected SFBasePage onSFPage() {
        return on(SFBasePage.class);
    }
}
