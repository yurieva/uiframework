package com.gridbuddy.steps;

import static com.gridbuddy.matchers.HasTextMatcher.hasText;
import static com.gridbuddy.matchers.IsCollectionHasSize.hasSize;
import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;

import com.gridbuddy.blocks.FolderRecord;
import com.gridbuddy.pages.ManageFoldersPage;

import io.qameta.allure.Step;

public class ManageFoldersSteps extends SFPageSteps {

    public ManageFoldersSteps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

    public ManageFoldersSteps createFolder(String folderName, String... profiles) {
        onManageFoldersPage().topRowButton("Add").should(isDisplayed()).click();
        onManageFoldersPage().createFolderName().should(isDisplayed()).sendKeys(folderName);
        Arrays.asList(profiles)
                .forEach(i -> onManageFoldersPage().profiles().item(i).should(isDisplayed()).setChecked(true));
        onManageFoldersPage().topRowButton("Save").should(isDisplayed()).click();
        switchToNewFrame();
        onManageFoldersPage().message().should(isDisplayed())
                .should(hasText("The " + folderName + " folder was successfully created."));
        onManageFoldersPage().folders().convert(i -> i.folderName().getText()).should(hasItem(folderName));
        return this;
    }

    @Step
    public ManageFoldersSteps deleteFolder(String folderName) {
        List<FolderRecord> r = onManageFoldersPage().folders().should(hasSize(greaterThan(0)))
                .filter(i -> i.folderName().getText().equals(folderName));
        if (r.size() > 0) {
            r.get(FIRST_ROW).checkbox().setChecked(true);
            onManageFoldersPage().topRowButton("Delete").should(isDisplayed()).click();
            confirmAlert();
            switchToNewFrame();
            onManageFoldersPage().message().should(isDisplayed())
                    .should(hasText("You successfully deleted the " + folderName + " folder."));
            onManageFoldersPage().folders().convert(i -> i.folderName().getText()).should(not(hasItem(folderName)));
        }
        return this;

    }

    private ManageFoldersPage onManageFoldersPage() {
        return on(ManageFoldersPage.class);
    }
}
