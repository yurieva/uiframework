package com.gridbuddy.steps;

import static com.gridbuddy.matchers.HasTextMatcher.hasText;
import static com.gridbuddy.matchers.IsCollectionHasSize.hasSize;
import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;

import org.openqa.selenium.WebDriver;

import com.gridbuddy.pages.ManageTabbedPagesPage;

import io.qameta.allure.Step;

public class ManageTabbedPagesSteps extends SFPageSteps {

    public ManageTabbedPagesSteps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

    @Step
    public ManageTabbedPagesSteps shouldBeOnManageTabbedPagesPage() {
        onManageTabbedPagesPage().pageTitle().should(isDisplayed()).should(hasText("Manage Tabbed Pages"));
        return this;
    }

    @Step
    public ManageTabbedPagesSteps selectPage(String page) {
        onManageTabbedPagesPage().selectPage().should(isDisplayed()).select(page);
        return this;

    }

    @Step
    public ManageTabbedPagesSteps clickNewButton() {
        onManageTabbedPagesPage().newButton().should(isDisplayed()).click();
        return this;
    }

    @Step
    public ManageTabbedPagesSteps enterTitle(String title) {
        onManageTabbedPagesPage().title().clear();
        onManageTabbedPagesPage().title().sendKeys(title);
        return this;
    }

    @Step
    public ManageTabbedPagesSteps selectObject(String object) {
        onManageTabbedPagesPage().selectObject().should(isDisplayed()).select(object);
        return this;
    }

    @Step
    public ManageTabbedPagesSteps addTab(String tabName, String content, String... params) {
        onManageTabbedPagesPage().addTab().should(isDisplayed()).click();
        onManageTabbedPagesPage().tabs().get(0).name().should(isDisplayed()).clear();
        onManageTabbedPagesPage().tabs().get(0).name().should(isDisplayed()).sendKeys(tabName);
        onManageTabbedPagesPage().tabs().get(0).content().should(isDisplayed()).clear();
        onManageTabbedPagesPage().tabs().get(0).content().should(isDisplayed()).sendKeys(content);
//        onManageTabbedPagesPage().tabs().get(0).arrow().should(isDisplayed()).click();
        onManageTabbedPagesPage().tabs().get(0).contentList().filter(i -> i.getText().contains(content))
                .should(hasSize(1)).get(0).click();
        return this;
    }

    @Step
    public ManageTabbedPagesSteps removeTab(String tabName) {
        onManageTabbedPagesPage().tabs().filter(i -> i.name().getAttribute("value").equals(tabName))
                .forEach(i -> i.remove().click());
        return this;
    }

    @Step
    public ManageTabbedPagesSteps clickSave() {
        onManageTabbedPagesPage().save().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GWLandingSteps goBack() {
        getDriver().navigate().back();
        switchToNewFrame();
        return new GWLandingSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    private ManageTabbedPagesPage onManageTabbedPagesPage() {
        return on(ManageTabbedPagesPage.class);
    }

}
