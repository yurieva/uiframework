package com.gridbuddy.steps;

import static com.gridbuddy.matchers.IsCheckboxCheckedMatcher.isChecked;
import static com.gridbuddy.matchers.IsElementDisplayedMatcher.isDisplayed;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;

import com.gridbuddy.matchers.HasAttributeMatcher;
import com.gridbuddy.pages.GW1Page;

import io.qameta.allure.Step;

public class GW1Steps extends GWSteps {

    public GW1Steps(WebDriver driver, String windowHandle, String currentFrameId) {
        super(driver, windowHandle, currentFrameId);
    }

    @Step
    public GW1Steps enterGridName(String name) {
        wait(1000);
        onGW1Page().name().should(isDisplayed()).clear();
        onGW1Page().name().sendKeys(name);
        return this;
    }

    @Step
    public GW1Steps gridNameShouldBeEmpty() {
        onGW1Page().name().should(isDisplayed()).should(HasAttributeMatcher.hasAttribute("value", ""));
        return this;
    }

    @Step
    public GW1Steps selectMakeThisGridEditable() {
        onGW1Page().edit().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW1Steps checkUserDefinedColumnsCheckbox() {
        onGW1Page().userDefinedColumns().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW1Steps uncheckUserDefinedColumnsCheckbox() {
        onGW1Page().userDefinedColumns().should(isDisplayed()).setChecked(false);
        return this;
    }

    @Step
    public GW1Steps selectParentObject(String parent) {
        onGW1Page().parentObject().should(isDisplayed()).selectByValue(parent);
        return this;
    }
    
    @Step
    public GW1Steps shouldSeeItemInParentObjectDropdown(String parent) {
        onGW1Page().parentObject().should(isDisplayed()).shouldHaveOption(parent);
        return this;
    }

    @Step
    public GW1Steps parentObjectShouldNotBeSelected() {
        assertThat(onGW1Page().parentObject().should(isDisplayed()).getSelectedOption(), equalTo("--Select Object--"));
        return this;
    }

    @Step
    public GW1Steps shouldSeeSelectedObject(String parent) {
        assertThat(onGW1Page().parentObject().should(isDisplayed()).getSelectedValue(), equalTo(parent));
        return this;
    }

    @Step
    public GW1Steps checkAllCheckboxes() {
        onGW1Page().checkboxes().forEach(i -> i.should(isDisplayed()).setChecked(true));
        return this;
    }
    
    @Step
    public GW1Steps shouldSeeAllCheckboxesChecked() {
        onGW1Page().checkboxes().should(everyItem(isChecked()));
        return this;
    }

    @Step
    public GW1Steps uncheckAllCheckboxes() {
        onGW1Page().checkboxes().forEach(i -> i.should(isDisplayed()).setChecked(false));
        return this;
    }

    @Step
    public GW1Steps uncheckEditCheckbox() {
        onGW1Page().edit().should(isDisplayed()).setChecked(false);
        return this;
    }

    @Step
    public GW1Steps checkEditCheckbox() {
        onGW1Page().edit().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW2Steps clickNext() {
        onGW1Page().next().should(isDisplayed()).click();
        switchToNewFrame();
        return new GW2Steps(getDriver(), mainWindowHandle, currentFrameId);
    }
    
    @Step
    public GW1Steps cloneGrid() {
        onGW1Page().clone().should(isDisplayed()).click();
        switchToNewFrame();
        return this;
    }

    @Step
    public GW1Steps clickRefresh() {
        onGW1Page().refresh().should(isDisplayed()).click();
        switchToNewFrame();
        return this;
    }

    @Step
    public GW1Steps alertConfirm() {
        confirmAlert();
        return this;
    }

    @Step
    public GW1Steps selectFolder(String folderName) {
        onGW1Page().folder(folderName).should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW1Steps selectShowGroupingExpanded() {
        onGW1Page().showGroupingsExpanded().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW1Steps shouldSeeShowGroupingExpandedChecked() {
        onGW1Page().showGroupingsExpanded().should(isDisplayed()).should(isChecked());
        return this;
    }

    @Step
    public GW1Steps selectShowRecordDetails() {
        onGW1Page().showRecordDetails().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW1Steps shouldSeeShowRecordDetailsChecked() {
        onGW1Page().showRecordDetails().should(isDisplayed()).should(isChecked());
        return this;
    }

    @Step
    public GW1Steps unselectShowGroupingExpanded() {
        onGW1Page().showGroupingsExpanded().should(isDisplayed()).setChecked(false);
        return this;
    }

    @Step
    public GW1Steps unselectShowRecordDetails() {
        onGW1Page().showRecordDetails().should(isDisplayed()).setChecked(false);
        return this;
    }

    @Step
    public GW1Steps selectShowRelatedObjectsExpanded() {
        onGW1Page().showRelatedObjectsExpanded().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GW1Steps checkMassUpdateCheckbox() {
        onGW1Page().massUpdate().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW1Steps checkShowHeaderCheckbox() {
        onGW1Page().showHeader().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW1Steps checkDeleteCheckbox() {
        onGW1Page().deleteCheckbox().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW1Steps checkShowSidebarCheckbox() {
        onGW1Page().showSidebar().should(isDisplayed()).setChecked(true);
        return this;
    }

    @Step
    public GW1Steps shouldSeeShowHeaderCheckboxChecked() {
        onGW1Page().showHeader().should(isDisplayed()).should(isChecked());
        return this;
    }

    @Step
    public GW1Steps shouldSeeCreateNewAtTheTopOfGridsDropdown() {
        onGW1Page().grid().should(isDisplayed()).click();
        assertThat(onGW1Page().grid().getOptions().get(0), equalTo("Create New..."));
        return this;
    }

    @Step
    public GWLandingSteps deleteGrid(String gridName) {
        if (onGW1Page().grid().should(isDisplayed()).hasOption(gridName)) {
            onGW1Page().grid().should(isDisplayed()).select(gridName);
            onGW1Page().delete().should(isDisplayed()).click();
            confirmAlert();
            switchToNewFrame();
            onGW1Page().grid().shouldNotHaveOption(gridName);
        }
        return new GWLandingSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW1Steps selectCustomCss(String css) {
        onGW1Page().availableCss().record(css).add().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GW1Steps selectCustomJs(String js) {
        onGW1Page().availableJavascript().record(js).add().should(isDisplayed()).click();
        return this;
    }

    @Step
    public GW1Steps shouldSeeAlert(String text) {
        Alert alert = getDriver().switchTo().alert();
        assertThat(alert.getText(), equalTo(text));
        alert.accept();
        return this;
    }

    @Step
    public GBSteps switchToDefaultContent() {
        getDriver().switchTo().defaultContent();
        return new GBSteps(getDriver(), mainWindowHandle, currentFrameId);
    }

    @Step
    public GW1Steps shouldSeeCheckboxChecked(String checkboxName) {
        onGW1Page().checkbox(checkboxName).should(isDisplayed()).should(isChecked());
        return this;
    }

    @Step
    public GW1Steps shouldSeeCheckboxUnchecked(String checkboxName) {
        onGW1Page().checkbox(checkboxName).should(isDisplayed()).should(not(isChecked()));
        return this;
    }

    @Step
    public GW1Steps shouldSeeRecordsPerPageSelected(int count) {
        assertThat(onGW1Page().recordsPerPage().should(isDisplayed()).getSelectedOption(),
                equalTo(String.valueOf(count)));
        return this;
    }
    
    @Step
    public GW1Steps selectRecordsPerPage(int count) {
    	onGW1Page().recordsPerPage().should(isDisplayed()).select(String.valueOf(count));
        return this;
    }

    @Step
    public GW1Steps hideRelatedObjectsShouldBeSelected() {
        onGW1Page().hideRelatedObjects().should(isDisplayed())
                .should(HasAttributeMatcher.hasAttribute("checked", "true"));
        return this;
    }

    @Step
    public GW1Steps setShowRelatedObjectsExpanded() {
        onGW1Page().showRelatedObjectsExpanded().should(isDisplayed()).click();
        return this;
    }
    
    @Step
    public GW1Steps saveGw1() {
        onGW1Page().save().should(isDisplayed()).click();
        switchToNewFrame();
        return this;
    }

    private GW1Page onGW1Page() {
        return on(GW1Page.class);
    }

}
