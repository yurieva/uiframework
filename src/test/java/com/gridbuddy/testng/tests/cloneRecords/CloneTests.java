package com.gridbuddy.testng.tests.cloneRecords;
import static com.gridbuddy.matchers.StringContains.containsStringIgnoringCase;
import static com.gridbuddy.objects.ObjectUtils.getRandomValue;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static com.gridbuddy.utils.Constants.SECOND_ROW;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.objects.Opportunity.OpportunityFields;
import com.gridbuddy.steps.GBSteps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Clone")
@Story("Clone Records")
public class CloneTests extends BaseTest {

    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02409", description = "Inline clone an existing record")
    public void at02409() {
        
        String gridName = "AT-02409GridAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE,
                AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE,
                AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE, ContactFields.BIRTHDATE, ContactFields.BUSINESS_FAX,
                ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION,
                ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);

        String clonedAccountName = RandomStringUtils.randomAlphanumeric(10);

        String clonedAssistantsName = RandomStringUtils.randomAlphanumeric(10);

        GBSteps userOnGb = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowGroupingExpanded().
        selectShowRecordDetails().
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid();
        
        String accountName = userOnGb.getParentObjectInputFieldValue(FIRST_ROW, AccountFields.ACCOUNT_NAME);
        
        userOnGb.cloneParentRecord(FIRST_ROW).
        checkParentObjectClonedSelectFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE)).
        checkParentObjectClonedInputFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_SITE)).
        cloneRelatedRecord(FIRST_ROW, FIRST_ROW).
//        checkRelatedObjectClonedSelectFields(FIRST_ROW, FIRST_ROW, Arrays.asList(ContactFields.CLEAN_STATUS)).
        checkRelatedObjectClonedInputFields(FIRST_ROW, FIRST_ROW, Arrays.asList(ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
                ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.LAST_NAME)).
        editClonedParentRecordInputField(FIRST_ROW, FIRST_ROW, AccountFields.ACCOUNT_NAME, clonedAccountName).
        editClonedRelatedRecordInputField(FIRST_ROW, FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, clonedAssistantsName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "contains", accountName).
        parentObjectShouldHaveRelateObjectWithTheFieldValue(FIRST_ROW, ContactFields.ASSISTANTS_NAME, clonedAssistantsName).
        openFilterPopup().
        createNewFiltrer().
        setFilterCondition(ACCOUNT, 0, AccountFields.ACCOUNT_NAME, "equals", clonedAccountName).
        setAdvancedFilterCondition(ACCOUNT, "1").
        enterFilterName("filter").
        setRelatedFilter("Does Not Have").
        saveFilter().
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NAME, clonedAccountName).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02360", description = "Inline clone(Inline clone an existing record that’s been edited)")
    public void at02360CloneEdited() {
        
        String gridName = "AT-02360GridEdited";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE,
                AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE,
                AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE, ContactFields.BIRTHDATE, ContactFields.BUSINESS_FAX,
                ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION,
                ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);

        String accountNumber = RandomStringUtils.randomAlphanumeric(10);
        
        String accountFax = RandomStringUtils.randomNumeric(10);
        
        String clonedAccountName = RandomStringUtils.randomAlphanumeric(10);

        String assistantsName = RandomStringUtils.randomAlphanumeric(10);

        GBSteps userOnGb = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowGroupingExpanded().
        selectShowRecordDetails().
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid();
        
        String accountName = userOnGb.getParentObjectInputFieldValue(FIRST_ROW, AccountFields.ACCOUNT_NAME);
        
        userOnGb.editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_FAX, accountFax).
        cloneParentRecord(FIRST_ROW).
        checkParentObjectClonedSelectFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE)).
        checkParentObjectClonedInputFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_SITE)).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        cloneRelatedRecord(FIRST_ROW, FIRST_ROW).
//        checkRelatedObjectClonedSelectFields(FIRST_ROW, FIRST_ROW, Arrays.asList(ContactFields.CLEAN_STATUS)).
        checkRelatedObjectClonedInputFields(FIRST_ROW, FIRST_ROW, Arrays.asList(ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
                ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.LAST_NAME)).
        editClonedParentRecordInputField(FIRST_ROW, FIRST_ROW, AccountFields.ACCOUNT_NAME, clonedAccountName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "contains", accountName).
        parentObjectShouldHaveRelateObjectWithTheFieldValue(FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        openFilterPopup().
        createNewFiltrer().
        setFilterCondition(ACCOUNT, 0, AccountFields.ACCOUNT_NAME, "equals", clonedAccountName).
        setAdvancedFilterCondition(ACCOUNT, "1").
        enterFilterName("filter").
        setRelatedFilter("Does Not Have").
        saveFilter().
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NAME, clonedAccountName).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02360", description = "Inline clone(Inline clone a record on a single object grid)")
    public void at02360CloneSingleObject() {
        
        String gridName = "AT-02360CloneSO";

        List<String> fields = Arrays.asList(ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE,
                ContactFields.BIRTHDATE, ContactFields.BUSINESS_FAX);

        String asstPhone = RandomStringUtils.randomNumeric(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(CONTACT).
        clickNext().
        selectFields(fields).
        saveGrid().
        launchGrid().
        cloneParentRecord(FIRST_ROW).
        checkParentObjectClonedInputFields(FIRST_ROW, fields).
        editClonedParentRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(ContactFields.ASST_PHONE, "contains", asstPhone).
        shouldSeeDataRows().
        parentObjectFieldShouldHaveValue(FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02360", description = "Inline clone(Inline clone a record on a multi-object grid with hierarchical child object)")
    public void at02360CloneHierarchicalChildObject() {
        
        String gridName = "AT-02360CloneICO";

        List<String> parentFields = Arrays.asList(ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE,
                ContactFields.BIRTHDATE, ContactFields.BUSINESS_FAX);
        
        List<String> childFields = Arrays.asList(ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE,
                ContactFields.BIRTHDATE, ContactFields.BUSINESS_FAX, ContactFields.FULL_NAME);

        String asstPhone = RandomStringUtils.randomNumeric(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(CONTACT).
        selectShowRecordDetails().
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentFields).
        selectAdditionalObject(CONTACT).
        selectFields(childFields).
        setObjectMapping(CONTACT, "Contact.Full Name (STRING)", "Contact.Full Name (STRING)").
        saveGrid().
        launchGrid().
        cloneParentRecord(FIRST_ROW).
        editClonedParentRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(ContactFields.ASST_PHONE, "contains", asstPhone).
        shouldSeeDataRows().
        parentObjectFieldShouldHaveValue(FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02360", description = "Inline clone(User clones a record on a grid with summary rows)")
    public void at02360CloneWithSummaryRow() {
        
        String gridName = "AT-02360CloneSum";

        GBSteps userOnGb = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, OPPORTUNITY).
        clickNext().
        selectFields(Arrays.asList(OpportunityFields.AMOUNT, OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE, OpportunityFields.OWNER)).
        openFieldProperties(OPPORTUNITY, OpportunityFields.AMOUNT).
        selectSummaryType("SUM").
        saveFieldProperties().
        saveGrid().
        launchGrid().
        editParentRecordInputField(FIRST_ROW, OpportunityFields.AMOUNT, "10000").
        saveChanges().
        shouldSeeSuccessMessage();
        
        Double amount = userOnGb.getSummaryValue(OpportunityFields.AMOUNT);
        
        userOnGb.cloneParentRecord(FIRST_ROW).
        shouldSeeSummaryValue(OpportunityFields.AMOUNT, amount + 10000).
        openGridWizard().
        deleteGrid(gridName);
    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02360", description = "Inline clone(Test different data types)")
    public void at02360CloneDifferentDataTypes() {
        
        String gridName = "AT-02360CloneDDT";

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, OPPORTUNITY).
        clickNext().
        selectFields(40).
        saveGrid().
        launchGrid().
        cloneParentRecord(SECOND_ROW).
        saveChanges().
        shouldSeeSuccessMessage().
        openGridWizard().
        deleteGrid(gridName);
    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02360", description = "Inline clone(Cloning same record multiple times)")
    public void at02360CloneMultipleTimes() {
        
        String gridName = "AT-02360CloneMult";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE,
                AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE,
                AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        cloneParentRecord(FIRST_ROW).
        checkParentObjectClonedSelectFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE)).
        checkParentObjectClonedInputFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_SITE)).
        cloneParentRecord(FIRST_ROW).
        checkParentObjectClonedSelectFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE)).
        checkParentObjectClonedInputFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_SITE)).
        cloneParentRecord(FIRST_ROW).
        checkParentObjectClonedSelectFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE)).
        checkParentObjectClonedInputFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_SITE)).
        cloneParentRecord(FIRST_ROW).
        checkParentObjectClonedSelectFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE)).
        checkParentObjectClonedInputFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_SITE)).
        cloneParentRecord(FIRST_ROW).
        checkParentObjectClonedSelectFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE)).
        checkParentObjectClonedInputFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_SITE)).
        cloneParentRecord(FIRST_ROW).
        checkParentObjectClonedSelectFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE)).
        checkParentObjectClonedInputFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_SITE)).
        saveChanges().
        shouldSeeSuccessMessage().
        openGridWizard().
        deleteGrid(gridName);
    }
    
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02360", description = "Inline clone(Create Fast Filter)")
    public void at02360CloneFastFilter() {
        
        String gridName = "AT-02360GridCerateFastFilter";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE,
                AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE,
                AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE, ContactFields.BIRTHDATE, ContactFields.BUSINESS_FAX,
                ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION,
                ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowGroupingExpanded().
        selectShowRecordDetails().
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        cloneParentRecord(FIRST_ROW).
        checkParentObjectClonedSelectFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE)).
        checkParentObjectClonedInputFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_SITE)).
        cloneRelatedRecord(FIRST_ROW, FIRST_ROW).
//        checkRelatedObjectClonedSelectFields(FIRST_ROW, FIRST_ROW, Arrays.asList(ContactFields.CLEAN_STATUS)).
        checkRelatedObjectClonedInputFields(FIRST_ROW, FIRST_ROW, Arrays.asList(ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
                ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.LAST_NAME)).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "contains", "w").
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("w")).
        applyRelatedColumnFilter(FIRST_ROW, ContactFields.BUSINESS_FAX, "1").
        relatedRecordInputFieldShouldMetCriteria(ContactFields.BUSINESS_FAX, containsStringIgnoringCase("1")).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02360", description = "Inline clone(Create new records in multi o grid)")
    public void at02360CloneCreateNewObjects() {
        
        String gridName = "AT-02360GridCloneCreateNew";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER,
                AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
                ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);
        
        String accountName = RandomStringUtils.randomAlphabetic(10);
        String description = RandomStringUtils.randomAlphabetic(30);
        String accountPhone = RandomStringUtils.randomNumeric(10);
        String accountFax = RandomStringUtils.randomNumeric(10);
        String accountSite = "http://" + RandomStringUtils.randomAlphabetic(10) + ".com";
        String accountNumber = RandomStringUtils.randomAlphanumeric(10).toUpperCase();
        String accountRating = getRandomValue(AccountFields.ACCOUNT_RATINGS);
        String accountSource = getRandomValue(AccountFields.ACCOUNT_SOURCES);
        String accountType = getRandomValue(AccountFields.ACCOUNT_TYPES);
        String accountStatus = getRandomValue(AccountFields.ACTIVE_STATUSES);
        
        String assistantsName = RandomStringUtils.randomAlphabetic(10);
        String asstPhone = RandomStringUtils.randomNumeric(10);
        String businessFax = RandomStringUtils.randomNumeric(10);
        String businessPhone = RandomStringUtils.randomNumeric(10);
//        String cleanStatus = getRandomValue(ContactFields.CLEAN_STATUSES);
        String contactDescription = RandomStringUtils.randomAlphabetic(30);
        String lastName = RandomStringUtils.randomAlphabetic(10);
        String birthDate = LocalDate.now().minusYears(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        saveGrid().
        launchGrid().
        checkRelatedRecordsCheckbox().
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        editParentRecordTextareaField(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, description).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_FAX, accountFax).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_PHONE, accountPhone).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACCOUNT_RATING, accountRating).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_SITE, accountSite).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACCOUNT_SOURCE, accountSource).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACCOUNT_TYPE, accountType).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACTIVE, accountStatus).
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        editRelatedRecordDateField(FIRST_ROW, FIRST_ROW, ContactFields.BIRTHDATE, birthDate).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_FAX, businessFax).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_PHONE, businessPhone).
//        editRelatedRecordSelectField(FIRST_ROW, FIRST_ROW, ContactFields.CLEAN_STATUS, cleanStatus).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.CONTACT_DESCRIPTION, contactDescription).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, lastName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "contains", accountName).
        cloneParentRecord(FIRST_ROW).
        checkParentObjectClonedSelectFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE)).
        checkParentObjectClonedInputFields(FIRST_ROW, Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_SITE)).
        cloneRelatedRecord(FIRST_ROW, FIRST_ROW).
//        checkRelatedObjectClonedSelectFields(FIRST_ROW, FIRST_ROW, Arrays.asList(ContactFields.CLEAN_STATUS)).
        checkRelatedObjectClonedInputFields(FIRST_ROW, FIRST_ROW, Arrays.asList(ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
                ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.LAST_NAME)).
        saveChanges().
        shouldSeeSuccessMessage().
        openGridWizard().
        deleteGrid(gridName);

    }   

}