package com.gridbuddy.testng.tests.specificFields;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static com.gridbuddy.utils.Constants.SECOND_ROW;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.steps.GBSteps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;

@Feature("Specific Fields")
@Story("Date Time Field Tests")
public class DateTimeTests extends BaseTest {

    @Test(groups = { "Regression", "P1" }, testName = "RT-01998", description = "Verify Date and Time fields are splitted when Clone record on grid")
    public void rt01998() {
        
        String gridName = "RT-01998GridAuto";

        String clonedAccountName = RandomStringUtils.randomAlphanumeric(10);

        GBSteps userOnGb = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowGroupingExpanded().
        selectShowRecordDetails().
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(Arrays.asList("CustomDateTime")).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, "CustomDateTime", "not equal to", "").
        saveGrid().
        launchGrid().
        shouldSeeRecords();
        
        LocalDateTime dateTime = userOnGb.getParentObjectDateTimeValue(FIRST_ROW, "CustomDateTime");
        
        userOnGb.cloneParentRecord(FIRST_ROW).
        parentRecordClonedDateTimeFieldShouldHaveValue(FIRST_ROW, "CustomDateTime", dateTime).
        editClonedParentRecordInputField(FIRST_ROW, FIRST_ROW, AccountFields.ACCOUNT_NAME, clonedAccountName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "equals", clonedAccountName).
        parentRecordDateTimeFieldShouldHaveValue(FIRST_ROW, "CustomDateTime", dateTime).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02000", description = "Verify Mass update for Time input only on splitted Date Time fields")
    public void rt02000() {
        
        String gridName = "RT-02000GridAuto";

        GBSteps userOnGb = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(Arrays.asList("CustomDateTime")).
        selectRelatedObject(OPPORTUNITY).
        selectFields(Arrays.asList("CustomDateTime")).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, "CustomDateTime", "not equal to", "").
        setRelatedFilter("Has").
        setFilterCondition(OPPORTUNITY, FIRST_ROW, "CustomDateTime", "not equal to", "").
        saveGrid().
        launchGrid().
        shouldSeeRecords();
        
        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("h:mm a"));
        
        List<LocalDateTime> dates = new ArrayList<LocalDateTime>();
        for (int i = 0; i< userOnGb.getRecordsCount(); i++) {
            dates.add(userOnGb.getParentObjectDateTimeValue(i, "CustomDateTime"));
        }
        
        userOnGb.openColumnActions("CustomDateTime").
        enterColumnTimeValue(time).
        enterColumnDateValue("").
        clickAllButton();
        
        for (int i = 0; i < userOnGb.getRecordsCount(); i++) {
            userOnGb.parentRecordDateTimeFieldShouldHaveValue(i, "CustomDateTime",
                    dates.get(i).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")), time);
        }
        
        userOnGb.refreshGrid().
        shouldSeeRecords();
        
        for (int i = 0; i < userOnGb.getRecordsCount(); i++) {
            userOnGb.parentRecordDateTimeFieldShouldHaveValue(i, "CustomDateTime", dates.get(i));
        }
        
        dates = new ArrayList<LocalDateTime>();
        
        for (int i = 0; i < userOnGb.getRecordsCount(); i++) {
            dates.add(userOnGb.getRelatedObjectDateTimeValue(i, FIRST_ROW, "CustomDateTime"));
        }
        
        userOnGb.selectRelatedRecord(FIRST_ROW, FIRST_ROW).
        selectRelatedRecord(SECOND_ROW, FIRST_ROW).
        openMassUpdatePopup().
        editMassUpdateTimeField("Opportunities", "CustomDateTime", time).
        editMassUpdateDateField("Opportunities", "CustomDateTime", "").
        clickApplyToSelectedRecordsButton();
        
        for (int i = 0; i < 2; i++) {
            userOnGb.relatedRecordDateTimeFieldShouldHaveValue(i, FIRST_ROW, "CustomDateTime",
                    dates.get(i).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")), time);
        }
        
        for (int i = 2; i < userOnGb.getRecordsCount(); i++) {
            userOnGb.relatedRecordDateTimeFieldShouldHaveValue(i, FIRST_ROW, "CustomDateTime", dates.get(i));
        }
        
        userOnGb.saveChanges().
        shouldSeeSuccessMessage();
        
        for (int i = 0; i < 2; i++) {
            userOnGb.relatedRecordDateTimeFieldShouldHaveValue(i, FIRST_ROW, "CustomDateTime",
                    dates.get(i).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")), time);
        }
        
        for (int i = 2; i < userOnGb.getRecordsCount(); i++) {
            userOnGb.relatedRecordDateTimeFieldShouldHaveValue(i, FIRST_ROW, "CustomDateTime", dates.get(i));
        }
        userOnGb.openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-01999", description = "Verify Mass create option for splitted Date field individually")
    public void rt01999() {
        
        String gridName = "RT-01999GridAuto";


        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("h:mm a"));

        GBSteps userOnGb = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(Arrays.asList("CustomDateTime")).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, "CustomDateTime", "not equal to", "").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        openMassCreatePopup().
        enterNumberOfRecords(3).
        clickCreateParentRecordsButton().
        editMassCreateInputField("Account Name", RandomStringUtils.randomAlphabetic(10)).
        editMassCreateTimeField("CustomDateTime", time).
        editMassCreateDateField("CustomDateTime", "").
        clickCreateParentRecordsButton();
        
        for (int i = 0; i < 2; i++) {
            userOnGb.parentRecordDateTimeFieldShouldHaveValue(i, "CustomDateTime", "", time);
        }
        
        userOnGb.saveChanges().
        shouldSeeRecords().
        shouldSeeMessage("Failed to create new Account record. Error saving CustomDateTime: Invalid date format").
        parentRecordDateTimeFieldShouldHaveValue(FIRST_ROW, "CustomDateTime", "", time).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P2" }, testName = "RT-02006", description = "Save splitted Date Time field with blank Date input and Time with small am/pm as time format")
    @Issue("https://newbuddy.atlassian.net/browse/GB-463")
    public void rt02006() {
        
        String gridName = "RT-02006GridAuto";

        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("h:mm a"));

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(Arrays.asList("CustomDateTime")).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, "CustomDateTime", "not equal to", "").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, "Account Name", RandomStringUtils.randomAlphabetic(10)).
        editParentRecordTimeField(FIRST_ROW, "CustomDateTime", time).
        editParentRecordDateField(FIRST_ROW, "CustomDateTime", "").
        saveChanges().
        shouldSeeRecords().
        shouldSeeMessage("Failed to create new Account record. Error saving CustomDateTime: Invalid date format").
        parentRecordDateTimeFieldShouldHaveValue(FIRST_ROW, "CustomDateTime", "", time).
        editParentRecordTimeField(FIRST_ROW, "CustomDateTime", time.toLowerCase()).
        editParentRecordDateField(FIRST_ROW, "CustomDateTime", "").
        saveChanges().
        shouldSeeRecords().
        shouldSeeMessage("Failed to create new Account record. Error saving CustomDateTime: Invalid date format").
        parentRecordDateTimeFieldShouldHaveValue(FIRST_ROW, "CustomDateTime", "", time).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01997", description = "Verify Date Time fields are splitted when navigate to the grid with this field")
    public void rt01997() {
        
        String gridName = "RT-01997GridAuto";
        
        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("h:mm a"));
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(Arrays.asList("CustomDateTime")).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        clickAddNewParentObject().
        editParentRecordTimeField(FIRST_ROW, "CustomDateTime", time).
        editParentRecordDateField(FIRST_ROW, "CustomDateTime", "").
        openGridWizard().
        deleteGrid(gridName);

    }
    

}