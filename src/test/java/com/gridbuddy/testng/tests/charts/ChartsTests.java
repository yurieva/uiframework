package com.gridbuddy.testng.tests.charts;

import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Opportunity.OpportunityFields;
import com.gridbuddy.steps.GW2Steps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Charts")
@Story("Charts")
public class ChartsTests extends BaseTest {

    @Test(groups = { "Regression", "P1" }, testName = "AT-02351", description = "Charts(Create Line chart GW2 - create single o)")
    public void at02351LineChart() {
        
        String gridName = "AT-02351GridAutoLine";

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, OPPORTUNITY).
        clickNext().
        selectFields(Arrays.asList(OpportunityFields.AMOUNT, OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE, OpportunityFields.OWNER)).
        addLineChart("Chart Name", "Medium", "Opportunity.Name (STRING)", "Opportunity Record Count", "Opportunity.Owner (REFERENCE)", true).
        saveGrid().
        launchGrid().
        openChart().
        shouldSeeChartSize("Medium").
        shouldSeeLineBarChartData("Chart Name", "Opportunity - Name", "Opportunity Record Count", "Opportunity - Owner").
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, OpportunityFields.NAME, RandomStringUtils.randomAlphabetic(10)).
        editParentRecordDateField(FIRST_ROW, OpportunityFields.CLOSE_DATE, LocalDate.now().format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        editParentRecordSelectField(FIRST_ROW, OpportunityFields.STAGE, "Prospecting").
        saveChanges().
        shouldSeeSuccessMessage().
        openChart().
        shouldSeeLineBarChartData("Chart Name", "Opportunity - Name", "Opportunity Record Count", "Opportunity - Owner").
        editParentRecordInputField(FIRST_ROW, OpportunityFields.AMOUNT, "1500000").
        saveChanges().
        shouldSeeSuccessMessage().
        openChart().
        shouldSeeLineBarChartData("Chart Name", "Opportunity - Name", "Opportunity Record Count", "Opportunity - Owner").
        applyColumnFilter(OpportunityFields.NAME, "contains", "a").
        openChart().
        shouldSeeLineBarChartData("Chart Name", "Opportunity - Name", "Opportunity Record Count", "Opportunity - Owner").
        editParentRecordDateField(FIRST_ROW, OpportunityFields.CLOSE_DATE, LocalDate.now().minusMonths(1).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        saveChanges().
        shouldSeeSuccessMessage().
        openChart().
        shouldSeeLineBarChartData("Chart Name", "Opportunity - Name", "Opportunity Record Count", "Opportunity - Owner").
        openGridWizard().
        deleteGrid(gridName);

    }
    

    @Test(testName = "AT-02351", description = "Charts(Create Bar chart GW2 - create single o)")
    public void at02351BarChart() {
        
        String gridName = "AT-02351GridAutoBar";

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, OPPORTUNITY).
        clickNext().
        selectFields(Arrays.asList(OpportunityFields.AMOUNT, OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE, OpportunityFields.OWNER)).
        addBarChart("Chart Name", "Small", "Opportunity.Name (STRING)", "Opportunity Record Count", "Opportunity.Owner (REFERENCE)", true).
        saveGrid().
        launchGrid().
        openChart().
        shouldSeeChartSize("Small").
        shouldSeeLineBarChartData("Chart Name", "Opportunity - Name", "Opportunity Record Count", "Opportunity - Owner").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(testName = "AT-02351", description = "Charts(Create Pie chart GW2 - create single o)")
    public void at02351PieChart() {
        
        String gridName = "AT-02351GridAutoPie";

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, OPPORTUNITY).
        clickNext().
        selectFields(Arrays.asList(OpportunityFields.AMOUNT, OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE, OpportunityFields.OWNER)).
        addDonutChart("Chart Name", "Large", "Opportunity.Name (STRING)", "Opportunity Record Count").
        saveGrid().
        launchGrid().
        openChart().
        shouldSeeChartSize("Large").
        shouldSeePieChartData("Chart Name", "Opportunity Record Count", "Opportunity - Name").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(testName = "AT-02351", description = "Charts(Fields Validation)")
    public void at02351FieldsValidation() {
        
        String gridName = "AT-02351Validation";

        GW2Steps userOnGw2 = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, OPPORTUNITY).
        clickNext().
        selectFields(Arrays.asList(OpportunityFields.AMOUNT, OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE, OpportunityFields.OWNER)).
        addBarChart("Chart Name", "Small", "Opportunity.Name (STRING)", "Opportunity Record Count", "Opportunity.Owner (REFERENCE)", true).
        saveGw2();
        userOnGw2.addBarChart("", "Small", "Opportunity.Name (STRING)", "Opportunity Record Count", "Opportunity.Owner (REFERENCE)", true).
        shouldSeeErrorMessage("Please enter a chart title.");
        userOnGw2.addBarChart("Title", "Small", "--Select--", "Opportunity Record Count", "Opportunity.Owner (REFERENCE)", true).
        shouldSeeErrorMessage("Please select a value for the X-Axis chart field.");
        userOnGw2.addBarChart("Title", "Small", "Opportunity.Name (STRING)", "--Select--", "Opportunity.Owner (REFERENCE)", true).
        shouldSeeErrorMessage("Please select a value for the Y-Axis chart field.");
        userOnGw2.addBarChart("Title", "--Select--", "Opportunity.Name (STRING)", "Opportunity Record Count", "Opportunity.Owner (REFERENCE)", true).
        shouldSeeErrorMessage("Please select a chart size.");
        userOnGw2.clickResetChart().
        saveGrid().
        launchGrid().
        shouldNotSeeChartIcon().
        openGridWizard().
        deleteGrid(gridName);

    }

}