package com.gridbuddy.testng.tests.editRecords;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static com.gridbuddy.utils.Constants.THIRD_ROW;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.objects.Opportunity.OpportunityFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Edit")
@Story("Edit Records")
public class EditTests extends BaseTest {

    @Test(groups = { "Regression", "P1" }, testName = "editParentAndRelatedRecordsTest", description = "Edit existing parent and related record.")
    public void editParentAndRelatedRecordsTest() {
        
        String gridName = "SmokeED01GridAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE);

        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
        ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.CONTACT_ID);

        String description = RandomStringUtils.randomAlphabetic(30);
        String accountNumber = RandomStringUtils.randomAlphanumeric(10).toUpperCase();
        String asstPhone = RandomStringUtils.randomAlphanumeric(10);
        String contactDescription = RandomStringUtils.randomAlphabetic(30);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        checkRelatedRecordsCheckbox().
        editParentRecordTextareaField(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, description).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        editParentRecordInputField(THIRD_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        editRelatedRecordInputField(THIRD_ROW, FIRST_ROW, ContactFields.CONTACT_DESCRIPTION, contactDescription).
        saveChanges().
        shouldSeeSuccessMessage().
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, description).
        parentObjectFieldShouldHaveValue(THIRD_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        relatedObjectFieldShouldHaveValue(THIRD_ROW, FIRST_ROW, ContactFields.CONTACT_DESCRIPTION, contactDescription).
        openGridWizard().
        deleteGrid(gridName);
    }

    @Test(groups = { "Regression", "P1" }, testName = "editParentAndUnrelatedRecordsTest", description = "Edit existing parent and unrelated record.")
    public void editParentAndUnrelatedRecordsTest() {
        final String CASE = "Case";
        String gridName = "editParentAndUnrelatedRecordsTest";

        List<String> parentObjectFields = Arrays.asList(OpportunityFields.AMOUNT, OpportunityFields.OWNER, OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE, OpportunityFields.ACCOUNT_NAME);
        List<String> unrelatedObjectFields = Arrays.asList("Account Name", "Asset Name", "Company", "Name");

        String opportunityName = RandomStringUtils.randomAlphabetic(10);
        String companyName = RandomStringUtils.randomAlphabetic(10);
        String caseName = RandomStringUtils.randomAlphabetic(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(parentObjectFields).
        selectAdditionalObject(CASE).
        selectFields(unrelatedObjectFields).
        setObjectMapping(CASE,"Opportunity.Account Name (ID REFERENCE)","Case.Account Name (ID REFERENCE)").
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        checkRelatedRecordsCheckbox().
        editParentRecordInputField(FIRST_ROW, OpportunityFields.NAME, opportunityName).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "Company", companyName).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "Name", caseName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(OpportunityFields.NAME, "equals", opportunityName).
        shouldSeeRecords().
        parentObjectFieldShouldHaveValue(FIRST_ROW, OpportunityFields.NAME, opportunityName).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, "Company", companyName).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, "Name", caseName).
        openGridWizard().
        deleteGrid(gridName);
    }

    @Test(groups = { "Regression", "P0" }, testName = "editParentRecordTest", description = "Edit existing parent")
    public void editParentRecordTest() {
        String gridName = "editParentRecordTest";

        List<String> parentObjectFields = Arrays.asList(OpportunityFields.AMOUNT, OpportunityFields.OWNER,
        OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE, OpportunityFields.ACCOUNT_NAME);

        String opportunityName = RandomStringUtils.randomAlphabetic(10);
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        editParentRecordInputField(FIRST_ROW, OpportunityFields.NAME, opportunityName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(OpportunityFields.NAME, "equals", opportunityName).
        shouldSeeRecords().
        parentObjectFieldShouldHaveValue(FIRST_ROW, OpportunityFields.NAME, opportunityName).
        openGridWizard().
        deleteGrid(gridName);
    }

    @Test(groups = { "Regression", "P0" }, testName = "editUnrelatedRecordTest", description = "Edit existing unrelated record.")
    public void editUnrelatedRecordTest() {
        final String CASE = "Case";
        String gridName = "editUnrelatedRecordTest";

        List<String> parentObjectFields = Arrays.asList(OpportunityFields.AMOUNT, OpportunityFields.OWNER,
        OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE, OpportunityFields.ACCOUNT_NAME);
        List<String> unrelatedObjectFields = Arrays.asList("Account Name", "Asset Name", "Company", "Name");

        String companyName = RandomStringUtils.randomAlphabetic(10);
        String caseName = RandomStringUtils.randomAlphabetic(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(parentObjectFields).
        selectAdditionalObject(CASE).
        selectFields(unrelatedObjectFields).
        setObjectMapping(CASE,"Opportunity.Account Name (ID REFERENCE)","Case.Account Name (ID REFERENCE)").
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        checkRelatedRecordsCheckbox().
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "Company", companyName).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "Name", caseName).
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeRecords().
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, "Company", companyName).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, "Name", caseName).
        openGridWizard().
        deleteGrid(gridName);
    }

    @Test(groups = { "Regression", "P0" }, testName = "editRelatedRecordTest", description = "Edit existing related record.")
    public void editRelatedRecordTest() {
        String gridName = "editChildRecordTest";


        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
        AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE,
        AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE);

        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
        ContactFields.ASST_PHONE, ContactFields.BIRTHDATE, ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE,
        ContactFields.CONTACT_DESCRIPTION, ContactFields.CONTACT_ID);

        String asstPhone = RandomStringUtils.randomAlphanumeric(10);
        String businessPhone = RandomStringUtils.randomAlphanumeric(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        checkRelatedRecordsCheckbox().
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_PHONE, businessPhone).
        saveChanges().
        shouldSeeSuccessMessage().
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_PHONE, businessPhone).
        openGridWizard().
        deleteGrid(gridName);
    }
}
