package com.gridbuddy.testng.tests.gridWizard;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.AccountHistory.AccountHistoryFields;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.objects.Contract.ContractFields;
import com.gridbuddy.objects.Event.EventFields;
import com.gridbuddy.objects.Opportunity.OpportunityFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Grid Wizard")
@Story("Grid Wizard Page 2")
public class GridWizardPageTwoTests extends BaseTest {

    @Test(groups = { "Regression", "P0" }, testName = "AT-02416", description = "Test adding 4 child objects to a Grid")
    public void at02416() {
        
        String gridName = "AT-02416GridAuto";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(Arrays.asList(ContactFields.ACCOUNT_NAME)).
        selectRelatedObject(AccountRelatedObjects.OPPORTUNITY).
        selectFields(Arrays.asList(OpportunityFields.AMOUNT)).
        selectRelatedObject(AccountRelatedObjects.CONTRACT).
        selectFields(Arrays.asList(ContractFields.ACTIVATED_DATE)).
        selectRelatedObject(AccountRelatedObjects.EVENT).
        selectFields(Arrays.asList(EventFields.ACCOUNT_NAME)).
        saveGw2().
        shouldSeeAddedFields(Arrays.asList(AccountFields.ACCOUNT_NAME, ContactFields.ACCOUNT_NAME, ContractFields.ACTIVATED_DATE, EventFields.ACCOUNT_NAME, OpportunityFields.AMOUNT)).
        shouldNotSeeErrorMessage().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "AT-02417", description = "Test adding 41 fields to parent object on Wizard2")
    public void at02417() {
        
        String gridName = "AT-02417GridAuto";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(41).
        addedFieldsListShouldHaveSize(42).
        saveGw2().
        shouldNotSeeErrorMessage().
        launchGrid().
        columnsNumberShouldBe(42).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02418", description = "Test adding 5 child objects to a Grid")
    public void at02418() {
        
        String gridName = "AT-02418GridAuto";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(Arrays.asList(ContactFields.ACCOUNT_NAME)).
        selectRelatedObject(AccountRelatedObjects.OPPORTUNITY).
        selectFields(Arrays.asList(OpportunityFields.AMOUNT)).
        selectRelatedObject(AccountRelatedObjects.CONTRACT).
        selectFields(Arrays.asList(ContractFields.ACTIVATED_DATE)).
        selectRelatedObject(AccountRelatedObjects.EVENT).
        selectFields(Arrays.asList(EventFields.ACCOUNT_NAME)).
        selectRelatedObject(AccountRelatedObjects.ACCOUNT_HISTORY).
        selectFields(Arrays.asList(AccountHistoryFields.ACCOUNT_HISTORY_ID)).
        saveGw2().
        shouldSeeErrorMessage("No more than 4 child objects are allowed per grid.").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02430", description = "Test that Converted Lead Object does not display under parent Opportunity and Contact objects in GW2")
    public void at02430() {
        
        String gridName = "AT-02430GridAuto";
        String child = "Converted Lead";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(CONTACT).
        clickNext().
        shouldNotSeeChildObject(child).
        clickBack().
        selectParentObject(OPPORTUNITY).
        alertConfirm().
        clickNext().
        shouldNotSeeChildObject(child).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Parent Object Plural Label)")
    public void at02444ParentPluralLabel() {
        
        String gridName = "AT-02444GridAutoParentPluralLabel";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE);
        
        String pluralLabel = RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(ACCOUNT).
        enterObjectPluralLabel(pluralLabel).
        saveObjectLevelSettings().
        saveGrid().
        launchGrid().
        shouldSeeParentObjectLabel(pluralLabel).
        shouldSeeObjectPluralLabelInPaginationSection(pluralLabel).
        openExportPopup().
        shouldSeeObjectOnExportPopup(pluralLabel).
        closePopup().
        openMassCreatePopup().
        shouldSeeObjectOnMassCreatePopup(pluralLabel).
        closePopup().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Parent Object Label)")
    public void at02444ParentLabel() {
        
        String gridName = "AT-02444GridAutoParentLabel";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE);
        
        String label = RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(ACCOUNT).
        enterObjectLabel(label).
        saveObjectLevelSettings().
        saveGrid().
        launchGrid().
        openInfoPopup().
        shouldSeeObjectOnInfoPopup(label).
        openReorderColumnsPopup().
        shouldSeeObjectReorderColumnsPopup(label).
        closePopup().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Related Object Label)")
    public void at02444RelatedLabel() {
        
        String gridName = "AT-02444GridAutoRelatedLabel";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE);
        
        String label = RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(AccountRelatedObjects.CONTACT).
        enterObjectLabel(label).
        saveObjectLevelSettings().
        saveGrid().
        launchGrid().
        openInfoPopup().
        shouldSeeObjectOnInfoPopup(label).
        openReorderColumnsPopup().
        shouldSeeObjectReorderColumnsPopup(label).
        closePopup().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Related Object Plural Label)")
    public void at02444RelatedPluralLabel() {
        
        String gridName = "AT-02444GridAutoRelatedPluralLabel";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE);
        
        String label = RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(AccountRelatedObjects.CONTACT).
        enterObjectPluralLabel(label).
        saveObjectLevelSettings().
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRelatedObject(label).
        shouldSeeColumn(label.toUpperCase()).
        openRelatedObjectsPopup(FIRST_ROW).
        shouldSeeRelatedObjectOnRelatedObjectPopupTitle("Related " + label).
        closePopup().
        openExportPopup().
        shouldSeeObjectOnExportPopup("Related " + label).
        closePopup().
        openMassCreatePopup().
        shouldSeeRelatedObjectOnMassCreatePopup(label).
        closePopup().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Related Object Column Label)")
    public void at02444RelatedColumnLabel() {
        
        String gridName = "AT-02444GridAutoRelatedColumnLabel";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE);
        
        String label = RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(AccountRelatedObjects.CONTACT).
        enterObjectColumnLabel(label).
        saveObjectLevelSettings().
        saveGrid().
        launchGrid().
        shouldSeeColumn(label.toUpperCase()).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Parent Object Create Checkbox")
    public void at02444ParentObjectCreateCheckbox() {
        
        String gridName = "AT-02444AutoParentCreateCheckbox";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(ACCOUNT).
        uncheckCreateCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        launchGrid().
        shouldNotSeeNewButton().
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        openObjectSettings(ACCOUNT).
        checkCreateCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeNewButton().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Related Object Create Checkbox")
    public void at02444RelatedObjectCreateCheckbox() {
        
        String gridName = "AT-02444AutoRelatedCreateCheckbox";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(AccountRelatedObjects.CONTACT).
        uncheckCreateCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        launchGrid().
        expandParentRecord(FIRST_ROW).
        shouldNotSeeNewButtonForRelatedObject().
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        openObjectSettings(AccountRelatedObjects.CONTACT).
        checkCreateCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        expandParentRecord(FIRST_ROW).
        shouldSeeNewButtonForRelatedObject().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Parent Object Edit Checkbox")
    public void at02444ParentObjectEditCheckbox() {
        
        String gridName = "AT-02444AutoParentEditCheckbox";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(ACCOUNT).
        checkCreateCheckbox().
        checkEditCheckbox().
        uncheckEditCheckbox().
        shouldSeeCreateCheckboxUnchecked().
        saveObjectLevelSettings().
        saveGrid().
        launchGrid().
        fieldShouldBeNotEditable(FIRST_ROW, AccountFields.ACCOUNT_NAME).
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        openObjectSettings(ACCOUNT).
        checkEditCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        fieldShouldBeEditable(FIRST_ROW, AccountFields.ACCOUNT_NAME).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Child Object Edit Checkbox")
    public void at02444ChildObjectEditCheckbox() {
        
        String gridName = "AT-02444AutoChildEditCheckbox";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(AccountRelatedObjects.CONTACT).
        checkCreateCheckbox().
        checkEditCheckbox().
        uncheckEditCheckbox().
        shouldSeeCreateCheckboxUnchecked().
        saveObjectLevelSettings().
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        expandParentRecord(FIRST_ROW).
        expandRelatedRecord(FIRST_ROW).
        fieldShouldBeNotEditable(FIRST_ROW, FIRST_ROW, ContactFields.ACCOUNT_NAME).
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        openObjectSettings(AccountRelatedObjects.CONTACT).
        checkEditCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        expandParentRecord(FIRST_ROW).
        expandRelatedRecord(FIRST_ROW).
        fieldShouldBeEditable(FIRST_ROW, FIRST_ROW, ContactFields.ACCOUNT_NAME).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Parent Object Delete Checkbox")
    public void at02444ParentObjectDeleteCheckbox() {
        
        String gridName = "AT-02444AutoParentDeleteCheckbox";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE);
        
        String accountName = "DeleteMe" + RandomStringUtils.randomAlphabetic(5);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        checkDeleteCheckbox().
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(ACCOUNT).
        uncheckDeleteCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        launchGrid().
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "contains", accountName).
        selectParentRecord(FIRST_ROW).
        deleteSelectedItems().
        shouldSeeAlert("You do not have permissions to delete the following object: "
                        + ACCOUNT + ". Please deselect records associated to this object and try again.").
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        openObjectSettings(ACCOUNT).
        checkDeleteCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        selectParentRecord(FIRST_ROW).
        deleteSelectedItems().
        shouldSeeMessage("1 record was successfully deleted.").
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Related Object Delete Checkbox")
    public void at02444RelatedObjectDeleteCheckbox() {
        
        String gridName = "AT-02444AutoRelatedDeleteCheckbox";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE, ContactFields.LAST_NAME);
        
        String accountName = "DeleteMe" + RandomStringUtils.randomAlphabetic(5);
        
        String contactName = "DeleteMe" + RandomStringUtils.randomAlphabetic(5);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        checkDeleteCheckbox().
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(AccountRelatedObjects.CONTACT).
        uncheckDeleteCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        launchGrid().
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "contains", accountName).
        expandParentRecord(FIRST_ROW).
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, contactName).
        saveChanges().
        shouldSeeSuccessMessage().
        expandParentRecord(FIRST_ROW).
        expandRelatedRecord(FIRST_ROW).
        selectRelatedRecord(FIRST_ROW, FIRST_ROW).
        deleteSelectedItems().
        shouldSeeAlert("You do not have permissions to delete the following object: " + AccountRelatedObjects.CONTACT
                                + ". Please deselect records associated to this object and try again.").
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        openObjectSettings(AccountRelatedObjects.CONTACT).
        checkDeleteCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        expandParentRecord(FIRST_ROW).
        expandRelatedRecord(FIRST_ROW).
        selectRelatedRecord(FIRST_ROW, FIRST_ROW).
        deleteSelectedItems().
        shouldSeeMessage("1 record was successfully deleted.").
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02444", description = "Verify Object Level Settings(Editable Related Column Checkbox")
    public void at02444EditableRelatedColumnCheckbox() {
        
        String gridName = "AT-02444AutoEditableRelatedColumn";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(AccountRelatedObjects.CONTACT).
        shouldSeeEditableRelatedColumnCheckboxChecked().
        shouldSeeFlatViewCheckboxUnchecked().
        checkFlatViewCheckbox().
        shouldSeeEditableRelatedColumnCheckboxUnchecked().
        saveObjectLevelSettings().
        clickBack().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02446", description = "Verify Read Only Mode works when setup in GW2 with Read Only Enable in Field Properties for a parent Object")
    public void at02446() {
        
        String gridName = "AT-02446GridAuto";
        
        List<String> fields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(fields).
        openFieldProperties(ACCOUNT, AccountFields.ACCOUNT_NUMBER).
        checkReadOnlyCheckbox().
        saveFieldProperties().
        saveGrid().
        launchGrid().
        fieldShouldBeNotEditable(FIRST_ROW, AccountFields.ACCOUNT_NUMBER).
        openMassCreatePopup().
        shouldNotSeeColumnOnMassUpdatePopup(AccountFields.ACCOUNT_NUMBER).
        closePopup().
        openMassUpdatePopup().
        shouldNotSeeColumnOnMassCreatePopup(AccountFields.ACCOUNT_NUMBER).
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        openFieldProperties(ACCOUNT, AccountFields.ACCOUNT_NUMBER).
        uncheckReadOnlyCheckbox().
        saveFieldProperties().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        fieldShouldBeEditable(FIRST_ROW, AccountFields.ACCOUNT_NUMBER).
        openMassCreatePopup().
        shouldSeeColumnOnMassUpdatePopup(AccountFields.ACCOUNT_NUMBER).
        closePopup().
        openMassUpdatePopup().
        shouldSeeColumnOnMassCreatePopup(AccountFields.ACCOUNT_NUMBER).
        openGridWizard().
        deleteGrid(gridName);
        
    }

}