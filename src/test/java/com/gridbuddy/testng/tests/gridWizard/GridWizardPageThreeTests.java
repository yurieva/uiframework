package com.gridbuddy.testng.tests.gridWizard;

import static com.gridbuddy.matchers.StringContains.containsStringIgnoringCase;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static com.gridbuddy.utils.Constants.SECOND_ROW;
import static com.gridbuddy.utils.Constants.THIRD_ROW;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Filters")
@Story("Filters Tests")
public class GridWizardPageThreeTests extends BaseTest {

    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02432", description = "Test using commas to delimit multiple values in filter criteria")
    public void at02432() {
        
        String gridName = "AT-02432GridAuto";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(Arrays.asList(AccountFields.BILLING_STATE)).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.BILLING_STATE, "includes", "CA, IL, TX").
        saveGrid().
        launchGrid().
        parentObjectsShouldHaveFieldValues(AccountFields.BILLING_STATE, "CA", "IL", "TX").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02447", description = "Verify UDF if working")
    public void at02447() {

        String gridName = "AT-02447GridAuto";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE,
                AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE,
                AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE, ContactFields.BIRTHDATE, ContactFields.BUSINESS_FAX,
                ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION,
                ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "united").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACTIVE, "not equal to", "Yes").
        setFilterCondition(ACCOUNT, THIRD_ROW, AccountFields.BILLING_CITY, "not equal to", "San Francisco").
        setFilterCondition(ACCOUNT, 5, AccountFields.ACCOUNT_NUMBER, "contains", "A").
        setAdvancedFilterCondition(ACCOUNT, "1 AND 2 AND 3 AND 6").
        setFilterCondition(AccountRelatedObjects.CONTACT, 1, ContactFields.ACCOUNT_NAME, "contains", "Text").
        setAdvancedFilterCondition(AccountRelatedObjects.CONTACT, "2").
        saveGrid().
        launchGrid().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, not(containsStringIgnoringCase("united"))).
        parentRecordTextFieldShouldMetCriteria(AccountFields.ACTIVE, not(equalTo("Yes"))).
        parentRecordInputFieldShouldMetCriteria(AccountFields.BILLING_CITY, not(equalTo("San Francisco"))).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NUMBER, containsStringIgnoringCase("A")).
        relatedRecordInputFieldShouldMetCriteria(ContactFields.ACCOUNT_NAME, containsStringIgnoringCase("Text")).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02367", description = "Testing Set Max Record Limit feature.")
    public void at02367() {

        String gridName = "AT-02367GridAuto";
        int recordsLimit = 3;

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        clickNext().
        setRecordsLimit(ACCOUNT, recordsLimit).
        saveGrid().
        launchGrid().
        shouldSeeNumberOfRecords(recordsLimit).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02366", description = "Testing Order by Field Descending")
    public void at02366() {

        String gridName = "AT-02366GridAuto";
        List<String> fields = Arrays.asList(AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_ID);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(fields).
        clickNext().
        selectSortByField(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_ID, "Descending").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        recordsShouldBeSortedByTextField(AccountFields.ACCOUNT_ID, "Descending").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02365", description = "Testing Advanced Filter condition 1 AND 2 AND 3.")
    public void at02365() {

        String gridName = "AT-02365GridAuto";
        
        List<String> fields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_TYPE,
                AccountFields.ACTIVE, AccountFields.CREATED_DATE, AccountFields.BILLING_CITY,
                AccountFields.BILLING_COUNTRY);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(fields).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "united").
        setFilterCondition(ACCOUNT,SECOND_ROW, AccountFields.ACTIVE, "not equal to", "Yes").
        setFilterCondition(ACCOUNT,THIRD_ROW, AccountFields.BILLING_CITY, "equals", "San Francisco").
        setAdvancedFilterCondition(ACCOUNT, "1 AND 2 AND 3").
        saveGrid().
        launchGrid().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, not(containsString("united"))).
        parentRecordTextFieldShouldMetCriteria(AccountFields.ACTIVE, not(equalTo("Yes"))).
        parentRecordInputFieldShouldMetCriteria(AccountFields.BILLING_CITY, equalTo("San Francisco")).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02357", description = "Filter by child option")
    public void at02357() {
        
        String gridName = "AT-02357GridAuto";
        List<String> fields = Arrays.asList(AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_SITE);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(fields).
        selectRelatedObject(ACCOUNT).
        selectFields(fields).
        clickNext().
        shouldNotSeeFilterByRelatedObject().
        openGridWizard().
        deleteGrid(gridName);

    }
    

    @Test(groups = {"Regression", "P0"}, testName = "AT-02863", description = "Verify Default filter is loaded in GW3 after the navigation from the grid while Default admin filter is selected and it is not the only ADF for single-object grid")
    public void at02863() {
        
        String gridName = "AT-02863GridAuto";
        
        List<String> parentObjectFields = Arrays.asList("Account Name", "Amount", "Created Date", "Close Date", "Description", "Owner", "Order Number");

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        createUserFilter().
        enterFilterName("Amount Filter").
        setFilterCondition(OPPORTUNITY, FIRST_ROW, "Amount", "not equal to", "null").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        myFilterShouldHaveValue("(Admin) Amount Filter").
        selectFilter("(Default filter)").
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        shouldSeeFilterSelected("Default filter").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = {"Regression", "P0"}, testName = "AT-02866", description = "Verify the current filter is loaded in GW3 after navigation from the grid in the multi-object grid")
    public void at02866() {
        
        String gridName = "AT-02866GridAuto";
        
        List<String> parentObjectFields = Arrays.asList("Account Name", "Amount", "Created Date", "Close Date", "Description", "Owner", "Order Number");
        
        List<String> relatedObjectFields = Arrays.asList("Product Name", "Total Price", "Product Code");

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject("Opportunity Product").
        selectFields(relatedObjectFields).
        clickNext().
        setFilterCondition(OPPORTUNITY, FIRST_ROW, "Amount", "not equal to", "").
        setRelatedFilter("Has").
        saveFilter().
        createUserFilter().
        enterFilterName("Created Date").
        setFilterCondition(OPPORTUNITY, FIRST_ROW, "Created Date", "equals", "THIS_QUARTER").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        myFilterShouldHaveValue("(Admin) Created Date").
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        selectFilter("Created Date").
        setRelatedFilter("Has").
        setFilterCondition("Opportunity Product", FIRST_ROW, "Total Price", "greater than", "1000").
        setFilterCondition("Opportunity Product", SECOND_ROW, "Total Price", "not equal to", "").
        saveFilter().
        switchToDefaultContent().
        closePopup().
        selectFilter("(Admin) Created Date").
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        shouldSeeFilterSelected("Created Date").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02020", description = "Verify new format option to group data by quarter is added in GW3")
    public void rt02020() {
        
        String gridName = "RT-02020GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        shouldSeeShowGroupingExpandedChecked().
        shouldSeeShowRecordDetailsChecked().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Fiscal Quarter", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Calendar Quarter", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Calendar Month", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    
}