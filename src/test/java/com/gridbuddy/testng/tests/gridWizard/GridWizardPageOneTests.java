package com.gridbuddy.testng.tests.gridWizard;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.Matchers.containsString;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.objects.Contract.ContractFields;
import com.gridbuddy.objects.Event.EventFields;
import com.gridbuddy.objects.Opportunity.OpportunityFields;
import com.gridbuddy.steps.GBSteps;
import com.gridbuddy.steps.GW1Steps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Grid Wizard")
@Story("Grid Wizard Page One")
public class GridWizardPageOneTests extends BaseTest {

    @Test(groups = { "Regression", "P0" }, testName = "AT-02419", description = "Test creating and saving a new grid")
    public void at02419() {
        
        String gridName = "AT-02419GridAuto";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        checkAllCheckboxes().
        saveGrid().
        launchGrid().
        columnsNumberShouldBe(1).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02431", description = "Test the required fields")
    public void at02431() {
        
        String gridName = "AT-02431GridAuto";
        
        GW1Steps gw1Steps = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, ACCOUNT).
        openGridWizard().
        createNew();
        gw1Steps.clickNext().
        shouldSeeErrorMessages("You must enter a name for your grid.", "You must select a parent object for your grid.");
        gw1Steps.selectParentObject(ACCOUNT).
        clickNext().
        shouldSeeErrorMessage("You must enter a name for your grid.");
        gw1Steps.selectParentObject("--Select Object--").
        enterGridName(RandomStringUtils.randomAlphabetic(10)).
        clickNext().
        shouldSeeErrorMessage("You must select a parent object for your grid.");
        gw1Steps.selectParentObject("--Select Object--").
        enterGridName(gridName).
        clickNext().
        shouldSeeErrorMessages("A Grid with this name already exists. Please choose a unique name.", "You must select a parent object for your grid.");
        gw1Steps.selectParentObject(ACCOUNT).
        enterGridName(gridName).
        clickNext().
        shouldSeeErrorMessages("A Grid with this name already exists. Please choose a unique name.").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02433", description = "Test values in Grid dropdown field")
    public void at02433() {
        
        getUser().
        login().
        openGridWizard().
        createNew().
        shouldSeeCreateNewAtTheTopOfGridsDropdown();

    }
    
    @Test(groups = { }, testName = "AT-02435", description = "Testing only show header")
    public void at02435() {
        
        String gridName = "AT-02435GridAuto";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        uncheckAllCheckboxes().
        saveGrid().
        launchGrid().
        shouldNotSeeSalesforceHader().
        shouldNotSeeSaveButton().
        openMoreMenu().
        clickEditSettingsMenuItem().
        switchToEditSettingsPopup().
        checkShowHeaderCheckbox().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeSalesforceHader().
        shouldNotSeeSaveButton().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02436", description = "Testing 'Refresh' button - an existing Grid")
    public void at02436() {
        
        String gridName = "AT-02436GridAuto";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        checkAllCheckboxes().
        saveGrid().
        openGridWizard().
        selectGrid(gridName).
        selectParentObject(CONTACT).
        alertConfirm().
        uncheckAllCheckboxes().
        clickRefresh().
        shouldSeeSelectedObject(ACCOUNT).
        shouldSeeShowGroupingExpandedChecked().
        shouldSeeShowRecordDetailsChecked().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { }, testName = "AT-02437", description = "Testing show sidebar")
    public void at02437() {
        
        String gridName = "AT-02437GridAuto";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        uncheckAllCheckboxes().
        saveGrid().
        launchGrid().
        shouldNotSeeSalesforceHader().
        shouldNotSeeSidebar().
        shouldNotSeeSaveButton().
        openMoreMenu().
        clickEditSettingsMenuItem().
        switchToEditSettingsPopup().
        checkShowSidebarCheckbox().
        shouldSeeShowHeaderCheckboxChecked().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeSidebar().
        shouldSeeSalesforceHader().
        shouldNotSeeSaveButton().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02445", description = "Verify Read Only Mode works when setup in GW1")
    public void at02445() {
        
        String gridName = "AT-02445GridAuto";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        uncheckEditCheckbox().
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(1).
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldNotSeeSaveButton().
        shouldNotSeeNewButton().
        shouldNotSeeMassUpdateButton().
        shouldNotSeeMassCreateButton().
        shouldNotSeeNewButtonForRelatedObject().
        fieldShouldBeNotEditable(FIRST_ROW, FIRST_ROW, ContactFields.ACCOUNT_NAME).
        fieldShouldBeNotEditable(FIRST_ROW, ContactFields.ACCOUNT_NAME).
        openMoreMenu().
        clickEditSettingsMenuItem().
        switchToEditSettingsPopup().
        checkEditCheckbox().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeSaveButton().
        fieldShouldBeEditable(FIRST_ROW, FIRST_ROW, ContactFields.ACCOUNT_NAME).
        fieldShouldBeEditable(FIRST_ROW, ContactFields.ACCOUNT_NAME).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { }, testName = "AT-02448", description = "Verify URL parameters (ea=0; ea=1; ea= {comma separated child object api name}; ea=show_related_objects ), embeded to a detail page")
    public void at02448() {
        
        String gridName = "AT-02416GridAuto";
        
        GBSteps userOnGbPage = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(Arrays.asList(ContactFields.ACCOUNT_NAME)).
        selectRelatedObject(AccountRelatedObjects.OPPORTUNITY).
        selectFields(Arrays.asList(OpportunityFields.AMOUNT)).
        selectRelatedObject(AccountRelatedObjects.CONTRACT).
        selectFields(Arrays.asList(ContractFields.ACTIVATED_DATE)).
        selectRelatedObject(AccountRelatedObjects.EVENT).
        selectFields(Arrays.asList(EventFields.ACCOUNT_NAME)).
        saveGrid().
        launchGrid();
        String pageUrl = userOnGbPage.getPageUrl();
        userOnGbPage.shouldSeeParentObjectsCollapsed();
        userOnGbPage.getGbPage(pageUrl + "&ea=1");
        userOnGbPage.shouldSeeParentObjectsExpanded().
        shouldSeeRelatedObjectsExpanded();
        userOnGbPage.getGbPage(pageUrl + "&ea=0");
        userOnGbPage.shouldSeeParentObjectsCollapsed();
        userOnGbPage.getGbPage(pageUrl + "&ea=show_related_objects");
        userOnGbPage.shouldSeeParentObjectsExpanded().
        shouldSeeRelatedObjectsCollapsed();
        userOnGbPage.getGbPage(pageUrl + "&ea=conTact,EVENT");
        userOnGbPage.shouldSeeParentObjectsExpanded().
        shouldSeeRelatedObjectsExpanded("Contacts","Events").
        shouldSeeRelatedObjectsCollapsed("Contracts","Opportunities").
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02368", description = "Testing Unique Name requirement")
    public void at02638() {
        
        String gridName = "AT-02368GridAuto";
        String message = "A Grid with this name already exists. Please choose a unique name.";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        openGridWizard().
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        shouldSeeErrorMessage(message).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02354", description = "Create a grid in GridBuddy.")
    public void at02354() {
        
        String gridName = "AT-02354GridAuto";
        
        List<String> fields = Arrays.asList(AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_SITE);
        List<String> expectedFields = Arrays.asList(AccountFields.ACCOUNT_NAME.toUpperCase() + "*", AccountFields.ACCOUNT_FAX.toUpperCase(),
                AccountFields.ACCOUNT_SITE.toUpperCase());
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(fields).
        saveGrid().
        launchGrid().
        shouldSeeColumns(expectedFields).
        goToPage(2).
        shouldSeeColumns(expectedFields).
        shouldSeeDataRows().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02363", description = "Reordering and hiding columns")
    public void at02363() {
        
        String gridName = "AT-02363GridAuto";
        
        List<String> fields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE,
                AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE,
                AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

        List<String> expectedFields = Arrays.asList(AccountFields.ACCOUNT_PHONE.toUpperCase(), AccountFields.ACCOUNT_FAX.toUpperCase(), AccountFields.ACCOUNT_ID.toUpperCase() + "*",
                AccountFields.ACCOUNT_SITE.toUpperCase(), AccountFields.ACCOUNT_SOURCE.toUpperCase(),
                AccountFields.ACCOUNT_TYPE.toUpperCase(), AccountFields.ACTIVE.toUpperCase());
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        checkUserDefinedColumnsCheckbox().
        clickNext().
        selectFields(fields).
        saveGrid().
        launchGrid().
        openReorderColumnsPopup().
        uncheckCheckboxesOnReorderPopup(AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_RATING).
        dragAndDropColumn(AccountFields.ACCOUNT_PHONE).
        saveReorderSettings().
        shouldSeeColumns(expectedFields).
        openMoreMenu().
        clickEditSettingsMenuItem().
        switchToEditSettingsPopup().
        uncheckUserDefinedColumnsCheckbox().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        openMoreMenu().
        shouldNotSeeGridMenuItem("Reorder/Hide Columns").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01981", description = "Check Contract Line Item object is available in Parent object dropdown on GW1")
    public void rt01981() {
        
        getUser().
        login().
        openGridWizard().
        createNew().
        shouldSeeItemInParentObjectDropdown("Contract Line Item");

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-01063", description = "Clone existing grid and launch")
    public void rt01063() {
        
        String gridName = "RT-01063GridAuto";
        
        String customCode = "RT-01063CustomCode";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        deleteGrid(gridName + "cloned").
        openManageCustomCode().
        deleteCustomCode(customCode).
        createCustomCode(customCode, "CSS", "Grid", ".gridBtns.top {background-color:#ff0000;}").
        openGridWizard().
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        setShowRelatedObjectsExpanded().
        checkAllCheckboxes().
        selectCustomCss(customCode).
        clickNext().
        selectFields(Arrays.asList("Employees", "Account Number")).
        selectRelatedObject(CONTACT).
        selectFields(Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.LAST_NAME, ContactFields.BUSINESS_PHONE)).
        addConditionFormattingRule("Parent", "Account:NumberOfEmployees", "greater than", "10", "Account:NumberOfEmployees", false, 2, 0, "bold", "italic").
        addConditionFormattingRule("Child", "Contact:LastName:AccountId", "contains", "1", "Contact:LastName:AccountId", true, 0, -1).
        clickNext().
        setFilterCondition(ACCOUNT, 0, "Employees", "greater than", "50").
        setSortingCondition(ACCOUNT, FIRST_ROW, "Account Number", "Descending").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        recordsShouldBeSortedByTextFieldIgnoringCase("Account Number", "Descending").
        topRowWithButtonsShouldHaveColorBackground("rgba(255, 0, 0, 1)").
        cellsShouldHaveColorBackground("Employees", "rgba(229, 239, 122, 1)").
        cellsShouldHaveTextColor("Employees", "rgba(179, 36, 36, 1)").
        cellsShouldHaveItalicFontStyle("Employees").
        cellsShouldHaveBoldFontStyle("Employees").
        childRowWithCellMatchingCriteriaShouldHaveColorBackground(ContactFields.LAST_NAME, containsString("1"), "rgba(255, 114, 114, 1)").
        openGridWizard().
        selectGrid(gridName).
        shouldSeeAllCheckboxesChecked().
        enterGridName(gridName + "cloned").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        recordsShouldBeSortedByTextFieldIgnoringCase("Account Number", "Descending").
        topRowWithButtonsShouldHaveColorBackground("rgba(255, 0, 0, 1)").
        cellsShouldHaveColorBackground("Employees", "rgba(229, 239, 122, 1)").
        cellsShouldHaveTextColor("Employees", "rgba(179, 36, 36, 1)").
        cellsShouldHaveItalicFontStyle("Employees").
        cellsShouldHaveBoldFontStyle("Employees").
        childRowWithCellMatchingCriteriaShouldHaveColorBackground(ContactFields.LAST_NAME, containsString("1"), "rgba(255, 114, 114, 1)").
        openGridWizard().
        deleteGrid(gridName).
        deleteGrid(gridName + "cloned").
        openManageCustomCode().
        deleteCustomCode(customCode);

    }
    
    
}