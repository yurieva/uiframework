package com.gridbuddy.testng.tests.editableRelatedColumn;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.Matchers.equalTo;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.steps.GBSteps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Editable Related Column")
@Story("Editable Related Column Tests")
public class EditableRelatedColumnTests extends BaseTest {
    
    @Test(groups = { "Regression", "P1" }, testName = "editableRelatedColumn", description = "Create the grid with Editable related column active for the related child object")
    public void editableRelatedColumn() {
        
        String gridName = "editableRelatedColumnGridAuto";
        
        GBSteps userOnGbPage = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, ACCOUNT).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(Arrays.asList(AccountFields.ACTIVE)).
        selectRelatedObject(CONTACT).
        selectFields(Arrays.asList("Last Name", "First Name")).
        openObjectSettings(CONTACT).
        uncheckEditableRelatedColumnCheckbox().
        saveObjectLevelSettings().
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeColumns(Arrays.asList("ACCOUNT NAME*", "ACTIVE")).
        openEditFieldsPopup().
        switchToEditFieldsPopup().
        openObjectSettings(CONTACT).
        checkEditableRelatedColumnCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().shouldSeeRecords();
        List<String> names = userOnGbPage.getChildObjectsInputFieldValues(FIRST_ROW, "Last Name");
        userOnGbPage.shouldSeeColumns(Arrays.asList("ACCOUNT NAME*", "ACTIVE", "CONTACTS")).
        editableRelatedColumnFieldShouldMetCriteria(FIRST_ROW, "Contacts", equalTo(String.join(", ", names) + "\nmore")).
        openEditFieldsPopup().
        switchToEditFieldsPopup().
        removeChildFields(CONTACT, Arrays.asList("Last Name", "First Name")).
        selectRelatedObject(CONTACT).
        selectFields(Arrays.asList("First Name", "Last Name")).
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeRecords();
        names = userOnGbPage.getChildObjectsInputFieldValues(FIRST_ROW, "First Name");
        userOnGbPage.shouldSeeColumns(Arrays.asList("ACCOUNT NAME*", "ACTIVE", "CONTACTS")).
        editableRelatedColumnFieldShouldMetCriteria(FIRST_ROW, "Contacts", equalTo(String.join(", ", names) + "\nmore")).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P1" }, testName = "editableUnrelatedColumn", description = "Create the grid with Editable related column active for the related child object")
    public void editableUnrelatedColumn() {
        
        String gridName = "editableUnrelatedColumnGridAuto";
        
        GBSteps userOnGbPage = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, OPPORTUNITY).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(Arrays.asList("Account Name")).
        selectAdditionalObject(CONTACT).
        selectFields(Arrays.asList("Last Name", "First Name", "Account Name")).
        setObjectMapping(CONTACT, "Opportunity.Account Name (ID REFERENCE)", "Contact.Account Name (ID REFERENCE)").
        openObjectSettings(CONTACT).
        uncheckEditableRelatedColumnCheckbox().
        saveObjectLevelSettings().
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeColumns(Arrays.asList("NAME*", "ACCOUNT NAME")).
        openEditFieldsPopup().
        switchToEditFieldsPopup().
        openObjectSettings(CONTACT).
        checkEditableRelatedColumnCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().shouldSeeRecords();
        List<String> names = userOnGbPage.getChildObjectsInputFieldValues(FIRST_ROW, "Last Name");
        userOnGbPage.shouldSeeColumns(Arrays.asList("NAME*", "ACCOUNT NAME", "CONTACTS")).
        editableRelatedColumnFieldShouldMetCriteria(FIRST_ROW, "Contacts", equalTo(String.join(", ", names) + "\nmore")).
        openEditFieldsPopup().
        switchToEditFieldsPopup().
        removeChildFields(CONTACT, Arrays.asList("Last Name", "First Name", "Account Name")).
        selectAdditionalObject(CONTACT).
        selectFields(Arrays.asList("First Name", "Last Name", "Account Name")).
        setObjectMapping(CONTACT, "Opportunity.Account Name (ID REFERENCE)", "Contact.Account Name (ID REFERENCE)").
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeRecords();
        names = userOnGbPage.getChildObjectsInputFieldValues(FIRST_ROW, "First Name");
        userOnGbPage.shouldSeeColumns(Arrays.asList("NAME*", "ACCOUNT NAME", "CONTACTS")).
        editableRelatedColumnFieldShouldMetCriteria(FIRST_ROW, "Contacts", equalTo(String.join(", ", names) + "\nmore")).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    


}
