package com.gridbuddy.testng.tests.createRecords;

import static com.gridbuddy.objects.ObjectUtils.getRandomValue;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static com.gridbuddy.utils.Constants.SECOND_ROW;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Create")
@Story("Create Tests")
public class CreateTests extends BaseTest {

    @Test(groups = {"Regression", "P0"}, testName = "AT-02361", description = "Inline create")
    public void at02361() {

        String gridName = "AT-02361GridAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
        ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);

        String accountName = RandomStringUtils.randomAlphabetic(10);
        String description = RandomStringUtils.randomAlphabetic(30);
        String accountPhone = RandomStringUtils.randomNumeric(10);
        String accountFax = RandomStringUtils.randomNumeric(10);
        String accountSite = "http://" + RandomStringUtils.randomAlphabetic(10) + ".com";
        String accountNumber = RandomStringUtils.randomAlphanumeric(10).toUpperCase();
        String accountRating = getRandomValue(AccountFields.ACCOUNT_RATINGS);
        String accountType = getRandomValue(AccountFields.ACCOUNT_TYPES);
        String accountStatus = getRandomValue(AccountFields.ACTIVE_STATUSES);

        String assistantsName = RandomStringUtils.randomAlphabetic(10);
        String asstPhone = RandomStringUtils.randomNumeric(10);
        String businessFax = RandomStringUtils.randomNumeric(10);
        String businessPhone = RandomStringUtils.randomNumeric(10);
//        String cleanStatus = getRandomValue(ContactFields.CLEAN_STATUSES);
        String contactDescription = RandomStringUtils.randomAlphabetic(30);
        String lastName = RandomStringUtils.randomAlphabetic(10);
        String birthDate = LocalDate.now().minusYears(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        saveGrid().
        launchGrid().
        checkRelatedRecordsCheckbox().
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        editParentRecordTextareaField(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, description).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_FAX, accountFax).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_PHONE, accountPhone).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACCOUNT_RATING, accountRating).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_SITE, accountSite).
//        editParentRecordSelectField(FIRST_ROW, AccountFields.ACCOUNT_SOURCE, accountSource).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACCOUNT_TYPE, accountType).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACTIVE, accountStatus).
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        editRelatedRecordDateField(FIRST_ROW, FIRST_ROW, ContactFields.BIRTHDATE, birthDate).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_FAX, businessFax).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_PHONE, businessPhone).
//        editRelatedRecordSelectField(FIRST_ROW, FIRST_ROW, ContactFields.CLEAN_STATUS, cleanStatus).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.CONTACT_DESCRIPTION, contactDescription).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, lastName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "equals", accountName).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, description).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_FAX, accountFax).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_PHONE, accountPhone).
        parentObjectFieldShouldHaveText(FIRST_ROW, AccountFields.ACCOUNT_RATING, accountRating).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_SITE, accountSite).
//        parentObjectFieldShouldHaveText(FIRST_ROW, AccountFields.ACCOUNT_SOURCE, accountSource).
        parentObjectFieldShouldHaveText(FIRST_ROW, AccountFields.ACCOUNT_TYPE, accountType).
        parentObjectFieldShouldHaveText(FIRST_ROW, AccountFields.ACTIVE, accountStatus).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ACCOUNT_NAME, accountName).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BIRTHDATE, birthDate).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_FAX, businessFax).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_PHONE, businessPhone).
//        relatedObjectFieldShouldHaveText(FIRST_ROW, FIRST_ROW, ContactFields.CLEAN_STATUS, cleanStatus).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.CONTACT_DESCRIPTION, contactDescription).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, lastName).
        openGridWizard().
        deleteGrid(gridName);
    }

    
    @Test(groups = {"Regression", "P0"}, testName = "AT-02361", description = "Create new record for unrelated child")
    public void createNewRecordForUnrelatedChild() {
        
        String gridName = "createNewRecordForUnrelatedChild";
        
        List<String> parentObjectFields = Arrays.asList("Account Name", "Amount", "Close Date");

        List<String> relatedObjectFields = Arrays.asList("Name", "Account Name", "Asset Name", "Case Currency", "Case ID", "Case Number", "Case Origin");
        
        String caseName = RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        selectShowRelatedObjectsExpanded().
        checkDeleteCheckbox().
        clickNext().
        selectFields(parentObjectFields).
        selectAdditionalObject("Case").
        selectFields(relatedObjectFields).
        setObjectMapping("Case", "Opportunity.Account Name (ID REFERENCE)", "Case.Account Name (ID REFERENCE)").
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "Name", caseName).
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeChildObjectWithFieldValue(FIRST_ROW, "Name", caseName).
        selectChildRecord(FIRST_ROW, "Name", caseName).
        openMoreMenu().
        deleteSelectedItems().
        shouldNotSeeChildObjectWithFieldValue(FIRST_ROW, "Name", caseName).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = {"Regression", "P0"}, testName = "createNewObjectForGroupedRecords", description = "Create new records when header is grouped")
    public void createNewObjectForGroupedRecords() {
        
        String gridName = "createNewObjectForGroupedRecords";
        
        String parentAccountName = RandomStringUtils.randomAlphabetic(10);
        
        String accountNumber = RandomStringUtils.randomAlphanumeric(10).toUpperCase();
        
        String assistantsName = RandomStringUtils.randomAlphabetic(10);
        
        String lastName = RandomStringUtils.randomAlphabetic(10);
        
        String asstPhone = RandomStringUtils.randomNumeric(10);
        
        List<String> accountFields = Arrays.asList(AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_CURRENCY);
        
        List<String> contactFields = Arrays.asList(ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.LAST_NAME);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(accountFields).
        selectRelatedObject(CONTACT).
        selectFields(contactFields).
        clickNext().
        setGroupByCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_TYPE, "Descending").
        setGroupByCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NAME).
//        setGroupByCondition(ACCOUNT, THIRD_ROW, AccountFields.ACCOUNT_CURRENCY).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeGroupingResults(1, AccountFields.ACCOUNT_TYPE).
        shouldSeeGroupingResults(2, AccountFields.ACCOUNT_NAME).
//        shouldSeeGroupingResults(3, AccountFields.ACCOUNT_CURRENCY).
        shouldSeeGroupingFieldDiv(1, AccountFields.ACCOUNT_TYPE).
        shouldSeeGroupingFieldInput(2, AccountFields.ACCOUNT_NAME).
        clickAddNewParentObject().
//        shouldSeeGroupingFieldDiv(3, AccountFields.ACCOUNT_CURRENCY).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NAME, parentAccountName).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        saveChanges().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "contains", parentAccountName).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NAME, parentAccountName).
        expandParentRecord(FIRST_ROW).
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, lastName).
        saveChanges().
        expandParentRecord(FIRST_ROW).
        expandRelatedRecord(FIRST_ROW).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        openGridWizard().
        deleteGrid(gridName);
    }

}
