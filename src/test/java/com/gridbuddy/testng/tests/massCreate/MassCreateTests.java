package com.gridbuddy.testng.tests.massCreate;

import static com.gridbuddy.objects.ObjectUtils.getNonEmptyRandomValue;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static com.gridbuddy.utils.Constants.SECOND_ROW;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.steps.GBSteps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;

@Feature("Mass Create")
@Story("Mass Create")
public class MassCreateTests extends BaseTest {

    @Test(groups = { "Regression", "P1" }, testName = "massCreateParent", description = "Create multiple parent records using Mass Create")
    public void massCreateParent() {
        
        String gridName = "massCreateParentGridAuto";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

        String accountName = RandomStringUtils.randomAlphabetic(10);
        String description = RandomStringUtils.randomAlphabetic(30);
        String accountPhone = RandomStringUtils.randomNumeric(10);
        String accountFax = RandomStringUtils.randomNumeric(10);
        String accountSite = "http://" + RandomStringUtils.randomAlphabetic(10) + ".com";
        String accountNumber = RandomStringUtils.randomAlphanumeric(10).toUpperCase();
        String accountType = getNonEmptyRandomValue(AccountFields.ACCOUNT_TYPES);
        String accountStatus = getNonEmptyRandomValue(AccountFields.ACTIVE_STATUSES);

        GBSteps userOnGbPage = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        openMassCreatePopup().
        enterNumberOfRecords(3).
        editMassCreateInputField(AccountFields.ACCOUNT_NAME, accountName).
        editMassCreateTextareaField(AccountFields.ACCOUNT_DESCRIPTION, description).
        editMassCreateInputField(AccountFields.ACCOUNT_FAX, accountFax).
        editMassCreateInputField(AccountFields.ACCOUNT_NUMBER, accountNumber).
        editMassCreateInputField(AccountFields.ACCOUNT_PHONE, accountPhone).
        editMassCreateInputField(AccountFields.ACCOUNT_SITE, accountSite).
        editMassCreateSelectField(AccountFields.ACCOUNT_TYPE, accountType).
        editMassCreateSelectField(AccountFields.ACTIVE, accountStatus).
        clickCreateParentRecordsButton();
        
		for (int i = 0; i < 3; i++) {
			userOnGbPage.parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
            parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, description).
            parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_FAX, accountFax).
            parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
            parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_PHONE, accountPhone).
            parentObjectFieldShouldHaveText(FIRST_ROW, AccountFields.ACCOUNT_RATING, "--None--").
            parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_SITE, accountSite).
            parentObjectSelectFieldShouldHaveOptionSelected(FIRST_ROW, AccountFields.ACCOUNT_TYPE, accountType).
            parentObjectSelectFieldShouldHaveOptionSelected(FIRST_ROW, AccountFields.ACTIVE, accountStatus);
		}
        
		userOnGbPage.saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "equals", accountName).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, description).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_FAX, accountFax).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_PHONE, accountPhone).
        parentObjectFieldShouldHaveText(FIRST_ROW, AccountFields.ACCOUNT_RATING, "--None--").
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_SITE, accountSite).
        parentObjectFieldShouldHaveText(FIRST_ROW, AccountFields.ACCOUNT_TYPE, accountType).
        parentObjectFieldShouldHaveText(FIRST_ROW, AccountFields.ACTIVE, accountStatus).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = {"Regression", "P0"}, testName = "massCreateChild", description = "Create multiple child record using Mass Create")
    public void massCreateChild() {

        String gridName = "massCreateChildGridAuto";

        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
        ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);

        String assistantsName = RandomStringUtils.randomAlphabetic(10);
        String asstPhone = RandomStringUtils.randomNumeric(10);
        String businessFax = RandomStringUtils.randomNumeric(10);
        String businessPhone = RandomStringUtils.randomNumeric(10);
        String contactDescription = RandomStringUtils.randomAlphabetic(30);
        String lastName = RandomStringUtils.randomAlphabetic(10);
        String birthDate = LocalDate.now().minusYears(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));

        GBSteps userOnGbPage = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        saveGrid().
        launchGrid().
        openMassCreatePopup().
        enterNumberOfRecords(3).
        openMassCreatePopup().
        enterNumberOfRecords(3).
        editMassCreateInputField("Contacts", ContactFields.ASSISTANTS_NAME, assistantsName).
        editMassCreateInputField("Contacts",  ContactFields.ASST_PHONE, asstPhone).
        editMassCreateInputField("Contacts", ContactFields.BIRTHDATE, birthDate).
        editMassCreateInputField("Contacts", ContactFields.BUSINESS_FAX, businessFax).
        editMassCreateInputField("Contacts", ContactFields.BUSINESS_PHONE, businessPhone).
        editMassCreateTextareaField("Contacts", ContactFields.CONTACT_DESCRIPTION, contactDescription).
        editMassCreateInputField("Contacts", ContactFields.LAST_NAME, lastName).
        selectParentRecord(FIRST_ROW).
        clickCreateRelated();
        
        String accountName = userOnGbPage.getCellValue(FIRST_ROW, AccountFields.ACCOUNT_NAME);
        
        for (int i = 0; i < 3; i++) {
        	userOnGbPage.relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
            relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
            relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BIRTHDATE, birthDate).
            relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_FAX, businessFax).
            relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_PHONE, businessPhone).
            relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.CONTACT_DESCRIPTION, contactDescription).
            relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, lastName);
		}
        
        userOnGbPage.saveChanges().
        shouldSeeSuccessMessage().
        applyRelatedColumnFilter(FIRST_ROW, ContactFields.LAST_NAME, lastName).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ACCOUNT_NAME, accountName).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BIRTHDATE, birthDate).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_FAX, businessFax).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_PHONE, businessPhone).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.CONTACT_DESCRIPTION, contactDescription).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, lastName).
        openGridWizard().
        deleteGrid(gridName);
    }
    
    @Test(groups = { "Regression", "P1" }, testName = "massCreateParentSeveralTimes", description = "Make mass create action for parent object several times")
    @Issue("https://newbuddy.atlassian.net/browse/GB-518")
    public void massCreateParentSeveralTimes() {
        
        String gridName = "massCreateParentSeveralTimesAuto";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

        String accountNameOne = RandomStringUtils.randomAlphabetic(10);
        
        String accountNameTwo = RandomStringUtils.randomAlphabetic(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        openMassCreatePopup().
        enterNumberOfRecords(1).
        editMassCreateInputField(AccountFields.ACCOUNT_NAME, accountNameOne).
        clickCreateParentRecordsButton().
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountNameOne).
        editMassCreateInputField(AccountFields.ACCOUNT_NAME, accountNameTwo).
        clickCreateParentRecordsButton().
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountNameTwo).
        parentObjectFieldShouldHaveValue(SECOND_ROW, AccountFields.ACCOUNT_NAME, accountNameOne).
        openGridWizard().
        deleteGrid(gridName);

    }
    
}