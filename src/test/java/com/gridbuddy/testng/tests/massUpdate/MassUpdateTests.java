package com.gridbuddy.testng.tests.massUpdate;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.utils.Constants.SECOND_ROW;
import static org.hamcrest.Matchers.equalTo;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.objects.Opportunity.OpportunityFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Mass Update")
@Story("Mass Update")
public class MassUpdateTests extends BaseTest {

    @Test(groups = { "Regression", "P1" }, testName = "AT-02422", description = "Test Enable mass updates")
    public void at02422() {
        
        String gridName = "AT-02422GridAuto";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        uncheckAllCheckboxes().
        checkMassUpdateCheckbox().
        saveGrid().
        launchGrid().
        shouldSeeMassUpdateButton().
        switchToMainWindow().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1", "Smoke" }, testName = "AT-02364", description = "Test Mass Update")
    public void at02364() {
        
        String gridName = "AT-02364GridAuto";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE,
                AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE,
                AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE, ContactFields.BIRTHDATE, ContactFields.BUSINESS_FAX,
                ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION,
                ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);

        String fax = RandomStringUtils.randomAlphanumeric(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        selectRelatedObject(AccountRelatedObjects.OPPORTUNITY).
        selectFields(Arrays.asList(OpportunityFields.AMOUNT)).
        saveGrid().
        launchGrid().
        openMassUpdatePopup().
        editMassUpdateInputField(AccountFields.ACCOUNT_NAME, RandomStringUtils.randomAlphabetic(10)).
        editMassUpdateInputField(AccountFields.ACCOUNT_NUMBER, RandomStringUtils.randomAlphabetic(10)).
        editMassUpdateInputField("Contacts", ContactFields.ASSISTANTS_NAME, RandomStringUtils.randomAlphabetic(10)).
        editMassUpdateInputField("Opportunities", OpportunityFields.AMOUNT, RandomStringUtils.randomNumeric(3)).
        clickClearAllOnMassUpdatePopup().
        shouldSeeMassUpdateFieldEmpty(AccountFields.ACCOUNT_NAME).
        shouldSeeMassUpdateFieldEmpty(AccountFields.ACCOUNT_NUMBER).
        shouldSeeMassUpdateFieldEmpty("Contacts", ContactFields.ASSISTANTS_NAME).
        shouldSeeMassUpdateFieldEmpty("Opportunities", OpportunityFields.AMOUNT).
        clickCollapseAllOnMassUpdatePopup().
        shouldSeeChildObjectsOnMassUpdatePopupCollapsed().
        clickExpandAllOnMassUpdatePopup().
        shouldSeeChildObjectsOnMassUpdatePopupExpanded().
        editMassUpdateInputField(AccountFields.ACCOUNT_FAX, fax).
        clickApplyToAllRecordsButton().
        shouldSeeMassUpdateMessage("To complete mass update, click Save on the Grid page.").
        closePopup().
        cellsShouldHaveColorBackground(AccountFields.ACCOUNT_FAX, "rgba(251, 254, 201, 1)").
        saveChanges().
        shouldSeeSuccessMessage().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_FAX, equalTo(fax)).
        selectParentRecord(SECOND_ROW).
        openMassUpdatePopup().
        editMassUpdateInputField(AccountFields.ACCOUNT_FAX, fax).
        clickApplyToSelectedRecordsButton().
        shouldSeeMassUpdateMessage("To complete mass update, click Save on the Grid page.").
        closePopup().
        cellShouldHaveColorBackground(SECOND_ROW, AccountFields.ACCOUNT_FAX, "rgba(251, 254, 201, 1)").
        refreshGrid().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_FAX, equalTo(fax)).
        switchToMainWindow().
        openGridWizard().
        deleteGrid(gridName);

    }
    
}