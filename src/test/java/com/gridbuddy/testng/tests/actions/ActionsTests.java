package com.gridbuddy.testng.tests.actions;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.utils.Constants.FIRST_ROW;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Actions")
@Story("Smoke tests from Actions tab")
public class ActionsTests extends BaseTest {

    @Test(groups = { "Regression", "P0" }, testName = "AT-02410", description = "Manage Actions")
    public void at02410() {

        getUser().
        login().
        openGridWizard().
        openManageActions().
        shouldBeOnManageActionsPage().
        goBack().
        shouldBeOnGwLandingPage();

    }

    @Test(groups = { "Regression", "P0" }, testName = "SmokeAP01", description = "Test New button in the Manage Actions page")
    public void SmokeAP01() {

        getUser().
        login().
        openGridWizard().
        openManageActions().
        clickNewButton().
        checkCreateActionFields();

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeAP02", description = "Test Save button in the Manage Actions page")
    public void SmokeAP02() {

        String actionName = "SmokeAP02Action";
        String gridName = "SmokeAP02Grid";
        String description = RandomStringUtils.randomAlphanumeric(30);

        getUser().
        login().
        openGridWizard().
        openManageActions().
        deleteAction(actionName).
        clickNewButton().
        createAction(actionName,description, ACCOUNT, "Single Record", "Menu", "Window Overlay", "URL", "https://www.appbuddy.com/").
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        shouldSeeAction(actionName).
        checkActionFields(actionName, ACCOUNT, "URL", "Window Overlay", "Single Record", description).
        openGridWizard().
        deleteGrid(gridName).
        openManageActions().
        deleteAction(actionName);
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeAP03", description = "Test Delete button in the Manage Actions page")
    public void SmokeAP03() {

        String actionName = "SmokeAP03Action";
        String gridName = "SmokeAP03Grid";

        getUser().
        login().
        openGridWizard().
        openManageActions().
        deleteAction(actionName).
        clickNewButton().
        createAction(actionName,RandomStringUtils.randomAlphanumeric(30), ACCOUNT, "Single Record", "Menu", "Window Overlay", "URL", "https://www.appbuddy.com/").
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        shouldSeeAction(actionName).
        openGridWizard().
        openManageActions().
        deleteAction(actionName).
        openGridWizard().
        selectGrid(gridName).
        clickNext().
        shouldNotSeeAction(actionName).
        openGridWizard().
        deleteGrid(gridName);
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeAP04", description = "Test Content Source field in the Manage Actions page")
    public void SmokeAP04() {

        getUser().
        login().
        openGridWizard().
        openManageActions().
        clickNewButton().
        shouldSeeContentSourceOptions();
        
    }
    
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeAP05", description = "Test Grid Content field value in the Manage Actions page")
    public void SmokeAP05Grid() {

        String actionName = "SmokeAP05ActionGrid";
        String gridName = "SmokeAP05GridGrid";
        String gridTwoName = "SmokeAP05GridTwoGrid";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        deleteGrid(gridTwoName).
        openManageActions().
        deleteAction(actionName).
        clickNewButton().
        createAction(actionName,RandomStringUtils.randomAlphanumeric(30), ACCOUNT, "Single Record", "Menu", "Window Overlay", "Grid", gridTwoName).
        openGridWizard().
        createGrid(gridTwoName, ACCOUNT).
        openGridWizard().
        createGrid(gridName, ACCOUNT).
        clickNext().
        selectAction(actionName).
        saveGrid().
        launchGrid().
        selectAction(FIRST_ROW, actionName).
        switchToWindow().
        shouldSeePageUrl("/apex/Grid?gname=" + gridTwoName + "&sfname=default&action_param=").
        shouldSeeGBTable().
        switchToMainWindow().
        openGridWizard().
        deleteGrid(gridName).
        deleteGrid(gridTwoName).
        openManageActions().
        deleteAction(actionName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeAP05", description = "Test URL Content field value in the Manage Actions page")
    public void SmokeAP05Url() {

        String actionName = "SmokeAP05ActionUrl";
        String gridName = "SmokeAP05GridUrl";
        String gridTwoName = "SmokeAP05GridTwoUrl";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        deleteGrid(gridTwoName).
        openManageActions().
        deleteAction(actionName).
        clickNewButton().
        createAction(actionName,RandomStringUtils.randomAlphanumeric(30), ACCOUNT, "Single Record", "Menu", "Window Overlay", "URL", "https://appbuddy.com/").
        openGridWizard().
        createGrid(gridTwoName, ACCOUNT).
        openGridWizard().
        createGrid(gridName, ACCOUNT).
        clickNext().
        selectAction(actionName).
        saveGrid().
        launchGrid().
        selectAction(FIRST_ROW, actionName).
        switchToWindow().
        shouldSeePageUrl("https://appbuddy.com/?action_param=").
        switchToMainWindow().
        openGridWizard().
        deleteGrid(gridName).
        deleteGrid(gridTwoName).
        openManageActions().
        deleteAction(actionName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeAP05", description = "Test Visualforce Page Content field value in the Manage Actions page")
    public void SmokeAP05VisualforcePage() {

        String actionName = "SmokeAP05ActionVisualforce";
        String gridName = "SmokeAP05GridVisualforce";
        String gridTwoName = "SmokeAP05GridTwoVisualforce";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        deleteGrid(gridTwoName).
        openManageActions().
        deleteAction(actionName).
        clickNewButton().
        createAction(actionName,RandomStringUtils.randomAlphanumeric(30), ACCOUNT, "Single Record", "Menu", "Window Overlay", "Visualforce Page", "GridBuddyUserGuide").
        openGridWizard().
        createGrid(gridTwoName, ACCOUNT).
        openGridWizard().
        createGrid(gridName, ACCOUNT).
        clickNext().
        selectAction(actionName).
        saveGrid().
        launchGrid().
        selectAction(FIRST_ROW, actionName).
        switchToWindow().
        shouldSeePageUrl("apex/GridBuddyUserGuide?action_param=").
        switchToMainWindow().
        openGridWizard().
        deleteGrid(gridName).
        deleteGrid(gridTwoName).
        openManageActions().
        deleteAction(actionName);
        
    }
    
    
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeAP06", description = "Test ID Parameter field in the Manage Actions page")
    public void SmokeAP06() {

        String actionName = "SmokeAP06Action";
        String gridName = "SmokeAP06Grid";

        getUser().
        login().
        openGridWizard().
        openManageActions().
        deleteAction(actionName).
        clickNewButton().
        createAction(actionName,RandomStringUtils.randomAlphanumeric(30), ACCOUNT, "Single Record", "Menu", "Window Overlay", "URL", "https://appbuddy.com/").
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectAction(actionName).
        saveGrid().
        launchGrid().
        selectAction(FIRST_ROW, actionName).
        switchToWindow().
        shouldSeePageUrl("https://appbuddy.com/?action_param=").
        switchToMainWindow().
        openGridWizard().
        deleteGrid(gridName).
        openManageActions().
        deleteAction(actionName);
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeAP07", description = "Test Display Behavior(picklist) field in the Manage Action page")
    public void SmokeAP07() {

        String actionName = "SmokeAP07Action";
        String gridName = "SmokeAP07Grid";

        getUser().
        login().
        openGridWizard().
        openManageActions().
        deleteAction(actionName).
        clickNewButton().
        createAction(actionName,RandomStringUtils.randomAlphanumeric(30), ACCOUNT, "Single Record", "Menu", "Grid Overlay", "URL", "https://appbuddy.com/").
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectAction(actionName).
        saveGrid().
        launchGrid().
        selectAction(FIRST_ROW, actionName).
        shouldSeePopup().
        closePopup().
        openGridWizard().
        openManageActions().
        selectAction(actionName).
        selectBehavior("Window Overlay").
        saveChanges().
        openGridWizard().
        selectGrid(gridName).
        launchGrid().
        selectAction(FIRST_ROW, actionName).
        switchToWindow().
        shouldSeePageUrl("https://appbuddy.com/?action_param=").
        switchToMainWindow().
        openGridWizard().
        deleteGrid(gridName).
        openManageActions().
        deleteAction(actionName);
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeAP08", description = "Test selecting Grid Actions section in GW2")
    public void SmokeAP08() {

        String actionNameOne = "SmokeAP08ActionOne";
        
        String actionNameTwo = "SmokeAP08ActionTwo";
        
        String gridName = "SmokeAP08Grid";

        getUser().
        login().
        openGridWizard().
        openManageActions().
        deleteAction(actionNameOne).
        deleteAction(actionNameTwo).
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        clickManageActionsLink().
        clickNewButton().
        createAction(actionNameOne,RandomStringUtils.randomAlphanumeric(30), ACCOUNT, "Single Record", "Menu", "Window Overlay", "URL", "https://www.appbuddy.com/").
        clickNewButton().
        createAction(actionNameTwo,RandomStringUtils.randomAlphanumeric(30), ACCOUNT, "Single Record", "Menu", "Window Overlay", "URL", "https://www.appbuddy.com/").
        openGridWizard().
        selectGrid(gridName).
        clickNext().
        selectAction(actionNameOne).
        selectAction(actionNameTwo).
        saveGrid().
        launchGrid().
        openActions(FIRST_ROW).
        shouldSeeAction(actionNameOne).
        shouldSeeAction(actionNameTwo).
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        unselectAction(actionNameOne).
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        openActions(FIRST_ROW).
        shouldNotSeeAction(actionNameOne).
        shouldSeeAction(actionNameTwo).
        openGridWizard().
        deleteGrid(gridName).
        openManageActions().
        deleteAction(actionNameOne).
        deleteAction(actionNameTwo);
    }
    
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeAP09", description = "Test the Actions in Grid")
    public void SmokeAP09() {
        

        String actionNameOne = "SmokeAP09ActionOne";
        
        String actionNameTwo = "SmokeAP09ActionTwo";
        
        String gridName = "SmokeAP09Grid";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        openManageActions().
        deleteAction(actionNameOne).
        deleteAction(actionNameTwo).
        clickNewButton().
        createAction(actionNameOne,RandomStringUtils.randomAlphanumeric(30), ACCOUNT, "Single Record", "Menu", "Window Overlay", "URL", "https://www.appbuddy.com/").
        clickNewButton().
        createAction(actionNameTwo,RandomStringUtils.randomAlphanumeric(30), ACCOUNT, "Batch", "Menu", "Window Overlay", "URL", "https://www.appbuddy.com/").
        openGridWizard().
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectAction(actionNameOne).
        selectAction(actionNameTwo).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        openActions(FIRST_ROW).
        shouldSeeAction(actionNameOne).
        shouldNotSeeAction(actionNameTwo).
        openMoreMenu().
        shouldNotSeeGridMenuItem(actionNameOne).
        shouldSeeGridMenuItem(actionNameTwo).
        openGridWizard().
        deleteGrid(gridName).
        openManageActions().
        deleteAction(actionNameOne).
        deleteAction(actionNameTwo);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01977", description = "Check there are no Conga options on Create new action page")
    public void rt01977() {
        
        getUser().
        login().
        openGridWizard().
        openManageActions().
        clickNewButton().
        shouldNotSeeCongaOptionsInContentSourceDropdown().
        selectType("Single Record").
        shouldNotSeeCongaOptionsInContentSourceDropdown().
        selectType("Batch").
        shouldNotSeeCongaOptionsInContentSourceDropdown();
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01978", description = "Check Conga options are not available if load and edit  existing non Conga action")
    public void rt01978() {
        
        getUser().
        login().
        openGridWizard().
        openManageActions().
        selectAction(FIRST_ROW).
        shouldNotSeeCongaOptionsInContentSourceDropdown().
        selectType("Single Record").
        shouldNotSeeCongaOptionsInContentSourceDropdown().
        selectType("Batch").
        shouldNotSeeCongaOptionsInContentSourceDropdown();
        
    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-01979", description = "Check Conga references availability if load existing Conga Conductor action")
    public void rt01979() {
        
        getUser().
        login().
        openGridWizard().
        openManageActions().
        selectAction("Send to Conga Conductor").
        shouldSeeContentSourceOption("Conga Conductor");
        
    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-01980", description = "Edit existing Conga Composer action and check there are no conga options once the action has been updated")
    public void rt01980() {
        
        getUser().
        login().
        openGridWizard().
        openManageActions().
        selectAction("Send to Conga Conductor").
        shouldSeeContentSourceOption("Conga Conductor").
        selectType("Single Record").
        shouldNotSeeCongaOptionsInContentSourceDropdown().
        selectType("Batch").
        shouldNotSeeCongaOptionsInContentSourceDropdown();
        
    }

}
