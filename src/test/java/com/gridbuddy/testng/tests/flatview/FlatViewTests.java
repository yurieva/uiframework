package com.gridbuddy.testng.tests.flatview;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.Matchers.equalTo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Data Card")
@Story("Data Card tests")
public class FlatViewTests extends BaseTest {

    @Test(groups = { "Regression", "P1" }, testName = "flatViewRelated", description = "Create the grid with one of the related child object being in the flat view")
    public void flatViewRelated() {
        
        String gridName = "flatViewRelatedGridAuto";
        
        String lastName = RandomStringUtils.randomAlphabetic(10);
        
        String accountName = RandomStringUtils.randomAlphabetic(10);
        
        String firstName = RandomStringUtils.randomAlphabetic(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectRelatedObject(CONTACT).
        selectFields(Arrays.asList("Last Name", "First Name")).
        openObjectSettings(CONTACT).
        checkFlatViewCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeColumns(Arrays.asList("ACCOUNT NAME*", "CONTACT LAST NAME*", "CONTACT FIRST NAME")).
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, "Account Name", accountName).
        editParentRecordInputField(FIRST_ROW, "Contact Last Name", lastName).
        editParentRecordInputField(FIRST_ROW, "Contact First Name", firstName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter("Account Name", "equals", accountName).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Account Name", equalTo(accountName)).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Contact Last Name", equalTo(lastName)).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Contact First Name", equalTo(firstName)).
        openGridWizard().
        deleteGrid(gridName);


    }
    
    @Test(groups = { "Regression", "P1" }, testName = "flatViewUnrelated", description = "Create the grid with one of the unrelated child object being in the flat view")
    public void flatViewUnrelated() {
        
        String gridName = "flatViewUnrelatedGridAuto";
        
        String lastName = RandomStringUtils.randomAlphabetic(10);
        
        String name = RandomStringUtils.randomAlphabetic(10);
        
        String firstName = RandomStringUtils.randomAlphabetic(10);
        
        String date = LocalDate.now().minusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(Arrays.asList("Account Name", "Stage", "Close Date")).
        selectAdditionalObject(CONTACT).
        selectFields(Arrays.asList("Account Name", "Last Name", "First Name")).
        setObjectMapping(CONTACT, "Opportunity.Account Name (ID REFERENCE)", "Contact.Account Name (ID REFERENCE)").
        openObjectSettings(CONTACT).
        checkFlatViewCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeColumns(Arrays.asList("NAME*", "ACCOUNT NAME", "STAGE*", "CLOSE DATE*", "CONTACT ACCOUNT NAME", "CONTACT LAST NAME*", "CONTACT FIRST NAME")).
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, "Name", name).
        editParentRecordInputField(FIRST_ROW, "Contact Last Name", lastName).
        editParentRecordInputField(FIRST_ROW, "Contact First Name", firstName).
        editParentRecordDateField(FIRST_ROW, "Close Date", date).
        editParentRecordSelectField(FIRST_ROW, "Stage", "Prospecting").
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter("Name", "equals", name).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Name", equalTo(name)).
//        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Contact Last Name", equalTo(lastName)).
//        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Contact First Name", equalTo(firstName)).
        openGridWizard().
        deleteGrid(gridName);


    }

}