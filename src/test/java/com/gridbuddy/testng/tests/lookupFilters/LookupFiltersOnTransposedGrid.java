package com.gridbuddy.testng.tests.lookupFilters;

import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Opportunity.OpportunityFields;
import com.gridbuddy.steps.GBSteps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Lookup Filters")
@Story("Lookup Filters On Transposed Grid")
public class LookupFiltersOnTransposedGrid extends BaseTest {

    @Test(groups = {"Regression", "P0"}, testName = "RT-01991", description = "Verify newly added lookup field is editable on transposed grid")
    public void rt01991() {

        String gridName = "RT-01991GridAuto";

        List<String> parentObjectFields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity");

        String additionalField = "Account Name";

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        setGroupByCondition(OPPORTUNITY, FIRST_ROW, OpportunityFields.STAGE, OpportunityFields.STAGE, "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, OpportunityFields.CLOSE_DATE, "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        openEditFieldsPopup().
        switchToEditFieldsPopup().
        selectFields(Arrays.asList(additionalField)).
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeForecastViewRecords().
        shouldSeeForecastField(additionalField).
        editForecastViewInputField(FIRST_ROW, FIRST_ROW, additionalField, "").
        shouldSeeAutocompleteWidget(FIRST_ROW, FIRST_ROW).
        openGridWizard().
        deleteGrid(gridName);

    }

    @Test(groups = {"Regression", "P1"}, testName = "RT-01992", description = "Verify lookup autocomplete works with all available options on transposed grid for Account name field")
    public void rt01992() {

        String gridName = "RT-01992GridAuto";

        List<String> parentObjectFields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity");

        String additionalField = "Account Name";

        String randomAccountName = RandomStringUtils.randomAlphabetic(10);

        GBSteps userOnGb = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        setGroupByCondition(OPPORTUNITY, FIRST_ROW, OpportunityFields.STAGE, OpportunityFields.STAGE, "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, OpportunityFields.CLOSE_DATE, "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        openEditFieldsPopup().
        switchToEditFieldsPopup().
        selectFields(Arrays.asList(additionalField)).
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeForecastViewRecords().
        shouldSeeForecastField(additionalField).
        editForecastViewInputField(FIRST_ROW, FIRST_ROW, additionalField, "").
        shouldSeeAutocompleteWidget(FIRST_ROW, FIRST_ROW);

        String option = userOnGb.getAutocompleteOption(FIRST_ROW, FIRST_ROW);
        String optionSubstring = option.substring(0, 2);

        userOnGb.editForecastViewInputField(FIRST_ROW, FIRST_ROW, additionalField, optionSubstring).
        shouldSeeAutocompleteWidget(FIRST_ROW, FIRST_ROW).
        shouldSeeValidAutocompleteOptions(FIRST_ROW, optionSubstring).
        editForecastViewInputField(FIRST_ROW, FIRST_ROW, additionalField, randomAccountName).
        shouldSeeEmptyAutocompleteWidget(FIRST_ROW).
        editForecastViewInputField(FIRST_ROW, FIRST_ROW, additionalField, option).
        shouldSeeAutocompleteWidget(FIRST_ROW, FIRST_ROW).
        shouldSeeValidAutocompleteOptions(FIRST_ROW, option).
        setAutocompleteOption(FIRST_ROW).
        saveChanges().
        shouldSeeAutocompleteWidget(FIRST_ROW, FIRST_ROW).
        shouldSeeSavedAutocompleteOption(FIRST_ROW, FIRST_ROW, additionalField, option).
        openGridWizard().
        deleteGrid(gridName);

    }
}
