package com.gridbuddy.testng.tests.gridwizards;

import static com.gridbuddy.matchers.StringContains.containsStringIgnoringCase;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Grid Wizards")
@Story("Grid Wizards")
public class GridWizardsTests extends BaseTest {

    @Test(groups = {"Smoke", "Regression", "P0"}, testName = "createMultiObjectGrid", description = "Create a multi object grid with related object")
    public void createMultiObjectGrid() {
        
        String gridName = "createMultiObjectGrid";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER,
                AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

        List<String> relatedObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
                ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relatedObjectFields).
        addConditionFormattingRule("rule", "Account:Name", "contains", "g", "Account:Name", true, 0, 3, "bold").
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "a").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("a")).
        rowWithCellContainingValueShouldHaveColorBackground(AccountFields.ACCOUNT_NAME, "g", "rgba(255, 114, 114, 1)").
        rowWithCellContainingValueShouldHaveTextColor(AccountFields.ACCOUNT_NAME, "g", "rgba(2, 171, 83, 1)").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = {"Regression", "P0"}, testName = "RT-01027", description = "Test default state on 'Create New...'")
    public void rt01027() {
        
        getUser().
        login().
        openGridWizard().
        createNew().
        gridNameShouldBeEmpty().
        parentObjectShouldNotBeSelected().
        shouldSeeCheckboxChecked("isEditableChkBx").
        shouldSeeCheckboxChecked("isCreateableChkBx").
        shouldSeeCheckboxChecked("massCreatesChkBx").
        shouldSeeCheckboxChecked("massUpdatesChkBx").
        shouldSeeCheckboxChecked("UDFChkBx").
        shouldSeeCheckboxChecked("UDCChkBx").
        shouldSeeCheckboxChecked("exportChkBx").
        shouldSeeCheckboxChecked("showHeaderChkBx").
        shouldSeeCheckboxChecked("repeatParentHeaderChkBx").
        shouldSeeCheckboxChecked("compactViewChkBx").
        shouldSeeCheckboxChecked("expandGroupingsChk").
        shouldSeeCheckboxChecked("showRecordDetailsChk").
        shouldSeeCheckboxUnchecked("isRollbackSavedChkBx").
        shouldSeeCheckboxUnchecked("isDeletableChkBx").
        shouldSeeCheckboxUnchecked("isDeleteAllChkBx").
        shouldSeeCheckboxUnchecked("isRollbackDeleteChkBx").
        shouldSeeCheckboxUnchecked("showSidebarChkBx").
        shouldSeeCheckboxUnchecked("readOnlyChkBx").
        hideRelatedObjectsShouldBeSelected();

    }
    
    @Test(groups = {"Smoke", "Regression", "P0"}, testName = "createMultiObjectGrid", description = "Create a multi object grid with unrelated object")
    public void createMultiObjectGridWithUnrelated() {
        
        String gridName = "createMultiObjectGridWithUnrelated";
        
        List<String> parentObjectFields = Arrays.asList("Account Name", "Amount", "Close Date");

        List<String> relatedObjectFields = Arrays.asList("Account Name", "Asset Name", "Case Currency", "Case ID", "Case Number", "Case Origin");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentObjectFields).
        selectAdditionalObject("Case").
        selectFields(relatedObjectFields).
        setObjectMapping("Case", "Opportunity.Account Name (ID REFERENCE)", "Case.Account Name (ID REFERENCE)").
        addConditionFormattingRule("rule", "Opportunity:Name", "contains", "g", "Opportunity:Name", true, 0, 3, "bold").
        clickNext().
        setFilterCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "a").
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("a")).
        rowWithCellContainingValueShouldHaveColorBackground("Name", "g", "rgba(255, 114, 114, 1)").
        rowWithCellContainingValueShouldHaveTextColor("Name", "g", "rgba(2, 171, 83, 1)").
        openGridWizard().
        deleteGrid(gridName);

    }

}
