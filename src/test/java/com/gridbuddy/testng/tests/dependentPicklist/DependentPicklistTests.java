package com.gridbuddy.testng.tests.dependentPicklist;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.Matchers.equalTo;

import java.util.Arrays;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Dependent Picklist")
@Story("Include the dependent picklist into the grid")
public class DependentPicklistTests extends BaseTest {
    
    @Test(groups = { "Regression", "P1" }, testName = "SmokeDP07", description = "Create a new record on a multi-object grid with single and multi dependent picklist fields for both parent and child objects")
    public void smokeDP07() {
        
        String gridName = "SmokeDP07Auto";
        
        String accountName = RandomStringUtils.randomAlphabetic(10);
        
        String contactLastName = RandomStringUtils.randomAlphabetic(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, ACCOUNT).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(Arrays.asList(AccountFields.ACTIVE, "DependentPicklistMultiAuto", "DependentPicklistSingleAuto")).
        selectRelatedObject(CONTACT).
        selectFields(Arrays.asList("Lead Source", "ContactDependentPicklistMultiAuto", "ContactDependentPicklistSingleAuto", "Last Name")).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        clickAddNewParentObject().
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACTIVE, "Yes").
        parentRecordSelectFieldShouldHaveItems(FIRST_ROW, "DependentPicklistMultiAuto", "10", "20", "30", "40").
        parentRecordSelectFieldShouldNotHaveItems(FIRST_ROW, "DependentPicklistMultiAuto", "50", "' \" & <> <br></br>").
        parentRecordSelectFieldShouldHaveItems(FIRST_ROW, "DependentPicklistSingleAuto", "1", "2", "3", "' \" & <> <br></br>").
        parentRecordSelectFieldShouldNotHaveItems(FIRST_ROW, "DependentPicklistSingleAuto", "4", "5").
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACTIVE, "No").
        parentRecordSelectFieldShouldNotHaveItems(FIRST_ROW, "DependentPicklistMultiAuto", "10", "20", "30", "40").
        parentRecordSelectFieldShouldHaveItems(FIRST_ROW, "DependentPicklistMultiAuto", "50", "' \" & <> <br></br>").
        parentRecordSelectFieldShouldNotHaveItems(FIRST_ROW, "DependentPicklistSingleAuto", "1", "2", "5", "' \" & <> <br></br>").
        parentRecordSelectFieldShouldHaveItems(FIRST_ROW, "DependentPicklistSingleAuto", "3", "4").
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACTIVE, "Yes").
        editParentRecordSelectField(FIRST_ROW, "DependentPicklistMultiAuto", "10").
        editParentRecordSelectField(FIRST_ROW, "DependentPicklistSingleAuto", "' \" & <> <br></br>").
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordSelectField(FIRST_ROW, FIRST_ROW, "Lead Source", "Phone Inquiry").
        relatedRecordSelectFieldShouldHaveItems(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistMultiAuto", "20", "30", "' \" & <> <br></br>").
        relatedRecordSelectFieldShouldNotHaveItems(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistMultiAuto", "10", "40", "50").
        relatedRecordSelectFieldShouldHaveItems(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistSingleAuto", "5").
        relatedRecordSelectFieldShouldNotHaveItems(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistSingleAuto", "1", "2", "3", "4", "' \" & <> <br></br>").
        editRelatedRecordSelectField(FIRST_ROW, FIRST_ROW, "Lead Source", "Web").
        relatedRecordSelectFieldShouldHaveItems(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistMultiAuto", "10", "' \" & <> <br></br>").
        relatedRecordSelectFieldShouldNotHaveItems(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistMultiAuto", "20", "30", "40", "50").
        relatedRecordSelectFieldShouldHaveItems(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistSingleAuto", "4").
        relatedRecordSelectFieldShouldNotHaveItems(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistSingleAuto", "1", "2", "3", "5", "' \" & <> <br></br>").
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, contactLastName).
        editRelatedRecordSelectField(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistMultiAuto", "' \" & <> <br></br>").
        editRelatedRecordSelectField(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistSingleAuto", "4").
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "equals", accountName).
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, AccountFields.ACCOUNT_NAME, equalTo(accountName)).
        parentRecordTextFieldShouldMetCriteria(FIRST_ROW, "DependentPicklistMultiAuto", equalTo("10")).
        parentRecordTextFieldShouldMetCriteria(FIRST_ROW, "DependentPicklistSingleAuto", equalTo("' \" & <> <br></br>")).
        relatedRecordTextFieldShouldMetCriteria(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistMultiAuto", equalTo("' \" & <> <br></br>")).
        relatedRecordTextFieldShouldMetCriteria(FIRST_ROW, FIRST_ROW, "ContactDependentPicklistSingleAuto", equalTo("4")).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    


}
