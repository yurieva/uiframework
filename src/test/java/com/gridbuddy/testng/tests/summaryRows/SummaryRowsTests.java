package com.gridbuddy.testng.tests.summaryRows;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;

import java.util.Arrays;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Opportunity.OpportunityFields;
import com.gridbuddy.steps.GBSteps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Summary Rows")
@Story("Summary Rows Tests")
public class SummaryRowsTests extends BaseTest {
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeSP01 - SmokeSP03", description = "Verifying that admin can save the summary type on the selected fields in GW2")
    public void smokeSP0103() {
        
        String gridName = "smokeSP01Auto";

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, OPPORTUNITY).
        clickNext().
        selectFields(Arrays.asList(OpportunityFields.AMOUNT, OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE, OpportunityFields.OWNER)).
        openFieldProperties(OPPORTUNITY, OpportunityFields.AMOUNT).
        selectSummaryType("SUM").
        saveFieldProperties().
        openFieldProperties(OPPORTUNITY, OpportunityFields.AMOUNT).
        shouldSeeSummaryType("SUM").
        selectSummaryType("MIN").
        saveFieldProperties().
        openFieldProperties(OPPORTUNITY, OpportunityFields.AMOUNT).
        shouldSeeSummaryType("MIN").
        selectSummaryType("--Select--").
        saveFieldProperties().
        openFieldProperties(OPPORTUNITY, OpportunityFields.AMOUNT).
        shouldSeeSummaryType("--Select--").
        closeFieldPropertiesPopup().
        clickBack().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "smokeSP04", description = "Test if summaries show up correctly on parent record")
    public void smokeSP04() {
        
        String gridName = "smokeSP04Auto";

        GBSteps userOnGridPage = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, OPPORTUNITY).
        clickNext().
        selectFields(Arrays.asList(OpportunityFields.AMOUNT, OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE, OpportunityFields.OWNER)).
        openFieldProperties(OPPORTUNITY, OpportunityFields.AMOUNT).
        selectSummaryType("SUM").
        saveFieldProperties().
        saveGrid().
        launchGrid().
        shouldSeeRecords();
        
        userOnGridPage.shouldSeeSummaryValue(OpportunityFields.AMOUNT, userOnGridPage.getParentRecordsFieldsSum(OpportunityFields.AMOUNT)).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "smokeSP04", description = "Test if summaries show up correctly on both parent and related records")
    public void smokeSP04_2() {
        
        String gridName = "smokeSP04Auto";

        GBSteps userOnGridPage = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, ACCOUNT).
        clickNext().
        selectFields(Arrays.asList("Employees")).
        selectRelatedObject(OPPORTUNITY).
        selectFields(Arrays.asList(OpportunityFields.AMOUNT)).
        openFieldProperties(ACCOUNT, "Employees").
        selectSummaryType("SUM").
        saveFieldProperties().
        openFieldProperties(OPPORTUNITY, OpportunityFields.AMOUNT).
        selectSummaryType("SUM").
        saveFieldProperties().
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords();
        
        userOnGridPage.shouldSeeSummaryValue("Employees", userOnGridPage.getParentRecordsFieldsSum("Employees")).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeSP07", description = "Summary input field should update before save")
    public void smokeSP07Input() {
        
        String gridName = "smokeSP07GridAuto";

        GBSteps userOnGb = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, OPPORTUNITY).
        clickNext().
        selectFields(Arrays.asList(OpportunityFields.AMOUNT, OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE, OpportunityFields.OWNER)).
        openFieldProperties(OPPORTUNITY, OpportunityFields.AMOUNT).
        selectSummaryType("SUM").
        saveFieldProperties().
        saveGrid().
        launchGrid().
        editParentRecordInputField(FIRST_ROW, OpportunityFields.AMOUNT, "10000").
        saveChanges().
        shouldSeeSuccessMessage();
        
        Double amount = userOnGb.getSummaryValue(OpportunityFields.AMOUNT);
        
        userOnGb.editParentRecordInputField(FIRST_ROW, OpportunityFields.AMOUNT, "9500").
        openMoreMenu().
        shouldSeeSummaryValue(OpportunityFields.AMOUNT, amount - 500).
        openGridWizard().
        deleteGrid(gridName);
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "SmokeSP07", description = "Summary picklist should update before save")
    public void smokeSP07Picklist() {
        
        String gridName = "smokeSP07GridAuto";

        GBSteps userOnGb = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, ACCOUNT).
        clickNext().
        selectFields(Arrays.asList("PicklistNumbers", AccountFields.ACCOUNT_DESCRIPTION)).
        openFieldProperties(ACCOUNT, "PicklistNumbers").
        selectSummaryType("SUM").
        saveFieldProperties().
        saveGrid().
        launchGrid();
        userOnGb.editParentRecordSelectField(FIRST_ROW, "PicklistNumbers", "6").
        editParentRecordTextareaField(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, RandomStringUtils.randomAlphanumeric(10)).
        saveChanges().
        shouldSeeSuccessMessage();
        
        Double amount = userOnGb.getSummaryValue("PicklistNumbers");
       
        
        userOnGb.editParentRecordSelectField(FIRST_ROW, "PicklistNumbers", "3").
        shouldSeeSummaryValue("PicklistNumbers", amount - 3).
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeSummaryValue("PicklistNumbers", amount - 3).
        openGridWizard().
        deleteGrid(gridName);
    }


}
