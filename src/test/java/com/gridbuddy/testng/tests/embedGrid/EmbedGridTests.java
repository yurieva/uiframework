package com.gridbuddy.testng.tests.embedGrid;

import static com.gridbuddy.matchers.StringContains.containsStringIgnoringCase;
import static com.gridbuddy.objects.ObjectUtils.getRandomValue;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.Matchers.equalTo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.objects.Opportunity.OpportunityFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Embed Grid")
@Story("Embed Grid")
public class EmbedGridTests extends BaseTest {

    @Test(groups = { "Regression", "P0", "Smoke" }, testName = "embedGrid", description = "Embed grid")
    public void embedGrid() {
        
        String gridName = "Embed Grid";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER,
                AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
                ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);
        
        String accountName = RandomStringUtils.randomAlphabetic(10);
        String description = RandomStringUtils.randomAlphabetic(30);
        String accountPhone = RandomStringUtils.randomNumeric(10);
        String accountFax = RandomStringUtils.randomNumeric(10);
        String accountSite = "http://" + RandomStringUtils.randomAlphabetic(10) + ".com";
        String accountNumber = RandomStringUtils.randomAlphanumeric(10).toUpperCase();
        String accountRating = getRandomValue(AccountFields.ACCOUNT_RATINGS);
        String accountType = getRandomValue(AccountFields.ACCOUNT_TYPES);
        String accountStatus = getRandomValue(AccountFields.ACTIVE_STATUSES);
        
        String assistantsName = RandomStringUtils.randomAlphabetic(10);
        String asstPhone = RandomStringUtils.randomNumeric(10);
        String businessFax = RandomStringUtils.randomNumeric(10);
        String businessPhone = RandomStringUtils.randomNumeric(10);
        String contactDescription = RandomStringUtils.randomAlphabetic(30);
        String lastName = RandomStringUtils.randomAlphabetic(10);
        String birthDate = LocalDate.now().minusYears(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        
        String fax = RandomStringUtils.randomAlphanumeric(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        selectRelatedObject(AccountRelatedObjects.OPPORTUNITY).
        selectFields(Arrays.asList(OpportunityFields.AMOUNT)).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        selectAction(FIRST_ROW, "View record detail").
        switchToNewWindow().
        switchToEmbedGrid().
        shouldSeeGridName(gridName).
        shouldSeeRecords().
        checkRelatedRecordsCheckbox().
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        editParentRecordTextareaField(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, description).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_FAX, accountFax).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_PHONE, accountPhone).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACCOUNT_RATING, accountRating).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_SITE, accountSite).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACCOUNT_TYPE, accountType).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACTIVE, accountStatus).
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        editRelatedRecordDateField(FIRST_ROW, FIRST_ROW, ContactFields.BIRTHDATE, birthDate).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_FAX, businessFax).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_PHONE, businessPhone).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.CONTACT_DESCRIPTION, contactDescription).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, lastName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "contains", accountName).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, description).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_FAX, accountFax).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_PHONE, accountPhone).
        parentObjectFieldShouldHaveText(FIRST_ROW, AccountFields.ACCOUNT_RATING, accountRating).
        parentObjectFieldShouldHaveValue(FIRST_ROW, AccountFields.ACCOUNT_SITE, accountSite).
        parentObjectFieldShouldHaveText(FIRST_ROW, AccountFields.ACCOUNT_TYPE, accountType).
        parentObjectFieldShouldHaveText(FIRST_ROW, AccountFields.ACTIVE, accountStatus).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ACCOUNT_NAME, accountName).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BIRTHDATE, birthDate).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_FAX, businessFax).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_PHONE, businessPhone).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.CONTACT_DESCRIPTION, contactDescription).
        relatedObjectFieldShouldHaveValue(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, lastName).
        selectFilter("(Default filter)").
        openMassUpdatePopup().
        editMassUpdateInputField(AccountFields.ACCOUNT_NAME, RandomStringUtils.randomAlphabetic(10)).
        editMassUpdateInputField(AccountFields.ACCOUNT_NUMBER, RandomStringUtils.randomAlphabetic(10)).
        editMassUpdateInputField("Contacts", ContactFields.ASSISTANTS_NAME, RandomStringUtils.randomAlphabetic(10)).
        editMassUpdateInputField("Opportunities", OpportunityFields.AMOUNT, RandomStringUtils.randomNumeric(3)).
        clickClearAllOnMassUpdatePopup().
        shouldSeeMassUpdateFieldEmpty(AccountFields.ACCOUNT_NAME).
        shouldSeeMassUpdateFieldEmpty(AccountFields.ACCOUNT_NUMBER).
        shouldSeeMassUpdateFieldEmpty("Contacts", ContactFields.ASSISTANTS_NAME).
        shouldSeeMassUpdateFieldEmpty("Opportunities", OpportunityFields.AMOUNT).
        clickCollapseAllOnMassUpdatePopup().
        shouldSeeChildObjectsOnMassUpdatePopupCollapsed().
        clickExpandAllOnMassUpdatePopup().
        shouldSeeChildObjectsOnMassUpdatePopupExpanded().
        editMassUpdateInputField(AccountFields.ACCOUNT_FAX, fax).
        clickApplyToAllRecordsButton().
        shouldSeeMassUpdateMessage("To complete mass update, click Save on the Grid page.").
        closePopup().
        cellsShouldHaveColorBackground(AccountFields.ACCOUNT_FAX, "rgba(251, 254, 201, 1)").
        saveChanges().
        shouldSeeSuccessMessage().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_FAX, equalTo(fax)).
        selectParentRecord(FIRST_ROW).
        openMassUpdatePopup().
        editMassUpdateInputField(AccountFields.ACCOUNT_FAX, fax).
        clickApplyToSelectedRecordsButton().
        shouldSeeMassUpdateMessage("To complete mass update, click Save on the Grid page.").
        closePopup().
        cellShouldHaveColorBackground(FIRST_ROW, AccountFields.ACCOUNT_FAX, "rgba(251, 254, 201, 1)").
        refreshGrid().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_FAX, equalTo(fax)).
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        switchToDefaultContent().
        closePopup().
        openMoreMenu().
        clickEditSettingsMenuItem().
        switchToEditSettingsPopup().
        switchToDefaultContent().
        closePopup().
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        createUserFilter().
        enterFilterName("User Filter").
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "a").
        saveFilter().
        switchToDefaultContent().
        closePopup().
        refreshGrid().
        selectFilter("(Admin) User Filter").
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("a")).
        switchToMainWindow().
        openGridWizard().
        deleteGrid(gridName);

    }

}