package com.gridbuddy.testng.tests.customCode;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;

import org.testng.annotations.Test;

import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Custom Code")
@Story("Custom Code")
public class CustomCodeTests extends BaseTest {

    @Test(groups = { "Regression", "P0" }, testName = "AT-02443", description = "Verify Manage Custom Code Page")
    public void at02443() {

        getUser().
        login().
        openGridWizard().
        openManageCustomCode().
        shouldBeOnManageCustomCodePage().
        goBack().
        shouldBeOnGwLandingPage();

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02355", description = "Custom css")
    public void at02355() {

        String name = "AT-02355CustomCss";
        
        String gridName = "AT-02355GridAuto";
        
        getUser().
        login().
        openGridWizard().
        openManageCustomCode().
        deleteCustomCode(name).
        createCustomCode(name, "CSS", "Grid", ".gridBtns.top {background-color:#ff0000;}").
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectCustomCss(name).
        saveGrid().
        launchGrid().
        topRowWithButtonsShouldHaveColorBackground("rgba(255, 0, 0, 1)").
        openGridWizard().
        deleteGrid(gridName).
        openManageCustomCode().
        deleteCustomCode(name);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "AT-02356", description = "Custom js")
    public void at02356() {

        String name = "AT-0235CustomJs";
        
        String gridName = "AT-02356GridAuto";
        
        getUser().
        login().
        openGridWizard().
        openManageCustomCode().
        deleteCustomCode(name).
        createCustomCode(name, "JavaScript", "Grid", "jq(document).ready(function() { jq('body').on('click', function() {alert('Custom JS working!');});});").
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectCustomJs(name).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        clickAddNewParentObject().
        shouldSeeAlert("Custom JS working!").
        openMoreMenu().
        clickEditSettingsMenuItem().
        shouldSeeAlert("Custom JS working!").
        switchToEditSettingsPopup().
        clickNext().
        switchToDefaultContent().
        switchToEditFieldsPopup().
        clickNext().
        switchToDefaultContent().
        switchToEditAdminFiltersPopup().
        clickBack().
        switchToDefaultContent().
        confirmAlertAndClosePopup("Custom JS working!").
        openGridWizard().
        deleteGrid(gridName).
        openManageCustomCode().
        deleteCustomCode(name);
    }

}