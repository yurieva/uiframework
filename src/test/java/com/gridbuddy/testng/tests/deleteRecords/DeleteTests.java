package com.gridbuddy.testng.tests.deleteRecords;

import static com.gridbuddy.objects.ObjectUtils.getRandomValue;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Delete")
@Story("Delete Tests")
public class DeleteTests extends BaseTest {

    @Test(groups = {"Regression", "P1"}, testName = "deleteParentRecord", description = "Delete parent record")
    public void deleteParentRecord() {
        
        String gridName = "deleteParentRecordGrid";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
        ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);
        
        String accountName = RandomStringUtils.randomAlphabetic(10);
        String description = RandomStringUtils.randomAlphabetic(30);
        String accountPhone = RandomStringUtils.randomNumeric(10);
        String accountFax = RandomStringUtils.randomNumeric(10);
        String accountSite = "http://" + RandomStringUtils.randomAlphabetic(10) + ".com";
        String accountNumber = RandomStringUtils.randomAlphanumeric(10).toUpperCase();
        String accountType = getRandomValue(AccountFields.ACCOUNT_TYPES);
        String accountStatus = getRandomValue(AccountFields.ACTIVE_STATUSES);
        
        String assistantsName = RandomStringUtils.randomAlphabetic(10);
        String asstPhone = RandomStringUtils.randomNumeric(10);
        String businessFax = RandomStringUtils.randomNumeric(10);
        String businessPhone = RandomStringUtils.randomNumeric(10);
        String contactDescription = RandomStringUtils.randomAlphabetic(30);
        String lastName = RandomStringUtils.randomAlphabetic(10);
        String birthDate = LocalDate.now().minusYears(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        checkDeleteCheckbox().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        saveGrid().
        launchGrid().
        checkRelatedRecordsCheckbox().
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        editParentRecordTextareaField(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, description).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_FAX, accountFax).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_PHONE, accountPhone).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_SITE, accountSite).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACCOUNT_TYPE, accountType).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACTIVE, accountStatus).
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        editRelatedRecordDateField(FIRST_ROW, FIRST_ROW, ContactFields.BIRTHDATE, birthDate).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_FAX, businessFax).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_PHONE, businessPhone).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.CONTACT_DESCRIPTION, contactDescription).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, lastName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "contains", accountName).
        selectParentRecord(FIRST_ROW).
        openMoreMenu().
        deleteSelectedItems().
        shouldSeeMessage("1 record was successfully deleted.").
        shouldNotSeeRecords().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = {"Regression", "P1"}, testName = "deleteRelatedChildObject", description = "Delete related child object")
    public void deleteRelatedChildObject() {
        
        String gridName = "deleteRelatedChildObjectGrid";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
        ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);
        
        String accountName = RandomStringUtils.randomAlphabetic(10);
        String description = RandomStringUtils.randomAlphabetic(30);
        String accountPhone = RandomStringUtils.randomNumeric(10);
        String accountFax = RandomStringUtils.randomNumeric(10);
        String accountSite = "http://" + RandomStringUtils.randomAlphabetic(10) + ".com";
        String accountNumber = RandomStringUtils.randomAlphanumeric(10).toUpperCase();
        String accountType = getRandomValue(AccountFields.ACCOUNT_TYPES);
        String accountStatus = getRandomValue(AccountFields.ACTIVE_STATUSES);
        
        String assistantsName = RandomStringUtils.randomAlphabetic(10);
        String asstPhone = RandomStringUtils.randomNumeric(10);
        String businessFax = RandomStringUtils.randomNumeric(10);
        String businessPhone = RandomStringUtils.randomNumeric(10);
        String contactDescription = RandomStringUtils.randomAlphabetic(30);
        String lastName = RandomStringUtils.randomAlphabetic(10);
        String birthDate = LocalDate.now().minusYears(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectMakeThisGridEditable().
        checkDeleteCheckbox().
        setShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        saveGrid().
        launchGrid().
        checkRelatedRecordsCheckbox().
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        editParentRecordTextareaField(FIRST_ROW, AccountFields.ACCOUNT_DESCRIPTION, description).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_FAX, accountFax).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NUMBER, accountNumber).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_PHONE, accountPhone).
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_SITE, accountSite).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACCOUNT_TYPE, accountType).
        editParentRecordSelectField(FIRST_ROW, AccountFields.ACTIVE, accountStatus).
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASSISTANTS_NAME, assistantsName).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.ASST_PHONE, asstPhone).
        editRelatedRecordDateField(FIRST_ROW, FIRST_ROW, ContactFields.BIRTHDATE, birthDate).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_FAX, businessFax).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.BUSINESS_PHONE, businessPhone).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.CONTACT_DESCRIPTION, contactDescription).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, ContactFields.LAST_NAME, lastName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "contains", accountName).
        selectRelatedRecord(FIRST_ROW, FIRST_ROW).
        openMoreMenu().
        deleteSelectedItems().
        shouldSeeMessage("1 record was successfully deleted.").
        shouldNotSeeChildRecords(FIRST_ROW).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = {"Regression", "P1"}, testName = "deleteUnrelatedChildObject", description = "Delete unrelated child object")
    public void deleteUnrelatedChildObject() {
        
        String gridName = "deleteUnrelatedChildObject";
        
        List<String> parentObjectFields = Arrays.asList("Account Name", "Amount", "Close Date");

        List<String> relatedObjectFields = Arrays.asList("Name", "Account Name", "Asset Name", "Case Currency", "Case ID", "Case Number", "Case Origin");
        
        String caseName = RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        selectShowRelatedObjectsExpanded().
        checkDeleteCheckbox().
        clickNext().
        selectFields(parentObjectFields).
        selectAdditionalObject("Case").
        selectFields(relatedObjectFields).
        setObjectMapping("Case", "Opportunity.Account Name (ID REFERENCE)", "Case.Account Name (ID REFERENCE)").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "Name", caseName).
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeChildObjectWithFieldValue(FIRST_ROW, "Name", caseName).
        selectChildRecord(FIRST_ROW, "Name", caseName).
        openMoreMenu().
        deleteSelectedItems().
        shouldNotSeeChildObjectWithFieldValue(FIRST_ROW, "Name", caseName).
        openGridWizard().
        deleteGrid(gridName);

    }

}
