package com.gridbuddy.testng.tests.salesGrids;

import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.steps.GBSteps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Sales Grids")
@Story("GridBuddy Workspace - Pipeline Tab")
public class WorkspacePipelineTests extends BaseTest {

    @Test(groups = {"Sales"}, testName = "workspacePipelineView", description = "GridBuddy Workspace - Pipeline Tab tests")
    public void workspacePipelineView() {
        
        String gridName = "GridBuddy Pipeline View";
        
        String opportunityName = RandomStringUtils.randomAlphabetic(10) + "Auto";
        
        String updatedName = RandomStringUtils.randomAlphabetic(10) + "Auto";
        
        String massCreateName = RandomStringUtils.randomAlphabetic(10) + "Auto";
        
        GBSteps userOnGrid = getUser().
        login().
        openTabbedPage("GridBuddy Workspace").
        clickTab("Pipeline").
        shouldSeeGridName(gridName).
        shouldSeeParentObjectLabel("Opportunities").
        clearColumnFilter("Name").
        shouldSeeRecords().
        dateCellMatchingCriteriaShouldHaveColorBackground("Close Date", lessThan(LocalDate.now()), "rgba(255, 114, 114, 1)");
        
        userOnGrid.clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, "Name", opportunityName).
        editParentRecordDateField(FIRST_ROW, "Close Date", LocalDate.now().minusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        editParentRecordSelectField(FIRST_ROW, "Stage", "Prospecting").
        editParentRecordInputField(FIRST_ROW, "Amount", "10000").
        editParentRecordDataCardSelectField(FIRST_ROW, "Competitor", "Clari").
        editParentRecordDataCardSelectField(FIRST_ROW, "Lead Source", "Web").
        clickAddNewRelatedObject(FIRST_ROW, "Opportunity Product").
        editRelatedRecordInputField(FIRST_ROW, "Opportunity Product", FIRST_ROW, "Product Name", "GenWatt Diesel 10kW").
        shouldSeeAutocompleteWidget().
        setAutocompleteOption(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, "Opportunity Product", FIRST_ROW, "Sales Price", "5000").
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeRecords().
        applyColumnFilter("Name", "equals", opportunityName).
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Name", equalTo(opportunityName)).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Close Date", equalTo(LocalDate.now().minusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")))).
        parentRecordTextFieldShouldMetCriteria(FIRST_ROW, "Stage", equalTo("Prospecting")).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Amount", equalTo("5,000.00")).
        expandDataCard(FIRST_ROW).
        parentRecordDataCardSelectFieldShouldHaveItemSelected(FIRST_ROW, "Competitor", "Clari").
        parentRecordDataCardSelectFieldShouldHaveItemSelected(FIRST_ROW, "Lead Source", "Web").
        editParentRecordDataCardSelectField(FIRST_ROW, "Lead Source", "Partner Referral").
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Name", equalTo(opportunityName)).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Close Date", equalTo(LocalDate.now().minusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")))).
        parentRecordTextFieldShouldMetCriteria(FIRST_ROW, "Stage", equalTo("Prospecting")).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Amount", equalTo("5,000.00")).
        expandDataCard(FIRST_ROW).
        parentRecordDataCardSelectFieldShouldHaveItemSelected(FIRST_ROW, "Competitor", "Clari").
        parentRecordDataCardSelectFieldShouldHaveItemSelected(FIRST_ROW, "Lead Source", "Partner Referral").
        openMassUpdatePopup().
        editMassUpdateInputField("Close Date", LocalDate.now().plusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        editMassUpdateInputField("Name", updatedName).
        clickCollapseAllOnMassUpdatePopup().
        shouldSeeChildObjectsOnMassUpdatePopupCollapsed().
        clickExpandAllOnMassUpdatePopup().
        shouldSeeChildObjectsOnMassUpdatePopupExpanded().
        editMassUpdateInputField("Close Date", LocalDate.now().plusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        editMassUpdateInputField("Name", updatedName).
        clickApplyToAllRecordsButton().
        shouldSeeMassUpdateMessage("To complete mass update, click Save on the Grid page.").
        closePopup().
        cellsShouldHaveColorBackground("Name", "rgba(251, 254, 201, 1)").
        cellsShouldHaveColorBackground("Close Date", "rgba(251, 254, 201, 1)").
        saveChanges().
        shouldSeeSuccessMessage().
        shouldNotSeeRecords().
        applyColumnFilter("Name", "equals", updatedName).
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Name", equalTo(updatedName)).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Close Date", equalTo(LocalDate.now().plusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")))).
        parentRecordTextFieldShouldMetCriteria(FIRST_ROW, "Stage", equalTo("Prospecting")).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Amount", equalTo("5,000.00")).
        selectParentRecord(FIRST_ROW).
        openMassUpdatePopup().
        editMassUpdateSelectField("Stage", "Closed Won").
        clickApplyToSelectedRecordsButton().
        shouldSeeMassUpdateMessage("To complete mass update, click Save on the Grid page.").
        parentRecordSelectFieldShouldHaveItemSelected(FIRST_ROW, "Stage", "Closed Won").
        closePopup().
        cellShouldHaveColorBackground(FIRST_ROW, "Stage", "rgba(251, 254, 201, 1)").
        cellShouldHaveColorBackground(FIRST_ROW, "Close Date", "rgba(132, 247, 149, 1)").
        refreshGrid().
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Name", equalTo(updatedName)).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Close Date", equalTo(LocalDate.now().plusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")))).
        parentRecordTextFieldShouldMetCriteria(FIRST_ROW, "Stage", equalTo("Prospecting")).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Amount", equalTo("5,000.00")).
        selectParentRecord(FIRST_ROW).
        deleteSelectedItems().
        shouldSeeMessage("1 record was successfully deleted.").
        shouldNotSeeRecords().
        clearColumnFilter("Name").
        openMassCreatePopup().
        enterNumberOfRecords(10).
        editMassCreateInputField("Name", massCreateName).
        editMassCreateDateField("Close Date", LocalDate.now().plusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        editMassCreateSelectField("Stage", "Prospecting").
        editMassCreateInputField("Opportunity Product", "Product Name", "GenWatt Diesel 10kW").
        editMassCreateInputField("Opportunity Product", "Sales Price", "10000").
        clickCreateParentRecordsButton().
        shouldSeeMassUpdateMessage("To complete mass create, click Save on the Grid page.").
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeRecords().
        applyColumnFilter("Name", "equals", massCreateName).
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Name", equalTo(massCreateName)).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Close Date", equalTo(LocalDate.now().plusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")))).
        parentRecordTextFieldShouldMetCriteria(FIRST_ROW, "Stage", equalTo("Prospecting")).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, "Amount", equalTo("10,000.00")).
        selectAllRecords().
        deleteSelectedItems().
        shouldSeeMessage("10 records were successfully deleted.").
        shouldNotSeeRecords().
        clearColumnFilter("Name").
        shouldSeeRecords();

    }
   

}
