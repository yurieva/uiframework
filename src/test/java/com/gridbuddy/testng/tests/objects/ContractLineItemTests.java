package com.gridbuddy.testng.tests.objects;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.utils.Constants.FIRST_ROW;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Objects")
@Story("Contract Line Item")
public class ContractLineItemTests extends BaseTest {

    @Test(groups = {"Regression", "P0"}, testName = "RT-01982", description = "Verify ability to create new grid with Contract Line Item as Parent object")
    public void rt01982FromGw2() {
        
        String gridName = "RT-01982GridAutoOne";
        
        List<String> fields = Arrays.asList("Asset Name", "Contract Line Item ID", "Created By", "Created Date",
                "Currency ISO Code", "Description", "Discount", "End Date", "Last Modified By",
                "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "List Price", "Product Name",
                "Quantity", "Sales Price", "Service Contract Name", "Start Date", "Status", "Subtotal", "System Modstamp", "Total Price");
        
        List<String> expectedColumns = Arrays.asList("LINE ITEM NUMBER*", "ASSET NAME", "CONTRACT LINE ITEM ID*",
                "CREATED BY*", "CREATED DATE*", "CURRENCY ISO CODE*", "DESCRIPTION", "DISCOUNT", "END DATE",
                "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LIST PRICE",
                "PRODUCT NAME*", "QUANTITY*", "SALES PRICE*", "SERVICE CONTRACT NAME*", "START DATE", "STATUS",
                "SUBTOTAL", "SYSTEM MODSTAMP*", "TOTAL PRICE");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject("ContractLineItem").
        clickNext().
        shouldSeeAddedFields(Arrays.asList("Line Item Number")).
        selectFields(fields).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeColumns(expectedColumns).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = {"Regression", "P0"}, testName = "RT-01982", description = "Verify ability to create new grid with Contract Line Item as Parent object")
    public void rt01982FromGw1() {
        
        String gridName = "RT-01982GridAutoTwo";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject("ContractLineItem").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeColumns(Arrays.asList("LINE ITEM NUMBER*")).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = {"Regression", "P0"}, testName = "RT-01983", description = "Create new grid with Contract Line Item as a child object")
    public void rt01983() {
        
        String gridName = "RT-01983GridAuto";
        
        List<String> childFields = Arrays.asList("Line Item Number", "Asset Name", "Contract Line Item ID", "Created By", "Created Date",
                "Currency ISO Code", "Description", "Discount", "End Date", "Last Modified By",
                "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "List Price", "Product Name",
                "Quantity", "Sales Price", "Service Contract Name", "Start Date", "Status", "Subtotal", "System Modstamp", "Total Price");
        
        List<String> expectedColumns = Arrays.asList("LINE ITEM NUMBER*", "ASSET NAME", "CONTRACT LINE ITEM ID*",
                "CREATED BY*", "CREATED DATE*", "CURRENCY ISO CODE*", "DESCRIPTION", "DISCOUNT", "END DATE",
                "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LIST PRICE",
                "PRODUCT NAME*", "QUANTITY*", "SALES PRICE*", "SERVICE CONTRACT NAME*", "START DATE", "STATUS",
                "SUBTOTAL", "SYSTEM MODSTAMP*", "TOTAL PRICE");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject("ServiceContract").
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(3).
        selectRelatedObject("Contract Line Item").
        selectFields(childFields).
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeChildObjectColumns(FIRST_ROW, expectedColumns).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = {"Regression", "P0"}, testName = "RT-01984", description = "Update existing grid with new Contract Line Item object")
    public void rt01984() {
        
        String gridName = "RT-01984GridAuto";
        
        List<String> fields = Arrays.asList("Asset Name", "Contract Line Item ID", "Created By", "Created Date",
                "Currency ISO Code", "Description", "Discount", "End Date", "Last Modified By",
                "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "List Price", "Product Name",
                "Quantity", "Sales Price", "Service Contract Name", "Start Date", "Status", "Subtotal", "System Modstamp", "Total Price");
        
        List<String> expectedColumns = Arrays.asList("LINE ITEM NUMBER*", "ASSET NAME", "CONTRACT LINE ITEM ID*",
                "CREATED BY*", "CREATED DATE*", "CURRENCY ISO CODE*", "DESCRIPTION", "DISCOUNT", "END DATE",
                "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LIST PRICE",
                "PRODUCT NAME*", "QUANTITY*", "SALES PRICE*", "SERVICE CONTRACT NAME*", "START DATE", "STATUS",
                "SUBTOTAL", "SYSTEM MODSTAMP*", "TOTAL PRICE");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        saveGw1().
        selectParentObject("ContractLineItem").
        alertConfirm().
        clickNext().
        shouldSeeAddedFields(Arrays.asList("Line Item Number")).
        selectFields(fields).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeColumns(expectedColumns).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = {"Regression", "P1"}, testName = "RT-01985", description = "Verify available fields for Contract Line Item can be selected/ deselected  and saved on GW2")
    public void rt01985() {
        
        String gridName = "RT-01985GridAuto";
        
        List<String> fieldsOne = Arrays.asList("Asset Name", "Contract Line Item ID", "Created By", "Created Date",
                "Currency ISO Code", "Description", "Discount", "End Date", "Last Modified By");

        List<String> fieldsTwo = Arrays.asList("Quantity", "Sales Price", "Service Contract Name", "Start Date",
                "Status", "Subtotal", "System Modstamp", "Total Price");

        List<String> expectedColumns = Arrays.asList("CURRENCY ISO CODE*", "DESCRIPTION", "DISCOUNT", "END DATE",
                "QUANTITY*", "SALES PRICE*", "SERVICE CONTRACT NAME*", "START DATE", "STATUS", "SUBTOTAL",
                "SYSTEM MODSTAMP*", "TOTAL PRICE");
        
        List<String> expectedColumnsTwo = Arrays.asList("CURRENCY ISO CODE*", "DESCRIPTION", "DISCOUNT", "END DATE",
                "SERVICE CONTRACT NAME*", "START DATE", "STATUS", "SUBTOTAL", "SYSTEM MODSTAMP*", "TOTAL PRICE",
                "LINE ITEM NUMBER*");

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        saveGw1().
        selectParentObject("ContractLineItem").
        alertConfirm().
        clickNext().
        shouldSeeAddedFields(Arrays.asList("Line Item Number")).
        selectFields(fieldsOne).
        saveGw2().
        removeFields(Arrays.asList("Line Item Number", "Asset Name", "Contract Line Item ID", "Created By", "Created Date", "Last Modified By")).
        selectFields(fieldsTwo).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeColumns(expectedColumns).
        openEditFieldsPopup().
        switchToEditFieldsPopup().
        removeFields(Arrays.asList("Quantity", "Sales Price")).
        selectFields(Arrays.asList("Line Item Number")).
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeColumns(expectedColumnsTwo).
        openGridWizard().
        deleteGrid(gridName);

    }
    
}
