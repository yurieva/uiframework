package com.gridbuddy.testng.tests.lightningComponents;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Issue;
import io.qameta.allure.Issues;

public class LightningComponentsTests extends BaseTest {
	
	@DataProvider
	public String[][] objects() {
		return new String[][] { 
			{ "AssetRelationship", "Asset Relationships" },
			{ "AssociatedLocation", "Associated Locations" },
			{ "Location", "Locations" },
			{ "ProductConsumed", "Products Consumed" },
			{ "ProductItem", "Product Items" },
			{ "ProductItemTransaction", "Product Item Transactions" },
			{ "ProductRequest", "Product Requests" },
			{ "ProductRequestLineItem", "Product Request Line Items" },
			{ "ProductRequired", "Products Required" },
			{ "ProductTransfer", "Product Transfers" },
			{ "ReturnOrder", "Return Orders" },
			{ "ReturnOrderLineItem", "Return Order Line Items" },
			{ "ServiceTerritoryLocation", "Service Territory Locations" },
			{ "Shipment", "Shipments" },
			{ "Address", "Addresses" },
			{ "OperatingHours", "Operating Hours" },
			{ "MaintenanceAsset", "Maintenance Assets" },
			{ "MaintenancePlan", "Maintenance Plans" },
			{ "ServiceCrew", "Service Crews" },
			{ "ServiceCrewMember", "Service Crew Members" },
			{ "ServiceResourceCapacity", "Service Resource Capacities" },
			{ "ServiceTerritoryMember", "Service Territory Members" },
			{ "SkillRequirement", "Skill Requirements" },
			};
	}
	
	@Test(dataProvider = "objects", groups = { "Regression", "P0" }, testName = "RT-02073", description = "Verify a grid can be created with new lightning objects as Parent")
	@Issues({ @Issue("https://newbuddy.atlassian.net/browse/GB-641"),
			@Issue("https://newbuddy.atlassian.net/browse/GB-642") })
    public void rt02073(String apiName, String name) {
        
        String gridName = "RT-02073GridAuto";
        
        getUser().
        loginToLightningOrg().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(apiName).
        saveGrid().
        launchGrid().
        shouldSeeGBTable().
        shouldSeeParentObjectLabel(name).
        openGridWizard().
        deleteGrid(gridName);

    }
	
	@DataProvider
	public String[][] childObjects() {
		return new String[][] { 
			{ "Asset", "Asset Relationship" },
			{ "Location", "Associated Location"},
			{ "Location", "Location" },
			{ "Product2", "Product Consumed" },
			{ "Location", "Product Item" },
			{ "ProductItem", "Product Item Transaction" },
			{ "Location", "Product Request" },
			{ "Location", "Product Request Line Item" },
			{ "Product2", "Product Required" },
			{ "Location", "Product Transfer" },
			{ "Location", "Return Order" },
			{ "Location", "Return Order Line Item" },
			{ "Location", "Service Territory Location" },
			{ "Location", "Shipment" },
			{ "Location", "Address" },
//			{ "OperatingHours", "Operating Hours" },
			{ "WorkType", "Maintenance Asset" },
			{ "WorkType", "Maintenance Plan" },
//			{ "ServiceCrew", "Service Crews" },
			{ "ServiceCrew", "Service Crew Member" },
			{ "ServiceResource", "Resource Capacity" },
			{ "ServiceResource", "Service Territory Member" },
			{ "Skill", "Skill Requirement" },
			};
	}
	
	@Test(dataProvider = "childObjects", groups = { "Regression", "P0" }, testName = "RT-02074", description = "Create new grid with new lightning object as a child object")
    public void rt02074(String parent, String child) {
        
        String gridName = "RT-02074GridAuto";
        
        getUser().
        loginToLightningOrg().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(parent).
        clickNext().
        selectRelatedObject(child).
        selectFields(5).
        saveGrid().
        launchGrid().
        shouldSeeGBTable().
        openGridWizard().
        deleteGrid(gridName);

    }
	
	
	@DataProvider
	public Object[][] objectFields() {
		return new Object[][] { 
//			{ "AssetRelationship", Arrays.asList("Asset Name", "Asset Name", "Asset Relationship ID", "Created By", "Created Date", "From Date", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Relationship Type", "System Modstamp", "To Date"), Arrays.asList("ASSET RELATIONSHIP NUMBER*", "ASSET NAME*", "ASSET NAME*", "ASSET RELATIONSHIP ID*", "CREATED BY*", "CREATED DATE*", "FROM DATE", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "RELATIONSHIP TYPE", "SYSTEM MODSTAMP*", "TO DATE") }
//			{ "AssociatedLocation", Arrays.asList("Account Name Name", "Active From", "Active To", "Associated Location ID", "Created By", "Created Date", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Location Name", "System Modstamp", "Type"), Arrays.asList("ASSOCIATED LOCATION NAME*", "ACCOUNT NAME NAME*", "ACTIVE FROM", "ACTIVE TO", "ASSOCIATED LOCATION ID*", "CREATED BY*", "CREATED DATE*", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LOCATION NAME*", "SYSTEM MODSTAMP*", "TYPE") },
//			{ "Location", Arrays.asList("Address Name", "Close Date", "Construction End Date", "Construction Start Date", "Created By", "Created Date", "Description", "Driving Directions", "Inventory Location", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Latitude", "Location ID", "Location Level", "Location Type", "Location", "Longitude", "Mobile Location", "Open Date", "Owner", "Parent Location Name", "Possession Date", "Remodel End Date", "Remodel Start Date", "Root Location Name", "System Modstamp", "Time Zone"), Arrays.asList("LOCATION NAME*", "ADDRESS NAME", "CLOSE DATE", "CONSTRUCTION END DATE", "CONSTRUCTION START DATE", "CREATED BY*", "CREATED DATE*", "DESCRIPTION", "DRIVING DIRECTIONS", "INVENTORY LOCATION", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LATITUDE", "LOCATION ID*", "LOCATION LEVEL", "LOCATION TYPE*", "LOCATION", "LONGITUDE", "MOBILE LOCATION", "OPEN DATE", "OWNER*", "PARENT LOCATION NAME", "POSSESSION DATE", "REMODEL END DATE", "REMODEL START DATE", "ROOT LOCATION NAME", "SYSTEM MODSTAMP*", "TIME ZONE") },
//			{ "ProductConsumed", Arrays.asList("Created By", "Created Date", "Description", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Line Item Name", "Product Consumed Number", "Product Item Name", "Product Name", "Product Name", "Product Name", "Quantity Consumed", "Quantity Unit Of Measure", "System Modstamp", "Unit Price", "Work Order Name"), Arrays.asList("PRODUCT CONSUMED ID*", "CREATED BY*", "CREATED DATE*", "DESCRIPTION", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LINE ITEM NAME", "PRODUCT CONSUMED NUMBER*", "PRODUCT ITEM NAME", "PRODUCT NAME", "PRODUCT NAME", "PRODUCT NAME", "QUANTITY CONSUMED*", "QUANTITY UNIT OF MEASURE", "SYSTEM MODSTAMP*", "UNIT PRICE", "WORK ORDER NAME*") },
//			{ "ProductItem", Arrays.asList("Created By", "Created Date", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Location Name", "Owner", "Product Item ID", "Product Name", "Product Name", "Quantity On Hand", "Quantity Unit Of Measure", "Serial Number", "System Modstamp"), Arrays.asList("PRODUCT ITEM NUMBER*", "CREATED BY*", "CREATED DATE*", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LOCATION NAME*", "OWNER*", "PRODUCT ITEM ID*", "PRODUCT NAME*", "PRODUCT NAME", "QUANTITY ON HAND*", "QUANTITY UNIT OF MEASURE", "SERIAL NUMBER", "SYSTEM MODSTAMP*") },
//			{ "ProductItemTransaction", Arrays.asList("Created By", "Created Date", "Description", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Product Item Name", "Product Item Transaction ID", "Quantity", "Related Record Name", "System Modstamp", "Transaction Type"), Arrays.asList("PRODUCT ITEM TRANSACTION NUMBER*", "CREATED BY*", "CREATED DATE*", "DESCRIPTION", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "PRODUCT ITEM NAME*", "PRODUCT ITEM TRANSACTION ID*", "QUANTITY*", "RELATED RECORD NAME", "SYSTEM MODSTAMP*", "TRANSACTION TYPE*") },
//			{ "ProductRequest", Arrays.asList("Account Name", "Address", "Billing Geocode Accuracy", "Case Number", "City", "Country", "Created By", "Created Date", "Description", "Destination Location Name", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Latitude", "Line Item Name", "Longitude", "Need By Date", "Owner", "Product Request ID", "Shipment Type", "Source Location Name", "State/Province", "Status", "System Modstamp", "Work Order Name", "Zip/Postal Code"), Arrays.asList("PRODUCT REQUEST NUMBER*", "ACCOUNT NAME", "ADDRESS", "BILLING GEOCODE ACCURACY", "CASE NUMBER", "CITY", "COUNTRY", "CREATED BY*", "CREATED DATE*", "DESCRIPTION", "DESTINATION LOCATION NAME", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LATITUDE", "LINE ITEM NAME", "LONGITUDE", "NEED BY DATE", "OWNER*", "PRODUCT REQUEST ID*", "SHIPMENT TYPE", "SOURCE LOCATION NAME", "STATE/PROVINCE", "STATUS", "SYSTEM MODSTAMP*", "WORK ORDER NAME", "ZIP/POSTAL CODE") },
//			{ "ProductRequestLineItem", Arrays.asList("Account Name", "Address", "Billing Geocode Accuracy", "Case Number", "City", "Country", "Created By", "Created Date", "Description", "Destination Location Name", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Latitude", "Line Item Name", "Longitude", "Need By Date", "Product Name", "Product Request Line Item ID", "Product Request Name", "Quantity Requested", "Quantity Unit Of Measure", "Shipment Type", "Source Location Name", "State/Province", "Status", "System Modstamp", "Work Order Name", "Zip/Postal Code"), Arrays.asList("PRODUCT REQUEST LINE ITEM NUMBER*", "ACCOUNT NAME", "ADDRESS", "BILLING GEOCODE ACCURACY", "CASE NUMBER", "CITY", "COUNTRY", "CREATED BY*", "CREATED DATE*", "DESCRIPTION", "DESTINATION LOCATION NAME", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LATITUDE", "LINE ITEM NAME", "LONGITUDE", "NEED BY DATE", "PRODUCT NAME*", "PRODUCT REQUEST LINE ITEM ID*", "PRODUCT REQUEST NAME*", "QUANTITY REQUESTED*", "QUANTITY UNIT OF MEASURE", "SHIPMENT TYPE", "SOURCE LOCATION NAME", "STATE/PROVINCE", "STATUS", "SYSTEM MODSTAMP*", "WORK ORDER NAME", "ZIP/POSTAL CODE") },
//			{ "ProductRequired", Arrays.asList("Created By", "Created Date", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Parent Record Name", "Parent Record Type", "Product Name", "Product Required ID", "Product Required Number", "Quantity Required", "Quantity Unit Of Measure", "System Modstamp"), Arrays.asList("PRODUCT NAME", "CREATED BY*", "CREATED DATE*", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "PARENT RECORD NAME*", "PARENT RECORD TYPE", "PRODUCT NAME*", "PRODUCT REQUIRED ID*", "PRODUCT REQUIRED NUMBER*", "QUANTITY REQUIRED", "QUANTITY UNIT OF MEASURE", "SYSTEM MODSTAMP*") },
//			{ "ProductTransfer", Arrays.asList("Created By", "Created Date", "Description", "Destination Location Name", "Expected Pickup Date", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Owner", "Product Item Name", "Product Name", "Product Request Line Item Name", "Product Request Name", "Product Transfer Number", "Quantity Received", "Quantity Sent", "Quantity Unit Of Measure", "Received By Name", "Received", "Return Order Line Item Name", "Return Order Name", "Shipment Expected Delivery Date", "Shipment Name", "Shipment Status", "Shipment Tracking Number", "Shipment Tracking URL", "Source Location Name", "Status", "System Modstamp"), Arrays.asList("PRODUCT TRANSFER ID*", "CREATED BY*", "CREATED DATE*", "DESCRIPTION", "DESTINATION LOCATION NAME", "EXPECTED PICKUP DATE", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "OWNER*", "PRODUCT ITEM NAME", "PRODUCT NAME", "PRODUCT REQUEST LINE ITEM NAME", "PRODUCT REQUEST NAME", "PRODUCT TRANSFER NUMBER*", "QUANTITY RECEIVED", "QUANTITY SENT*", "QUANTITY UNIT OF MEASURE", "RECEIVED BY NAME", "RECEIVED", "RETURN ORDER LINE ITEM NAME", "RETURN ORDER NAME", "SHIPMENT EXPECTED DELIVERY DATE", "SHIPMENT NAME", "SHIPMENT STATUS", "SHIPMENT TRACKING NUMBER", "SHIPMENT TRACKING URL", "SOURCE LOCATION NAME", "STATUS", "SYSTEM MODSTAMP*") },
//			{ "ReturnOrder", Arrays.asList("Account Name", "Address", "Billing Geocode Accuracy", "Case Number", "City", "Contact Name", "Country", "Created By", "Created Date", "Description", "Destination Location Name", "Expected Arrival Date", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Latitude", "Longitude", "Order Name", "Owner", "Product Request Name", "Return Order ID", "Returned By Name", "Shipment Type", "Source Location Name", "State/Province", "Status", "System Modstamp", "Zip/Postal Code"), Arrays.asList("RETURN ORDER NUMBER*", "ACCOUNT NAME", "ADDRESS", "BILLING GEOCODE ACCURACY", "CASE NUMBER", "CITY", "CONTACT NAME", "COUNTRY", "CREATED BY*", "CREATED DATE*", "DESCRIPTION", "DESTINATION LOCATION NAME", "EXPECTED ARRIVAL DATE", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LATITUDE", "LONGITUDE", "ORDER NAME", "OWNER*", "PRODUCT REQUEST NAME", "RETURN ORDER ID*", "RETURNED BY NAME", "SHIPMENT TYPE", "SOURCE LOCATION NAME", "STATE/PROVINCE", "STATUS", "SYSTEM MODSTAMP*", "ZIP/POSTAL CODE") },
//			{ "ReturnOrderLineItem", Arrays.asList("Asset Name", "Created By", "Created Date", "Description", "Destination Location Name", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Order Product Name", "Processing Plan", "Product Item Name", "Product Name", "Product Request Line Item Name", "Quantity Returned", "Quantity Unit Of Measure", "Reason For Return", "Repayment Method", "Return Order Line Item Number", "Return Order Name", "Source Location Name", "System Modstamp"), Arrays.asList("RETURN ORDER LINE ITEM ID*", "ASSET NAME", "CREATED BY*", "CREATED DATE*", "DESCRIPTION", "DESTINATION LOCATION NAME", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "ORDER PRODUCT NAME", "PROCESSING PLAN", "PRODUCT ITEM NAME", "PRODUCT NAME", "PRODUCT REQUEST LINE ITEM NAME", "QUANTITY RETURNED*", "QUANTITY UNIT OF MEASURE", "REASON FOR RETURN", "REPAYMENT METHOD", "RETURN ORDER LINE ITEM NUMBER*", "RETURN ORDER NAME*", "SOURCE LOCATION NAME", "SYSTEM MODSTAMP*") },
//			{ "ServiceTerritoryLocation", Arrays.asList("Created By", "Created Date", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Location Name", "Service Territory Location ID", "System Modstamp", "Territory Name"), Arrays.asList("SERVICE TERRITORY LOCATION NUMBER*", "CREATED BY*", "CREATED DATE*", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LOCATION NAME*", "SERVICE TERRITORY LOCATION ID*", "SYSTEM MODSTAMP*", "TERRITORY NAME*") },
//			{ "Shipment", Arrays.asList("Actual Delivery Date", "Created By", "Created Date", "Delivered To Name", "Description", "Destination Location Name", "Expected Delivery Date", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Owner", "Ship From City", "Ship From Country", "Ship From Geocode Accuracy", "Ship From Latitude", "Ship From Longitude", "Ship From State/Province", "Ship From Street", "Ship From Zip/Postal Code", "Ship To Geocode Accuracy", "Ship To City", "Ship To Country", "Ship To Latitude", "Ship To Longitude", "Ship To Name", "Ship To State/Province", "Ship To Street", "Ship To Zip/Postal Code", "Shipment ID", "Shipping Provider", "Source Location Name", "Status", "System Modstamp", "Tracking Number", "Tracking URL"), Arrays.asList("SHIPMENT NUMBER*", "ACTUAL DELIVERY DATE", "CREATED BY*", "CREATED DATE*", "DELIVERED TO NAME", "DESCRIPTION", "DESTINATION LOCATION NAME", "EXPECTED DELIVERY DATE", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "OWNER*", "SHIP FROM CITY", "SHIP FROM COUNTRY", "SHIP FROM GEOCODE ACCURACY", "SHIP FROM LATITUDE", "SHIP FROM LONGITUDE", "SHIP FROM STATE/PROVINCE", "SHIP FROM STREET", "SHIP FROM ZIP/POSTAL CODE", "SHIP TO GEOCODE ACCURACY", "SHIP TO CITY", "SHIP TO COUNTRY", "SHIP TO LATITUDE", "SHIP TO LONGITUDE", "SHIP TO NAME*", "SHIP TO STATE/PROVINCE", "SHIP TO STREET", "SHIP TO ZIP/POSTAL CODE", "SHIPMENT ID*", "SHIPPING PROVIDER", "SOURCE LOCATION NAME", "STATUS", "SYSTEM MODSTAMP*", "TRACKING NUMBER", "TRACKING URL") },
//			{ "Address", Arrays.asList("Address ID", "Address Type", "Address", "City", "Country", "Created By", "Created Date", "Description", "Driving Directions", "Geocode Accuracy", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Latitude", "Location Type", "Longitude", "Parent Name", "State/Province", "System Modstamp", "Time Zone", "Zip/Postal Code"), Arrays.asList("NAME*", "ADDRESS ID*", "ADDRESS TYPE", "ADDRESS", "CITY", "COUNTRY", "CREATED BY*", "CREATED DATE*", "DESCRIPTION", "DRIVING DIRECTIONS", "GEOCODE ACCURACY", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LATITUDE", "LOCATION TYPE*", "LONGITUDE", "PARENT NAME*", "STATE/PROVINCE", "SYSTEM MODSTAMP*", "TIME ZONE", "ZIP/POSTAL CODE") },
//			{ "OperatingHours", Arrays.asList("Created By", "Created Date", "Description", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Operating Hour ID", "System Modstamp", "Time Zone"), Arrays.asList("NAME*", "CREATED BY*", "CREATED DATE*", "DESCRIPTION", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "OPERATING HOUR ID*", "SYSTEM MODSTAMP*", "TIME ZONE*") },
//			{ "MaintenanceAsset", Arrays.asList("Asset Name", "Created By", "Created Date", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Maintenance Asset ID", "Maintenance Plan Name", "System Modstamp", "Work Type Name"), Arrays.asList("MAINTENANCE ASSET NUMBER*", "ASSET NAME*", "CREATED BY*", "CREATED DATE*", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "MAINTENANCE ASSET ID*", "MAINTENANCE PLAN NAME*", "SYSTEM MODSTAMP*", "WORK TYPE NAME") },
//			{ "MaintenancePlan", Arrays.asList("Account Name", "Contact Name", "Created By", "Created Date", "Date of the first work order in the next batch", "Description", "End Date", "Frequency Type", "Frequency", "Generation Timeframe Type", "Generation Timeframe", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Location Name", "Maintenance Plan ID", "Maintenance Plan Title", "Maintenance Window End (Days)", "Maintenance Window Start (Days)", "Owner", "Start Date", "System Modstamp", "Work Order Generation Status", "Work Type Name"), Arrays.asList("MAINTENANCE PLAN NUMBER*", "ACCOUNT NAME", "CONTACT NAME", "CREATED BY*", "CREATED DATE*", "DATE OF THE FIRST WORK ORDER IN THE NEXT BATCH*", "DESCRIPTION", "END DATE", "FREQUENCY TYPE*", "FREQUENCY*", "GENERATION TIMEFRAME TYPE*", "GENERATION TIMEFRAME*", "LAST MODIFIED BY*", "LAST MODIFIED DATE*", "LAST REFERENCED DATE", "LAST VIEWED DATE", "LOCATION NAME", "MAINTENANCE PLAN ID*", "MAINTENANCE PLAN TITLE", "MAINTENANCE WINDOW END (DAYS)", "MAINTENANCE WINDOW START (DAYS)", "OWNER*", "START DATE*", "SYSTEM MODSTAMP*", "WORK ORDER GENERATION STATUS", "WORK TYPE NAME") },
			{ "ServiceCrew", Arrays.asList("Created By", "Created Date", "Crew Size", "Last Modified By", "Last Modified Date", "Last Referenced Date", "Last Viewed Date", "Owner", "Service Crew ID", "System Modstamp"), Arrays.asList("") },
//			{ "ServiceCrewMember", Arrays.asList(""), Arrays.asList("") },
//			{ "ServiceResourceCapacity", Arrays.asList(""), Arrays.asList("") },
//			{ "ServiceTerritoryMember", Arrays.asList(""), Arrays.asList("") },
//			{ "SkillRequirement", Arrays.asList(""), Arrays.asList("") },
			};
	}
	
	@Test(dataProvider = "objectFields", groups = { "Regression", "P1" }, testName = "RT-02076", description = "Verify only selected fields are available on the grid for new lightning objects as parent and child")
    public void rt02076(String object, List<String> fields, List<String> expectedFields) {
        
        String gridName = "RT-02073GridAuto";
        
        getUser().
        loginToLightningOrg().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(object).
        clickNext().
        selectFields(fields).
        saveGrid().
        launchGrid().
        shouldSeeGBTable().
        shouldSeeColumns(expectedFields).
        openGridWizard().
        deleteGrid(gridName);

    }

}
