package com.gridbuddy.testng.tests.conditionalFormatting;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static org.hamcrest.Matchers.containsString;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Conditional Formatting")
@Story("Conditional Formatting")
public class ConditionalFormattingTests extends BaseTest {

    @Test(groups = { "Regression", "P0", "Smoke" }, testName = "AT-02415", description = "Conditional formatting on a cell")
    public void at02415() {
        
        String gridName = "AT-02415GridAuto";
        
        List<String> fields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER, AccountFields.ACCOUNT_PHONE,
                AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE, AccountFields.ACCOUNT_SOURCE,
                AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);


        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(fields).
        addConditionFormattingRule("rowRule", "Account:Name", "contains", "W", "0", true, 0, 0, "").
        addConditionFormattingRule("cellRule", "Account:Name", "contains", "W", "Account:Name", false, 3, 0, "").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        cellContainingTextShouldHaveColorBackground(AccountFields.ACCOUNT_NAME, "W", "rgba(132, 247, 149, 1)").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01581", description = "Create Conditional Formatting for parent and child records")
    public void rt01581() {

        String gridName = "RT-01581GridAuto";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        setShowRelatedObjectsExpanded().
        clickNext().
        selectFields(Arrays.asList("Employees")).
        selectRelatedObject(CONTACT).
        selectFields(Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.LAST_NAME, ContactFields.BUSINESS_PHONE)).
        addConditionFormattingRule("Parent", "Account:NumberOfEmployees", "greater than", "10", "Account:NumberOfEmployees", false, 2, 0, "bold", "italic").
        addConditionFormattingRule("Child", "Contact:LastName:AccountId", "contains", "1", "Contact:LastName:AccountId", true, 0, -1).
        clickNext().
        setFilterCondition(ACCOUNT, 0, "Employees", "greater than", "50").
        saveGrid().
        launchGrid().
        cellsShouldHaveColorBackground("Employees", "rgba(229, 239, 122, 1)").
        cellsShouldHaveTextColor("Employees", "rgba(179, 36, 36, 1)").
        cellsShouldHaveItalicFontStyle("Employees").
        cellsShouldHaveBoldFontStyle("Employees").
        childRowWithCellMatchingCriteriaShouldHaveColorBackground(ContactFields.LAST_NAME, containsString("1"), "rgba(255, 114, 114, 1)").
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        removeConditionalFormattingRule("Parent").
        saveGrid().
        switchToDefaultContent().
        closePopup().
        refreshGrid().
        cellsShouldNotHaveColorBackground("Employees", "rgba(229, 239, 122, 1)").
        cellsShouldHaveTextColor("Employees", "rgba(51, 51, 51, 1)").
        cellsShouldNotHaveItalicFontStyle("Employees").
        cellsShouldNotHaveBoldFontStyle("Employees").
        childRowWithCellMatchingCriteriaShouldHaveColorBackground(ContactFields.LAST_NAME, containsString("1"), "rgba(255, 114, 114, 1)").
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        removeConditionalFormattingRule("Child").
        saveGrid().
        switchToDefaultContent().
        closePopup().
        refreshGrid().
        cellsShouldNotHaveColorBackground("Employees", "rgba(229, 239, 122, 1)").
        cellsShouldHaveTextColor("Employees", "rgba(51, 51, 51, 1)").
        cellsShouldNotHaveItalicFontStyle("Employees").
        cellsShouldNotHaveBoldFontStyle("Employees").
        childRowWithCellMatchingCriteriaShouldNotHaveColorBackground(ContactFields.LAST_NAME, containsString("1"), "rgba(255, 114, 114, 1)").
        openGridWizard().
        deleteGrid(gridName);

    }
    
}