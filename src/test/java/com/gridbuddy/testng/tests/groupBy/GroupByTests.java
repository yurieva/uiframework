package com.gridbuddy.testng.tests.groupBy;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static com.gridbuddy.utils.Constants.SECOND_ROW;
import static com.gridbuddy.utils.Constants.THIRD_ROW;
import static org.hamcrest.Matchers.equalTo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Opportunity;
import com.gridbuddy.objects.Opportunity.OpportunityFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Group By")
@Story("Grouping Data By Fields")
public class GroupByTests extends BaseTest {

    @Test(groups = { "Regression", "P0" }, testName = "AT-02408", description = "Hovering over the Groupings button")
    public void at02408() {

        String gridName = "AT-02408GridAuto";

        List<String> fields = Arrays.asList(AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_SITE);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        shouldSeeShowGroupingExpandedChecked().
        shouldSeeShowRecordDetailsChecked().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "--No Field Selected--", "Ascending").
        saveGrid().
        launchGrid().
        openGroupingsMenu().
        shouldSeeShowRecordDetailsCheckboxChecked().
        shouldSeeExpandGroupingsCheckboxChecked().
        shouldSeeDataRows().
        openGridWizard().
        selectGrid(gridName).
        unselectShowGroupingExpanded().
        unselectShowRecordDetails().
        saveGrid().
        launchGrid().
        openGroupingsMenu().
        shouldSeeShowRecordDetailsCheckboxUnchecked().
        shouldSeeExpandGroupingsCheckboxUnchecked().
        shouldNotSeeDataRows().
        openGridWizard().
        deleteGrid(gridName);

    }

    @Test(groups = { "Regression", "P0" }, testName = "AT-02412", description = "Modify Show record details and take any action on the grid")
    public void at02412() {

        String gridName = "AT-02412GridAuto";

        List<String> fields = Arrays.asList(AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_SITE);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowGroupingExpanded().
        unselectShowRecordDetails().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "--No Field Selected--", "Ascending").
        saveGrid().
        launchGrid().
        shouldNotSeeDataRows().
        openGroupingsMenu().
        shouldSeeShowRecordDetailsCheckboxUnchecked().
        shouldSeeExpandGroupingsCheckboxUnchecked().
        checkShowRecordDetailsCheckbox().
        shouldSeeDataRows().
        goToPage(3).
        shouldSeeDataRows().
//        shouldSeePageUrl("show_record_details=1").
        openGridWizard().
        deleteGrid(gridName);

    }

    @Test(groups = { "Regression", "P0", "Smoke" }, testName = "RT-01907", description = "Create ADF with Group By 3 fields")
    public void rt01907() {
        
        String gridName = "RT-01907GridAuto";

        List<String> accountFields = Arrays.asList(AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_CURRENCY);
        List<String> opportunityFields = Arrays.asList(Opportunity.OpportunityFields.OWNER, Opportunity.OpportunityFields.NAME);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(accountFields).
        selectRelatedObject(OPPORTUNITY).
        selectFields(opportunityFields).
        clickNext().
        setGroupByCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_TYPE, "Descending").
        setGroupByCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NAME).
        setGroupByCondition(ACCOUNT, THIRD_ROW, AccountFields.ACCOUNT_CURRENCY).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeGroupingResults(1, AccountFields.ACCOUNT_TYPE).
        shouldSeeGroupingResults(2, AccountFields.ACCOUNT_NAME).
        shouldSeeGroupingResults(3, AccountFields.ACCOUNT_CURRENCY).
        shouldSeeGroupingFieldDiv(1, AccountFields.ACCOUNT_TYPE).
        shouldSeeGroupingFieldInput(2, AccountFields.ACCOUNT_NAME).
        shouldSeeGroupingFieldDiv(3, AccountFields.ACCOUNT_CURRENCY).
        openGridWizard().
        deleteGrid(gridName);
    }

    @Test(groups = { "Regression", "P0", "Smoke" }, testName = "createUDFWithGroupBy3Fields", description = "Create UDF with Group By 3 fields")
    public void createUDFWithGroupBy3Fields() {
        
        String gridName = "createUDFWithGroupBy3Fields";
        
        String filterName = "FilterNew";
        
        List<String> accountFields = Arrays.asList(AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_CURRENCY);
        
        List<String> opportunityFields = Arrays.asList(Opportunity.OpportunityFields.OWNER, Opportunity.OpportunityFields.NAME);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(accountFields).
        selectRelatedObject(OPPORTUNITY).
        selectFields(opportunityFields).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName(filterName).
        setGroupByCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_TYPE, "Descending").
        setGroupByCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NAME).
        setGroupByCondition(ACCOUNT, THIRD_ROW, AccountFields.ACCOUNT_CURRENCY).
        saveFilter().
        shouldSeeRecords().
        shouldSeeGroupingResults(1, AccountFields.ACCOUNT_TYPE).
        shouldSeeGroupingResults(2, AccountFields.ACCOUNT_NAME).
        shouldSeeGroupingResults(3, AccountFields.ACCOUNT_CURRENCY).
        shouldSeeGroupingFieldDiv(1, AccountFields.ACCOUNT_TYPE).
        shouldSeeGroupingFieldInput(2, AccountFields.ACCOUNT_NAME).
        shouldSeeGroupingFieldDiv(3, AccountFields.ACCOUNT_CURRENCY).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01908", description = "Create new records when Group By is active(parent)")
    public void rt01908CreateParent() {
        
        String gridName = "RT-01908GridAutoOne";

        List<String> accountFields = Arrays.asList(AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_CURRENCY);
        List<String> opportunityFields = Arrays.asList(Opportunity.OpportunityFields.OWNER, Opportunity.OpportunityFields.NAME);
        
        String accountName = RandomStringUtils.randomAlphabetic(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(accountFields).
        selectRelatedObject(OPPORTUNITY).
        selectFields(opportunityFields).
        clickNext().
        setGroupByCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_TYPE, "Descending").
        setGroupByCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NAME).
        setGroupByCondition(ACCOUNT, THIRD_ROW, AccountFields.ACCOUNT_CURRENCY).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "equals", accountName).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, AccountFields.ACCOUNT_NAME, equalTo(accountName)).
        openGridWizard().
        deleteGrid(gridName);
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01908", description = "Create new records when Group By is active(related)")
    public void rt01908CreateRelated() {
        
        String gridName = "RT-01908GridAutoTwo";

        List<String> accountFields = Arrays.asList(AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_CURRENCY);
        List<String> opportunityFields = Arrays.asList(OpportunityFields.OWNER, OpportunityFields.NAME, OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE);
        
        String opportunityName = RandomStringUtils.randomAlphabetic(10);
        String closeDate = LocalDate.now().plusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(accountFields).
        selectRelatedObject(OPPORTUNITY).
        selectFields(opportunityFields).
        clickNext().
        setGroupByCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_TYPE, "Descending").
        setGroupByCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NAME).
        setGroupByCondition(ACCOUNT, THIRD_ROW, AccountFields.ACCOUNT_CURRENCY).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "Name", opportunityName).
        editRelatedRecordDateField(FIRST_ROW, FIRST_ROW, "Close Date", closeDate).
        editRelatedRecordSelectField(FIRST_ROW, FIRST_ROW, "Stage", "Prospecting").
        saveChanges().
        shouldSeeSuccessMessage().
        applyRelatedColumnFilter(FIRST_ROW, "Name", opportunityName).
        relatedRecordInputFieldShouldMetCriteria(FIRST_ROW, FIRST_ROW, "Name", equalTo(opportunityName)).
        relatedRecordInputFieldShouldMetCriteria(FIRST_ROW, FIRST_ROW, "Close Date", equalTo(closeDate)).
        relatedRecordTextFieldShouldMetCriteria(FIRST_ROW, FIRST_ROW, "Stage", equalTo("Prospecting")).
        openGridWizard().
        deleteGrid(gridName);
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01908", description = "Create new records when Group By is active(unrelated)")
    public void rt01908CreateUnrelated() {
        
        String gridName = "RT-01908GridAutoThree";

        List<String> parentObjectFields = Arrays.asList("Account Name", "Amount", "Close Date");

        List<String> relatedObjectFields = Arrays.asList("Name", "Account Name", "Asset Name", "Case Currency", "Case ID", "Case Number", "Case Origin");
        
        String caseName = RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        selectShowRelatedObjectsExpanded().
        checkDeleteCheckbox().
        clickNext().
        selectFields(parentObjectFields).
        selectAdditionalObject("Case").
        selectFields(relatedObjectFields).
        setObjectMapping("Case", "Opportunity.Account Name (ID REFERENCE)", "Case.Account Name (ID REFERENCE)").
        clickNext().
        setGroupByCondition(OPPORTUNITY, FIRST_ROW, "Account Name", "Account Name", "Descending").
        setGroupByCondition(OPPORTUNITY, SECOND_ROW, "Name").
        setGroupByCondition(OPPORTUNITY, THIRD_ROW, "Close Date").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "Name", caseName).
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeChildObjectWithFieldValue(FIRST_ROW, "Name", caseName).
        selectChildRecord(FIRST_ROW, "Name", caseName).
        openMoreMenu().
        deleteSelectedItems().
        shouldNotSeeChildObjectWithFieldValue(FIRST_ROW, "Name", caseName).
        openGridWizard().
        deleteGrid(gridName);
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01909", description = "Edit records when Group By is active(parent)")
    public void rt01909EditParent() {
        
        String gridName = "RT-01909GridAutoOne";

        List<String> accountFields = Arrays.asList(AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_CURRENCY);
        List<String> opportunityFields = Arrays.asList(Opportunity.OpportunityFields.OWNER, Opportunity.OpportunityFields.NAME);
        
        String accountName = RandomStringUtils.randomAlphabetic(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(accountFields).
        selectRelatedObject(OPPORTUNITY).
        selectFields(opportunityFields).
        clickNext().
        setGroupByCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_TYPE, "Descending").
        setGroupByCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NAME).
        setGroupByCondition(ACCOUNT, THIRD_ROW, AccountFields.ACCOUNT_CURRENCY).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        editParentRecordInputField(FIRST_ROW, AccountFields.ACCOUNT_NAME, accountName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "equals", accountName).
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, AccountFields.ACCOUNT_NAME, equalTo(accountName)).
        openGridWizard().
        deleteGrid(gridName);
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01909", description = "Edit records when Group By is active(related)")
    public void rt01909EditRelated() {
        
        String gridName = "RT-01909GridAutoTwo";

        List<String> accountFields = Arrays.asList(AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_NUMBER,
        AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_CURRENCY);
        List<String> opportunityFields = Arrays.asList(OpportunityFields.OWNER, OpportunityFields.NAME, OpportunityFields.STAGE, OpportunityFields.CLOSE_DATE);
        
        String opportunityName = RandomStringUtils.randomAlphabetic(10);
        String closeDate = LocalDate.now().plusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(accountFields).
        selectRelatedObject(OPPORTUNITY).
        selectFields(opportunityFields).
        clickNext().
        setRelatedFilter("Has").
        setGroupByCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_TYPE, AccountFields.ACCOUNT_TYPE, "Descending").
        setGroupByCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NAME).
        setGroupByCondition(ACCOUNT, THIRD_ROW, AccountFields.ACCOUNT_CURRENCY).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "Name", opportunityName).
        editRelatedRecordDateField(FIRST_ROW, FIRST_ROW, "Close Date", closeDate).
        editRelatedRecordSelectField(FIRST_ROW, FIRST_ROW, "Stage", "Prospecting").
        saveChanges().
        shouldSeeSuccessMessage().
        applyRelatedColumnFilter(FIRST_ROW, "Name", opportunityName).
        relatedRecordInputFieldShouldMetCriteria(FIRST_ROW, FIRST_ROW, "Name", equalTo(opportunityName)).
        relatedRecordInputFieldShouldMetCriteria(FIRST_ROW, FIRST_ROW, "Close Date", equalTo(closeDate)).
        relatedRecordTextFieldShouldMetCriteria(FIRST_ROW, FIRST_ROW, "Stage", equalTo("Prospecting")).
        openGridWizard().
        deleteGrid(gridName);
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01909", description = "Edit records when Group By is active(unrelated)")
    public void rt01909EditUnrelated() {
        
        String gridName = "RT-01909GridAutoThree";

        List<String> parentObjectFields = Arrays.asList("Account Name", "Amount", "Close Date");

        List<String> relatedObjectFields = Arrays.asList("Name", "Account Name", "Asset Name", "Case Currency", "Case ID", "Case Number", "Case Origin");
        
        String caseName = RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        selectShowRelatedObjectsExpanded().
        checkDeleteCheckbox().
        clickNext().
        selectFields(parentObjectFields).
        selectAdditionalObject("Case").
        selectFields(relatedObjectFields).
        setObjectMapping("Case", "Opportunity.Account Name (ID REFERENCE)", "Case.Account Name (ID REFERENCE)").
        clickNext().
        setRelatedFilter("Has").
        setGroupByCondition(OPPORTUNITY, FIRST_ROW, "Account Name", "Account Name", "Descending").
        setGroupByCondition(OPPORTUNITY, SECOND_ROW, "Name").
        setGroupByCondition(OPPORTUNITY, THIRD_ROW, "Close Date").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "Name", caseName).
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeChildObjectWithFieldValue(FIRST_ROW, "Name", caseName).
        selectChildRecord(FIRST_ROW, "Name", caseName).
        openMoreMenu().
        deleteSelectedItems().
        shouldNotSeeChildObjectWithFieldValue(FIRST_ROW, "Name", caseName).
        openGridWizard().
        deleteGrid(gridName);
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01935", description = "Check validation for Group By criteria fields")
    public void rt01935() {
        
        String gridName = "RT-01935GridAuto";

        List<String> parentObjectFields = Arrays.asList("Account Name", "Amount", "Close Date");

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        selectShowRelatedObjectsExpanded().
        checkDeleteCheckbox().
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        setGroupByCondition(OPPORTUNITY, FIRST_ROW, "Account Name", "Account Name", "--No Sort Direction Selected--").
        saveFilter().
        shouldSeeErrorMessage("You must specify both a Group By field and its sort direction, or no Group By field criteria at all.").
        setGroupByCondition(OPPORTUNITY, FIRST_ROW, "Account Name", "Account Name", "Ascending").
        saveFilter().
        shouldNotSeeErrorMessage().
        setGroupByCondition(OPPORTUNITY, FIRST_ROW, "--No Field Selected--", "--No Field Selected--", "Ascending").
        saveFilter().
        shouldSeeErrorMessage("You must specify both a Group By field and its sort direction, or no Group By field criteria at all.").
        setGroupByCondition(OPPORTUNITY, FIRST_ROW, "Account Name", "Account Name", "Ascending").
        saveFilter().
        shouldNotSeeErrorMessage().
        openGridWizard().
        deleteGrid(gridName);
    }

}