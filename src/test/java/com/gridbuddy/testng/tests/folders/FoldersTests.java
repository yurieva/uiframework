package com.gridbuddy.testng.tests.folders;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;

import org.testng.annotations.Test;

import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Folders")
@Story("Folders Tests")
public class FoldersTests extends BaseTest {

    @Test(groups = { "Regression", "P1" }, testName = "AT-02353", description = "Create a grid folder")
    public void at02353() {

        String folderName = "AT-02353Folder";

        getUser().
        login().
        openGridWizard().
        openManageFolders().
        createFolder(folderName, "Standard User", "System Administrator").
        deleteFolder(folderName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "AT-02350", description = "Adding grids to a folder")
    public void at02350() {

        String folderName = "AT-02350Folder";
        
        String gridName = "AT-02350GridAuto";

        getUser().
        login().
        openGridWizard().
        openManageFolders().
        deleteFolder(folderName).
        createFolder(folderName, "Standard User", "System Administrator").
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectFolder(folderName).
        saveGrid().
        openGrids().
        openGridsDropdown().
        shouldSeeGridUnderFolder(folderName, gridName).
        openGridWizard().
        deleteGrid(gridName).
        openManageFolders().
        deleteFolder(folderName);

    }
    
   
}
