package com.gridbuddy.testng.tests.rowsAsColumns;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.steps.GBSteps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;

@Feature("Rows As Columns")
@Story("Rows As Columns")
public class RowsAsColumnsTests extends BaseTest {

    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-02080", description = "Load column grouping by Picklist field (Kanban View)")
    public void rt02080() {
        
        String gridName = "RT-02080GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        List<String> expectedHeaders = Arrays.asList("closed won", "id. decision makers", "qualification", "prospecting");

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        shouldSeeShowGroupingExpandedChecked().
        shouldSeeShowRecordDetailsChecked().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Descending").
        shouldSeeForecastFiewFormatFieldDisabled(OPPORTUNITY, FIRST_ROW).
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        shouldSeeForecastViewHeaders(expectedHeaders).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-02081", description = "Load column grouping by Date field (Forecast View)")
    public void rt02081() {
        
        String gridName = "RT-02081GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        shouldSeeShowGroupingExpandedChecked().
        shouldSeeShowRecordDetailsChecked().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Calendar Month", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        shouldSeeForecastViewByMonthHeaders().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01986", description = "Verify Actions widget is available for first column on transposed grid")
    public void rt01986() {
        
        String gridName = "RT-01986GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        shouldSeeShowGroupingExpandedChecked().
        shouldSeeShowRecordDetailsChecked().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Calendar Month", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        shouldSeeActionsWidget(FIRST_ROW, 0).
        shouldSeeActionsWidget(FIRST_ROW, 1).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-01994", description = "Verify long formula field is ellipsified on transposed grid")
    public void rt01994() {
        
        String gridName = "RT-01994GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        transposedGridTextFieldShouldHaveWidth(FIRST_ROW, 0, "Custom Formula Field", "125px").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-01996", description = "Verify default Sort Direction of 'Ascending' is automatically selected when user selects a Column Grouping - GW3")
    public void rt01996() {
        
        String gridName = "RT-01996GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        clearForecastViewCondition(OPPORTUNITY, FIRST_ROW).
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Calendar Month", "Ascending").
        clearForecastViewCondition(OPPORTUNITY, FIRST_ROW).
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Ascending").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02003", description = "Verify admin can set conditional formatting rule for fields alone in forecast view")
    @Issue("https://newbuddy.atlassian.net/browse/GB-475")
    public void rt02003() {
        
        String gridName = "RT-02003GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        addConditionFormattingRule("Amount Rule", "Opportunity:Amount", "greater than", "20000", "Opportunity:Amount", false, 2, 3, "bold", "italic").
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Name", "Name", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        forecastViewCurrencyFieldMatchingCriteriaShouldHaveColorBackground("Amount", greaterThan(20000.0), "rgba(229, 239, 122, 1)").
        forecastViewCurrencyFieldMatchingCriteriaShouldHaveTextColor("Amount", greaterThan(20000.0), "rgba(2, 171, 83, 1)").
        forecastViewCurrencyFieldMatchingCriteriaShouldHaveFontStyle("Amount", greaterThan(20000.0), "bold").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02005", description = "Verify conditional formatting rule is evaluated real time for transposed grid")
    @Issue("https://newbuddy.atlassian.net/browse/GB-476")
    public void rt02005() {
        
        String gridName = "RT-02005GridAuto";
        
        List<String> fields = Arrays.asList("Employees", "Account Currency", "Account Description", "Account ID", "Website", "Account Phone");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(fields).
        addConditionFormattingRule("Account Rule", "Account:CurrencyIsoCode", "U.S. Dollar", "Account:CurrencyIsoCode", false, 2, 3, "bold", "italic").
        clickNext().
        selectGroupByCondition(ACCOUNT, FIRST_ROW, "Account Name", "Account Name", "Ascending").
        selectForecastViewCondition(ACCOUNT, FIRST_ROW, "Account Currency", "Descending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        forecastViewTextFieldMatchingCriteriaShouldHaveColorBackground("Account Currency", equalTo("U.S. Dollar"), "rgba(229, 239, 122, 1)").
        forecastViewTextFieldMatchingCriteriaShouldHaveTextColor("Account Currency", equalTo("U.S. Dollar"), "rgba(2, 171, 83, 1)").
        forecastViewTextFieldMatchingCriteriaShouldHaveFontStyle("Account Currency", equalTo("U.S. Dollar"), "italic").
        editForecastViewSelectField(FIRST_ROW, 0, "Account Currency", "Euro").
        forecastViewFieldShouldHaveColorBackground(FIRST_ROW, 0, "Account Currency", "rgba(251, 254, 201, 1)").
        forecastViewSelectShouldHaveFontStyle(FIRST_ROW, 0, "Account Currency", "normal").
        forecastViewSelectShouldHaveTextColor(FIRST_ROW, 0, "Account Currency", "rgba(51, 51, 51, 1)").
        editForecastViewSelectField(FIRST_ROW, 0, "Account Currency", "U.S. Dollar").
        forecastViewFieldShouldHaveColorBackground(FIRST_ROW, 0, "Account Currency", "rgba(229, 239, 122, 1)").
        forecastViewSelectShouldHaveFontStyle(FIRST_ROW, 0, "Account Currency", "italic").
        forecastViewSelectShouldHaveTextColor(FIRST_ROW, 0, "Account Currency", "rgba(2, 171, 83, 1)").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02002", description = "Verify admin is able to set conditional formatting rules on GW-2 for entire record in forecast view")
    public void rt02002() {
        
        String gridName = "RT-02002GridAuto";
        
        List<String> fields = Arrays.asList("Employees", "Account Currency", "Account Description", "Account ID", "Website", "Account Phone");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(fields).
        addConditionFormattingRule("Account Rule", "Account:CurrencyIsoCode", "U.S. Dollar", "Account:CurrencyIsoCode", true, 2, 3, "bold", "italic").
        clickNext().
        selectGroupByCondition(ACCOUNT, FIRST_ROW, "Account Name", "Account Name", "Ascending").
        selectForecastViewCondition(ACCOUNT, FIRST_ROW, "Account Currency", "Descending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        forecastViewCellWithTextFieldMatchingCriteriaShouldHaveBorderStyle("Account Currency", equalTo("U.S. Dollar"), "2px solid rgb(229, 239, 122)").
        forecastViewCellWithTextFieldMatchingCriteriaShouldHaveFontStyle("Account Currency", equalTo("U.S. Dollar"), "italic").
        forecastViewCellWithTextFieldMatchingCriteriaShouldHaveTextColor("Account Currency", equalTo("U.S. Dollar"), "rgba(2, 171, 83, 1)").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02021", description = "Verify user has the ability to group the dates by Calendar or Fiscal quarter for transposed grids")
    public void rt02021CalendarQuater() {
        
        String gridName = "RT-02021GridAutoOne";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        shouldSeeShowGroupingExpandedChecked().
        shouldSeeShowRecordDetailsChecked().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Calendar Quarter", "Descending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        shouldSeeForecastViewByQuaterHeaders().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02021", description = "Verify user has the ability to group the dates by Calendar or Fiscal quarter for transposed grids")
    public void rt02021FiscalQuarter() {
        
        String gridName = "RT-02021GridAutoTwo";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        shouldSeeShowGroupingExpandedChecked().
        shouldSeeShowRecordDetailsChecked().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Fiscal Quarter", "Descending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        shouldSeeForecastViewByQuaterHeaders().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02022", description = "Verify transposed grid displays data gaps when grouped by calendar quarter")
    public void rt02022() {
        
        String gridName = "RT-02022GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        GBSteps userOnGb = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        shouldSeeShowGroupingExpandedChecked().
        shouldSeeShowRecordDetailsChecked().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Calendar Quarter", "Descending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        shouldSeeForecastViewByQuaterHeaders();
        
        LocalDate emptyQuater = userOnGb.getEmptyForecatViewQuater();
        
        userOnGb.editForecastViewInputField(FIRST_ROW, 0, "Close Date", emptyQuater.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeForecastViewRecords().
        shouldSeeForecastViewByQuaterHeaders().
        shouldSeeQuaterHeader("Q" + String.valueOf(emptyQuater.getMonthValue()/3 + 1) + " " + String.valueOf(emptyQuater.getYear() + "(1)"));
        
        userOnGb.openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02023", description = "Verify transposed  grid displays data gaps when grouped by Fiscal quarter")
    public void rt02023() {
        
        String gridName = "RT-02023GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        GBSteps userOnGb = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        shouldSeeShowGroupingExpandedChecked().
        shouldSeeShowRecordDetailsChecked().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Fiscal Quarter", "Descending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        shouldSeeForecastViewByQuaterHeaders();
        
        LocalDate emptyQuater = userOnGb.getEmptyForecatViewQuater();
        
        userOnGb.editForecastViewInputField(FIRST_ROW, 0, "Close Date", emptyQuater.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeForecastViewRecords().
        shouldSeeForecastViewByQuaterHeaders().
        shouldSeeQuaterHeader("Q" + String.valueOf(emptyQuater.getMonthValue()/3 + 1) + " " + String.valueOf(emptyQuater.getYear() + "(1)"));
        
        userOnGb.openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-02027", description = "Verify cog icon is displayed for suported fields on transposed grid")
    public void rt02027() {
        
        String gridName = "RT-02027GridAuto";
        
        List<String> fields = Arrays.asList("Account Name", "Amount", "Close Date", "CustomDateTime", "CustomPhone001", "Forecast Category", "Stage", "Record Type", "Probability (%)", "Order Number");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        shouldSeeShowGroupingExpandedChecked().
        shouldSeeShowRecordDetailsChecked().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Descending").
        shouldSeeForecastFiewFormatFieldDisabled(OPPORTUNITY, FIRST_ROW).
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        shouldSeeCogIconsNextToFields().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02025", description = "Verify tooltips for Formula and Picklist fields if read only mode applied for transposed grid")
    public void rt02025() {
        
        String gridName = "RT-02025GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        clickReadOnlyButton().
        shouldSeeForecastViewRecords().
        forecastViewCellsShouldHaveTitleEqualToText("Stage").
        forecastViewCellsShouldHaveTitleEqualToText("Custom Formula Field").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02026", description = "Verify tooltips for Formula field on transposed grid in editable view")
    public void rt02026() {
        
        String gridName = "RT-02026GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        forecastViewCellsShouldHaveTitleEqualToText("Custom Formula Field").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02028", description = "Verify content of the Field Actions widget on transposed grid")
    public void rt02028() {
        
        String gridName = "RT-02028GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        openForecastViewFieldActionsMenu("Name").
        shouldSeeFieldActionsPopup().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P2" }, testName = "RT-02029", description = "Verify color for cog icon after transposed grid load")
    public void rt02029() {
        
        String gridName = "RT-02029GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        forecastViewCogIconsShouldHaveGreyColor().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02030", description = "Verify Filter on the Field Actions widget on transposed grid")
    public void rt02030() {
        
        String gridName = "RT-02030GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        applyForecastViewFieldFilter("Amount", "not equal to", "").
        forecastViewCogIconsShouldHaveGreenColor("Amount").
        forecastRecordNumberFieldShouldMetCriteria("Amount", greaterThan(Double.parseDouble("0.00"))).
        myFilterShouldHaveValueSelected("My " + gridName).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02031", description = "Verify Sort on the Field Actions widget on transposed grid")
    public void rt02031() {
        
        String gridName = "RT-02031GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Account Name", "Account Name", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        sortForecastViewDescending("Close Date").
        shouldSeeForecastViewRecords().
        forecastViewCogIconsShouldHaveGreyColor().
        forecastViewRecordsShouldBeSortedByDateField("Close Date", "Descending").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02032", description = "Verify Clear filtering condition on Field Actions widget of transposed grid")
    public void rt02032() {
        
        String gridName = "RT-02032GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Closed", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Account Name", "Account Name", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        forecastViewCogIconsShouldHaveGreyColor().
        applyForecastViewFieldFilter("Account Name", "starts with", "e").
        shouldSeeForecastViewRecords().
        forecastViewCogIconsShouldHaveGreenColor("Account Name").
        forecastRecordInputFieldShouldMetCriteria("Account Name", anyOf(startsWith("e"), startsWith("E"))).
        applyForecastViewFieldFilter("Closed", "equals", "true").
        shouldSeeForecastViewRecords().
        forecastViewCogIconsShouldHaveGreenColor("Closed").
        forecastRecordCheckboxShouldBeChecked("Closed").
        clearForecastViewFilter("Account Name").
        forecastViewCogIconsShouldHaveGreyColor("Account Name").
        forecastViewCogIconsShouldHaveGreenColor("Closed").
        forecastRecordCheckboxShouldBeChecked("Closed").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02033", description = "Verify Reset sorting on the Field Actions widget of transposed grid")
    public void rt02033() {
        
        String gridName = "RT-02033GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Account Name", "Account Name", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        sortForecastViewDescending("Close Date").
        shouldSeeForecastViewRecords().
        forecastViewCogIconsShouldHaveGreyColor().
        forecastViewRecordsShouldBeSortedByDateField("Close Date", "Descending").
        resetForecastViewSorting("Close Date").
        shouldSeeForecastViewRecords().
        forecastViewCogIconsShouldHaveGreyColor().
        forecastViewRecordsShouldNotBeSortedByDateField("Close Date", "Descending").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02034", description = "Verify unsaved filter condition is not apllied after clicking Close X on Field Actions widget")
    public void rt02034() {
        
        String gridName = "RT-02034GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        selectForecastViewFieldFilterAndCloseActionsPopup("Amount", "not equal to", "").
        forecastViewCogIconsShouldHaveGreyColor().
        myFilterShouldNotDisplayed().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-02035", description = "Verify transposed grid displays data gaps when grouped by calendar quarter")
    public void rt02035() {
        
        String gridName = "RT-02035GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Calendar Month", "Descending").
        saveFilter().
        clickBack().
        clickBack().
        selectRecordsPerPage(100).
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        applyForecastViewFieldFilter("Closed", "equals", "true").
        shouldSeeTheNumberOfRecordsInColumnHeaders().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-02036", description = "Verify record counts are available on tranposed grid right after defining settings on GW3")
    public void rt02036() {
        
        String gridName = "RT-02036GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        setFilterCondition(OPPORTUNITY, FIRST_ROW, "Closed", "equals", "true").
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Descending").
        saveFilter().
        clickBack().
        clickBack().
        selectRecordsPerPage(100).
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        shouldSeeTheNumberOfRecordsInColumnHeaders().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02037", description = "Check records count on grouping level if add new or clone record via hierarchical grid")
    public void rt02037() {
        
        String gridName = "RT-02037GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        GBSteps userOnGbPage = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        setFilterCondition(OPPORTUNITY, FIRST_ROW, "Closed", "equals", "true").
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Calendar Quarter", "Descending").
        saveFilter().
        clickBack().
        clickBack().
        selectRecordsPerPage(100).
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords();
        
        int column = new Random().nextInt(10);
        int recordsCount = userOnGbPage.getForecatViewColumnRecordsCount(column);
        LocalDate quarter = userOnGbPage.getForecatViewQuater(column);
        
        userOnGbPage.openFilterPopup().
        cloneFilter().
        enterFilterName(gridName + " filter").
        clearForecastViewCondition(OPPORTUNITY, FIRST_ROW).
        saveFilter().
        shouldSeeRecords().
        clickAddNewParentObject().
        editParentRecordInputField(FIRST_ROW, "Name", RandomStringUtils.randomAlphabetic(10)).
        editParentRecordSelectField(FIRST_ROW, "Stage", "Closed Won").
        editParentRecordDateField(FIRST_ROW, "Close Date", quarter.plusDays(10).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        saveChanges().
        shouldSeeSuccessMessage().
        selectFilter("(Default filter)").
        shouldSeeForecastViewRecords().
        shouldSeeTheNumberOfRecordsInColumnHeaders().
        shouldSeeQuaterHeader("Q" + String.valueOf(quarter.getMonthValue() / 3 + 1) + " " + String.valueOf(quarter.getYear() + "(" + (recordsCount + 1) +")")).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02038", description = "Verify records count displaying after update column grouping options on GW3")
    public void rt02038() {
        
        String gridName = "RT-02038GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        setFilterCondition(OPPORTUNITY, FIRST_ROW, "Closed", "equals", "true").
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Ascending").
        saveFilter().
        clickBack().
        clickBack().
        selectRecordsPerPage(100).
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        shouldSeeTheNumberOfRecordsInColumnHeaders().
        openEditAdminFilters().
        switchToEditAdminFiltersPopup().
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Descending").
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeForecastViewRecords().
        shouldSeeTheNumberOfRecordsInColumnHeaders().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02039", description = "Verify records count displaying if there are no records available for given column grouping")
    public void rt02039() {
        
        String gridName = "RT-02039GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Descending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Fiscal Quarter", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        shouldSeeEmptyForecastViewColumns().
        openEditAdminFilters().
        switchToEditAdminFiltersPopup().
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Calendar Quarter", "Ascending").
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeForecastViewRecords().
        shouldSeeEmptyForecastViewColumns().
        openEditAdminFilters().
        switchToEditAdminFiltersPopup().
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Calendar Month", "Ascending").
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeForecastViewRecords().
        shouldSeeEmptyForecastViewColumns().
        openEditAdminFilters().
        switchToEditAdminFiltersPopup().
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeForecastViewRecords().
        shouldNotSeeEmptyForecastViewColumns().
        openEditAdminFilters().
        switchToEditAdminFiltersPopup().
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Ascending").
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeForecastViewRecords().
        shouldNotSeeEmptyForecastViewColumns().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02040", description = "Check record count on tranposed grid if delete record")
    public void rt02040() {
        
        String gridName = "RT-02040GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Last Modified Date", "Stage", "Closed", "Account Name");
        
        GBSteps userOnGbPage = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        checkDeleteCheckbox().
        clickNext().
        selectFields(fields).
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, AccountFields.ACCOUNT_NAME, AccountFields.ACCOUNT_NAME, "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Descending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords();
        
        int recordsCount = userOnGbPage.getForecatViewColumnRecordsCount("Qualification");
        
        userOnGbPage.openFilterPopup().
        cloneFilter().
        enterFilterName(gridName + " filter").
        clearForecastViewCondition(OPPORTUNITY, FIRST_ROW).
        setFilterCondition(OPPORTUNITY, FIRST_ROW, "Stage", "equals", "Qualification").
        saveFilter().
        shouldSeeRecords().
        selectParentRecord(FIRST_ROW).
        deleteSelectedItems().
        shouldSeeMessage("1 record was successfully deleted.").
        selectFilter("(Default filter)").
        shouldSeeForecastViewRecords().
        shouldSeeForecatViewColumnRecordsCount("Qualification", recordsCount - 1).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-02041", description = "Verify that changes on detail panel can be saved on transposed grid")
    public void rt02041() {
        
        String gridName = "RT-02041GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        String description = RandomStringUtils.randomAlphabetic(50);
        
        String amount = String.valueOf(new Random().nextInt(50000));
        
        String quantity = String.valueOf(new Random().nextInt(10) + 1);
        
        String name =  RandomStringUtils.randomAlphabetic(10);
        
        DecimalFormat dFormat = new DecimalFormat("###,###.00");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        openParentDataCard().
        moveParentObjectFieldToDataCard("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name").
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        selectForecastViewCellAction(FIRST_ROW, 0, "Open detail panel").
        editForecastViewDetailPanelDateField("Close Date", LocalDate.now().minusDays(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        editForecastViewDetailPanelInputField("Quantity", quantity).
        editForecastViewDetailPanelInputField("Amount", amount).
        editForecastViewDetailPanelTextareaField("Description", description).
        editForecastViewDetailPanelSelectField("Stage", "Closed Won").
        editForecastViewInputField(FIRST_ROW, 0, "Name", name).
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeForecastViewRecords().
        applyForecastViewFieldFilter("Name", "equals", name).
        shouldSeeForecastViewRecords().
        selectForecastViewCellAction(FIRST_ROW, 0, "Open detail panel").
        shouldSeeForecastViewDetailPanelDateFieldValue("Close Date", equalTo(LocalDate.now().minusDays(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")))).
        shouldSeeForecastViewDetailPanelSelectFieldValueSelected("Stage", "Closed Won").
        shouldSeeForecastViewDetailPanelInputFieldValue("Quantity", equalTo(quantity + ".00")).
        shouldSeeForecastViewDetailPanelInputFieldValue("Amount", equalTo(dFormat.format(Double.parseDouble(amount)))).
        shouldSeeForecastViewDetailPanelTextareaFieldValue("Description", equalTo(description)).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-02042", description = "Verify that detail panel looks ok in read-only mode on transposed grid")
    public void rt02042() {
        
        String gridName = "RT-02042GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        String description = RandomStringUtils.randomAlphabetic(50);
        
        String amount = String.valueOf(new Random().nextInt(50000));
        
        String quantity = String.valueOf(new Random().nextInt(10) + 1);
        
        String name =  RandomStringUtils.randomAlphabetic(10);
        
        DecimalFormat dFormat = new DecimalFormat("###,###.00");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        openParentDataCard().
        moveParentObjectFieldToDataCard("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name").
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        selectForecastViewCellAction(FIRST_ROW, 0, "Open detail panel").
        editForecastViewDetailPanelDateField("Close Date", LocalDate.now().minusDays(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        editForecastViewDetailPanelInputField("Quantity", quantity).
        editForecastViewDetailPanelInputField("Amount", amount).
        editForecastViewDetailPanelTextareaField("Description", description).
        editForecastViewDetailPanelSelectField("Stage", "Closed Won").
        editForecastViewInputField(FIRST_ROW, 0, "Name", name).
        saveChanges().
        shouldSeeSuccessMessage().
        shouldSeeForecastViewRecords().
        applyForecastViewFieldFilter("Name", "equals", name).
        shouldSeeForecastViewRecords().
        clickReadOnlyButton().
        selectForecastViewCellAction(FIRST_ROW, 0, "Open detail panel").
        shouldSeeForecastViewDetailPanelTextFieldValue("Close Date", LocalDate.now().minusDays(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        shouldSeeForecastViewDetailPanelTextFieldValue("Stage", "Closed Won").
        shouldSeeForecastViewDetailPanelTextFieldValue("Quantity", quantity + ".00").
        shouldSeeForecastViewDetailPanelTextFieldValue("Amount", containsString(dFormat.format(Double.parseDouble(amount)))).
        shouldSeeForecastViewDetailPanelTextFieldValue("Description", description).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-02043", description = "Verify that detail panel opens when the Detail Panel option is clicked on transposed grid")
    public void rt02043() {
        
        String gridName = "RT-02043GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        openParentDataCard().
        moveParentObjectFieldToDataCard("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name").
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        selectForecastViewCellAction(FIRST_ROW, 0, "Open detail panel").
        shouldSeeForecastViewDetailPanel().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-02044", description = "Verify that the detail panel updates when another tile is clicked on transposed grid")
    public void rt02044() {
        
        String gridName = "RT-02044GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        String description = RandomStringUtils.randomAlphabetic(50);
        
        String amount = String.valueOf(new Random().nextInt(50000));
        
        String quantity = String.valueOf(new Random().nextInt(10) + 1);
        
        GBSteps userOnGbPage = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        openParentDataCard().
        moveParentObjectFieldToDataCard("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name").
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        selectForecastViewCellAction(FIRST_ROW, 0, "Open detail panel");
        String closeDate = userOnGbPage.getForecastViewDetailPanelInputFieldValue("Close Date");
        userOnGbPage.selectForecastViewCellAction(FIRST_ROW, 1, "Open detail panel").
        shouldSeeForecastViewDetailPanelDateFieldValue("Close Date", not(equalTo(closeDate)));
        closeDate = userOnGbPage.getForecastViewDetailPanelInputFieldValue("Close Date");
        userOnGbPage.editForecastViewDetailPanelDateField("Close Date", LocalDate.now().minusDays(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        editForecastViewDetailPanelInputField("Quantity", quantity).
        editForecastViewDetailPanelInputField("Amount", amount).
        editForecastViewDetailPanelTextareaField("Description", description).
        editForecastViewDetailPanelSelectField("Stage", "Closed Won").
        selectForecastViewCellAction(FIRST_ROW, 2, "Open detail panel").
        shouldSeeForecastViewDetailPanelDateFieldValue("Close Date", not(equalTo(closeDate))).
        shouldSeeForecastViewDetailPanelDateFieldValue("Close Date", not(equalTo(LocalDate.now().minusDays(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))))).
        shouldSeeForecastViewDetailPanelInputFieldValue("Quantity", not(equalTo(quantity + ".00"))).
        shouldSeeForecastViewDetailPanelInputFieldValue("Amount", not(equalTo(amount))).
        shouldSeeForecastViewDetailPanelTextareaFieldValue("Description", not(equalTo(description))).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02049", description = "Verify that the detail panel is not available if fields are not selected in Data Card Fields section on GW2 for FCV")
    public void rt02049() {
        
        String gridName = "RT-02049GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        openParentDataCard().
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        openForecastViewCellActionsMenu(FIRST_ROW, 0).
        shouldNotSeeAction("Open detail panel").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02046", description = "Verify that record level option includes View record detail, Open record detail, Detail Panel and record_level_actions on FCV")
    @Issue("https://newbuddy.atlassian.net/browse/GB-640")
    public void rt02046() {
        
        String gridName = "RT-02046GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name");
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        openParentDataCard().
        moveParentObjectFieldToDataCard("Description").
        selectAction("Add Product").
        selectAction("Send Email").
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        openForecastViewCellActionsMenu(FIRST_ROW, 0).
        shouldSeeAction("Open detail panel").
        shouldSeeAction("View record detail").
        shouldSeeAction("Open record detail").
        shouldSeeAction("Send email").
        shouldSeeAction("Add product").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02048", description = "Verify that the background of edited fields are in yellow on detail panel on FCV")
    public void rt02048() {
        
        String gridName = "RT-02048GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        String description = RandomStringUtils.randomAlphabetic(50);
        
        String amount = String.valueOf(new Random().nextInt(50000));
        
        String quantity = String.valueOf(new Random().nextInt(10) + 1);
        
        String name =  RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        openParentDataCard().
        moveParentObjectFieldToDataCard("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name").
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        selectForecastViewCellAction(FIRST_ROW, 0, "Open detail panel").
        editForecastViewDetailPanelDateField("Close Date", LocalDate.now().minusDays(30).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))).
        editForecastViewDetailPanelInputField("Quantity", quantity).
        editForecastViewDetailPanelInputField("Amount", amount).
        editForecastViewDetailPanelTextareaField("Description", description).
        editForecastViewDetailPanelSelectField("Stage", "Closed Won").
        editForecastViewInputField(FIRST_ROW, 0, "Name", name).
        forecastViewDetailPanelInputFieldShoudHaveBackgroundColor("Close Date", "rgba(251, 254, 201, 1)").
        forecastViewDetailPanelInputFieldShoudHaveBackgroundColor("Quantity", "rgba(251, 254, 201, 1)").
        forecastViewDetailPanelInputFieldShoudHaveBackgroundColor("Amount", "rgba(251, 254, 201, 1)").
        forecastViewDetailPanelTextareaFieldShoudHaveBackgroundColor("Description", "rgba(251, 254, 201, 1)").
        forecastViewDetailPanelSelectFieldShoudHaveBackgroundColor("Stage", "rgba(251, 254, 201, 1)").
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02047", description = "Verify the required field validation on detail panel on FCV")
    public void rt02047() {
        
        String gridName = "RT-02047GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        String name =  RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        openParentDataCard().
        moveParentObjectFieldToDataCard("Name", "Close Date", "Description", "Stage", "Quantity", "Account Name").
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        selectForecastViewCellAction(FIRST_ROW, 0, "Open detail panel").
        editForecastViewDetailPanelInputField("Name", "").
        saveChanges().
        shouldSeeMessage("Please enter required information").
        editForecastViewDetailPanelInputField("Name", name).
        saveChanges().
        shouldSeeSuccessMessage().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "RT-02045", description = "Verify that field value is retained and the tile and detail panel have error background color when saving fails on FCV grid")
    public void rt02045() {
        
        String gridName = "RT-02045GridAuto";
        
        List<String> fields = Arrays.asList("Amount", "Close Date", "Description", "Stage", "Quantity", "Account Name", "Custom Formula Field");
        
        String name =  RandomStringUtils.randomAlphabetic(10);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        clickNext().
        selectFields(fields).
        openParentDataCard().
        moveParentObjectFieldToDataCard("Name", "Close Date", "Description", "Stage", "Quantity", "Account Name").
        clickNext().
        selectGroupByCondition(OPPORTUNITY, FIRST_ROW, "Stage", "Stage", "Ascending").
        selectForecastViewCondition(OPPORTUNITY, FIRST_ROW, "Close Date", "Day", "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeForecastViewRecords().
        selectForecastViewCellAction(FIRST_ROW, 0, "Open detail panel").
        editForecastViewDetailPanelInputField("Name", name).
        editForecastViewDetailPanelDateField("Close Date", "15/15/2018").
        saveChanges().
        shouldSeeMessage("Error saving Close Date: invalid value: 15/15/2018").
        forecastViewDetailPanelInputFieldShoudHaveBackgroundColor("Close Date", "rgba(251, 254, 201, 1)").
        forecastViewDetailPanelInputFieldShoudHaveBackgroundColor("Name", "rgba(251, 254, 201, 1)").
        shouldSeeForecastViewDetailPanelDateFieldValue("Close Date", equalTo("15/15/2018")).
        shouldSeeForecastViewDetailPanelInputFieldValue("Name", equalTo(name)).
        forecastViewDetailPanelShoudHaveBackgroundColor("rgba(255, 232, 231, 1)").
        forecastViewDataCellShoudHaveBackgroundColor(FIRST_ROW, 0, "rgba(255, 232, 231, 1)").
        openGridWizard().
        deleteGrid(gridName);

    }


}