package com.gridbuddy.testng.tests.datacard;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.objects.ParentObjects.OPPORTUNITY;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.Matchers.equalTo;

import java.util.Arrays;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Data Card")
@Story("Data Card tests")
public class DataCardTests extends BaseTest {

    @Test(groups = { "Regression", "P1" }, testName = "dataCardParent", description = "Create the grid with data card active for parent object")
    public void dataCardParent() {
        
        String gridName = "dataCardParentGridAuto";
        
        String lastName = RandomStringUtils.randomAlphabetic(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(CONTACT).
        clickNext().
        selectFields(Arrays.asList("Account Name", "Last Name")).
        openParentDataCard().
        moveParentObjectFieldToDataCard("Last Name").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeColumns(Arrays.asList("FULL NAME*", "ACCOUNT NAME")).
        shouldSeeDataCardIcon().
        clickAddNewParentObject().
        editParentRecordDataCardInputField(FIRST_ROW, "Last Name", lastName).
        saveChanges().
        shouldSeeSuccessMessage().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName("filter").
        setFilterCondition(CONTACT, FIRST_ROW, "Last Name", "equals", lastName).
        saveFilter().
        shouldSeeRecords().
        expandDataCard(FIRST_ROW).
        parentRecordDataCardInputFieldShouldMetCriteria(FIRST_ROW, "Last Name", equalTo(lastName)).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "dataCardRelated", description = "Create the grid with data card active for related child object")
    public void dataCardRelated() {
        
        String gridName = "dataCardRelatedGridAuto";
        
        String lastName = RandomStringUtils.randomAlphabetic(10);
        
        String firstName = RandomStringUtils.randomAlphabetic(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectRelatedObject(CONTACT).
        selectFields(Arrays.asList("Account Name", "Last Name", "First Name")).
        openChildDataCard(CONTACT).
        moveChildObjectFieldToDataCard(CONTACT, "Last Name").
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeChildObjectColumns(FIRST_ROW, Arrays.asList("ACCOUNT NAME", "FIRST NAME")).
        shouldSeeChildObjectDataCardIcon().
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "First Name", firstName).
        editChildRecordDataCardInputField(FIRST_ROW, FIRST_ROW, "Last Name", lastName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyRelatedColumnFilter(FIRST_ROW, "First Name", firstName).
        expandDataCard(FIRST_ROW, FIRST_ROW).
        childRecordDataCardInputFieldShouldMetCriteria(FIRST_ROW, FIRST_ROW, "Last Name", equalTo(lastName)).
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "dataCardUnrelated", description = "Create the grid with data card active for unrelated child object")
    public void dataCardUnrelated() {
        
        String gridName = "dataCardUnrelatedGridAuto";
        
        String lastName = RandomStringUtils.randomAlphabetic(10);
        
        String firstName = RandomStringUtils.randomAlphabetic(10);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(OPPORTUNITY).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(Arrays.asList("Account Name")).
        selectAdditionalObject(CONTACT).
        selectFields(Arrays.asList("Account Name", "Last Name", "First Name")).
        openChildDataCard(CONTACT).
        setObjectMapping(CONTACT, "Opportunity.Account Name (ID REFERENCE)", "Contact.Account Name (ID REFERENCE)").
        moveChildObjectFieldToDataCard(CONTACT, "Last Name").
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        shouldSeeChildObjectColumns(FIRST_ROW, Arrays.asList("ACCOUNT NAME", "FIRST NAME")).
        shouldSeeChildObjectDataCardIcon().
        clickAddNewRelatedObject(FIRST_ROW).
        editRelatedRecordInputField(FIRST_ROW, FIRST_ROW, "First Name", firstName).
        editChildRecordDataCardInputField(FIRST_ROW, FIRST_ROW, "Last Name", lastName).
        saveChanges().
        shouldSeeSuccessMessage().
        applyRelatedColumnFilter(FIRST_ROW, "First Name", firstName).
        expandDataCard(FIRST_ROW, FIRST_ROW).
        childRecordDataCardInputFieldShouldMetCriteria(FIRST_ROW, FIRST_ROW, "Last Name", equalTo(lastName)).
        openGridWizard().
        deleteGrid(gridName);

    }
   

}