package com.gridbuddy.testng.tests.tabbedPages;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Tabbed Pages")
@Story("Tabbed Pages")
public class TabbedPagesTests extends BaseTest {

    @Test(groups = { "Regression", "P1" }, testName = "RT-01582", description = "Manage Tabbed Pages")
    public void rt01582() {

        getUser().
        login().
        openGridWizard().
        openManageTabbedPages().
        shouldBeOnManageTabbedPagesPage().
        goBack().
        shouldBeOnGwLandingPage();

    }
    
    @Test(groups = { "Regression", "P1" }, testName = "AT-02439", description = "Verify Enbedding Grid with Flat View")
    public void at02439() {
        
        String gridName = "AT-02439GridAuto";
        
        String tabbedPageTitle = "Account Tabbed Page";
        
        String tabName = "AT_02439TabName";

        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE, ContactFields.BIRTHDATE, ContactFields.BUSINESS_FAX,
                ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION,
                ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);

        List<String> expectedColumns = Arrays.asList(AccountFields.ACCOUNT_NAME.toUpperCase() + "*",
                AccountRelatedObjects.CONTACT.toUpperCase() + " " + ContactFields.ACCOUNT_NAME.toUpperCase(),
                AccountRelatedObjects.CONTACT.toUpperCase() + " " + ContactFields.ASSISTANTS_NAME.toUpperCase(),
                AccountRelatedObjects.CONTACT.toUpperCase() + " " + ContactFields.ASST_PHONE.toUpperCase(),
                AccountRelatedObjects.CONTACT.toUpperCase() + " " + ContactFields.BIRTHDATE.toUpperCase(),
                AccountRelatedObjects.CONTACT.toUpperCase() + " " + ContactFields.BUSINESS_FAX.toUpperCase(),
                AccountRelatedObjects.CONTACT.toUpperCase() + " " + ContactFields.BUSINESS_PHONE.toUpperCase(),
                ContactFields.CONTACT_DESCRIPTION.toUpperCase(),
                ContactFields.CONTACT_ID.toUpperCase() + "*",
                AccountRelatedObjects.CONTACT.toUpperCase() + " " + ContactFields.CREATED_BY.toUpperCase() + "*",
                AccountRelatedObjects.CONTACT.toUpperCase() + " " + ContactFields.LAST_NAME.toUpperCase() + "*");

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        openGridWizard().
        createGrid(gridName, ACCOUNT).
        clickNext().
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(AccountRelatedObjects.CONTACT).
        checkFlatViewCheckbox().
        saveObjectLevelSettings().
        saveGrid().
        openGridWizard().
        openManageTabbedPages().
        selectPage(tabbedPageTitle).
        addTab(tabName, gridName).
        clickSave().
        openGridWizard().
        selectGrid(gridName).
        launchGrid().
        shouldSeeRecords().
        shouldSeeColumns(expectedColumns).
        openAccountPage().
        switchToFrame(1).
        clickTab(tabName).
        shouldSeeColumns(expectedColumns).
        openGridWizard().
        openManageTabbedPages().
        selectPage(tabbedPageTitle).
        removeTab(tabName).
        clickSave().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P1" }, testName = "AT-02440", description = "Verify Fast Filters with Flat View")
    public void at02440() {
        
        String gridName = "AT-02440GridAuto";
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME,
                ContactFields.ASST_PHONE, ContactFields.BIRTHDATE, ContactFields.BUSINESS_FAX,
                ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION,
                ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, ACCOUNT).
        openGridWizard().
        selectGrid(gridName).
        clickNext().
        selectRelatedObject(CONTACT).
        selectFields(relaterObjectFields).
        openObjectSettings(CONTACT).
        checkFlatViewCheckbox().
        saveObjectLevelSettings().
        clickNext().
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "contains", "s").
        parentObjectsShouldHaveFieldContainingValues(AccountFields.ACCOUNT_NAME, "s").
        applyColumnFilter(CONTACT + " "  + ContactFields.ASSISTANTS_NAME, "contains", "t").
        parentObjectsShouldHaveFieldContainingValues(AccountRelatedObjects.CONTACT + " "  + ContactFields.ASSISTANTS_NAME, "t").
        openGridWizard().
        deleteGrid(gridName);
        
    }

}