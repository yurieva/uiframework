package com.gridbuddy.testng.tests.filters;

import static com.gridbuddy.matchers.IsEqual.equalToIgnoringCase;
import static com.gridbuddy.matchers.StringContains.containsStringIgnoringCase;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.Matchers.not;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.steps.GW3Steps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Filters")
@Story("Admin Defined Filters")
public class AdminDefinedFiltersTests extends BaseTest {

    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01400", description = "Verify Admin Filter can be defined for Parent Object")
    public void rt01400() {

        String gridName = "RT-01400GridAuto";
        
        List<String> fields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE, AccountFields.CREATED_DATE, AccountFields.BILLING_CITY, AccountFields.BILLING_COUNTRY);

        GW3Steps userOnGW3Page =  getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(fields).
        clickNext();
        
        userOnGW3Page.setFilterCondition(ACCOUNT, 0, AccountFields.ACCOUNT_NAME, "does not contain", "united").
        saveGrid().
        shouldNotSeeErrorMessage();
        
        userOnGW3Page.setFilterCondition(ACCOUNT, 1, AccountFields.ACTIVE, "not equal to", "Yes").
        setFilterCondition(ACCOUNT, 2, AccountFields.BILLING_CITY, "equals", "San Francisco").
        setFilterCondition(ACCOUNT, 3, AccountFields.ACCOUNT_ID, "equals", "0011r00001nhKn5AAE").
        setFilterCondition(ACCOUNT, 4, AccountFields.ACTIVE, "equals", "Yes").
        setFilterCondition(ACCOUNT, 5, AccountFields.ACCOUNT_SOURCE, "equals", "other").
        setFilterCondition(ACCOUNT, 6, AccountFields.CREATED_DATE, "greater than", "05/27/2017 12:58 PM").
        setFilterCondition(ACCOUNT, 7, AccountFields.ACCOUNT_RATING, "equals", "Hot").
        setFilterCondition(ACCOUNT, 8, AccountFields.ACCOUNT_NUMBER, "includes", "1").
        setFilterCondition(ACCOUNT, 9, AccountFields.ACCOUNT_SITE, "does not contain", "http").
        saveGrid().
        shouldNotSeeErrorMessage();
        
        userOnGW3Page.clearFilter(ACCOUNT, 2).
        saveGrid().
        shouldNotSeeErrorMessage();
        
        userOnGW3Page.setFilterCondition(ACCOUNT, 9, AccountFields.ACTIVE, "not equal to", "No").
        saveGrid().
        shouldNotSeeErrorMessage().
        openGridWizard().
        deleteGrid(gridName);

    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01408", description = "Verify Admin defined fillters (Admin saves the Default filter)")
    public void rt01408CaseOne() {

        String gridName = "RT-01408GridOneAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "equals", "test").
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, equalToIgnoringCase("test")).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01408", description = "Verify Admin defined fillters (Admin edits the Default filter)")
    public void rt01408CaseTwo() {

        String gridName = "RT-01408GridTwoAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "equals", "test").
        saveGrid().
        launchGrid().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, equalToIgnoringCase("test")).
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "not equal to", "test").
        setSortingCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "Ascending").
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, not(equalToIgnoringCase("test"))).
        recordsShouldBeSortedByInputFieldIgnoringCase(AccountFields.ACCOUNT_NAME, "Ascending").
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01408", description = "Verify Admin defined fillters (Admin clicks the Refresh button without Saving)")
    public void rt01408CaseThree() {

        String gridName = "RT-01408GridThreeAuto";
        
        String filterName = "RT-01408Filter";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        createUserFilter().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "equals", "test").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        myFilterShouldHaveValue("(Admin) " + filterName).
        selectFilter("(Admin) " + filterName).
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        shouldSeeFilterSelected(filterName).
        shouldSeeFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "equals", "test").
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "not equal to", "some new value").
        setSortingCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "Ascending").
        clickRefresh().
        switchToDefaultContent().
        switchToEditAdminFiltersPopup().
        shouldSeeFilterSelected(filterName).
        shouldSeeFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "equals", "test").
        shouldSeeSortingCondition(ACCOUNT, FIRST_ROW, "--No Field Selected--", "--No Sort Direction Selected--").
        switchToDefaultContent().
        closePopup().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01408", description = "Verify Admin defined fillters (Admin clicks the Back button)")
    public void rt01408CaseFour() {

        String gridName = "RT-01408GridFourAuto";
        
        String filterName = "!RT-01408Filter";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        createUserFilter().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "equals", "test").
        saveGrid().
        launchGrid().
        myFilterShouldHaveValue("(Admin) " + filterName).
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        selectFilter(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "not equal to", "some new value").
        setSortingCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "Ascending").
        clickBack().
        switchToEditFieldsPopup().
        clickNext().
        switchToEditAdminFiltersPopup().
        selectFilter(filterName).
        shouldSeeFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "not equal to", "some new value").
        shouldSeeSortingCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "Ascending").
        switchToDefaultContent().
        closePopup().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01408", description = "Verify Admin defined fillters (User selects admin-defined filter on grid, and it gets saved as last accessed)")
    public void rt01408CaseFive() {

        String gridName = "RT-01408GridFiveAuto";
        
        String filterName = "RT-01408FilterOne";
        
        String filterNameTwo = "RT-01408FilterTwo";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        createUserFilter().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "equals", "test").
        saveFilter().
        createUserFilter().
        enterFilterName(filterNameTwo).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NUMBER, "contains", "a").
        setSortingCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        myFilterShouldHaveValueSelected("(Default filter)").
        selectFilter("(Admin) " + filterName).
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, equalToIgnoringCase("test")).
        logout().
        login().
        openGridWizard().
        openGrids().
        openGrid("All Grids", gridName).
        shouldSeeRecords().
        myFilterShouldHaveValueSelected("(Admin) " + filterName).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, equalToIgnoringCase("test")).
        selectFilter("(Admin) " + filterNameTwo).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NUMBER, containsStringIgnoringCase("a")).
        recordsShouldBeSortedByInputFieldIgnoringCase(AccountFields.ACCOUNT_NAME, "Ascending").
        logout().
        login().
        openGridWizard().
        openGrids().
        openGrid("All Grids", gridName).
        shouldSeeRecords().
        myFilterShouldHaveValueSelected("(Admin) " + filterNameTwo).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NUMBER, containsStringIgnoringCase("a")).
        recordsShouldBeSortedByInputFieldIgnoringCase(AccountFields.ACCOUNT_NAME, "Ascending").
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P1" }, testName = "RT-01843", description = "Verify Admin defined filters can be deleted (Use case 1)")
    public void rt01843CaseOne() {

        String gridName = "RT-01843GridOneAuto";

        String filterName = "RT-01843Filter";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);

        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        createUserFilter().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "united").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        myFilterShouldHaveValue("(Admin) " + filterName).
        openMoreMenu().
        clickEditSettingsMenuItem().
        switchToEditSettingsPopup().
        selectParentObject(CONTACT).
        alertConfirm().
        saveGrid().
        switchToEditSettingsPopup().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldSeeRecords().
        switchToMainWindow().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P1" }, testName = "RT-01843", description = "Verify Admin defined filters can be deleted (Use case 2)")
    public void rt01843CaseTwo() {

        String gridName = "RT-01843GridTwoAuto";

        String filterName = "RT-01843Filter";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        List<String> relatedObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);

        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relatedObjectFields).
        clickNext().
        createUserFilter().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "united").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        myFilterShouldHaveValue("(Admin) " + filterName).
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        selectRelatedObject("Case").
        selectFields(Arrays.asList("Case ID")).
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldNotSeeMyFilter().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P1" }, testName = "RT-01843", description = "Verify Admin defined filters can be deleted (Use case 3)")
    public void rt01843CaseThree() {

        String gridName = "RT-01843GridThreeAuto";

        String filterName = "RT-01843Filter";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        List<String> relatedObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);

        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relatedObjectFields).
        selectRelatedObject("Case").
        selectFields(Arrays.asList("Case ID")).
        clickNext().
        createUserFilter().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "united").
        saveGrid().
        launchGrid().
        myFilterShouldHaveValue("(Admin) " + filterName).
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        removeChildFields("Case", Arrays.asList("Case ID")).
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldNotSeeMyFilter().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
   
}
