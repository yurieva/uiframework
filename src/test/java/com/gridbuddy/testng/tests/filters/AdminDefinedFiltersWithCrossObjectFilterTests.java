package com.gridbuddy.testng.tests.filters;

import static com.gridbuddy.matchers.IsEqual.equalToIgnoringCase;
import static com.gridbuddy.matchers.StringContains.containsStringIgnoringCase;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static org.hamcrest.Matchers.not;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Filters")
@Story("Admin Defined Filters With Cross Object Filter")
public class AdminDefinedFiltersWithCrossObjectFilterTests extends BaseTest {

      
    @Test(groups = { "Regression", "P1" }, testName = "adminCreatesCrossObjectFilter", description = "Verify Admin defined fillters (Admin saves the Default filter)")
    public void adminCreatesCrossObjectFilter() {

        String gridName = "adminCreatesCrossObjectFilterOne";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);
        
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relaterObjectFields).
        saveGrid().
        launchGrid().
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "t").
        setRelatedFilter("Has").
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("t")).
        everyParentObjectShouldHaveRelatedObjects().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P1" }, testName = "adminEditsCrossObjectFilter", description = "Verify Admin defined fillters (Admin edits the Default filter)")
    public void adminEditsCrossObjectFilter() {

        String gridName = "adminEditsCrossObjectFilterTwoAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        selectShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relaterObjectFields).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "equals", "test").
        setRelatedFilter("Does Not Have").
        saveGrid().
        launchGrid().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, equalToIgnoringCase("test")).
        everyParentObjectShouldNotHaveRelatedObjects().
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "not equal to", "test").
        setRelatedFilter("Has").
        setSortingCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "Ascending").
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, not(equalToIgnoringCase("test"))).
        recordsShouldBeSortedByInputFieldIgnoringCase(AccountFields.ACCOUNT_NAME, "Ascending").
        everyParentObjectShouldHaveRelatedObjects().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P1" }, testName = "adminClicksRefreshButton", description = "Verify Admin defined fillters (Admin clicks the Refresh button without Saving)")
    public void adminClicksRefreshButton() {

        String gridName = "adminClicksRefreshButtonAuto";
        
        String filterName = "RT-01408Filter";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);

        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        setShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relaterObjectFields).
        clickNext().
        createUserFilter().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "t").
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        myFilterShouldHaveValue("(Admin) " + filterName).
        selectFilter("(Admin) " + filterName).
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        shouldSeeFilterSelected(filterName).
        shouldSeeFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "t").
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "not equal to", "some new value").
        setSortingCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "Ascending").
        setRelatedFilter("Does Not Have").
        clickRefresh().
        switchToDefaultContent().
        switchToEditAdminFiltersPopup().
        shouldSeeFilterSelected(filterName).
        shouldSeeFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "t").
        shouldSeeSortingCondition(ACCOUNT, FIRST_ROW, "--No Field Selected--", "--No Sort Direction Selected--").
        shouldSeeRelatedFilterValue("Has").
        switchToDefaultContent().
        closePopup().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P1" }, testName = "adminClicksBackButton", description = "Verify Admin defined fillters (Admin clicks the Back button)")
    public void adminClicksBackButton() {

        String gridName = "adminClicksBackButtonAuto";
        
        String filterName = "!RT-01408Filter";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);
        
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        setShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relaterObjectFields).
        clickNext().
        createUserFilter().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "equals", "test").
        setRelatedFilter("Does Not Have").
        saveGrid().
        launchGrid().
        myFilterShouldHaveValue("(Admin) " + filterName).
        openMoreMenu().
        clickEditAdminFiltersMenuItem().
        switchToEditAdminFiltersPopup().
        selectFilter(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "not equal to", "some new value").
        setSortingCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "Ascending").
        setRelatedFilter("Has").
        clickBack().
        switchToEditFieldsPopup().
        clickNext().
        switchToEditAdminFiltersPopup().
        selectFilter(filterName).
        shouldSeeFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "not equal to", "some new value").
        shouldSeeSortingCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "Ascending").
        shouldSeeRelatedFilterValue("Has").
        switchToDefaultContent().
        closePopup().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P1" }, testName = "lastFilterIsSaved", description = "Verify Admin defined fillters (User selects admin-defined filter on grid, and it gets saved as last accessed)")
    public void lastFilterIsSaved() {

        String gridName = "lastFilterIsSavedAuto";
        
        String filterName = "RT-01408FilterOne";
        
        String filterNameTwo = "RT-01408FilterTwo";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);
        
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        setShowRelatedObjectsExpanded().
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relaterObjectFields).
        clickNext().
        createUserFilter().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "equals", "test").
        setRelatedFilter("Does Not Have").
        saveFilter().
        createUserFilter().
        enterFilterName(filterNameTwo).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NUMBER, "contains", "a").
        setSortingCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "Ascending").
        setRelatedFilter("Has").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        myFilterShouldHaveValueSelected("(Default filter)").
        selectFilter("(Admin) " + filterName).
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, equalToIgnoringCase("test")).
        everyParentObjectShouldNotHaveRelatedObjects().
        logout().
        login().
        openGrids().
        openGrid("All Grids", gridName).
        shouldSeeRecords().
        myFilterShouldHaveValueSelected("(Admin) " + filterName).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, equalToIgnoringCase("test")).
        everyParentObjectShouldNotHaveRelatedObjects().
        selectFilter("(Admin) " + filterNameTwo).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NUMBER, containsStringIgnoringCase("a")).
        recordsShouldBeSortedByInputFieldIgnoringCase(AccountFields.ACCOUNT_NAME, "Ascending").
        everyParentObjectShouldHaveRelatedObjects().
        logout().
        login().
        openGrids().
        openGrid("All Grids", gridName).
        shouldSeeRecords().
        myFilterShouldHaveValueSelected("(Admin) " + filterNameTwo).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NUMBER, containsStringIgnoringCase("a")).
        recordsShouldBeSortedByInputFieldIgnoringCase(AccountFields.ACCOUNT_NAME, "Ascending").
        everyParentObjectShouldHaveRelatedObjects().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
      
}
