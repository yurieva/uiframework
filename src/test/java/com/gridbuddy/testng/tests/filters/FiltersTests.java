package com.gridbuddy.testng.tests.filters;

import static com.gridbuddy.matchers.StringContains.containsStringIgnoringCase;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static com.gridbuddy.utils.Constants.SECOND_ROW;
import static com.gridbuddy.utils.Constants.THIRD_ROW;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Account.AccountRelatedObjects;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Filters")
@Story("Smoke tests from Filters tab")
public class FiltersTests extends BaseTest {
    
    private static List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION,
            AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER,
            AccountFields.ACCOUNT_PHONE, AccountFields.ACCOUNT_RATING, AccountFields.ACCOUNT_SITE,
            AccountFields.ACCOUNT_SOURCE, AccountFields.ACCOUNT_TYPE, AccountFields.ACTIVE);

    private static List<String> relaterObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME,
            ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE, ContactFields.BIRTHDATE,
            ContactFields.BUSINESS_FAX, ContactFields.BUSINESS_PHONE, ContactFields.CONTACT_DESCRIPTION, ContactFields.CONTACT_ID, ContactFields.CREATED_BY, ContactFields.LAST_NAME);
    
    @Test(groups = { "Regression", "P1" }, testName = "SmokeFI01", description = "Create User Define Filters")
    public void SmokeFI01() {

        String gridName = "SmokeFI01GridAuto";
        
        String filterNameOne = "FilterOne";
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createGrid(gridName, ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(AccountRelatedObjects.CONTACT).
        selectFields(relaterObjectFields).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "united").
        addFilterRow(ACCOUNT).
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACTIVE, "not equal to", "Yes").
        addFilterRow(ACCOUNT).
        setFilterCondition(ACCOUNT, THIRD_ROW, AccountFields.BILLING_CITY, "not equal to", "San Francisco").
        setAdvancedFilterCondition(ACCOUNT, "1 AND 2 AND 3").
        saveGrid().
        launchGrid().
        openFilterPopup().
        createNewFiltrer().
        setFilterCondition(ACCOUNT, 5, AccountFields.ACCOUNT_NUMBER, "contains", "A").
        setAdvancedFilterCondition(ACCOUNT, "1 AND 2 AND 3 AND 6").
        setFilterCondition(AccountRelatedObjects.CONTACT, 1, ContactFields.ACCOUNT_NAME, "contains", "Text").
        setAdvancedFilterCondition(AccountRelatedObjects.CONTACT, "2").
        enterFilterName(filterNameOne).
        saveFilter().
        myFilterShouldHaveValueSelected(filterNameOne).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, not(containsStringIgnoringCase("united"))).
        parentRecordTextFieldShouldMetCriteria(AccountFields.ACTIVE, not(equalTo("Yes"))).
        parentRecordInputFieldShouldMetCriteria(AccountFields.BILLING_CITY, not(equalTo("San Francisco"))).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NUMBER, containsStringIgnoringCase("A")).
        relatedRecordInputFieldShouldMetCriteria(ContactFields.ACCOUNT_NAME, containsStringIgnoringCase("Text")).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P2" }, testName = "RT-01910", description = "Verify there is no dynamic sorting available after clicking on column header")
    public void rt01910() {

        String gridName = "RT-01910GridAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        setSortingCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "Ascending").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        recordsShouldBeSortedByInputFieldIgnoringCase(AccountFields.ACCOUNT_NAME, "Ascending").
        clickColumnHeader(AccountFields.ACCOUNT_NUMBER).
        shouldSeeRecords().
        recordsShouldBeSortedByInputFieldIgnoringCase(AccountFields.ACCOUNT_NAME, "Ascending").
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
}
