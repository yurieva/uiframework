package com.gridbuddy.testng.tests.filters;

import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static org.hamcrest.Matchers.equalTo;

import java.util.Arrays;

import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.steps.GBSteps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Filters")
@Story("Fpv and fpf filters")
public class FpfFpvFiltersTests extends BaseTest {
    
    @Test(groups = { "Regression", "P1" }, testName = "fpfFpv", description = "Create url filters fpv and fpf")
    public void fpfFpv() {
        
        String gridName = "fpfFpvGridAuto";

        GBSteps onGridPage = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(Arrays.asList(AccountFields.ACCOUNT_TYPE)).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        openEditSettingsPopup().
        switchToEditSettingsPopup().
        launchGrid().
        switchToNewWindow();
        onGridPage.getPage(onGridPage.getPageUrl() + "&fpf=Type&fpv=Customer - Direct");
        onGridPage.shouldSeeRecords().
        parentRecordTextFieldShouldMetCriteria(AccountFields.ACCOUNT_TYPE, equalTo("Customer - Direct")).
        switchToMainWindow().
        openGridWizard().
        deleteGrid(gridName);
        
    }

    
}
