package com.gridbuddy.testng.tests.filters;

import static com.gridbuddy.matchers.StringContains.containsStringIgnoringCase;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.objects.ParentObjects.CONTACT;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static com.gridbuddy.utils.Constants.SECOND_ROW;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.not;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.objects.Contact.ContactFields;
import com.gridbuddy.steps.GBSteps;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Filters")
@Story("User defined filters")
public class UserDeifinedFiltersTests extends BaseTest {

    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01427", description = "Verify User Defined Filter - Filter widget (Use case 1)")
    public void rt01427CaseOne() {

        String gridName = "RT-01427GridOneAuto";

        String filterName = "RT-01427Filter";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "united").
        saveFilter().
        myFilterShouldHaveValueSelected(filterName).
        openMoreMenu().
        clickEditSettingsMenuItem().
        switchToEditSettingsPopup().
        selectParentObject(CONTACT).
        alertConfirm().
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldNotSeeMyFilter().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01427", description = "Verify User Defined Filter - Filter widget (Use case 2)")
    public void rt01427CaseTwo() {

        String gridName = "RT-01427GridTwoAuto";

        String filterName = "RT-01427Filter";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        List<String> relatedObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);

        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relatedObjectFields).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "united").
        saveFilter().
        shouldSeeRecords().
        myFilterShouldHaveValueSelected(filterName).
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        selectRelatedObject("Case").
        selectFields(Arrays.asList("Case ID")).
        saveGrid().
        switchToDefaultContent().closePopupAndRefresh().
        shouldNotSeeMyFilter().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01427", description = "Verify User Defined Filter - Filter widget (Use case 3)")
    public void rt01427CaseThree() {

        String gridName = "RT-01427GridThreeAuto";

        String filterName = "RT-01427Filter";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        List<String> relatedObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);

        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relatedObjectFields).
        selectRelatedObject("Case").
        selectFields(Arrays.asList("Case ID")).
        saveGrid().
        launchGrid().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "united").
        saveFilter().
        shouldSeeRecords().
        myFilterShouldHaveValueSelected(filterName).
        openMoreMenu().
        clickEditFieldsMenuItem().
        switchToEditFieldsPopup().
        removeChildFields("Case", Arrays.asList("Case ID")).
        saveGrid().
        switchToDefaultContent().
        closePopupAndRefresh().
        shouldNotSeeMyFilter().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01427", description = "Verify User Defined Filter - Filter widget (Use case 4)")
    public void rt01427CaseFour() {

        String gridName = "RT-01427GridFourAuto";

        String filterName = "RT-01427Filter";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        List<String> relatedObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);

        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relatedObjectFields).
        selectRelatedObject("Case").
        selectFields(Arrays.asList("Case ID")).
        saveGrid().
        launchGrid().
        openFilterPopup().
        cloneFilter().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        selectSortByField(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NUMBER, "Descending").
        saveFilter().
        shouldSeeRecords().
        myFilterShouldHaveValueSelected(filterName).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test")).
        recordsShouldBeSortedByInputFieldIgnoringCase(AccountFields.ACCOUNT_NUMBER, "Descending").
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01427", description = "Verify User Defined Filter - Filter widget (Use case 5)")
    public void rt01427CaseFive() {

        String gridName = "RT-01427GridFiveAuto";

        String filterName = "RT-01427Filter";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER, AccountFields.ACTIVE);
        
        List<String> relatedObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);

        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relatedObjectFields).
        selectRelatedObject("Case").
        selectFields(Arrays.asList("Case ID")).
        saveGrid().
        launchGrid().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACTIVE, "not equal to", "Yes").
        selectSortByField(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_ID, "Descending").
        selectSortByField(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NUMBER, "Ascending").
        saveFilter().
        shouldSeeRecords().
        myFilterShouldHaveValueSelected(filterName).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test")).
        parentRecordTextFieldShouldMetCriteria(AccountFields.ACTIVE, not(equalTo("Yes"))).
        recordsShouldBeSortedByTextField(AccountFields.ACCOUNT_ID, "Descending").
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01427", description = "Verify User Defined Filter - Filter widget (Use case 6)")
    public void rt01427CaseSix() {

        String gridName = "RT-01427GridSixAuto";

        String filterName = "RT-01427Filter";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER, AccountFields.ACTIVE);
        
        List<String> relatedObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);

        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relatedObjectFields).
        selectRelatedObject("Case").
        selectFields(Arrays.asList("Case ID")).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACTIVE, "not equal to", "Yes").
        selectSortByField(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_ID, "Descending").
        selectSortByField(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NUMBER, "Ascending").
        saveFilter().
        shouldSeeRecords().
        myFilterShouldHaveValueSelected(filterName).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test")).
        parentRecordTextFieldShouldMetCriteria(AccountFields.ACTIVE, not(equalTo("Yes"))).
        recordsShouldBeSortedByTextField(AccountFields.ACCOUNT_ID, "Descending").
        openFilterPopup().
        editFilter(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACTIVE, "not equal to", "No").
        selectSortByField(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_ID, "Ascending").
        saveFilter().
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, not(containsStringIgnoringCase("test"))).
        parentRecordTextFieldShouldMetCriteria(AccountFields.ACTIVE, not(equalTo("No"))).
        recordsShouldBeSortedByTextField(AccountFields.ACCOUNT_ID, "Ascending").
        openGridWizard().
        deleteGrid(gridName);
        
    }
   
    @Test(groups = { "Smoke", "Regression", "P0" }, testName = "RT-01427", description = "Verify User Defined Filter - Filter widget (Use case 7)")
    public void rt01427CaseSeven() {

        String gridName = "RT-01427GridSevenAuto";

        String filterName = "RT-01427Filter";
        
        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER, AccountFields.ACTIVE);
        
        List<String> relatedObjectFields = Arrays.asList(ContactFields.ACCOUNT_NAME, ContactFields.ASSISTANTS_NAME, ContactFields.ASST_PHONE);

        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        selectRelatedObject(CONTACT).
        selectFields(relatedObjectFields).
        selectRelatedObject("Case").
        selectFields(Arrays.asList("Case ID")).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACTIVE, "not equal to", "Yes").
        selectSortByField(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_ID, "Descending").
        selectSortByField(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NUMBER, "Ascending").
        saveFilter().
        shouldSeeRecords().
        myFilterShouldHaveValueSelected(filterName).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test")).
        parentRecordTextFieldShouldMetCriteria(AccountFields.ACTIVE, not(equalTo("Yes"))).
        recordsShouldBeSortedByTextField(AccountFields.ACCOUNT_ID, "Descending").
        openFilterPopup().
        editFilter(filterName).
        deleteFilter().
        shouldSeeRecords().
        shouldNotSeeMyFilter().
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P1" }, testName = "RT-01468", description = "Verify User Defined Filter - Field actions (Use case 1)")
    public void rt01468CaseOne() {

        String gridName = "RT-01468GridOneAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "equals", "test").
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, equalToIgnoringCase("test")).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P1" }, testName = "RT-01468", description = "Verify User Defined Filter - Field actions (Use case 2)")
    public void rt01468CaseTwo() {

        String gridName = "RT-01468GridTwoAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "equals", "test").
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, equalToIgnoringCase("test")).
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "does not contain", "test").
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, not(containsStringIgnoringCase("test"))).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P1" }, testName = "RT-01468", description = "Verify User Defined Filter - Field actions (Use case 3)")
    public void rt01468CaseThree() {

        String gridName = "RT-01468GridThreeAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        GBSteps userOnGrid = getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        shouldSeeRecords();
        String account = userOnGrid.getCellValue(FIRST_ROW, AccountFields.ACCOUNT_NAME);
        userOnGrid.applyColumnFilter(AccountFields.ACCOUNT_NAME, "not equal to", account).
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, not(equalTo(account)));
        userOnGrid.clearColumnFilter(AccountFields.ACCOUNT_NAME).
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(FIRST_ROW, AccountFields.ACCOUNT_NAME, equalTo(account)).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P1" }, testName = "RT-01468", description = "Verify User Defined Filter - Field actions (Use case 4)")
    public void rt01468CaseFour() {

        String gridName = "RT-01468GridFourAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        applyColumnFilter(AccountFields.ACCOUNT_NAME, "equals", "test").
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria("-Name", equalToIgnoringCase("test")).
        myFilterShouldHaveValueSelected("My " + gridName).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Smoke", "Regression", "P1" }, testName = "RT-01468", description = "Verify User Defined Filter - Field actions (Use case 5)")
    public void rt01468CaseFive() {

        String gridName = "RT-01468GridFiveAuto";
        
        String filterName = "!RT-01468Filter";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX,
                AccountFields.ACCOUNT_ID, AccountFields.ACCOUNT_NUMBER);
        
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        createUserFilter().
        enterFilterName(filterName).
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "equals", "test").
        saveGrid().
        launchGrid().
        shouldSeeRecords().
        selectFilter("(Admin) " + filterName).
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, equalToIgnoringCase("test")).
        applyColumnFilter(AccountFields.ACCOUNT_NUMBER, "contains", "A").
        shouldSeeRecords().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, equalToIgnoringCase("test")).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NUMBER, containsStringIgnoringCase("a")).
        myFilterShouldHaveValueSelected("My " + filterName).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
   
}
