package com.gridbuddy.testng.tests.filters;

import static com.gridbuddy.matchers.StringContains.containsStringIgnoringCase;
import static com.gridbuddy.objects.ParentObjects.ACCOUNT;
import static com.gridbuddy.utils.Constants.FIRST_ROW;
import static com.gridbuddy.utils.Constants.SECOND_ROW;
import static com.gridbuddy.utils.Constants.THIRD_ROW;
import static org.hamcrest.Matchers.not;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.gridbuddy.objects.Account.AccountFields;
import com.gridbuddy.testng.BaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

@Feature("Filters")
@Story("Advanced Filter Conditions")
public class AdvancedFilterConditionsTests extends BaseTest {

    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01798", description = "Verify Advanced Filter Conditions (Use case 1)")
    public void rt01798CaseOne() {

        String gridName = "RT-01798GridOneAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NUMBER, "does not contain", "a").
        setAdvancedFilterCondition(ACCOUNT, "1 AND 2").
        saveGrid().
        launchGrid().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test")).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NUMBER, not(containsStringIgnoringCase("a"))).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01798", description = "Verify Advanced Filter Conditions (Use case 2)")
    public void rt01798CaseTwo() {

        String gridName = "RT-01798GridTwoAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "1").
        setAdvancedFilterCondition(ACCOUNT, "1 AND 2").
        saveGrid().
        launchGrid().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test")).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, not(containsStringIgnoringCase("1"))).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01798", description = "Verify Advanced Filter Conditions (Use case 3)")
    public void rt01798CaseThree() {

        String gridName = "RT-01798GridThreeAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NUMBER, "contains", "S").
        setAdvancedFilterCondition(ACCOUNT, "1 OR 2").
        saveGrid().
        launchGrid().
        parentRecordInputFieldShouldMetAnyCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test"), AccountFields.ACCOUNT_NUMBER, containsStringIgnoringCase("s")).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01798", description = "Verify Advanced Filter Conditions (Use case 4)")
    public void rt01798CaseFour() {

        String gridName = "RT-01798GridFourAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        clickNext().
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NUMBER, "contains", "S").
        setFilterCondition(ACCOUNT, THIRD_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "account").
        setAdvancedFilterCondition(ACCOUNT, "(NOT 3) AND (1 OR 2)").
        saveGrid().
        launchGrid().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test"), AccountFields.ACCOUNT_NUMBER, containsStringIgnoringCase("s"), AccountFields.ACCOUNT_NAME, not(containsStringIgnoringCase("account"))).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01798", description = "Verify Advanced Filter Conditions (Use case 5)")
    public void rt01798CaseFive() {

        String gridName = "RT-01798GridFiveAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName("filter").
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NUMBER, "does not contain", "a").
        setAdvancedFilterCondition(ACCOUNT, "1 AND 2").
        saveFilter().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test")).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NUMBER, not(containsStringIgnoringCase("a"))).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01798", description = "Verify Advanced Filter Conditions (Use case 6)")
    public void rt01798CaseSix() {

        String gridName = "RT-01798GridSixAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName("filter").
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "1").
        setAdvancedFilterCondition(ACCOUNT, "1 AND 2").
        saveFilter().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test")).
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, not(containsStringIgnoringCase("1"))).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01798", description = "Verify Advanced Filter Conditions (Use case 7)")
    public void rt01798CaseSeven() {

        String gridName = "RT-01798GridSevenAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName("filter").
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NUMBER, "contains", "S").
        setAdvancedFilterCondition(ACCOUNT, "1 OR 2").
        saveFilter().
        parentRecordInputFieldShouldMetAnyCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test"), AccountFields.ACCOUNT_NUMBER, containsStringIgnoringCase("s")).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
    @Test(groups = { "Regression", "P0" }, testName = "RT-01798", description = "Verify Advanced Filter Conditions (Use case 8)")
    public void rt01798CaseEight() {

        String gridName = "RT-01798GridEightAuto";

        List<String> parentObjectFields = Arrays.asList(AccountFields.ACCOUNT_DESCRIPTION, AccountFields.ACCOUNT_FAX, AccountFields.ACCOUNT_ID,
                AccountFields.ACCOUNT_NUMBER);
        
        getUser().
        login().
        openGridWizard().
        deleteGrid(gridName).
        createNew().
        enterGridName(gridName).
        selectParentObject(ACCOUNT).
        clickNext().
        selectFields(parentObjectFields).
        saveGrid().
        launchGrid().
        openFilterPopup().
        createNewFiltrer().
        enterFilterName("filter").
        setFilterCondition(ACCOUNT, FIRST_ROW, AccountFields.ACCOUNT_NAME, "contains", "test").
        setFilterCondition(ACCOUNT, SECOND_ROW, AccountFields.ACCOUNT_NUMBER, "contains", "S").
        setFilterCondition(ACCOUNT, THIRD_ROW, AccountFields.ACCOUNT_NAME, "does not contain", "account").
        setAdvancedFilterCondition(ACCOUNT, "(NOT 3) AND (1 OR 2)").
        saveFilter().
        parentRecordInputFieldShouldMetCriteria(AccountFields.ACCOUNT_NAME, containsStringIgnoringCase("test"), AccountFields.ACCOUNT_NUMBER, containsStringIgnoringCase("s"), AccountFields.ACCOUNT_NAME, not(containsStringIgnoringCase("account"))).
        openGridWizard().
        deleteGrid(gridName);
        
    }
    
   
}
